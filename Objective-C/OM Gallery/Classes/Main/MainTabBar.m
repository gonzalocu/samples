//
//  MainTabBar.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 30/12/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "MainTabBar.h"

@implementation MainTabBar


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return ((interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown) &&
                (interfaceOrientation != UIInterfaceOrientationLandscapeLeft) &&
                (interfaceOrientation != UIInterfaceOrientationLandscapeRight));
    } else {
        return YES;
    }
}

- (BOOL)shouldAutorotate
{
    NSLog(@"am I called1?");
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    NSLog(@"am I called?");
    return UIInterfaceOrientationMaskPortrait;
}
@end
