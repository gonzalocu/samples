//
//  UITabBarController+UITabBarController_autoRotate.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 30/12/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "UITabBarController+UITabBarController_autoRotate.h"

@implementation UITabBarController (UITabBarController_autoRotate)

- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

@end
