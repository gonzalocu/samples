//
//  SecondViewController.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 26/08/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "SecondViewController.h"


@implementation SecondViewController

@synthesize navigationBar;

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [navigationBar setBarTintColor:[UIColor colorWithRed:0 green:0.6 blue:1 alpha:0.4]];

}

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
