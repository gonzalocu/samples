//
//  AdMarvelIntegrationViewController.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 30/12/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "AdMarvelIntegrationViewController.h"

@interface AdMarvelIntegrationViewController ()

@end

@implementation AdMarvelIntegrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)displayOldView:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8f];
    [self dismissViewControllerAnimated:YES completion:nil];

    self.view.transform = CGAffineTransformMakeTranslation(self.view.frame.origin.x, 480.0f + (self.view.frame.size.height/2));
    [UIView commitAnimations];
}



@end
