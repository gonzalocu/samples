//
//  SecondViewController.h
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 26/08/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;

@end

