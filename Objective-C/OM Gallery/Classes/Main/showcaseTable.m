//
//  showcaseTable.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 28/08/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "showcaseTable.h"

@implementation showcaseTable

//@synthesize navigationBar;


- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [[[self navigationController] navigationBar] setBarTintColor:[UIColor colorWithRed:0 green:0.6 blue:1 alpha:0.4]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
