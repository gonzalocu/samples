//
//  UITabBarController+UITabBarController_autoRotate.h
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 30/12/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (UITabBarController_autoRotate)
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
@end
