//
//  FirstViewController.m
//  OM Gallery
//
//  Created by Gonzalo Cuadrado on 26/08/2014.
//  Copyright (c) 2014 Opera Mediaworks. All rights reserved.
//

#import "LaunchScreenController.h"

@interface LaunchScreenController ()


@end

@implementation LaunchScreenController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    // Do any additional setup after loading the view, typically from a nib.
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
    [NSThread sleepForTimeInterval:5.0];
}

@end
