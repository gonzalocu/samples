"use strict";

const   STATE_INIT = 'init',
        STATE_LOADING_DEFAULT = 'loading_default',
        STATE_LOADING_EXPANDED = 'loading_expanded',
        STATE_DEFAULT = 'default',
        STATE_EXPANDED = 'expanded'; 
        
const _cachedApplicationRef = Symbol('_cachedApplicationRef');
const _mixinRef = Symbol('_mixinRef');
const mix = (superClass) => new MixinBuilder(superClass);

class MixinBuilder {
    
    constructor(superclass) {    
        this.superclass = superclass;
    }
    
    with () { 
        let mixins = Array.prototype.slice.call(arguments);
        return mixins.reduce((c, mixin) => {
    
            let applicationRef = mixin[_cachedApplicationRef];
            if (!applicationRef) {
                applicationRef = mixin[_cachedApplicationRef] = Symbol(mixin.name);
            }
    
            if (c.hasOwnProperty(applicationRef)) {
                return c[applicationRef];
            }
    
            let application = mixin(c);
            application.prototype[_mixinRef] = mixin;
            c[applicationRef] = application;
    
            if (Symbol.hasInstance && !mixin.hasOwnProperty(Symbol.hasInstance)) {
                mixin[Symbol.hasInstance] = function(o) {
                    do {
                        if (o.hasOwnProperty(_mixinRef) && o[_mixinRef] === this) {
                            return true;
                        }
                        o = Object.getPrototypeOf(o);
                    } while (o !== Object)
                    return false;
                }
            }
            return application;
        }, this.superclass);
    }
}
        
class Core {

	constructor (options) {	
		this.parentId = "core-container";
		this.options = options;
		this.state = STATE_INIT;
	}
	
	init () {
        this.setContainer();
        this.setEvents();
        
		this.state = STATE_LOADING_DEFAULT;
		this.changeState();
	}
	
	render () {
        return `<div id="${this.options.name}" style="display:block;margin:0 auto;text-align:center;">IT WORKS!</div>`;	
	}
	
	getContainer () {
		return document.querySelector('#' + this.options.name);
	}

	setContainer () {
        document.body.style.margin = '0';
        document.body.style.padding = '0';
    	
    	document.getElementById(this.parentId).innerHTML = this.render();
	}
	
	preCollapsedHandler () {
        this.state = STATE_DEFAULT;
		this.changeState();
	}
	
	displayCollapsed () {
        window[this.options.callbackDisplayCollapsed](this);
    }
    
	preExpandedHandler () {
        this.state = STATE_EXPANDED;
		this.changeState();
	}
	        
	displayExpanded () {
        window[this.options.callbackDisplayExpanded](this); 
    }
    
    setEvents () {
        var container = this.getContainer ();
        container.addEventListener('touchstart',    this.handleEvent.bind(this), false);
        container.addEventListener('touchmove',     this.handleEvent.bind(this), false);
        container.addEventListener('touchend',      this.handleEvent.bind(this), false);

        container.addEventListener("mousedown",     this.handleEvent.bind(this), false);
        container.addEventListener("mousemove",     this.handleEvent.bind(this), false);
        container.addEventListener("mouseup",       this.handleEvent.bind(this), false);
        container.addEventListener("mouseout",      this.handleEvent.bind(this), false);
        
        container.addEventListener("click",         this.handleEvent.bind(this), false);
    }
    
    handleEvent (e) {
        e.stopPropagation();
        if('touchmove' === e.type) {
            e.preventDefault();
        }

        switch (e.type) {
            case 'touchstart':
            case 'mousedown':
                //TouchStart
                break;
            case 'touchmove':
            case 'mousemove':
                //TouchMove
                break;
            case 'touchend':
            case 'mouseup':
            case 'mouseout':
                //TouchEnd
                break;
            case 'click':
                if (this.state === STATE_DEFAULT) {
                    this.state = STATE_LOADING_EXPANDED;
                    this.changeState();
                } else {
                    this.state = STATE_LOADING_DEFAULT;
                    this.changeState();
                }
                break;
        }
    }

	        
    changeState  () { 
        console.log(this.state);
        switch (this.state) {
            case STATE_INIT:
                this.init();
                break;
            case STATE_LOADING_DEFAULT:
                this.preCollapsedHandler ();
                break;
            case STATE_DEFAULT:
            	this.displayCollapsed ();
                break;
            case STATE_LOADING_EXPANDED:
                this.preExpandedHandler ();
                break;
            case STATE_EXPANDED:
                this.displayExpanded ();
                break;
        }  
    }
	       
}

