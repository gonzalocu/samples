"use strict";

const Tracking = Base => class extends Base {

    constructor(options) {

        super(options);
        this.params = options.params;



    }

    init () {

        this.resetIncrementTimer ();
		super.init();

    }


    displayCollapsed () {

        this.trackEvent('tap collapsed');
        this.resetIncrementTimer ();
	    super.displayCollapsed ();

    }

	displayExpanded () {

        this.startIncrementTimer ();
        super.displayExpanded ();

    }

    resetIncrementTimer () {

        this.currentNumSeconds = 0;
		this.incrementTimer = 0;
	    if(this.trackingTimerInterval) { clearInterval(this.trackingTimerInterval); }

    }

    startIncrementTimer () {

        this.resetIncrementTimer();
        this.trackingTimerInterval = setInterval(this.checkIncrementTimer.bind(this), 100);

    }

    checkIncrementTimer () {

        if(this.currentNumSeconds>6000){
            this.resetIncrementTimer();
            return;
        }
        this.incrementTimer++;

        //update the tracking in the admarvel console every 5 seconds
        if(this.incrementTimer % 50 === 0){
            this.currentNumSeconds =  parseInt(this.incrementTimer/10);

            var arrayOfDwellTimeMilestones = [5,10,20,30,60,120,240,420,600];

            for(var i=0; i<=arrayOfDwellTimeMilestones.length;i++){

                if(parseInt(this.currentNumSeconds)===parseInt(arrayOfDwellTimeMilestones[i])){
                    try {
                        this.adEventTracker.recordAdEventAsynchImage('dwell time ' + currentNumSeconds + ' seconds');
                    } catch(e) { }

                    break;
                }
            }

        }
    }

    checkIsBadvalue () {

        var badValues = {'partnerId':'{partnerid}','siteId':'{site_id}','bannerId':'{bannerid}'};
        for(var val in badValues){
            if(!this.params[val] || this.params[val] === '' || this.params[val] === badValues[val]){

                if(typeof (logger) === "object"){
                    console.log({'badParam':val, 'badValue':c[val] });
                }
                return true;
            }
        }
        return false;

    }

    getAdMarvelEventUrl (ev_name, ev_value) {

        if(this.checkIsBadvalue ()) { return; }

        return "https://ads.admarvel.com/fam/et.php?partner_id=" + this.params.partnerId + "&site_id=" + this.params.siteId + "&banner_id=" + this.params.bannerId + (this.params.cacheBuster?"&cb=" + this.params.cacheBuster:'') + "&evt_name=" + ev_name + "&evt_value=" + ev_value;

    }

	/**
		trackEvent<br/>
		Function to track events

		@method trackEvent
		@return {null}
	**/
    trackEvent (eventName) {

        if(this.checkIsBadvalue ()) { return; }

        try{
            this.recordEvent(this.getAdMarvelEventUrl(eventName, (this.incrementTimer/10)));
        } catch(e){}

    };


    /**
		recordEvent<br/>

		@method recordEvent
		@return {null}
	**/
    recordEvent (url, callback, params) {

        console.log(url);
        return;

        var imgElement = new Image();

        imgElement.onerror = function() { if (typeof callback !== "undefined") { callback(params); } };
        imgElement.onload  = function() { if (typeof callback !== "undefined") { callback(params); } };

        try {
            imgElement.src = url;
        } catch(e) { }

    }


}
