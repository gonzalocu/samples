"use strict";

const AssetLoader = Base => class extends Base {
	
	constructor (options) {
        
        super(options);
                
        this.cAssets = [];
		this.eAssets = [];
        this.assetsCache = {};	
        
	}
	
	init () {
    	
    	        console.log(3);
		super.init();		
		this.loadAssets(this.cAssets);

	}
	
    loadAssets (assetArray) {
        
    	this.loadedAssets = 0;
    	this.failedAssets = 0;
        
        if (assetArray.length == 0){
            this.changeState();
        } else {        
        	for (let i = 0; i < assetArray.length; i++) {
        		this.loadAsset(assetArray, assetArray[i]);
        	}
    	}
    	
    }
    
    
    loadAsset (assetArray, assetPath) {
        
    	let image = new Image();
    	var that = this;

    	image.onload = function () {
    		that.loadedAssets += 1;
    		if (that.loadedAssets + that.failedAssets === assetArray.length) {
    			that.changeState();
    		}    
    	}

    	image.onerror = function () {
    		that.failedAssets += 1;
    		if (that.loadedAssets + that.failedAssets === assetArray.length) {
    			that.changeState();
    		}  
    	}

    	image.src = assetPath;
    	this.assetsCache[assetPath] = image;
    	
    }
	
}