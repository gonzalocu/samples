"use strict";

class Canvas extends mix(Core).with(Tracking, AssetLoader) {
    
    
    constructor (options) {
        
        super(options);
        
        this.cAssets = ['http://4sa.s3.amazonaws.com/dev-area/msite-formats/reveal-banner/images/sure-banner.png'];
		this.eAssets = ['http://4sa.s3.amazonaws.com/dev-area/msite-formats/reveal-banner/images/sure.jpg',
						'http://4sa.s3.amazonaws.com/staging/air-canada-15-04/images/expanded/loading.png'];

		this.assetsCache = {};
    }
    
    render () {

        return `
			<canvas id="${this.options.name}" 
					width="${this.options.width}" 
					height="${this.options.height}" 
					style="display:block;margin:0 auto;"></canvas>
		`;	
		      
    }
    
	setContainer () {
    
       super.setContainer ();
       
       document.body.innerHTML = this.render();
    	
    	this.canvas = this.getContainer();
		this.ctx = this.canvas.getContext('2d');
		
		if (this.canvas.getContext){
	        
	        this.canvas.addEventListener("click", function(e){            
	             if (this.state === STATE_EXPANDED && this.canvas.width - 40 < e.pageX && this.canvas.width > e.pageX && 0 < e.pageY && 40 > e.pageY) {
	                 this.state = STATE_DEFAULT;
					 this.changeState();  
	             } else if (this.state === STATE_EXPANDED) {
		         	window.open('http://operamediaworks.tools');
		         } else if (this.state === STATE_DEFAULT) {
		         	this.state = STATE_EXPANDED;
		         	this.loadAssets(this.eAssets);
		         } else {
	                 this.changeState();
	             }
	                
	        }.bind(this));
	        
			this.canvas.addEventListener('touchmove', function (e) {
	            console.log('touchmove');
	            e.preventDefault(); 
			}, false);
			
			this.canvas.addEventListener('scroll', function (e) {
	            console.log('scroll');
	            e.preventDefault(); 
			}, false);
	        
	        window.addEventListener("resize", function() {
	            
	            console.log ('resize');
	            
	            this.changeState();
	                
	        }.bind(this))
	        
	        // this.changeState();
       
	    } else {
	        
	        console.log('Canvas Unsupported');
	        
	    }
    	
	}
	
	displayCollapsed () {
    	
        this.canvas.width = this.options.width;
        this.canvas.height = this.options.height;
        
        super.displayCollapsed();
        
    }
    
    displayExpanded () {

        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        
        super.displayExpanded();   
    }

} 