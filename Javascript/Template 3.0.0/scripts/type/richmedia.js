"use strict";

class RichMedia extends mix(Core).with(Tracking, AssetLoader) {

    constructor (options) {

        super(options);

        this.cAssets = [];
		this.eAssets = [];

		this.assetsCache = {};
    }

    render () {

        return `
			<div id="${this.options.name}" style="display:block;margin:0 auto;"></div>
		`;

    }

	setContainer () {

        super.setContainer ();

        this.adContainer = this.getContainer();
        this.adContainer.style.backgroundColor = "red";

        window.addEventListener("resize", this.resizeContainer.bind(this), false);
        window.addEventListener("orientationchange", this.resizeContainer.bind(this), false);

	}

	resizeContainer(data) {

    	console.log(data.srcElement.width);
    	if (this.state === STATE_DEFAULT) {
                this.adContainer.style.width = this.options.width + "px";
                this.adContainer.style.height = this.options.height + "px";
        } else if (this.state === STATE_EXPANDED) {
            setTimeout(function(){
                this.adContainer.style.width = window.innerWidth + "px";
                this.adContainer.style.height = window.innerHeight + "px";
            }.bind(this), 500);

        }

	}

	displayCollapsed () {

        this.resizeContainer();
        super.displayCollapsed();

    }

    displayExpanded () {

        this.resizeContainer();
        super.displayExpanded();
    }

}
