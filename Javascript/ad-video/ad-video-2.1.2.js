/**
	AdVideo Module </br>
	Load and Play Videos </br>
    If Custom Video Controls or Inline Video are desired use other video module for now. </br>
	Copyright 4th Screen, 2013 </br>
	Version 2.1 </br>
	@module AdVideo
	@main AdVideo
**/

/* global AdCore, adUnit, admarvelURLRedirect */

(function (win, doc) {

    /**
		AdVideo Class <br/>
		Documentation url: http://www.w3.org/TR/html5/embedded-content-0.html#mediaevents </br>
        Example ad-unit: <a target="_blank" href="http://4sa.s3.amazonaws.com/ad-units/playstation4/overlay/standard/sample.html">ad-units/playstation4</a>
		@class AdVideo
		@constructor
	**/
    AdCore.Modules.AdVideo = function (options) {
        var i;
        this.name_class = "AdVideo";
        this.core_version = "2.1.2";
        this.base = adUnit;


        this.options = {

            /**
				List of video used in the module
				The array of videos could contain these parameters:
				id, name, poster, src, autoplay, controls, width, height, loop, preload, muted, zoom, rotate.

				@property videoPlayList
				@type {Object}
			**/
            videoPlaylist: [{
                id: 1,
                name: "test-video",
                poster: "",
                src: {
                    hq: "http://4sa.s3.amazonaws.com/common-assets/videos/ad-video/video_default.mp4",
                    lq: "http://4sa.s3.amazonaws.com/common-assets/videos/ad-video/video_default.mp4"
                },
                autoplay: true,
                controls: false,
                width: 320,
                height: 240,
                loop: false,
                preload: "auto",
                muted: true,
                zoom: 0, // It is not working on Android Native Browser
                rotate: 0 // It is not working on Android Native Browser
            }],
            /**
				It is the parent div where the video will be created. <br/>
				String or DOM Object are accepted.

				@property videoWrapperId
				@type {String or DOM Object}
			**/
            videoWrapperId: this.base.expandedRootElement,
            /**
				Name of the Video Container
				@property videoContainerId
				@type {String}
			**/
            videoContainerId: "exp-video-container",
            /**
				Name of the Video Element
				@property videoElementId
				@type {String}
			**/
            videoElementId: "exp-video-element",
            /**
				Name of the Video Touch Area
				@property videoTouchAreaId
				@type {String}
			**/
            videoTouchAreaId: "exp-video-touch-area",
            /**
	            Video poster image ID
	            @property videoPosterImageId
	            @type {String}
            **/
            videoPosterImageId: "exp-video-poster-image",
            /**
	            Play button ID
	            @property playButtonId
	            @type {String}
            **/
            videoPlayButtonId: "exp-video-play-button",
            /**
	            Loading Spinner ID
	            @property videoLoadingSpinnerId
	            @type {String}
            **/
            videoLoadingSpinnerId: "exp-video-loading-spinner",
            /**
				Default value to play video inline
				@property playVideoInLine
				@type {Boolean}
			**/
            playVideoInLine: true,
            /**
				Default value to show basic control in the video
				@property showBasicControls
				@type {Boolean}
			**/
            showBasicControls: true,
            /**
				Default value to indicate if the video is autoplay
				@property autoPlay
				@type {Boolean}
			**/
            autoplay: false,
            /**
				Default value to indicate if the video has to preload. The values are: auto, metadata, none.
				@property preload
				@type {Boolean}
			**/
            preload: "auto",
            /**
				Default value to indicate if the video has to repeat.
				@property loop
				@type {Boolean}
			**/
            loop: false,
            /**
				Default width of the videos.
				@property width
				@type {Number}
			**/
            width: 320,
            /**
				Default height of the videos.
				@property height
				@type {Number}
			**/
            height: 240,
            /**
				Default zoom of the videos.
				@property zoom
				@type {Number}
			**/
            zoom: 0,
            /**
				Default rotate value of the videos. It is in degrees.
				@property rotate
				@type {Number}
			**/
            rotate: 0,
            /**
				Stop audio using ad marvel functions
				@property useAdMarvelFunctionToStopAudio
				@type {Boolean}
			**/
            useAdMarvelFunctionToStopAudio: true,
            /**
				Call this function when the video is playing
				@property callbackPlayFunction
				@type {Function}
			**/
            callbackPlayFunction: null,
            /**
				Call this function when the video is on pause
				@property callbackPauseFunction
				@type {Function}
			**/
            callbackPauseFunction: null,
            /**
				Call this function after the video has finished
				@property callbackExitFunction
				@type {Function}
			**/
            callbackExitFunction: null,
            /**
				Call back when loadstart event is fired
				@property callbackLoadStartFunction
				@type {Function}
			**/
            callbackLoadStartFunction: null,
            /**
				Call back when progress event is fired
				@property callbackProgressFunction
				@type {Function}
			**/
            callbackProgressFunction: null,
            /**
				Call back when suspend event is fired
				@property callbackSuspendFunction
				@type {Function}
			**/
            callbackSuspendFunction: null,
            /**
				Call back when abort event is fired
				@property callbackAbortFunction
				@type {Function}
			**/
            callbackAbortFunction: null,
            /**
				Call back when error event is fired
				@property callbackErrorFunction
				@type {Function}
			**/
            callbackErrorFunction: null,
            /**
				Call back when emptied event is fired
				@property callbackEmptiedFunction
				@type {Function}
			**/
            callbackEmptiedFunction: null,
            /**
				Call back when stalled event is fired
				@property callbackStalledFunction
				@type {Function}
			**/
            callbackStalledFunction: null,
            /**
				Call back when loadedmetadata event is fired
				@property callbackLoadedMetaDataFunction
				@type {Function}
			**/
            callbackLoadedMetaDataFunction: null,
            /**
				Call back when loadeddata event is fired
				@property callbackLoadedDataFunction
				@type {Function}
			**/
            callbackLoadedDataFunction: null,
            /**
				Call back when canplay event is fired
				@property callbackCanPlayFunction
				@type {Function}
			**/
            callbackCanPlayFunction: null,
            /**
				Call back when canplaythrough event is fired
				@property callbackCanPlayThroughFunction
				@type {Function}
			**/
            callbackCanPlayThroughFunction: null,
            /**
				Call back when playing event is fired
				@property callbackPlayingFunction
				@type {Function}
			**/
            callbackPlayingFunction: null,
            /**
				Call back when waiting event is fired
				@property callbackWaitingFunction
				@type {Function}
			**/
            callbackWaitingFunction: null,
            /**
				Call back when seeking event is fired
				@property callbackSeekingFunction
				@type {Function}
			**/
            callbackSeekingFunction: null,
            /**
				Call back when seeked event is fired
				@property callbackSeekedFunction
				@type {Function}
			**/
            callbackSeekedFunction: null,
            /**
				Call back when ended event is fired
				@property callbackEndedFunction
				@type {Function}
			**/
            callbackEndedFunction: null,
            /**
				Call back when durationchange event is fired
				@property callbackDurationChangeFunction
				@type {Function}
			**/
            callbackDurationChangeFunction: null,
            /**
				Call back when timeupdate event is fired
				@property callbackTimeUpdateFunction
				@type {Function}
			**/
            callbackTimeUpdateFunction: null,
            /**
				Call back when the user click on touch area is fired
				@property callbackTouchAreaFunction
				@type {Function}
			**/
            callbackTouchAreaFunction: null,
            /**
				Show Console Messages
				@property showConsoleMessages
				@type {Boolean}
			**/
            showConsoleMessages: false,
            /**
				Toggle pause on video tap
				@property pauseOnTap
				@type {Boolean}
			**/
            pauseOnTap: true
        };

        for (i in options) {
            this.options[i] = options[i];
        }

        this.init();
    };

    AdCore.Modules.AdVideo.prototype = (
        function () {
            /**
                Initializing AdVideo Class

                @private
                @method __init
                @param {String or DOM Object} el
                @param {Array} options
                @return {null}
            **/
            var init = function () {

                this.videoWrapperId = typeof this.options.videoWrapperId === "object" ? this.options.videoWrapperId : doc.getElementById(this.options.videoWrapperId);

                this.videoContainer = doc.createElement("div");
                this.videoContainer.id = this.options.videoContainerId;
                this.videoContainer.className = this.options.videoContainerId + "-initial";
                this.base.els.push(this.videoContainer);

                this.videoElement = doc.createElement("video");
                this.videoElement.id = this.options.videoElementId;
                this.videoElement.className = this.options.videoElementId + "-initial";
                this.videoContainer.appendChild(this.videoElement);
                this.base.els.push(this.videoElement);

                this.videoPosterImage = doc.createElement("div");
                this.videoPosterImage.id = this.options.videoPosterImageId;
                this.videoPosterImage.className = this.options.videoPosterImageId + "-initial";
                this.videoContainer.appendChild(this.videoPosterImage);
                this.base.els.push(this.videoPosterImage);

                this.videoPlayButton = doc.createElement("div");
                this.videoPlayButton.id = this.options.videoPlayButtonId;
                this.videoPlayButton.className = this.options.videoPlayButtonId + "-initial";
                this.videoContainer.appendChild(this.videoPlayButton);
                this.base.els.push(this.videoPlayButton);

                this.videoLoadingSpinner = doc.createElement("div");
                this.videoLoadingSpinner.id = this.options.videoLoadingSpinnerId;
                this.videoLoadingSpinner.className = this.options.videoLoadingSpinnerId + "-initial";
                this.videoContainer.appendChild(this.videoLoadingSpinner);
                this.videoLoadingSpinner.innerHTML = "<div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div> <div class='spinner-bar'></div>";
                this.base.els.push(this.videoLoadingSpinner);

                this.videoTouchArea = doc.createElement("div");
                this.videoTouchArea.id = this.options.videoTouchAreaId;
                this.videoTouchArea.className = this.options.videoTouchAreaId + "-initial";
                this.videoContainer.appendChild(this.videoTouchArea);
                this.base.els.push(this.videoTouchArea);

                this.videoWrapperId.appendChild(this.videoContainer);

                if (this.options.playVideoInLine) {
                    this.videoElement.setAttribute("webkit-playsinline", "webkit-playsinline");
                }

                if (this.options.showBasicControls) {
                    this.videoElement.setAttribute("controls", "true");
                }

                // if (this.options['loop']) {
                //     this.videoElement.setAttribute('loop', 'true');
                // }

                if (this.base.mobileWeb) {
                    this.base.discardMarvelAudio = true;
                }

                __addingListeners.call(this);
            },

            /**
                Get the Module Name

                @method getName
                @return string
            **/
            getName = function () { return this.name_class; },

            /**
                Get the Module Version

                @method getVersion
                @return string
            **/
            getVersion = function () { return this.core_version; },

            /**
                Remove the video elements from the DOM

                @private
                @method __dealloc
                @return {Boolean}
            **/
            dealloc = function () {
                if (this.videoContainer) {
                    this.videoWrapperId.removeChild(this.videoContainer);
                }
                return false;
            },

            /**
                Exit Video

                @method exit
                @return {null}
            **/
            exit = function () {
                if (typeof callAdMarvelFunction !== "undefined" && this.options.useAdMarvelFunctionToStopAudio) {
                    admarvelURLRedirect("admarvelsdk://donePlayingAudio");
                }

                if (this.options.callbackExitFunction != null) {
                    this.base[this.options.callbackExitFunction].bind(this.base)();
                }

                if (!this.base.isAndroid) {
                    if (this.videoElement.webkitDisplayingFullscreen) {
                        this.videoElement.webkitExitFullscreen();
                    }
                }

                this.videoElement.currentTime = this.startPoint;

                if (this.options.loop) {
                    this.play(this.startPoint, true);
                } else {
                    this.pause();
                }

            },

            /**
                Get Video Element By Id

                @method getVideoById
                @param {Number} id
                @return {Boolean}
            **/
            getVideoById = function (id) {
                var v;
                for (v in this.options.videoPlaylist) {
                    if (this.options.videoPlaylist[v].id === id) {
                        return this.options.videoPlaylist[v];
                    }
                }
                return false;

            },

            /**
            	Return true if is playing the video and false if is on pause

            	@method isPlaying
            	@return {Bolean}
            **/
            isPlaying = function () {
                return this.isPlaying;
            },

            /**
            	Load and preload video

            	@method load
            	@param {Number} id
            	@param {String} quality
            	@return {null}
            **/
            load = function (id, quality) {
                if (typeof id === "undefined" || typeof quality === "undefined") {
                    alert("LoadVideo - Some parameters are undefined");
                    return false;
                }

                this.curVideo = this.getVideoById(id);

                if (typeof this.curVideo.src[quality] !== "undefined") {

                    this.base.trackEvent("Load Video:" + this.curVideo.name);

                    this.passed1Percent = false;
                    this.passed25Percent = false;
                    this.passed50Percent = false;
                    this.passed75Percent = false;
                    this.videoCompleted  = false;
                    this.isPlaying = false;
                    this.firstTime = (typeof this.videoElement.src === "undefined" ||  this.videoElement.src === "" ? true : false);

                    __resetTransform.call(this);
                    this.videoElement.controls = (typeof this.curVideo.controls === "undefined" ? this.options.showBasicControls : this.curVideo.controls);
                    this.videoElement.width = (typeof this.curVideo.width === "undefined" ? this.options.width : this.curVideo.width);
                    this.videoElement.height = (typeof this.curVideo.height === "undefined" ? this.options.height : this.curVideo.height);

                    this.videoElement.autoplay =  (typeof this.curVideo.autoplay === "undefined" ? this.options.autoplay : this.curVideo.autoplay);

                    //Mobile web is not autoplay - currently
                    if (this.base.mobileWeb && !this.base.samplePage) {
                        this.videoElement.autoplay = false;
                        this.curVideo.autoplay = false;
                    }

                    if (this.videoElement.autoplay === false) {
                        document.getElementById(this.options.videoLoadingSpinnerId).style.opacity = "0";
                        document.getElementById(this.options.videoPlayButtonId).style.opacity = "0.99";
                    } else {
                        document.getElementById(this.options.videoPlayButtonId).style.opacity = "0";
                    }

                    this.videoElement.poster =  (typeof this.curVideo.poster === "undefined" ? this.options.poster : this.curVideo.poster);
                    this.videoElement.muted =  (typeof this.curVideo.muted === "undefined" ? this.options.muted : this.curVideo.muted);
                    this.videoElement.preload =  (typeof this.curVideo.preload === "undefined" ? this.options.preload : this.curVideo.preload);

                    this.videoElement.rotate = (typeof this.curVideo.rotate === "undefined" ? this.options.rotate : this.curVideo.rotate);
                    if (this.videoElement.rotate !== 0) { __rotate.call(this, this.videoElement.rotate); }
                    this.videoElement.zoom = (typeof this.curVideo.zoom === "undefined" ? this.options.zoom : this.curVideo.zoom);
                    if (this.videoElement.zoom !== 0) { __zoom.call(this, this.videoElement.zoom); }

                    this.startPoint = (typeof this.curVideo.start !== "undefined" ? this.curVideo.start : 0);

                    if (typeof callAdMarvelFunction !== "undefined" && this.options.useAdMarvelFunctionToStopAudio) {
                        admarvelURLRedirect("admarvelsdk://startPlayingAudio");
                    }

                    if (this.videoElement.src !== this.curVideo.src[quality]) {
                        this.videoElement.src = this.curVideo.src[quality];
                    }

                    this.videoElement.load();

                    if (this.videoElement.autoplay && !this.firstTime) {
                        this.play(this.startPoint, true);
                    }
                } else {
                    alert("Load Video - The video source is undefined.");
                }

            },

            /**
            	Pause Video

            	@method pause
            	@return {null}
            **/
            pause = function (calledFromCode) {
                if (!calledFromCode) {
                    this.base.trackEvent("Pause Video");
                }

                this.videoElement.pause();
            },

            /**
            	Play Video

            	@method play
            	@return {null}
            **/
            play = function (second, calledFromCode) {
                if (typeof second !== "undefined") {
                    this.videoElement.currentTime = second;
                }

                if (!calledFromCode) {
                    this.base.trackEvent("Play Video");
                }

                if (typeof callAdMarvelFunction !== "undefined" && this.options.useAdMarvelFunctionToStopAudio) {
                    admarvelURLRedirect("admarvelsdk://startPlayingAudio");
                }

                this.videoElement.play();

            },

            /**
             Toggle Pause

             @method togglePause
             @return {null}
             **/
            togglePause = function () {
                if (this.isPlaying) {
                    this.pause();
                } else {
                    this.play();
                }
            },

            /**
            	Adding the listeners

            	@private
            	@method __addingListeners
            	@return {Null}
            **/
            __addingListeners = function () {

                this.videoElement.addEventListener("loadstart", __onLoadStart.bind(this), false);
                this.videoElement.addEventListener("progress", __onProgress.bind(this), false);
                this.videoElement.addEventListener("suspend", __onSuspend.bind(this), false);
                this.videoElement.addEventListener("abort", __onAbort.bind(this), false);
                this.videoElement.addEventListener("error", __onError.bind(this), false);
                this.videoElement.addEventListener("emptied", __onEmptied.bind(this), false);
                this.videoElement.addEventListener("stalled", __onStalled.bind(this), false);

                this.videoElement.addEventListener("loadedmetadata", __onLoadedMetaData.bind(this), false);
                this.videoElement.addEventListener("loadeddata", __onLoadedData.bind(this), false);
                this.videoElement.addEventListener("canplay", __onCanPlay.bind(this), false);
                this.videoElement.addEventListener("canplaythrough", __onCanPlayThrough.bind(this), false);
                this.videoElement.addEventListener("playing", __onPlaying.bind(this), false);
                this.videoElement.addEventListener("waiting", __onWaiting.bind(this), false);

                this.videoElement.addEventListener("seeking", __onSeeking.bind(this), false);
                this.videoElement.addEventListener("seeked", __onSeeked.bind(this), false);
                this.videoElement.addEventListener("ended", __onEnded.bind(this), false);

                this.videoElement.addEventListener("durationchange", __onDurationChange.bind(this), false);
                this.videoElement.addEventListener("timeupdate", __onTimeUpdate.bind(this), false);
                this.videoElement.addEventListener("play", __onPlay.bind(this), false);
                this.videoElement.addEventListener("pause", __onPause.bind(this), false);

                this.videoElement.addEventListener("webkitendfullscreen", exit.bind(this), false);

                this.videoTouchArea.addEventListener("click", __onTouchArea.bind(this), false);
            },

            /**
            	The user agent stops fetching the media data before it is completely downloaded, but not due to an error.<br/>
            	error is an object with the code MEDIA_ERR_ABORTED.<br/>
            	networkState equals either NETWORK_EMPTY or NETWORK_IDLE, depending on when the download was aborted.

            	@private
            	@method __onAbort
            	@return {null}
            **/
            __onAbort = function () {

                if (this.options.callbackAbortFunction != null) {
                    this.base[this.options.callbackAbortFunction].bind(this.base)();
                }
            },

            /**
            	The user agent can resume playback of the media data, but estimates that if playback were to be started now, the media resource could not be rendered at the current playback rate up to its end without having to stop for further buffering of content.<br/>
            	readyState newly increased to HAVE_FUTURE_DATA or greater.

            	@private
            	@method __onCanPlay
            	@return {null}
            **/
            __onCanPlay = function () {

                if (this.options.callbackCanPlayFunction != null) {
                    this.base[this.options.callbackCanPlayFunction].bind(this.base)();
                }
            },

            /**
            	The user agent estimates that if playback were to be started now, the media resource could be rendered at the current playback rate all the way to its end without having to stop for further buffering.<br/>
            	readyState is newly equal to HAVE_ENOUGH_DATA.

            	@private
            	@method __onCanPlayThrough
            	@return {null}
            **/
            __onCanPlayThrough = function () {

                if (this.options.callbackCanPlayThroughFunction != null) {
                    this.base[this.options.callbackCanPlayThroughFunction].bind(this.base)();
                }
            },


            /**
            	The duration attribute has just been updated.<br/>

            	@private
            	@method __onDurationChange
            	@return {null}
            **/
            __onDurationChange = function () {
                this.videoElement.end = (typeof this.curVideo.end !== "undefined" ? Math.round(this.curVideo.end) : Math.round(this.videoElement.duration));

                // do...while is neccesary to fix a bug on ios6
                do {
                    this.videoElement.currentTime = this.startPoint;
                } while (this.videoElement.currentTime !== this.startPoint);

                this.videoDurationTime =  this.videoElement.end - this.startPoint;

                if (this.options.callbackDurationChangeFunction != null) {
                    this.base[this.options.callbackDurationChangeFunction].bind(this.base)();
                }
            },

            /**
            	A media element whose networkState was previously not in the NETWORK_EMPTY state has just switched to that state (either because of a fatal error during load that's about to be reported, or because the load() method was invoked while the resource selection algorithm was already running).<br/>
            	networkState is NETWORK_EMPTY; all the IDL attributes are in their initial states.

            	@private
            	@method __onEmptied
            	@return {null}
            **/
            __onEmptied = function () {

                if (this.options.callbackEmptiedFunction != null) {
                    this.base[this.options.callbackEmptiedFunction].bind(this.base)();
                }
            },

            /**
            	Playback has stopped because the end of the media resource was reached.<br/>
            	currentTime equals the end of the media resource; ended is true.<br/>

            	@private
            	@method __onEnded
            	@return {null}
            **/
            __onEnded = function () {

                // video ended event called several times
                // http://stackoverflow.com/questions/11684056/html5-video-ended-event-called-several-times
                if (this.noLoop100PercentFired) {
                    return;
                }
                if (!this.options.loop) {
                    this.noLoop100PercentFired = true;
                    this.base.trackEvent(this.curVideo.name + "-video-no-loop-100-perc");
                } else if (!this.videoFirstLoopComplete) {
                    this.base.trackEvent(this.curVideo.name + "-video-loop-first-100-perc");
                } else {
                    this.base.trackEvent(this.curVideo.name + "-video-loop-subsequent-100-perc");
                }

                this.videoElement.currentTime = this.startPoint;

                this.passed1Percent  = false;
                this.passed25Percent = false;
                this.passed50Percent = false;
                this.passed75Percent = false;

                if (this.options.callbackEndedFunction != null) {
                    this.base[this.options.callbackEndedFunction].bind(this.base)();
                }

                if (this.options.loop) {
                    this.videoFirstLoopComplete = true;
                    this.play(this.startPoint, true);
                } else {
                    this.pause(true);
                }

            },

            /**
            	An error occurs while fetching the media data.<br/>
            	error is an object with the code MEDIA_ERR_NETWORK or higher.<br/>
            	networkState equals either NETWORK_EMPTY or NETWORK_IDLE, depending on when the download was aborted.

            	@private
            	@method __onError
            	@return {null}
            **/
            __onError = function () {

                if (this.options.callbackErrorFunction != null) {
                    this.base[this.options.callbackErrorFunction].bind(this.base)();
                }
            },

            /**
            	The user agent can render the m edia data at the current playback position for the first time.<br/>
            	readyState newly increased to HAVE_CURRENT_DATA or greater for the first time.

            	@private
            	@method __onLoadedData
            	@return {null}
            **/
            __onLoadedData = function () {

                if (this.options.callbackLoadedDataFunction != null) {
                    this.base[this.options.callbackLoadedDataFunction].bind(this.base)();
                }
            },

            /**
            	The user agent has just determined the duration and dimensions of the media resource and the text tracks are ready.<br/>
            	readyState is newly equal to HAVE_METADATA or greater for the first time.

            	@private
            	@method __onLoadedMetaData
            	@return {null}
            **/
            __onLoadedMetaData = function () {

                if (this.options.callbackLoadedMetaDataFunction != null) {
                    this.base[this.options.callbackLoadedMetaDataFunction].bind(this.base)();
                }
            },

            /**
            	The user agent begins looking for media data, as part of the resource selection algorithm.<br/>
            	networkState equals NETWORK_LOADING

            	@private
            	@method __onLoadStart
            	@return {null}
            **/
            __onLoadStart = function () {

                if (this.options.callbackLoadStartFunction != null) {
                    this.base[this.options.callbackLoadStartFunction].bind(this.base)();
                }
            },

            /**
            	The element has been paused. Fired after the pause() method has returned.<br/>
            	paused is newly true.<br/>

            	@private
            	@method __onPause
            	@return {null}
            **/
            __onPause = function () {

                this.isPlaying = false;
                document.getElementById(this.options.videoPlayButtonId).style.opacity = "0.99";

                if (this.options.callbackPauseFunction != null) {
                    this.base[this.options.callbackPauseFunction].bind(this.base)();
                }
            },


            /**
            	The element is no longer paused. Fired after the play() method has returned, or when the autoplay attribute has caused playback to begin.<br/>
            	paused is newly false.<br/>

            	@private
            	@method __onPlay
            	@return {null}
            **/
            __onPlay = function () {
                this.isPlaying = true;
                document.getElementById(this.options.videoPlayButtonId).style.opacity = "0";

                if (this.options.callbackPlayFunction != null) {
                    this.base[this.options.callbackPlayFunction].bind(this.base)();
                }
            },

            /**
            	Playback is ready to start after having been paused or delayed due to lack of media data.<br/>
            	readyState is newly equal to or greater than HAVE_FUTURE_DATA and paused is false, or paused is newly false and readyState is equal to or greater than HAVE_FUTURE_DATA.<br/> Even if this event fires, the element might still not be potentially playing, e.g. if the element is blocked on its media controller (e.g. because the current media controller is paused, or another slaved media element is stalled somehow, or because the media resource has no data corresponding to the media controller position), or the element is paused for user interaction or paused for in-band content.<br/>

            	@private
            	@method __onPlaying
            	@return {null}
            **/
            __onPlaying = function () {
                this.isPlaying = true;

                if (this.options.callbackPlayingFunction != null) {
                    this.base[this.options.callbackPlayingFunction].bind(this.base)();
                }
            },

            /**
            	The user agent is fetching media data.<br/>
            	networkState equals NETWORK_LOADING

            	@private
            	@method __onProgress
            	@return {null}
            **/
            __onProgress = function () {
                if (this.options.callbackProgressFunction != null) {
                    this.base[this.options.callbackProgressFunction].bind(this.base)();
                }
            },

            /**
            	The seeking IDL attribute changed to false.<br/>

            	@private
            	@method __onSeeked
            	@return {null}
            **/
            __onSeeked = function () {
                if (this.options.callbackSeekedFunction != null) {
                    this.base[this.options.callbackSeekedFunction].bind(this.base)();
                }
            },

            /**
            	The seeking IDL attribute changed to true.<br/>

            	@private
            	@method __onSeeking
            	@return {null}
            **/
            __onSeeking = function () {
                if (this.options.callbackSeekingFunction != null) {
                    this.base[this.options.callbackSeekingFunction].bind(this.base)();
                }
            },

            /**
            	The user agent is trying to fetch media data, but data is unexpectedly not forthcoming.<br>
            	networkState is NETWORK_LOADING.

            	@private
            	@method __onStalled
            	@return {null}
            **/
            __onStalled = function () {
                if (this.options.callbackStalledFunction != null) {
                    this.base[this.options.callbackStalledFunction].bind(this.base)();
                }
            },

            /**
            	The user agent is intentionally not currently fetching media data.<br/>
            	networkState equals NETWORK_IDLE

            	@private
            	@method __onSuspend
            	@return {null}
            **/
            __onSuspend = function () {

                if (this.options.callbackSuspendFunction != null) {
                    this.base[this.options.callbackSuspendFunction].bind(this.base)();
                }
            },


            /**
            	The current playback position changed as part of normal playback or in an especially interesting way, for example discontinuously.

            	@private
            	@method __onTimeUpdate
            	@return {null}
            **/
            __onTimeUpdate = function () {
                var currentTime, videoDuration, trackName;
                if (this.videoElement) {

                    currentTime = Math.floor(this.videoElement.currentTime);

                    //var startPoint = this.videoElement.start;
                    videoDuration = this.videoDurationTime;
                    trackName = this.curVideo.name;

                    if (currentTime - this.startPoint >= (videoDuration * 0.01) && !this.passed1Percent) {
                        this.base.trackEvent(trackName + "-video-1-perc");
                        this.passed1Percent = true;
                        this.videoLoadingSpinner.style.opacity = "0";
                        this.noLoop100PercentFired = false;
                    }

                    if (currentTime - this.startPoint >= (videoDuration * 0.25) && !this.passed25Percent) {
                        this.base.trackEvent(trackName + "-video-25-perc");
                        this.passed25Percent = true;
                    }

                    if (currentTime - this.startPoint >= (videoDuration * 0.5)  && !this.passed50Percent) {
                        this.base.trackEvent(trackName + "-video-50-perc");
                        this.passed50Percent = true;
                    }

                    if (currentTime - this.startPoint >= (videoDuration * 0.75)  && !this.passed75Percent) {
                        this.base.trackEvent(trackName + "-video-75-perc");
                        this.passed75Percent = true;
                    }
                }

                if (this.videoElement.currentTime > 0) {
                    document.getElementById(this.options.videoPosterImageId).style.opacity = "0";
                    document.getElementById(this.options.videoLoadingSpinnerId).style.opacity = "0";
                } else {
                    document.getElementById(this.options.videoPosterImageId).style.opacity = "0.99";
                }

                if (this.options.callbackTimeUpdateFunction != null) {
                    this.base[this.options.callbackTimeUpdateFunction].bind(this.base)();
                }

            },

            /**
            	The user click on touch area

            	@private
            	@method __onTouchArea
            	@return {null}
            **/
            __onTouchArea = function () {
                if (this.options.callbackTouchAreaFunction != null) {
                    this.base[this.options.callbackTouchAreaFunction].bind(this.base)();
                }

                if (this.options.pauseOnTap === true) {
                    this.togglePause();
                }

            },

            /**
            	Playback has stopped because the next frame is not available, but the user agent expects that frame to become available in due course.<br/>
            	readyState is equal to or less than HAVE_CURRENT_DATA, and paused is false.<br/>
            	Either seeking is true, or the current playback position is not contained in any of the ranges in buffered.<br/>
            	It is possible for playback to stop for other reasons without paused being false, but those reasons do not fire this event (and when those situations resolve, a separate playing event is not fired either): e.g. the element is newly blocked on its media controller, or playback ended, or playback stopped due to errors, or the element has paused for user interaction or paused for in-band content.<br/>

            	@private
            	@method __onWaiting
            	@return {null}
            **/
            __onWaiting = function () {
                if (this.options.callbackWaitingFunction != null) {
                    this.base[this.options.callbackWaitingFunction].bind(this.base)();
                }
            },

            /**
            	Reset transform style

            	@private
            	@method __resetTransform
            	@return {null}
            **/
            __resetTransform = function () {
                this.videoElement.style["-moz-transform"] = "";
                this.videoElement.style["-webkit-transform"] = "";
                this.videoElement.style["-o-transform"] = "";
                this.videoElement.style["-ms-transform"] = "";
                this.videoElement.style.transform = "";
            },

            /**
            	Rotate video <br/>
            	It is not working on Android Native Browser

            	@private
            	@method __rotate
            	@param {Number} value
            	@return {null}
            **/
            __rotate = function (value) {
                this.videoElement.style["-moz-transform"] += "rotate(" + value + "deg)";
                this.videoElement.style["-webkit-transform"] += "rotate(" + value + "deg)";
                this.videoElement.style["-o-transform"] += "rotate(" + value + "deg)";
                this.videoElement.style["-ms-transform"] += "rotate(" + value + "deg)";
                this.videoElement.style.transform += "rotate(" + value + "deg)";
            },

            /**
            	Zoom video <br/>
            	It is not working on Android Native Browser

            	@private
            	@method __zoom
            	@param {Number} value
            	@return {null}
            **/
            __zoom = function (value) {
                this.videoElement.style["-moz-transform"] += "scale(" + value + ")";
                this.videoElement.style["-webkit-transform"] += "scale(" + value + ")";
                this.videoElement.style["-o-transform"] += "scale(" + value + ")";
                this.videoElement.style["-ms-transform"] += "scale(" + value + ")";
                this.videoElement.style.transform += "scale(" + value + ")";
            };

            return {
                init: init,
                getVersion: getVersion,
                getName: getName,
                dealloc: dealloc,
                exit: exit,
                getVideoById: getVideoById,
                isPlaying: isPlaying,
                load: load,
                pause: pause,
                play: play,
                togglePause: togglePause
            };
        })();

})(window, document);
