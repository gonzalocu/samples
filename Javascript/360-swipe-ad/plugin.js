/*jshint undef: true, browser: true, devel: true, bitwise: true, curly: true, newcap: true, nonew: false, onevar: true, regexp: false, evil: true, rhino: true */

(function () {
    var ThreeSixtyAd = function () {
        this.init.apply(this, arguments);
    };
    ThreeSixtyAd.prototype = {    
        options : {
            preload : true,
            preloadAmount : null,
            urlFormat : null,
            numberOfImages : null,
            container : null,
            width: 320,
            height: 250,
            startFrom : 0,
            redirectURL: '[RM_CLICK_DI_LINK]',
            distanceToleranceToRedirect: 20,
            timeToleranceToRedirect: 6,
            onPreload : function (loaded, toLoad, overall) {},
            onPreloaded : function () {},
            onStart : function () {},
            onFirstTouch : function () {}
        },
    
        init : function (options) {
            this.touched = 0;
            // global variables
            this.displayWidth         = null;
            this.currentPositionX     = null;
            this.currentAngel         = 0;
            this.images                = [];
            this.imagePerAngle        = null;
            // For URL redirect
            this.touchStartTime        = null;
            this.touchStartPosition    = null;
        
            //local variables
            var adContainer, i, k, img, imageLoaded;
        
            // match options
            for(k in options){
                this.options[k] = options[k];
            }
        
            // create container
            if (!this.options.container) {
                if (document.readyState == "complete" || document.readyState == "interactive") {
                    this.options.container = document.createElement("div");
                    document.body.appendChild(this.options.container);
                 } else {
                    this.options.container = "intad-" + (new Date()).getTime() + "-" + Math.round(Math.random() * 10000);
                    document.write("<div id=\""+this.options.container+"\"></div>");
                    this.options.container = document.getElementById(this.options.container);
                 }
            }
            
            if (typeof(this.options.container) == 'string') {
                this.options.container = document.getElementById(this.options.container);
            }
        
            // check container exists
            if (!this.options.container || !this.options.container.nodeType) {
                window.console.error("Container does not exists.");
                return;
            }   
        
            adContainer                         = document.createElement("div");
            adContainer.style.overflow             = "hidden";
            adContainer.style.height             = this.options.height + 'px';
            adContainer.style.width             = this.options.width + 'px';
            adContainer.style.webkitTransition     = "opacity 0.1s linear";
            this.adContainer = adContainer;
            
            this.options.container.appendChild(adContainer);
            
            imageLoaded = (function (self) {
                var amount = Math.min(self.options.preloadAmount || self.options.numberOfImages, self.options.numberOfImages),
                    toLoad = amount + 0,
                    loaded = 0,
                    started = 0;
                if (typeof(amount) == 'number' && amount > 0 && self.options.preload) {
                    
                    return function () {
                        setTimeout(function () {
                            --amount;
                            ++loaded;
                        
                            if (amount <= 0 && !(started++)) {
                                self.options.onPreload(toLoad, toLoad, self.options.numberOfImages);
                                self.options.onPreloaded();
                                self.start();
                            } else if (!started) {
                                self.options.onPreload(loaded, toLoad, self.options.numberOfImages);
                            }
                        }, 0);
                    };
                }
                return function () {};
            }(this));
            
            for (i = 0 ; i < this.options.numberOfImages ; ++i) {
                img                                = new Image();
                img.onload       = img.onerror     = imageLoaded;
                img.src                            = this.options.urlFormat.replace('{id}', i + 1);
                img.style.zIndex                   = i + 1;
                img.style.position                 = 'absolute';
                img.style.opacity                  = '0';
                adContainer.appendChild(img);
                this.images.push(img);
            }
        
            //calculate image per Angel
            this.imagePerAngle = 360 / this.options.numberOfImages;
            
            if (!this.options.preload) {
                this.start();
            }
        },
        
        start : function () {
            var self = this, factor, leftRotate;
            this.lastImage                             = this.options.startFrom || 0;
            if (typeof(this.options.initialRotate) == 'number' && this.options.initialRotate) {
                leftRotate = Math.abs(this.options.initialRotate);
                factor = this.options.initialRotate / leftRotate;
                (function () {
                    var me = arguments.callee;
                    if (leftRotate <= 0) {
                        self.setupEvents();
                    } else {
                        self.images[self.lastImage].style.opacity = '0';
                        self.lastImage += factor;
                        if (self.lastImage >= self.images.length) {
                            self.lastImage = 0;
                        } else if (self.lastImage < 0) {
                            self.lastImage = self.images.length - 1;
                        }
                        self.images[self.lastImage].style.opacity = '1';
                        leftRotate--;
                        setTimeout(me, self.options.initialRotateSpeed || 50);
                    }
                }());
            } else {
                this.images[this.lastImage].style.opacity  = '1';
                this.setupEvents();
            }
        },
        
        setupEvents : function () {
            var self = this;
            // Add EventListener
            this.adContainer.addEventListener("touchstart", function (e) {
                self.touchStart(e);
            }, false);
            this.adContainer.addEventListener("touchmove", function (e) {
                self.touchMove(e);
            }, false);
        
            this.adContainer.addEventListener("touchend", function (e) {
                self.touchEnd(e);
            }, false);
            
            self.options.onStart();
        },
            
        touchStart : function (event) {
            if (!(this.touched++)) {
                this.options.onFirstTouch();
            }
            event.preventDefault();
            event.stopPropagation();
            this.currentPositionX = event.touches[0].clientX;
        
            // for URL redirect
            this.touchStartPosition = event.touches[0].clientX;
            this.touchStartTime        = new Date().getTime();
        },
    
        touchMove : function (event) {
            event.preventDefault();
            event.stopPropagation();
            this.calculateAngel(event.touches[0].clientX);
                        
            if (Math.floor(this.currentAngel % 360 / this.imagePerAngle) < 0 ) {
                this.currentAngel                           = this.currentAngel + 360;
                this.images[this.lastImage].style.opacity = '0';
                this.images[this.lastImage = Math.floor(this.currentAngel % 360 / this.imagePerAngle)].style.opacity = '1';
            } else {
                this.images[this.lastImage].style.opacity = '0';
                this.images[this.lastImage = Math.floor(this.currentAngel % 360 / this.imagePerAngle)].style.opacity = '1';
            }
        },
    
        touchEnd : function (event) {
            var distanceDiff      = Math.abs(this.touchStartPosition -  event.changedTouches[0].clientX),
                touchCurrentTime = new Date().getTime(),
                timeDiff          = (touchCurrentTime - this.touchStartTime) / 100;

            if(timeDiff < this.options.timeToleranceToRedirect && distanceDiff < this.options.distanceToleranceToRedirect && this.options.redirectURL) {
                window.top.location.href = this.options.redirectURL;
            }    
        },
    
        calculateAngel : function (newCurrentPostionX) {
            var diff               = this.currentPositionX - newCurrentPostionX,
                deltaAngel            = 180 / this.options.width * diff;
            this.currentAngel       = this.currentAngel + deltaAngel;
            this.currentPositionX = newCurrentPostionX;
        }
    };
    this.threeSixtyAd = function (options) {
        (new ThreeSixtyAd(options));
    };
}());