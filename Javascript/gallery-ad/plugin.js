/*jshint undef: true, browser: true, devel: true, bitwise: true, curly: true, newcap: true, nonew: false, onevar: true, regexp: false, evil: true */

(function() {
    var GalleryAd;
    (GalleryAd = function() {
        this.initialize.apply(this, arguments);
    }).prototype = {
        initialize: function(e) {
            this.options = function() {
                var t, n = {
                    url: null,
                    containerId: null,
                    width: 300,
                    height: 250,
                    clickAreaY: 220,
                    photoAmount: 5,
                    threshold: 0.2,
                    duration: 0.3,
                    pointsSize: 13,
                    pointsGap: 6,
                    pointsPosition: 228,
                    pointsInactiveColor: "#c7c7c7",
                    pointsActiveColor: "#fa7338",
                    touchToClickMoveThreshold: 20,
                    automaticSlide: false,
                    automaticSlideTime: 3
                };
                for (t in e) {
                    if (e.hasOwnProperty(t)) {
                        n[t] = e[t];
                    }
                }
                return n;
            }();
            this.divContainer = document.getElementById(this.options.containerId);
            if (!this.divContainer) {
                throw new Error("no container");
            }
            this.photoArray = new Array(this.options.photoAmount + 2);
            this.loaded = 0;
            this.currentPosition = 0;
            this.positionArray = new Array(this.options.photoAmount + 2);
            this.direction = 1;
            this.startX = 0;
            this.lastX = 0;
            this.startY = 0;
            var t, n, r, i = this.options.width,
                s = this;
            for (t = -1; t < this.options.photoAmount + 1; t++) {
                this.positionArray[t] = i;
                i = i - this.options.width;
            }
            this.container = function(e) {
                var t = document.createElement("div");
                t.style.width = e.width + "px";
                t.style.height = e.height + "px";
                t.style.margin = "0px";
                t.style.padding = "0px";
                t.style.border = "none";
                t.style.overflow = "hidden";
                return t;
            }(this.options);
            this.divContainer.appendChild(this.container);
            this.pointsContainer = function(e) {
                var t = e.photoAmount * (e.pointsSize + e.pointsGap),
                    n = document.createElement("div"),
                    fl = 'floa';
                n.style.height = e.pointsSize + "px";
                n.style.width = t + "px";
                n.style.position = "absolute";
                n.style.marginTop = e.pointsPosition + "px";
                n.style.zIndex = 10;
                n.style[fl + 't'] = "right";
                n.style.marginLeft = e.width / 2 - t / 2 + "px";
                return n;
            }(this.options);
            this.container.appendChild(this.pointsContainer);
            this.points = function(e) {
                var t, n, r = [], fl = 'floa';
                for (t = 0; t < e.photoAmount; t++) {
                    n = document.createElement("div");
                    n.style.height = e.pointsSize + "px";
                    n.style.width = e.pointsSize + "px";
                    n.style.borderRadius = e.pointsSize / 2 + "px";
                    n.style.marginLeft = e.pointsGap / 2 + "px";
                    n.style.marginRight = e.pointsGap / 2 + "px";
                    n.style[fl + 't'] = "left";
                    n.style.backgroundColor = e.pointsInactiveColor;
                    n.addEventListener("click", this, false);
                    r.push(n);
                }
                return r;
            }.call(this, this.options);
            this.points.forEach(function(e) {
                s.pointsContainer.appendChild(e);
            });
            this.touchArea = function(e) {
                var t = document.createElement("div");
                t.style.height = e.height + "px";
                t.style.width = e.width + "px";
                t.style.position = "absolute";
                t.style.zIndex = 11;
                return t;
            }(this.options);
            this.container.appendChild(this.touchArea);
            this.touchArea.addEventListener("touchstart", this, false);
            this.touchArea.addEventListener("mousedown", this, false);
            this.touchArea.addEventListener("touchmove", this, false);
            this.touchArea.addEventListener("mousemove", this, false);
            this.touchArea.addEventListener("touchend", this, false);
            this.touchArea.addEventListener("mouseup", this, false);
            this.touchArea.addEventListener("mouseout", this, false);
            this.slideContainer = function(e) {
                var t = document.createElement("div"), fl = 'floa';
                t.style.width = e.width * e.photoAmount + "px";
                t.style.height = e.height + "px";
                t.style[fl + 't'] = "left";
                t.style.webkitTransitionTimingFunction = "linear";
                t.style.webkitTransitionProperty = "-webkit-transform";
                t.style.webkitTransitionDuration = e.duration + "s";
                return t;
            }(this.options);
            this.container.appendChild(this.slideContainer);
            this.slideContainer.style.webkitTransform = "translateX(" + this.positionArray[this.currentPosition] + "px)";
            this.photoArray = function(e) {
                var t, n, r, i, s = [],
                    o = -e.width;
                for (t = -1; t < e.photoAmount + 1; t++) {
                    if (t === -1) {
                        i = e.photoAmount - 1;
                    } else if (t === e.photoAmount) {
                        i = 0;
                    } else {
                        i = t;
                    }
                    n = e.imagesFormatString.replace("#", i + 1);
                    r = new Image();
                    r.src = n;
                    r.position = o;
                    r.style.position = "absolute";
                    r.style.left = o + "px";
                    r.style.top = "0px";
                    r.width = e.width;
                    r.height = e.height;
                    o = o + e.width;
                    s.push(r);
                }
                return s;
            }(this.options);
            this.photoArray.forEach(function(e) {
                s.slideContainer.appendChild(e);
            });
            this.points[this.currentPosition].style.backgroundColor = this.options.pointsActiveColor;
            if (this.options.automaticSlide) {
                this.automaticSlideInterval = setInterval(function() {
                    s.moveToPosition(s.currentPosition + 1);
                }, this.options.automaticSlideTime * 1e3);
            }
        },
        moveToPosition: function(e) {
            var t = e, n = this;
            if (e < 0) {
                t = this.options.photoAmount - 1;
                this.slideContainer.style.webkitTransitionDuration = "0s";
                this.slideContainer.style.webkitTransform = "translate3d(" + (this.positionArray[t + 1] + this.lastX - this.startX) + "px,0,0)";
            } else if (e > this.options.photoAmount - 1) {
                t = 0;
                this.slideContainer.style.webkitTransitionDuration = "0s";
                this.slideContainer.style.webkitTransform = "translate3d(" + (this.positionArray[t - 1] + this.lastX - this.startX) + "px,0,0)";
            }
            setTimeout(function() {
                n.slideContainer.style.webkitTransitionDuration = n.options.duration + "s";
                n.slideContainer.style.webkitTransform = "translate3d(" + n.positionArray[t] + "px,0,0)";
            }, 50);
            this.points[this.currentPosition].style.backgroundColor = this.options.pointsInactiveColor;
            this.points[t].style.backgroundColor = this.options.pointsActiveColor;
            this.currentPosition = t;
        },
        handleEvent: function(e) {
            switch (e.type) {
            case "touchstart":
            case "mousedown":
                this.onStart(e);
                break;
            case "touchmove":
            case "mousemove":
                this.onMove(e);
                break;
            case "touchend":
            case "mouseup":
            case "mouseout":
                this.onEnd(e);
                break;
            }
        },
        onStart: function(e) {
            e.preventDefault();
            var t = this.getClickPosition(e);
            this.startX = t[0];
            this.lastX = t[0];
            this.startY = t[1];
        },
        onMove: function(e) {
            e.preventDefault();
            var t = this.getClickPosition(e);
            if (this.startX !== 0) {
                this.lastX = t[0];
                this.slideContainer.style.webkitTransitionDuration = "0s";
                this.slideContainer.style.webkitTransform = "translate3d(" + (this.positionArray[this.currentPosition] + this.lastX - this.startX) + "px,0,0)";
            }
            clearInterval(this.automaticSlideInterval);
        },
        onEnd: function(e) {
            e.preventDefault();
            if (Math.abs(this.lastX - this.startX) < this.options.touchToClickMoveThreshold) {
                if (this.startY > this.options.clickAreaY) {
                    if (this.startX > this.options.width * 0.5) {
                        this.moveToPosition(this.currentPosition + 1);
                    } else if (this.startX < this.options.width * 0.5) {
                        this.moveToPosition(this.currentPosition - 1);
                    }
                } else {
                    this.moveToPosition(this.currentPosition);
                    if ("mraid" in window) {
                        window.mraid.open(this.options.url);
                    } else if ("cvAdSpace" in window && "open" in window.cvAdSpace) {
                        window.cvAdSpace.open(this.options.url);
                    } else {
                        window.open(this.options.url);
                    }
                }
            } else {
                if (Math.abs(this.startX - this.lastX) > this.options.width * this.options.threshold) {
                    if (this.startX > this.lastX) {
                        this.moveToPosition(this.currentPosition + 1);
                    } else if (this.lastX > this.startX) {
                        this.moveToPosition(this.currentPosition - 1);
                    }
                } else {
                    this.moveToPosition(this.currentPosition);
                }
            }
            this.startX = 0;
            this.lastX = 0;
            this.startY = 0;
            if (this.options.automaticSlide) {
                var t = this;
                this.automaticSlideInterval = setInterval(function() {
                    t.moveToPosition(t.currentPosition + 1);
                }, this.options.automaticSlideTime * 1e3);
            }
        },
        getClickPosition: function(e) {
            var t = e.target,
                n = 0,
                r = 0;
            while (t) {
                n += t.offsetLeft;
                r += t.offsetTop;
                if (t.position == "fixed") {
                    break;
                }
                t = t.offsetParent;
            }
            n = (e.touches && e.touches.length ? e.touches[0].clientX : e.pageX) - n;
            r = (e.touches && e.touches.length ? e.touches[0].clientY : e.pageY) - r;
            return [n, r];
        }
    };
    this.galleryAd = function(t) {
        return new GalleryAd(t);
    };
})();
