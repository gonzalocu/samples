<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/************************** DAPP SUBDOMAIN ********************************/
Route::group(['domain' => 'dapp.adcolony.tools', 'middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Developer', 'Designer']], function() {
    Route::get ('/', function() { return view('dapp.dashboard.index'); });
});

Route::group(['domain' => 'dapp.local', 'middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Developer','Designer']], function() {
    Route::get ('/', function() { return view('dapp.dashboard.index'); });
});
/************************** END DAPP SUBDOMAIN ********************************/



/************************** PREVIEW SUBDOMAIN ********************************/
Route::group(array('domain' => "preview.adcolony.tools"), function() {
  Route::get('/', function(){ return Redirect::to('http://adcolony.tools/'); });
  Route::get('/{hash}', ['uses' => 'ACTools\PreviewController@index']);
  Route::get('/tag/{name}', ['uses' => 'ACTools\PreviewController@tag'] );
});
/************************** END PREVIEW SUBDOMAIN ********************************/


/*** PUBLIC ***/
Route::get('/',           ['uses' => 'ACTools\HomeController@index']);
Route::get('/home',       ['uses' => 'ACTools\HomeController@index']);
Route::get('/tag/{name}', ['uses' => 'ACTools\PreviewController@tag']);
Route::get('/qrcode/{slug}', function ($slug) {
    $response = Response::make(QrCode::format('png')->merge("/public/assets/images/basic-adc/qr-logo-overlay.png", .16)->size(230)->generate("http://preview.operamediaworks.tools/".$slug), 200 );
    $response->header( 'content-type', 'image/png' );
    return $response;
});

/*** ROLES : ADMINISTRATOR, DEVELOPER, DESIGNER, TOOLS, TESTER, SALES, STAFF, GALLERY ***/

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Briefing', 'Developer', 'Designer', 'AdOps', 'Tester', 'Sales', 'Staff', 'Gallery' ]], function() {
	Route::get('/ad-locker', ['uses' => 'ACTools\HomeController@adlocker']);
});
/****************************/

/*** AUTHENTICATION ***/
Route::get ('auth/login',        'Auth\AuthController@showLoginForm');
Route::post('auth/login',        'Auth\AuthController@login');
Route::get ('auth/login_dapp',   'Auth\AuthController@getLoginDapp');
Route::get ('auth/logout',       'Auth\AuthController@logout');
Route::post('auth/googlesignin', 'Auth\AuthController@googleSignIn');

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'ACTools\LanguageController@switchLang']);

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Developer', 'AdOps']], function() {

    // Ad Colony Tools
    Route::get ('/admin/useful-links',                    function() { return view('admin.useful-links.index'); });

    // Tools
    Route::get ('/tools/portal',                                      function() { return view('admin.ad-tools.index'); });

    Route::get ('/tools/make-medialets-tag',                          function() { return view('admin.ad-tools.make-medialets-tag'); });

    Route::get ('/tools/image-interstitial-creator',                  function() { return view('admin.ad-tools.image-interstitial-creator'); });
    Route::get ('/tools/zen-wrapper',                                 function() { return view('admin.ad-tools.zen-wrapper'); });
    Route::get ('/tools/tag-maker',                                   function() { return view('admin.ad-tools.tag-maker'); });
    Route::get ('/tools/app-detect-tag-maker',                        function() { return view('admin.ad-tools.app-detect-tag-maker'); });
    Route::get ('/tools/off-network-tag-generator',                   function() { return view('admin.ad-tools.off-network-tag-generator'); });

    Route::get ('/tools/sticky-converter',                            function() { return view('admin.ad-tools.sticky-converter'); });
    Route::get ('/tools/ios-video-interstitial-maker',                function() { return view('admin.ad-tools.ios-video-interstitial-maker'); });
    Route::get ('/tools/ios-video-interstitial-maker-bannerstitial',  function() { return view('admin.ad-tools.ios-video-interstitial-maker-bannerstitial'); });
    Route::get ('/tools/pixel-maker',                                 function() { return view('admin.ad-tools.pixel-maker'); });
    Route::get ('/tools/research-wrapper',                            function() { return view('admin.ad-tools.research-wrapper'); });
    Route::get ('/tools/expandable-builder-mobile',                   function() { return view('admin.ad-tools.expandable-builder-mobile'); });
    Route::get ('/tools/expandable-builder-tablet',                   function() { return view('admin.ad-tools.expandable-builder-tablet'); });
    Route::get ('/tools/interstitial-close-button-adder',             function() { return view('admin.ad-tools.interstitial-close-button-adder'); });
    Route::get ('/tools/doubleclick',                                 function() { return view('admin.ad-tools.doubleclick'); });
    Route::get ('/tools/sanitize-tag',                                function() { return view('admin.ad-tools.sanitize-tag'); });
    Route::get ('/tools/zenwrapper',                                  function() { return view('admin.ad-tools.zenwrapper'); });
    Route::get ('/tools/interstitialbuilder',                         function() { return view('admin.ad-tools.interstitialbuilder'); });

    // Developer Tools
    Route::get ('/tools/adtech-converter',                function() { return view('admin.tools.ops-adtech-converter'); });
    Route::get ('/tools/csv-to-json-converter',           function() { return view('admin.tools.ops-csv-to-json-converter'); });
    Route::get ('/tools/tag-converter',                   function() { return view('admin.tools.ops-tag-converter'); });
    Route::get ('/tools/admarvel-initialisation',         function() { return view('admin.tools.ops-admarvel-initialisation'); });
    Route::get ('/tools/app-detect-tag-creator',          function() { return view('admin.tools.ops-app-detect-tag-creator'); });
    Route::get ('/tools/doubleclick-tag-converter',       function() { return view('admin.tools.ops-doubleclick-tag-converter'); });
    Route::get ('/tools/expandable-mobile-builder',       function() { return view('admin.tools.ops-expandable-mobile-builder'); });
    Route::get ('/tools/expandable-tablet-builder',       function() { return view('admin.tools.ops-expandable-tablet-builder'); });
    Route::get ('/tools/image-interstitial-creator',      function() { return view('admin.tools.ops-image-interstitial-creator'); });
    Route::get ('/tools/interstitial-close-button-adder', function() { return view('admin.tools.ops-interstitial-close-button-adder'); });
    Route::get ('/tools/ios-video-bannerstitial-maker',   function() { return view('admin.tools.ops-ios-video-bannerstitial-maker'); });
    Route::get ('/tools/ios-video-interstitial-maker',    function() { return view('admin.tools.ops-ios-video-interstitial-maker'); });
    Route::get ('/tools/off-network-tag-creator',         function() { return view('admin.tools.ops-off-network-tag-creator'); });
    Route::get ('/tools/pixel-maker',                     function() { return view('admin.tools.ops-pixel-maker'); });

    Route::get ('/tools/research-tag-wrapper',            function() { return view('admin.tools.ops-research-tag-wrapper'); });
    Route::get ('/tools/sticky-converter',                function() { return view('admin.tools.ops-sticky-converter'); });
    Route::get ('/tools/tag-creator',                     function() { return view('admin.tools.ops-tag-creator'); });
    Route::get ('/tools/tag-sanitizer',                   function() { return view('admin.tools.ops-tag-sanitizer'); });
    Route::get ('/tools/zenwrapper-ad-tool',              function() { return view('admin.tools.ops-zenwrapper-ad-tool'); });
    Route::get ('/tools/adcolony-validation',             'OMTools\AdColonyValidationController@index');
    Route::get ('/tools/adcolony-validation/refresh',     'OMTools\AdColonyValidationController@refresh');
    Route::get ('/tools/adcolony-validation/jira',        'OMTools\AdColonyValidationController@jira');
    //Route::get ('/tools/pixel-management',              'OMTools\PixelManagementController@index');
    Route::resource('/tools/pixel-management',            'OMTools\PixelManagementController');

    Route::get('qa-tool',         'OMTools\QAToolController@index');
    Route::post('qa-tool',        'OMTools\QAToolController@upload');
    Route::get('qa-tool/compare', 'OMTools\QAToolController@compare');

    // Preview Page
    Route::get    ('/preview',      'ACTools\PreviewController@listing' );
    Route::post   ('/preview',      'ACTools\PreviewController@store' );
    Route::delete ('/preview/{id}', 'ACTools\PreviewController@destroy' );

    // Tag Generator
    Route::get ('/tag-generator/campaign/{campaign}/watching',      ['uses' => 'OMTools\CampaignController@addWatching']);
    Route::get ('/tag-generator/campaign/{campaign}/unwatching',    ['uses' => 'OMTools\CampaignController@removeWatching']);
    Route::get ('/tag-generator/campaign/search/json/&i=',          ['uses' => 'OMTools\CampaignController@searchCampaignJSON']);
    Route::get ('/tag-generator/campaign/search/json/&i={input?}',  ['uses' => 'OMTools\CampaignController@searchCampaignJSON']);
    Route::resource('/tag-generator/campaign',                      'OMTools\CampaignController');
    Route::resource('/tag-generator/campaign.banner',               'OMTools\BannerController');

    // Ad Colony Tools
    Route::get ('/tools/',                                function() { return view('admin.tools.adc-url-comparison'); });
    Route::get ('/tools/url-comparison',                  function() { return view('admin.tools.adc-url-comparison'); });
    Route::get ('/tools/adjust',                          function() { return view('admin.tools.adc-adjust'); });
    Route::get ('/tools/appsflyer',                       function() { return view('admin.tools.adc-appsflyer'); });
    Route::get ('/tools/apsalar',                         function() { return view('admin.tools.adc-apsalar'); });
    Route::get ('/tools/king',                            function() { return view('admin.tools.adc-king-com'); });
    Route::get ('/tools/kochava',                         function() { return view('admin.tools.adc-kochava'); });
    Route::get ('/tools/tune',                            function() { return view('admin.tools.adc-tune'); });

    Route::resource('/feature',     'OMTools\FeatureController',  ['except' => ['create', 'edit', 'update']]);
    Route::resource('/vertical',    'OMTools\VerticalController', ['except' => ['create', 'edit', 'update']]);
    Route::resource('/category',    'OMTools\CategoryController', ['except' => ['create', 'edit', 'update']]);

    //Route::resource('/admin/briefing', 'OMTools\BriefingController', ['except' => ['create', 'edit', 'update', 'destroy']]);

});
/****************************/

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Briefing', 'Designer', 'Developer', 'AdOps']], function() {
    Route::resource('/admin/briefing', 'OMTools\BriefingController', ['except' => ['show', 'edit', 'update', 'destroy']]);
    Route::get     ('/admin/briefing/pdf/{id}', 'OMTools\BriefingController@streamPdfFile' );
    Route::get     ('/admin/briefing/basecamp', 'OMTools\BriefingController@basecamp' );
    Route::get     ('/admin/briefing/email', 'OMTools\BriefingController@email' );

    Route::get ('/admin/guideline', function() { return view('admin.guideline.index'); });
});

/*** ROLES : ADMINISTRATOR, DEVELOPER, GALLERY ***/

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['Administrator', 'Developer', 'Gallery']], function() {
    Route::post('/gallery/upload-feature-file',       ['uses' => 'OMTools\GalleryController@uploadFeatureFile']);
    Route::post('/gallery/upload-showcase-file',       ['uses' => 'OMTools\GalleryController@uploadShowcaseFile']);
    Route::resource('/gallery',      'OMTools\GalleryController');
});

/****************************/

/*** ROLES : ADMINISTRATOR, DEVELOPER, DESIGNER ***/

Route::group(['middleware' => ['web','auth', 'roles'], 'roles' => ['Administrator', 'Developer', 'Designer', 'Briefing']], function() {
    Route::post('/ad-sample/upload-file',       ['uses' => 'OMTools\AdSampleController@uploadFile']);
    Route::get('/ad-sample/remove-file/{id}',   ['uses' => 'OMTools\AdSampleController@removeFile']);
    Route::resource('/admin/ad-locker',         'OMTools\AdLockerController');
    Route::resource('/ad-sample',               'OMTools\AdSampleController');
});

/****************************/


// API 1.0
Route::get ('/api/1.0/docs/ad-locker',                      function() { return view('api.v1_0.ad-locker'); });
Route::get ('/api/1.0/docs/ad-sample',                      function() { return view('api.v1_0.ad-sample');   });
Route::get ('/api/1.0/docs/campaign',                       function() { return view('api.v1_0.campaign'); });
Route::get ('/api/1.0/docs/feature',                        function() { return view('api.v1_0.feature');  });
Route::get ('/api/1.0/docs/vertical',                       function() { return view('api.v1_0.vertical'); });
Route::get ('/api/1.0/ad-locker/search/{input}', 	        ['uses' => 'API\V1_0\AdLockerAPIController@search']);
Route::get ('/api/1.0/ad-locker/searchbyfeature/{input}', 	['uses' => 'API\V1_0\AdLockerAPIController@searchAdsByFeature']);
Route::get ('/api/1.0/ad-locker/searchbyvertical/{input}', 	['uses' => 'API\V1_0\AdLockerAPIController@searchAdsByVertical']);
Route::get ('/api/1.0/url/create/live/{project}',           ['uses' => 'API\V1_0\UrlAPIController@createLiveUrl']);
Route::get ('/api/1.0/url/create/staging/{project}', 	    ['uses' => 'API\V1_0\UrlAPIController@createStagingUrl']);
Route::resource('/api/1.0/ad-locker',                       'API\V1_0\AdLockerAPIController',   ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
Route::resource('/api/1.0/campaign',                        'API\V1_0\CampaignAPIController',   ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
Route::resource('/api/1.0/campaign.banner',                 'API\V1_0\BannerAPIController',     ['except' => ['create', 'edit', 'update', 'destroy']]);
Route::resource('/api/1.0/ad-sample',                       'API\V1_0\AdSampleAPIController',   ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
Route::resource('/api/1.0/feature',                         'API\V1_0\FeatureAPIController',    ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
Route::resource('/api/1.0/vertical',                        'API\V1_0\VerticalAPIController',   ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
