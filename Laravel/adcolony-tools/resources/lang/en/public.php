<?php

return [

    'tell-me-more-btn' => 'Tell me more',
    'about-us-title' => 'About Us',
    'about-us-message' => 'Ad Colony is part of a rich cache of over 60 creative innovators and mobile masterminds, spanning the world’s top markets. It’s in our DNA, we’re almost programmed. We connect brand to audience via mobile, taking full advantage of a contextual mobile user experience.',
    'sample-title' => 'Ad Samples',
    'sample-message' => 'Choose a sample and speak to your friendly Ad Colony contact about what we can do for your brand.Of course if you have an idea for something a bit different, our developers are always hungry to create bespoke campaigns too.',
    'made-with-message1' => 'Made with',
    'made-with-message2' => 'at Ad Colony',
    'hero-text1' => 'We craft creative.',
    'hero-text2' => 'copy.',
    'hero-text3' => 'code.',
    'hero-text4' => 'awesome mobile ad experiences.',
];
