<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos 6 caracteres y que coincida con la confirmación.',
    'reset' => 'Tu contraseña ha sido reseteada!',
    'sent' => 'Hemos enviado por correo el enlace para resetear la contraseña!',
    'token' => 'Este identificador de reseteo de la contraseña es invalido.',
    'user' => "No podemos encontrar un usuario con ese correo.",

];
