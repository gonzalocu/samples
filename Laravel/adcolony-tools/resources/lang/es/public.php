<?php

return [

    'tell-me-more-btn' => 'Cuentame más',
    'about-us-title' => 'Sobre nosotros',
    'about-us-message' => 'Ad Colony es una de las más importante plataformas de anuncios con más de 60 creativos y cerebros innovadores para dispositivos moviles, que abarca los principales mercados del mundo. Esta en nuesto ADN. Conectamos marcas con audiencias a través de dispositivos moviles, aprovechando al máximo la experiencia contextual de los usuarios.',
    'sample-title' => 'Ad Sample',
    'sample-message' => 'Elije un ejemplo y habla con su contacto en Ad Colony sobre lo que podemos hacer para su marca. Por supuesto si tiene una idea algo diferente, nuestros programadores siempre estan hambrientos por crear campañas hechas a medida.',
    'made-with-message1' => 'Hecho con',
    'made-with-message2' => 'por Ad Colony',
];
