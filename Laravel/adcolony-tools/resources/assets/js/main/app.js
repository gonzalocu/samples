//import lodash from 'lodash';
import jquery from 'jquery';
import react from 'react';
import reactdom from 'react-dom';
import { default as Auth } from './auth';
import select from 'react-select';
import toggle from 'react-toggle';



//window._ = lodash;
window.jQuery = window.$ = jquery;
window.React = window.React = react;
window.ReactDOM = window.ReactDOM = reactdom;

window.Select = select;
window.Toggle = toggle;

const createLogInBtn = (id) => {
    if (document.getElementById(id)) {
        ReactDOM.render(<Auth.LogInButton />, document.getElementById(id));
    }
},
createLogOutBtn = (id) => {
    if (document.getElementById(id)) {
        ReactDOM.render(<Auth.LogOutButton />, document.getElementById(id));
    }
};

$(document).ready(function () {
    createLogInBtn('auth-login-btn');
    createLogInBtn('auth-login-btn2');
    createLogOutBtn('auth-logout-btn');
    createLogOutBtn('auth-logout-btn2');
    createLogOutBtn('auth-logout-btn3');
});
