var headers = document.getElementsByClassName('omw-header'),
    loginModal = document.getElementsByClassName('omw-login')[0],
    modalIsOpen = false;

window.addEventListener('scroll', function () {
    if (window.pageYOffset > 80) {
        headers[0].classList.add('omw-header-hide');
        headers[1].classList.remove('omw-header-hide');
    } else {
        headers[0].classList.remove('omw-header-hide');
        headers[1].classList.add('omw-header-hide');
    }
});

if (window.pageYOffset > 80) {
    headers[0].classList.add('omw-header-hide');
    headers[1].classList.remove('omw-header-hide');
} else {
    headers[0].classList.remove('omw-header-hide');
    headers[1].classList.add('omw-header-hide');
}

window.addEventListener('click', function (event) {
    if (!window.isInsideBounds(event, loginModal) && modalIsOpen && typeof event.target.onclick !== 'function') {
        window.closeModal();
    }
});


window.toggleModal = function () {
    if (modalIsOpen) {
        window.closeModal();
    } else {
        window.openModal();
    }
};


window.openModal = function () {
    modalIsOpen = true;
    loginModal.classList.remove('omw-modal-hidden');
};

window.closeModal = function () {
    modalIsOpen = false;
    loginModal.classList.add('omw-modal-hidden');
};

window.isInsideBounds = function (event, element) {

    return true;
};
