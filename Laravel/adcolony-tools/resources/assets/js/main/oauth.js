/* global gapi, auth2 */

var googleUser = {},
  startApp = function () {
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '316502315250-qvql7cbbh7mrgaun8q2ctd17uprn1aig.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin'

            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignin(document.getElementById('customBtn'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {}, function (googleUser) {
        var id_token = googleUser.getAuthResponse().id_token,
            profile = googleUser.getBasicProfile(),
            xhr = new XMLHttpRequest();
        xhr.open('POST', '/auth/googlesignin', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('X-CSRF-TOKEN', window.Laravel.csrfToken);
        xhr.setRequestHeader('X-XSRF-TOKEN', window.Laravel.csrfToken);

        xhr.onload = function () {
            window.location = '/';
        };
        xhr.send('id_token=' + id_token + '&name=' + profile.getName() + '&email=' + profile.getEmail());
    }, function (error) {
        throw new Error(JSON.stringify(error, undefined, 2));
    });
}

function logOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        window.location = '/auth/logout';
    });
}

(function () {
    startApp();
})();
