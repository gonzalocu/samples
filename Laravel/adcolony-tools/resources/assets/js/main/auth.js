import React from 'react';
import GoogleLogin from 'react-google-login';

class LogInButton extends React.Component {
    __failureResponse(response) {
        throw new Error(response);
    }

    __laravelSignIn(googleUser) {
        let id_token = googleUser.getAuthResponse().id_token,
            profile = googleUser.getBasicProfile(),
            xhr = new XMLHttpRequest();
        xhr.open('POST', '/auth/googlesignin', true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('X-CSRF-TOKEN', window.Laravel.csrfToken);
        xhr.setRequestHeader('X-XSRF-TOKEN', window.Laravel.csrfToken);
        xhr.onload = function () {
            window.location = '/';
        };
        xhr.send('id_token=' + id_token + '&name=' + profile.getName() + '&email=' + profile.getEmail());
    }

    __successResponse(response) {
        this.__laravelSignIn(response);
    }

    render() {
        return (
            <GoogleLogin
                clientId="316502315250-qvql7cbbh7mrgaun8q2ctd17uprn1aig.apps.googleusercontent.com"
                style={{ background: '#db4537', color: 'white', padding: '10px 0', borderRadius: '2px', border: '1px solid transparent', fontSize: '.8em', width: '100%', fontFamily: 'Arial', fontWeight: 'normal', outline: 0 }}
                buttonText="Log In with Google+"
                onSuccess={this.__successResponse.bind(this)}
                onFailure={this.__failureResponse.bind(this)}
            />
        );
    }
}

class LogOutButton extends React.Component {
    __logout() {
        let auth2 = window.gapi.auth2.getAuthInstance();
        auth2.signOut().then(() => {
            window.location = '/auth/logout';
        });
    }

    render() {
        return (
            <a className='adc-green-btn' onClick={this.__logout}>Log Out</a>
        );
    }
}

export default {
    LogInButton:  LogInButton,
    LogOutButton: LogOutButton
};
