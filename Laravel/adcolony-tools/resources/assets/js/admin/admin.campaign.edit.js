var BannerApp = angular.module('bannerApp', ['bannerCtrl', 'bannerService'],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
