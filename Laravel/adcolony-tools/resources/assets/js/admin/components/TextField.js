
'use strict';

export default React.createClass({
    displayName: 'TextField',
    propTypes: {
        label: React.PropTypes.string,
        placeholder: React.PropTypes.string
    },
    getInitialState() {
        return {
            disabled: false,
            value: '',
        };
    },
    handleChange(e) {
        this.setState({ value: e.target.value });
    },
    toggleDisabled(e) {
        this.setState({ disabled: e.target.checked });
    },
    render() {
        return (
            <div className="section">
                <h6 className="section-heading">{this.props.label}</h6>
                <input  type="text"
                        className="form-control"
                        placeholder={this.props.placeholder}
                        value={this.state.value}
                        onChange={this.handleChange} />
			</div>
        );
    }
});
