/* global Select */

'use strict';

export default React.createClass({
    displayName: 'MultiSelectField',
    propTypes: {
        label: React.PropTypes.string,
        options: React.PropTypes.array,
        placeholder: React.PropTypes.string
    },
    getInitialState() {
        return {
            disabled: false,
            options: this.props.options,
            value: [],
        };
    },
    handleSelectChange(value) {
        //console.log('You\'ve selected:', value);
        this.setState({ value });
    },
    toggleDisabled(e) {
        this.setState({ disabled: e.target.checked });
    },
    render() {
        return (
            <div className="section">
            <h6 className="section-heading">{this.props.label}</h6>
				<Select multi simpleValue   disabled={this.state.disabled}
                                            value={this.state.value}
                                            placeholder={this.props.placeholder}
                                            options={this.state.options}
                                            onChange={this.handleSelectChange} />
			</div>
        );
    }
});
