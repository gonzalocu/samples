/* global Toggle */

'use strict';

export default React.createClass({
    displayName: 'ToogleButton',
    propTypes: {
        label: React.PropTypes.string,
        text1: React.PropTypes.string,
        text2: React.PropTypes.string,
        checked: React.PropTypes.bool
    },
    getInitialState() {
        return {
            disabled: false,
            checked: this.props.checked
        };
    },
    componentDidMount() {
        this.updateThumbText(this.props.checked);
    },
    updateThumbText(checked) {
        let el = this.refs.toggle,
            input = $(ReactDOM.findDOMNode(el)).find('.react-toggle-thumb');
        if (checked) {
            input.html(this.props.text2);
        } else {
            input.html(this.props.text1);
        }
    },
    handleChange(e) {
        this.updateThumbText(e.target.checked);
        this.setState({ checked: e.target.checked });
    },
    toggleDisabled(e) {
        this.setState({ disabled: e.target.checked });
    },
    render() {
        return (
            <div className="section">
                <h6 className="section-heading">{this.props.label}</h6>
                <Toggle ref='toggle' defaultChecked={this.state.checked}
                        disabled={this.state.disabled}
                        icons={{
                            checked: <span>{this.props.text1}</span>,
                            unchecked: <span>{this.props.text2}</span>,
                        }}
                        onChange={this.handleChange} />
			</div>
        );
    }
});
