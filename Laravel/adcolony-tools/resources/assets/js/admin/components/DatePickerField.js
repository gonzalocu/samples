'use strict';

export default React.createClass({
    displayName: 'DatePickerField',
    propTypes: {
        label: React.PropTypes.string,
        value: React.PropTypes.string
    },
    getInitialState() {
        return ({ value: this.props.value });
    },
    componentDidMount() {
        this.setupDatepicker();
    },
    componentDidUpdate() {
        this.setupDatepicker();
    },
    setupDatepicker() {
        let el = this.refs.datepicker,
            input = $(ReactDOM.findDOMNode(el)).pickadate({
                        firstDay: 1,
                        format: 'dd/mm/yyyy',
                        formatSubmit: 'yyyy/mm/dd'
                    }),
            picker = input.pickadate('picker');
        picker.on('set', () => {
            this.setState({ value: picker.get('select', 'dd/mm/yyyy') });
        });

        picker.on('open', () => {
            $(ReactDOM.findDOMNode(this.refs.arrow)).addClass('open');
        });

        picker.on('close', () => {
            $(ReactDOM.findDOMNode(this.refs.arrow)).removeClass('open');
        });

        $(ReactDOM.findDOMNode(this.refs.arrowzone)).on('click', (event) => {

            picker.open(true);
            event.stopPropagation();
            event.preventDefault();
        });
    },
    onDateChange(event) {
        this.setState({ operand: event.target.value });
    },
    render() {
        return (
            <div className="section">
                <h6 className="section-heading">{this.props.label}</h6>
                <div className="datepicker-wrapper">
                    <input type="date"
                        ref="datepicker"
                        value={this.state.value}
                        onChange={this.onDateChange} />
                    <span ref="arrowzone" className="select-arrow-zone">
                        <span ref="arrow" className="select-arrow"></span>
                    </span>
                </div>
            </div>
        );
    }
});
