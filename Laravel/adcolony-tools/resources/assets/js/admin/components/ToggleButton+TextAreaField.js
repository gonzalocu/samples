import ToggleButton from './ToggleButton';
import TextAreaField from './TextAreaField';

'use strict';

export default React.createClass({
    displayName: 'ToogleButton+TextAreaField',
    propTypes: {
        label: React.PropTypes.string,
        text1: React.PropTypes.string,
        text2: React.PropTypes.string,
        checked: React.PropTypes.bool
    },
    getInitialState() {
        return {
            disabled: false,
            checked: this.props.checked
        };
    },
    componentDidMount() {
        this.updateThumbText(this.props.checked);
    },
    updateThumbText(checked) {
        let el = this.refs.toggle,
            input = $(ReactDOM.findDOMNode(el)).find('.react-toggle-thumb');
        if (checked) {
            input.html(this.props.text2);
        } else {
            input.html(this.props.text1);
        }
    },
    handleChange(e) {
        this.updateThumbText(e.target.checked);
        this.setState({ checked: e.target.checked });
    },
    toggleDisabled(e) {
        this.setState({ disabled: e.target.checked });
    },
    render() {
        return (
            <div className="section">
                <h6 className="section-heading">{this.props.label}</h6>
                <ToggleButton ref="togglebutton" checked={this.state.checked} text1={this.props.text1} text2={this.props.text2} />
                <TextAreaField ref="textarea1" placeholder={'Value'} />
                <TextAreaField ref="textarea2" placeholder={'Value'} />
			</div>
        );
    }
});
