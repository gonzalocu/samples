$(document).ready(function () {
    function isUrlorTagChecked() {
        if ($('#urlortag:checked').length) {
            $('#urlortag + h3 .text').html('Tag');
            $('#url').hide();
            $('#url').prev().hide();
            $('#tag').show();
            $('#tag').prev().show();
        } else {
            $('#urlortag + h3 .text').html('Url');
            $('#url').show();
            $('#url').prev().show();
            $('#tag').hide();
            $('#tag').prev().hide();
        }
    }

    if ($('#urlortag')) {
        $('#urlortag').click(function () { isUrlorTagChecked(); });

        if ($('#tag').val()) {
            $('#urlortag')[0].checked = true;
        }
        isUrlorTagChecked();
    }

});
