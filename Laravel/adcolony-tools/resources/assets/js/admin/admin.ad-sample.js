
function isUrlorTagChecked() {
    if ($('#urlortag:checked').length) {
        $('#urlortag + label .text').html('Tag');
        $('#url').hide();
        $('#url').prev().hide();
        $('#tag').show();
        $('#tag').prev().show();
    } else {
        $('#urlortag + label .text').html('Url');
        $('#url').show();
        $('#url').prev().show();
        $('#tag').hide();
        $('#tag').prev().hide();
    }
}

$(document).ready(function () {
    if ($('#urlortag')) {
        $('#urlortag').click(function () { isUrlorTagChecked(); });
        if ($('#tag').val()) {
            $('#urlortag')[0].checked = true;
        }
        isUrlorTagChecked();
    }
});
