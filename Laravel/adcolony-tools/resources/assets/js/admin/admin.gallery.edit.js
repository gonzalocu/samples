$( document ).ready(function() {
    $('#live_date').pickadate({
        format: 'yyyy/mm/dd',
    	formatSubmit: 'yyyy/mm/dd',
    }); 
    
   // var baseUrl = "{{ url('/') }}";
    // var token = "{{ Session::getToken() }}";
    Dropzone.autoDiscover = false;
    
  
    var myDropzone2 = new Dropzone("#showcase-dropzone", {
        url: baseUrl + "/gallery/upload-showcase-file",
        uploadMultiple: false,
        maxFiles: 1,
        params: { _token: token }
    });
    
    myDropzone2.on("drop", function() {
        $( "#showcase-image" ).css('opacity',0);
        myDropzone.removeAllFiles()
    });
    
    myDropzone2.on("success", function(file,object,event) {
    
        var path = "/" + object.path;
        
        $("#showcase-container").html("<div id='showcase-image' class='omw-admin-format-image' style='opacity:0;' ></div>");
        $("#showcase-image" ).css('background-image','url('+path+')');
        $("#showcase_image_url").val ('true');
        setInterval(function(){ 
            this.removeAllFiles();
            $( "#showcase-image" ).css('opacity',1);
        }.bind(myDropzone2), 1000);
        
    });

    
    var myDropzone = new Dropzone("#feature-dropzone", {
        url: baseUrl + "/gallery/upload-feature-file",
        uploadMultiple: false,
        maxFiles: 1,
        params: { _token: token }
    });
    
    myDropzone.on("drop", function() {
        $( "#feature-image" ).css('opacity',0);
        myDropzone.removeAllFiles()
    });
    
    myDropzone.on("success", function(file,object,event) {
    
        var path = "/" + object.path;
        
        $("#feature-container").html("<div id='feature-image' class='omw-admin-format-image' style='opacity:0;' ><div id='feature-header'>Featured Creative</div><div id='feature-mask'></div></div>");
        $("#feature-image" ).css('background-image','url('+path+')');
        $("#feature_image_url").val ('true');
        setInterval(function(){ 
            this.removeAllFiles();
            $( "#feature-image" ).css('opacity',1);
        }.bind(myDropzone), 1000);
        
    });
    
    setInterval(function(){ 
        $( "#showcase-image" ).css('opacity',1);
        $( "#showcase-image" ).fadeIn('slow', function(){});
    }, 500);
   
    setInterval(function(){ 
        $( "#feature-image" ).css('opacity',1);
        $( "#feature-image" ).fadeIn('slow', function(){});
    }, 500);    
});