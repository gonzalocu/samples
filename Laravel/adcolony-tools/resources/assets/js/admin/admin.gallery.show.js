$( document ).ready(function() {
    setInterval(function(){ 
        $( "#showcase-image" ).css('opacity',1);
        $( "#showcase-image" ).fadeIn('slow', function(){});
    }, 100);
   
    setInterval(function(){ 
        $( "#feature-image" ).css('opacity',1);
        $( "#feature-image" ).fadeIn('slow', function(){});
    }, 300);    
});