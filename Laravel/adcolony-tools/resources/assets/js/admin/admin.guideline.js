import TextField from './components/TextField';
import TextAreaField from './components/TextAreaField';
import MultiSelectField from './components/MultiSelectField';
import SingleSelectField from './components/SingleSelectField';
import DatePickerField from './components/DatePickerField';
import ToggleButton from './components/ToggleButton';
import ToggleButtonTextAreaField from './components/ToggleButton+TextAreaField';

$(document).ready(function () {
    const OPTIONS = [
        { label: 'Option 1', value: '1' },
        { label: 'Option 2', value: '2' },
        { label: 'Option 3', value: '3' },
        { label: 'Option 4', value: '4' }
    ],
    PLACEHOLDER = 'Value',
    PLACEHOLDER_SELECT = 'Select the option';

    if (document.getElementById('textfield1')) {
        ReactDOM.render(<TextField label="Text Field" placeholder={PLACEHOLDER} />, document.getElementById('textfield1'));
    }

    if (document.getElementById('textarea1')) {
        ReactDOM.render(<TextAreaField label="TextArea Field" placeholder={PLACEHOLDER} />, document.getElementById('textarea1'));
    }

    if (document.getElementById('select1')) {
        ReactDOM.render(<SingleSelectField label="Select (Single Option)" options={OPTIONS} placeholder={PLACEHOLDER_SELECT} />, document.getElementById('select1'));
    }

    if (document.getElementById('select2')) {
        ReactDOM.render(<MultiSelectField label="Select (Multi Option)" options={OPTIONS} placeholder={PLACEHOLDER_SELECT} />, document.getElementById('select2'));
    }

    if (document.getElementById('date1')) {
        ReactDOM.render(<DatePickerField label="Date Picker" value="dd/mm/yyyy"  />, document.getElementById('date1'));
    }

    if (document.getElementById('toggle1')) {
        ReactDOM.render(<ToggleButton label="Toggle Button" checked={false} text1='yes' text2='no' />, document.getElementById('toggle1'));
    }

    if (document.getElementById('toggle2')) {
        ReactDOM.render(<ToggleButtonTextAreaField label="Toggle Button + TextArea" checked={false} text1='yes' text2='no' value={''} />, document.getElementById('toggle2'));
    }
});
