/* global DappComponents */
/* eslint no-unused-vars: [2, {"vars": "all", "varsIgnorePattern": "DappRoot"}] */

var DappRoot = React.createClass({
    getInitialState: function () {
        return {
        };
    },
    render: function () {
        return (
            <div id="omw-dapp-main" >
                <DappHeader />
                <DappMainColumn />
                <DappFooter />
            </div>
        );
    }
}),

DappHeader = React.createClass({
    render: function () {
        return (
            <div id='omw-dapp-header'></div>
        );
    }
}),

DappFooter = React.createClass({
    render: function () {
        return (
            <div id='omw-dapp-footer'></div>
        );
    }
}),

DappMainColumn = React.createClass({
    getInitialState: function () {
        return {
            componentList: ['briefing'],
            componentUrl: ['http://dapp.adcolony.tools/qa-tool/'],
            selected: 0
        };
    },
    _onChangeItem: function (id) {
        this.setState({ selected: id });
    },
    render: function () {
        return (
            <div id='omw-dapp-main-column'>
                <DappLeftColumn  componentList={this.state.componentList} selected={this.state.selected} onChangeItem={this._onChangeItem} />
                <DappRightColumn />
                <DappCentralColumn  componentUrl={this.state.componentUrl} selected={this.state.selected} />
            </div>
        );
    }
}),

DappLeftColumn = React.createClass({
    propTypes: {
        componentList: React.PropTypes.array.isRequired,
        selected: React.PropTypes.number.isRequired,
        onChangeItem: React.PropTypes.func.isRequired
    },
    _onChangeItem: function (id) {
        this.props.onChangeItem(id);
    },
    render: function () {
        return (
            <div id='omw-dapp-left-column'>
                <DappComponents componentList={this.props.componentList} selected={this.props.selected} onChangeItem={this._onChangeItem} />
            </div>
        );
    }
}),

DappRightColumn = React.createClass({
    render: function () {
        return (
            <div id='omw-dapp-right-column'></div>
        );
    }
}),

DappCentralColumn = React.createClass({
    propTypes: {
        componentUrl: React.PropTypes.array.isRequired,
        selected: React.PropTypes.number.isRequired
    },
    render: function () {
        var handleComponents = this.props.componentUrl.map((cmp, id) => {
            return <iframe key={id} id={'omw-dapp-iframe'} className={'omw-dapp-iframe' + id} style={{ position: 'absolute', display:(this.props.selected === id ? 'block' : 'none')}} src={this.props.componentUrl[id]} />
            //return <DappComponentItem key={cmp} id={id} item={cmp} selected={(this.props.selected === id ? 'active' : '')} onChangeItem={this._onChangeItem} />;
        });
        return (
            <div id='omw-dapp-central-column'>{handleComponents}</div>
        );
    }
});
