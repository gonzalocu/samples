/* eslint no-unused-vars: [2, {"vars": "all", "varsIgnorePattern": "DappComponents"}] */

var DappComponents = React.createClass({
    propTypes: {
        componentList: React.PropTypes.array.isRequired,
        selected: React.PropTypes.number.isRequired,
        onChangeItem: React.PropTypes.func.isRequired
    },
    _onChangeItem: function (id) {
        this.props.onChangeItem(id);
    },
    render: function () {
        return (
            <div id='omw-dapp-components'>
                <DappComponentList componentList={this.props.componentList} selected={this.props.selected} onChangeItem={this._onChangeItem} />
                <DappAddComponent />
            </div>
        );
    }
}),

DappComponentList = React.createClass({
    propTypes: {
        componentList: React.PropTypes.array.isRequired,
        selected: React.PropTypes.number.isRequired,
        onChangeItem: React.PropTypes.func.isRequired
    },
    _onChangeItem: function (id) {
        this.props.onChangeItem(id);
    },
    render: function () {
        var handleComponents = this.props.componentList.map((cmp, id) => {
            return <DappComponentItem key={cmp} id={id} item={cmp} selected={(this.props.selected === id ? 'active' : '')} onChangeItem={this._onChangeItem} />;
        });
        return (
            <div id='omw-dapp-component-list'>
                {handleComponents}
            </div>
        );
    }
}),

DappComponentItem = React.createClass({
    propTypes: {
        id: React.PropTypes.number.isRequired,
        item: React.PropTypes.string.isRequired,
        selected: React.PropTypes.string.isRequired,
        onChangeItem: React.PropTypes.func.isRequired
    },
    _onClick: function () {
        this.props.onChangeItem(this.props.id);
    },
    render: function () {
        return (
            <div id='omw-dapp-component-item' className={this.props.selected} >
                <button className={ 'omw-dapp-btn btn-component-' + this.props.item + ' ' + this.props.selected} onClick={this._onClick} ></button>
            </div>
        );
    }
}),

DappAddComponent = React.createClass({
    render: function () {
        return (
            <div id='omw-dapp-add-component'>
                <button className="btn-add"></button>
            </div>
        );
    }
});
