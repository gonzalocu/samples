//SVG TEXT
var path = document.querySelector('.omw-hero-text-underline-path'),
    length = path.getTotalLength();

path.style.strokeDasharray = length + ' ' + length;
path.style.strokeDashoffset = length;
