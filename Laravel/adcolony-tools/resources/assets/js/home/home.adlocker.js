/* global toggleAdvertModal, as_json, autoSuggest */

$(document).ready(function () {
    var options_search_by_name = {
        maxheight: 400,
        timeout: 250000000000000000000,
        script: "/api/1.0/ad-locker/search/&",
        varname: "i",
        json: true,
        offsety: 15,
        callback: function (obj) { toggleAdvertModal(obj.url.url, obj.url.slug, obj.value, ""); }
    },
    options_search_by_feature = {
        maxheight: 400,
        timeout: 250000000000000000000,
        script: "/api/1.0/ad-locker/searchbyfeature/&",
        varname: "i",
        json: true,
        offsety: 15,
        callback: function (obj) { toggleAdvertModal(obj.url.url, obj.url.slug, obj.value, ""); }
    },
    options_search_by_vertical = {
        maxheight: 400,
        timeout: 250000000000000000000,
        script: "/api/1.0/ad-locker/searchbyvertical/&",
        varname: "i",
        json: true,
        offsety: 15,
        callback: function (obj) { toggleAdvertModal(obj.url.url, obj.url.slug, obj.value, ""); }
    },
    option_filter_selected = 1;

    $("#omw-adlocker-search-input").focus();
    $(".dropdown-menu li").click(function () {
        for (var i=1; i<=3; i++) {
            if ($("#elem" + i).attr("id") == this.id) {
                selectFilter(i);
            }
        }
    });

    function selectFilter(opt) {
        for (var i=1;i<=3;i++){
            $("#elem" + i).show();
        }

        $("#omw-adlocker-search-input").val("");
        if (typeof as_json != "undefined") {
            as_json.clearSuggestions();
        }
        var default_text = ["SEARCH CAMPAIGN NAMES", "SEARCH AD FEATURES", "SEARCH INDUSTRIAL VERTICALS"];
        switch (opt) {
            case 1:
                $("#omw-search-text").html(default_text[0]);
                $("#elem1").hide();
                option_filter_selected = 1;
                as_json = new AutoSuggest("omw-adlocker-search-input", options_search_by_name);
                break;
            case 2:
                $("#omw-search-text").html(default_text[1]);
                $("#elem2").hide();
                option_filter_selected = 2;
                as_json = new AutoSuggest("omw-adlocker-search-input", options_search_by_feature);
                break;
            case 3:
                $("#omw-search-text").html(default_text[2]);
                $("#elem3").hide();
                option_filter_selected = 3;
                as_json = new AutoSuggest("omw-adlocker-search-input", options_search_by_vertical);
                break;
        }
    }

    selectFilter(option_filter_selected);

});
