
window.addEventListener('load', function () {
    document.getElementsByClassName('omw-overlay')[0].style.visibility = 'hidden';
    document.getElementsByClassName('omw-overlay')[0].style.opacity = '0';

    window.detectScreenSize();

    setTimeout(function () {
        window.detectScreenSize();
    }, 500);
});
