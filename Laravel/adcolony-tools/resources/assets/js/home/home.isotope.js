/* global Isotope */

// ISOTOPE
var grid = document.querySelector('.omw-flex-products'),
    iso = new Isotope(grid, { itemSelector: '.omw-product', layoutMode: 'fitRows' }
);

for (var i = 0; i < document.getElementsByClassName('omw-products-filter-feature').length; i++) {
    document.getElementsByClassName('omw-products-filter-feature')[i].addEventListener('click', function (event) {
        iso.arrange({ filter: event.target.getAttribute('data-filter') });
    });
}
