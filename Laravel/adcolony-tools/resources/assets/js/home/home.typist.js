/* global path, herotext1, herotext2, herotext3, herotext4 */

// TYPIST
$(window).on('load', function () {
    $('.omw-hero-text')
    .typist({ speed: 15 })

    .typistPause(100)
    .typistAdd(herotext1)
    .typistPause(1500)

    .typistRemove(9)

    .typistPause(100)
    .typistAdd(herotext2)
    .typistPause(1500)

    .typistRemove('5')

    .typistPause(100)
    .typistAdd(herotext3)
    .typistPause(1500)

    .typistRemove('5')

    .typistPause(100)

    .typistAdd(herotext4, function () {
        path.style.transition = path.style.WebkitTransition = path.style.MozTransition = 'stroke-dashoffset 500ms ease-out 200ms';
        path.style.strokeDashoffset = 0;

        setTimeout(function () { path.style.opacity = '1'; }, 200);
    })

    .typistStop();
});
