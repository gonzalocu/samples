//SCROLL TO
function scrollToElement(element) {
    $('html, body').animate({
        scrollTop: $(element).offset().top - 78,
        duration: 1000,
        easing: 'swing'
    });
}

window.addEventListener('scroll', function () {
    if (document.getElementsByClassName('omw-products')[0].getBoundingClientRect().top <= 79) {
        $('.omw-inverse .omw-navigation li a')[1].className = 'active';
        $('.omw-inverse .omw-navigation li a')[0].className = '';
    } else if (document.getElementsByClassName('omw-about')[0].getBoundingClientRect().top <= 79) {
        $('.omw-inverse .omw-navigation li a')[0].className = 'active';
        $('.omw-inverse .omw-navigation li a')[1].className = '';
    } else {
        $('.omw-inverse .omw-navigation li a')[0].className = '';
        $('.omw-inverse .omw-navigation li a')[1].className = '';
    }
});
