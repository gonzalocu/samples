//MODAL
var deviceInformation = {
    iphone4s: {
        portrait: {
            width: "320px",
            height: "480px"
        },
        landscape: {
            width: "480px",
            height: "320px"
        }
    },
    iphone5s: {
        portrait: {
            width: "320px",
            height: "568px"
        },
        landscape: {
            width: "568px",
            height: "320px"
        }
    },
    iphone6: {
        portrait: {
            width: "375px",
            height: "667px"
        },
        landscape: {
            width: "667px",
            height: "375px"
        }
    },
    iphone6plus: {
        portrait: {
            width: "414px",
            height: "736px"
        },
        landscape: {
            width: "736px",
            height: "414px"
        }
    },
    s5: {
        portrait: {
            width: "320px",
            height: "568px"
        },
        landscape: {
            width: "568px",
            height: "320px"
        }
    },
    nexus5: {
        portrait: {
            width: "320px",
            height: "568px"
        },
        landscape: {
            width: "568px",
            height: "320px"
        }
    }
},
advertModalOpen = false,
qrcodePopUpOpen = false,
iframe = document.getElementsByClassName("omw-device-iframe")[0];

window.addEventListener("resize", function () {
    window.detectScreenSize();
});

window.addEventListener("orientationchange", function () {
    window.detectScreenSize();
});

function openAdvertModal(link, share, name, description) {
    advertModalOpen = true;
    document.getElementsByClassName("omw-product-modal-wrapper")[0].classList.add("omw-product-modal-wrapper-expand");
    document.head.parentElement.style.overflow = "hidden";

    document.getElementsByClassName("omw-product-modal-title")[0].innerHTML = name;
    document.getElementsByClassName("omw-product-modal-description")[0].innerHTML = description;
    document.getElementsByClassName("omw-device-iframe")[0].src = link;
    if (share !== "") {
        if (document.getElementById("qrcode-image")) {
            document.getElementById("qrcode-image").style.backgroundImage = "url(/qrcode/" + share + ")";
        }
    }
    if (typeof document.getElementsByClassName("omw-product-modal-share-button")[0] !== "undefined") {
        document.getElementsByClassName("omw-product-modal-share-button")[0].href = "http://preview.adcolony.tools/" + share;
    }

    setTimeout(function () {
        document.getElementsByClassName("omw-product-modal-content")[0].style.opacity = "0.99";

        if (typeof document.getElementsByClassName("omw-product-modal-close")[0] !== "undefined") {
            document.getElementsByClassName("omw-product-modal-close")[0].style.visibility = "visible";
            document.getElementsByClassName("omw-product-modal-close")[0].style.opacity = "0.99";
        }

        document.getElementsByClassName("omw-product-modal-menu")[0].classList.remove("omw-product-modal-menu-hide");
    }, 300);
}

function closeAdvertModal() {
    advertModalOpen = false;
    console.log('close advert modal');
    document.getElementsByClassName("omw-product-modal-content")[0].style.opacity = "0";

    if (typeof document.getElementsByClassName("omw-product-modal-close")[0] !== "undefined") {
        document.getElementsByClassName("omw-product-modal-close")[0].style.visibility = "hidden";
        document.getElementsByClassName("omw-product-modal-close")[0].style.opacity = "0";
    }

    document.getElementsByClassName("omw-product-modal-menu")[0].classList.add("omw-product-modal-menu-hide");
    document.getElementsByClassName("omw-device-iframe")[0].src = "";

    setTimeout(function () {
        document.getElementsByClassName("omw-product-modal-wrapper")[0].classList.remove("omw-product-modal-wrapper-expand");
		document.head.parentElement.style.overflow = "";
    }, 300);
}

function openCodePopUp() {
    qrcodePopUpOpen = true;

    document.getElementsByClassName("qrcode-copy")[0].innerHTML = "SCAN";

    document.getElementsByClassName("omw-product-modal-qrcode-button")[0].style.width = "230px";
    document.getElementsByClassName("omw-product-modal-qrcode-button")[0].style.height = "277px";

    document.getElementById("qrcode-close").style.opacity = 1;

    // document.getElementById('omw-product-qr').style.opacity = 1;
    // if(document.getElementById('omw-qrcode-image')) {
    //     document.getElementById('omw-qrcode-image').style.opacity = 1;
    //     document.getElementById('omw-qrcode-image').style.bottom = "10px";
    // }
    document.getElementById("omw-product-qr").style.bottom = "10px";
}

function closeCodePopUp() {
    qrcodePopUpOpen = false;
    console.log('close');
    document.getElementsByClassName("qrcode-copy")[0].innerHTML = "QR CODE";

    document.getElementsByClassName("omw-product-modal-qrcode-button")[0].style.width = "88px";
    document.getElementsByClassName("omw-product-modal-qrcode-button")[0].style.height = "35px";

    document.getElementById("qrcode-close").style.opacity = 0;
    document.getElementById("omw-product-qr").style.opacity = 0;

    // if(document.getElementById('omw-qrcode-image')) {
    //     document.getElementById('omw-qrcode-image').style.opacity = 0;
    //     document.getElementById('omw-qrcode-image').style.bottom = "-40px";
    // }
    document.getElementById("omw-product-qr").style.bottom = "-40px";
}

function toggleAdvertModal(link, share, name, description) {
    window.detectScreenSize();
    console.log('toggle advert modal', link, share, name, description);
    if (!advertModalOpen && link) {
        openAdvertModal(link, share, name, description);
    } else {
        closeAdvertModal();
    }
}

function toggleQrCodePopUp () {
    if (qrcodePopUpOpen) {
        closeCodePopUp();
    } else {
        openCodePopUp();
    }
}

function toggleAdvertDevice() {
    var device = $("input:checked.omw-device-btn")[0].value,
        orientation = $("input:checked.omw-device-btn")[1].value;

    document.getElementsByClassName("marvel-device")[0].className = "marvel-device silver " + device + " " + orientation;

    document.getElementsByClassName("omw-device-iframe")[0].width = deviceInformation[device][orientation]["width"];
    document.getElementsByClassName("omw-device-iframe")[0].height = deviceInformation[device][orientation]["height"];

    document.getElementsByClassName("omw-device-iframe")[0].style.width = deviceInformation[device][orientation]["width"];
    document.getElementsByClassName("omw-device-iframe")[0].style.height = deviceInformation[device][orientation]["height"];
}

window.detectScreenSize = function () {
    if (window.innerWidth > 767) {
        document.getElementsByClassName("screen")[0].appendChild(iframe);
        toggleAdvertDevice();
    } else {
        document.getElementsByClassName("omw-product-modal-wrapper")[0].scrollTop = 0;
        document.getElementsByClassName("omw-product-modal-content")[0].appendChild(iframe);

        iframe.width = "100%";
        iframe.height = window.innerHeight + "px";

        iframe.style.width = "100%";
        iframe.style.height = window.innerHeight + "px";
    }
};
