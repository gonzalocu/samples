<!-- resources/views/auth/login_dapp.blade.php -->

@extends('layouts.master_dapp')

@section('content')
    <div id="omw-dapp-login">
        <div id="omw-dapp-innerbox">
            <div class="omw-dapp-logo"></div>
            <div id="gSignInWrapper">
                <div id="customBtn" class="customGPlusSignIn">
                    <span class="icon"></span>
                    <span class="buttonText">Log in with Google</span>
                </div>
            </div>
        </div>
        <div id="omw-dapp-powered-by"></div>
    </div>
@endsection
