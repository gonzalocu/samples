<!-- resources/views/auth/login.blade.php -->

@extends('layouts.master')

@section('header')
    @include("layouts/header_mainmenu", array('type' => 'white-show', 'active' => ''))
@endsection

@section('script')
    <script src="/js/home/home.preload.js"></script>
@endsection

@section('script_bottom')

@endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="/css/home/styles.css">
    <style>
    .omw-content {
        background-color: #fff;
        height: 700px;
    }
    </style>
@endsection

@section('content')
<div class='auth-login'>
    <form method="POST" action="/auth/login">
        <div class="auth-login-title">Log in</div>
        {!! csrf_field() !!}
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email address" value="{{ old('email') }}" required="" autofocus="">

        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">

        <input type="submit" value='Log In'>
    </form>
    <div class="auth-line"><div class="auth-or">or</div></div>
    <div id='auth-login-btn2'></div>
</div>
@endsection
