<!-- resources/views/auth/register.blade.php -->

@extends('layouts.master_admin')

@section('content')
    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <form method="POST" action="/auth/register" class="form-signin">
            <h2 class="form-signin-heading">Please sign up</h2>
            {!! csrf_field() !!}
            <br/>
            <label for="name" class="sr-only">Name</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="{{ old('name') }}" required="" autofocus="">
            <br/>
            <label for="email" class="sr-only">Email</label>
            <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
            <br/>
            <label for="password" class="sr-only">Pasword</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            <br/>
            <label for="password_confirmation" class="sr-only">Confirm Pasword</label>
            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password">
            <br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </form>
    </div>
@endsection