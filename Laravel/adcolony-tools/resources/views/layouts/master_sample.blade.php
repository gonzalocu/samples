<meta name="viewport" content="width=device-width, initial-scale=1">

<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alex+Brush' rel='stylesheet' type='text/css'>

<div class='omw-sample-wrapper'>
	<ul class='omw-sample-header'>
		<li class='omw-sample-icon'><svg version="1.1" viewBox="-303.8 556 26 26" xml:space="preserve"> <path d="M-285.5,575.5l-6.5-6.5l6.5-6.5l-2-2l-8.5,8.5l8.5,8.5L-285.5,575.5z"/> </svg>
		<li class='omw-sample-icon'><svg></svg> 
		<li class='omw-sample-icon'><svg version="1.1" viewBox="-110.6 556 26 26" xml:space="preserve"> <polygon points="-97.6,557 -109.1,569.7 -106.8,569.7 -106.8,581 -100.2,581 -99.9,581 -99.9,575.6 -95.3,575.6 -95.3,581 -95,581 -88.4,581 -88.4,569.7 -86.1,569.7 "/> </svg>
	</ul>

	<div class='omw-sample-content'>
		@yield("sample-content")
	</div>
	
	<ul class='omw-sample-footer'>
        <li class='omw-sample-icon'><svg version="1.1" viewBox="-161.9 556 26 26" xml:space="preserve"> <path d="M-135.9,566.1l-9.3-0.8l-3.7-8.6l-3.7,8.6l-9.3,0.8l7.1,6.1l-2.1,9.1l8-4.8l8,4.8l-2.1-9.1L-135.9,566.1z M-148.9,573.2 l-4,2.4l1.1-4.5l-3.5-3l4.6-0.4l1.8-4.2l1.8,4.3l4.6,0.4l-3.5,3l1.1,4.5L-148.9,573.2z"/> </svg>
        <li class='omw-sample-icon'><svg version="1.1" viewBox="-210 556 26 26" xml:space="preserve"> <path d="M-189.8,574c-0.9,0-1.7,0.4-2.4,0.9l-8.6-5c0.1-0.3,0.1-0.6,0.1-0.8c0-0.3,0-0.6-0.1-0.8l8.5-5c0.7,0.6,1.5,1,2.5,1 c2,0,3.6-1.6,3.6-3.6s-1.6-3.6-3.6-3.6s-3.6,1.6-3.6,3.6c0,0.3,0,0.6,0.1,0.8l-8.5,5c-0.7-0.6-1.5-1-2.5-1c-2,0-3.6,1.6-3.6,3.6 s1.6,3.6,3.6,3.6c1,0,1.8-0.4,2.5-1l8.6,5c-0.1,0.3-0.1,0.5-0.1,0.8c0,1.9,1.6,3.5,3.5,3.5s3.5-1.6,3.5-3.5S-187.8,574-189.8,574z" /> </svg>
		<li class='omw-sample-icon'><svg version="1.1" viewBox="-265.5 556 26 26" xml:space="preserve"> <path d="M-261.5,566c-1.6,0-3,1.3-3,3s1.4,3,3,3s3-1.3,3-3S-259.9,566-261.5,566z M-243.5,566c-1.6,0-3,1.3-3,3s1.4,3,3,3s3-1.3,3-3 S-241.9,566-243.5,566z M-252.5,566c-1.6,0-3,1.3-3,3s1.4,3,3,3s3-1.3,3-3S-250.9,566-252.5,566z"/> </svg>
	</ul>
</div>

<style>
	html, body {
		margin: 0;
		padding: 0;
	}

    .omw-sample-wrapper {
    	height: 100%;
    	padding: 55px 0;
    	box-sizing: border-box;
    }	

    .omw-sample-header {
    	position: fixed;
    	width: 100%;
    	height: 55px;
    	top: 0;
    	left: 0;
    	background-color: #000;
    	list-style: none;
    	padding: 0;
    	margin: 0;
    }

    .omw-sample-icon {
        position: relative;
    	display: inline-block;
    	width: 33.3333333333%;
    	height: 55px;
    	text-align: center;
    }

    .omw-sample-icon svg {
        fill: #fff;
        fill-opacity: 0.4;
        width: 100%;
        height: 26px;
        margin: 14px 0;
    }

    .omw-sample-content {
        position: relative;
        width: 100%;
        height: 100%;
        overflow: scroll;
        -webkit-overflow-scrolling: touch;
    }

    .omw-sample-footer {
    	position: fixed;
    	width: 100%;
    	height: 55px;
    	bottom: 0;
    	left: 0;
    	background-color: #000; 
    	list-style: none;
    	padding: 0;
    	margin: 0;    	   	
    }
</style>