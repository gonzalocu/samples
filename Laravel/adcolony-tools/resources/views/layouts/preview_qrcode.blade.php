@if (Request::root() == "http://preview.adcolony.tools")
	<?php $qrclass = "omw-product-modal-qrcode";  $qrimage = "background-image:url(/qrcode/".$item->slug ."); "; ?>
@else
	<?php $qrclass = "omw-product-modal-qrcode-bottom"; $qrimage = ''; ?>
@endif

<!-- <div class='omw-product-modal-qrcode-button {{ $qrclass }}' onclick='toggleQrCodePopUp()'>
    <span class="qrcode-copy">QR CODE</span>
    <div id="qrcode-close">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 30.2 33" enable-background="new 0 0 30.2 33" xml:space="preserve">
			<g id="XMLID_4_">
				<polygon id="XMLID_18_" points="14.5,19.4 6.4,27.5 3.7,24.8 11.8,16.7 3.7,8.6 6.4,5.9 14.5,14 22.6,5.9 25.3,8.6 17.2,16.7
				25.3,24.8 22.6,27.5 "/>
				<path id="XMLID_15_" fill="#FFFFFF" d="M22.6,6.4l2.1,2.1l-8.1,8.1l8.1,8.1L22.6,27l-8.1-8.1L6.4,27l-2.1-2.1l8.1-8.1L4.3,8.6
				l2.1-2.1l8.1,8.1L22.6,6.4 M22.6,5.3l-0.6,0.6l-7.6,7.6L6.9,5.9L6.4,5.3L5.8,5.9L3.7,8L3.1,8.6l0.6,0.6l7.6,7.6l-7.6,7.6l-0.6,0.6
				l0.6,0.6l2.1,2.1l0.6,0.6l0.6-0.6l7.6-7.6l7.6,7.6l0.6,0.6l0.6-0.6l2.1-2.1l0.6-0.6l-0.6-0.6l-7.6-7.6l7.6-7.6l0.6-0.6L25.3,8
				l-2.1-2.1L22.6,5.3L22.6,5.3z"/>
			</g>
        </svg>
    </div>
    <div id='omw-qrcode-image' style='{{ $qrimage }}'></div>


</div> -->
<div id="qrcode-title">Scan by a QR Reader to view the ad on your mobile device</div>
<div id='qrcode-image' style='{{ $qrimage }}'></div>
