@extends('layouts.master_sample')

@section('sample-content')
<style type="text/css">
	.omw-banner-slot {
		position: fixed;
		width: 100%;
		height: 50px;
		bottom: 55px;
		background-color: #fff;
	}

	iframe {
		display: block;
		width: 320px;
		height: 50px;
		margin: 0 auto;
		overflow: hidden;
	}

	.omw-banner-slot {
		background-color: #000;
	}

	.omw-banner-content {
		padding-bottom: 50px;
	}

	::-webkit-scrollbar { 
		display: none; 
	}	

	.omw-banner-post {

	}

	.omw-banner-post:last-child {
		padding-bottom: 1em;
	}

	.omw-banner-user {
		display: flex;
		display: -webkit-flex;

		align-items: center;
		justify-content: flex-start;

		margin: 0 5vw;
		padding: 1em 0 0 0;
	}

	.omw-banner-icon {
		width: 35px;
		height: 35px;
		border-radius: 50%;
		background-color: #dedede;		
	}

	.omw-banner-name {
		margin-left: 0.5em;
		font-family: 'Lato';
		font-size: 1em;
	}

	.omw-banner-image {
		position: relative;
		width: 90vw;
		height: 90vw;
		margin: 0 auto;
		margin-top: 1em;
		background-color: #dedede;

		display: flex;
		display: -webkit-flex;

		align-items: center;
		justify-content: flex-start;	
	}

	.omw-banner-image svg:nth-child(1) {
		position: absolute;
		width: 90vw;
		top: 0;
		left: 0;
	}

	.omw-banner-image svg:nth-child(3) {
		position: absolute;
		width: 90vw;
		bottom: 0;
		left: 0;
	}

	.omw-banner-message {
		font-family: 'Alex Brush', cursive;
		display: block;
		width: 100%;
		text-align: center;
		color: #fff;
		font-size: 6em;
		line-height: 0.6;
		padding-top: 0.25em;
	}

	@media (min-width: 375px) {
		.omw-banner-message {
			font-size: 6.6em;
		}
	}

	@media (min-width: 414px) {
		.omw-banner-message {
			font-size: 7.7em;
		}
	}

	.omw-banner-message span {
		font-family: 'Lato';
		font-size: 0.3em;
	}

	.omw-banner-likes {
		display: flex;
		display: -webkit-flex;

		align-items: center;
		justify-content: flex-start;

		margin: 0 5vw;
		padding: 1em 0;
	}

	.omw-banner-heart {
		width: 25px;
		height: 25px;
		fill: #dedede;
	}

	.omw-banner-score {
		margin-left: 0.5em;
		font-family: 'Lato';
		font-size: 1em;
	}
</style>

<div class='omw-banner-slot'>
	<iframe srcdoc="" width="320px" height="50px" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="yes"></iframe>	
</div>

<div class='omw-banner-content'>
	<div class='omw-banner-post'>
		<div class='omw-banner-user'>
			<div class='omw-banner-icon'></div>
			<div class='omw-banner-name'>Steve23</div>			
		</div>

		<div class='omw-banner-image'>
			<div class='omw-banner-message'>Fail<br>fast<br><span>AND</span><br>carry<br>on</div>
		</div>

		<div class='omw-banner-likes'>
			<div class='omw-banner-heart'><svg version="1.1" viewBox="-363 554 30 30" enable-background="new -363 554 30 30" xml:space="preserve"> <path d="M-348.4,582.4l-2-1.8c-7.2-6.5-12-10.9-12-16.1c0-4.3,3.4-7.7,7.7-7.7c2.4,0,4.8,1.1,6.3,2.9c1.5-1.8,3.9-2.9,6.3-2.9 c4.3,0,7.7,3.4,7.7,7.7c0,5.3-4.8,9.6-12,16.2L-348.4,582.4z"/> </svg></div>
			<div class='omw-banner-score'>23</div>
		</div>
	</div>
	
	<div class='omw-banner-post'>
		<div class='omw-banner-user'>
			<div class='omw-banner-icon'></div>
			<div class='omw-banner-name'>James05</div>			
		</div>

		<div class='omw-banner-image'>
			<div class='omw-banner-message'>Question<br><span>EVERYTHING</span><br>&<br><span>IMAGINE THE</span><br>Impossible</div>
		</div>

		<div class='omw-banner-likes'>
			<div class='omw-banner-heart'><svg version="1.1" viewBox="-363 554 30 30" enable-background="new -363 554 30 30" xml:space="preserve"> <path d="M-348.4,582.4l-2-1.8c-7.2-6.5-12-10.9-12-16.1c0-4.3,3.4-7.7,7.7-7.7c2.4,0,4.8,1.1,6.3,2.9c1.5-1.8,3.9-2.9,6.3-2.9 c4.3,0,7.7,3.4,7.7,7.7c0,5.3-4.8,9.6-12,16.2L-348.4,582.4z"/> </svg></div>
			<div class='omw-banner-score'>0</div>
		</div>
	</div>

	<div class='omw-banner-post'>
		<div class='omw-banner-user'>
			<div class='omw-banner-icon'></div>
			<div class='omw-banner-name'>Frank45</div>			
		</div>

		<div class='omw-banner-image'>
			<div class='omw-banner-message'>Stay Humble<br><span>WORK HARD</span><br>Be kind<br><span>SMILE OFTEN</span></div>
		</div>

		<div class='omw-banner-likes'>
			<div class='omw-banner-heart'><svg version="1.1" viewBox="-363 554 30 30" enable-background="new -363 554 30 30" xml:space="preserve"> <path d="M-348.4,582.4l-2-1.8c-7.2-6.5-12-10.9-12-16.1c0-4.3,3.4-7.7,7.7-7.7c2.4,0,4.8,1.1,6.3,2.9c1.5-1.8,3.9-2.9,6.3-2.9 c4.3,0,7.7,3.4,7.7,7.7c0,5.3-4.8,9.6-12,16.2L-348.4,582.4z"/> </svg></div>
			<div class='omw-banner-score'>11</div>
		</div>
	</div>

</div>

@endsection