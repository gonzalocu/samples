
@if (Request::path() == "/" || Request::path() == 'home')
	<?php $home = 1 ?>
@else
	<?php $home = 0 ?>
@endif

@if ($type == 'transparent')
	<header class='omw-header noselect'>
@elseif ($type == 'white-hide')
	<header class='omw-header omw-inverse omw-header-hide noselect'>
@elseif ($type == 'white-show')
	<header class='omw-header omw-inverse noselect'>
@endif

	<a @if ($home) onclick='scrollToElement(".omw-content")' @else href='/' @endif class='company-logo'></a>

	<ul class='omw-navigation'>
        <li><a @if ($home) onclick='scrollToElement(".omw-about")' @else href='/#omw-about' @endif>About Us</a></li>
        <li><a @if ($home) onclick='scrollToElement(".omw-products")' @else href='/#omw-products' @endif>Ad Samples</a></li>

        @if(Auth::check())
            @if ( Auth::user()->role[0]->name == "Administrator" || Auth::user()->role[0]->name == "Developer" || Auth::user()->role[0]->name == "AdOps" || Auth::user()->role[0]->name == "Designer" || Auth::user()->role[0]->name == "Briefing" || Auth::user()->role[0]->name == "Staff" || Auth::user()->role[0]->name == "Gallery" )
                <li>|</li>
                <li><a href="/ad-locker">Ad Locker</a></li>
            @endif
            @if ( Auth::user()->role[0]->name == "Administrator" || Auth::user()->role[0]->name == "Developer" || Auth::user()->role[0]->name == "Designer" )
                <li class="{{ ($active == 'admin'?'active':'') }}"><a href="/ad-sample">Admin</a></li>
            @elseif ( Auth::user()->role[0]->name == "Briefing" )
        		<li class="{{ ($active == 'admin'?'active':'') }}"><a href="/admin/briefing">Admin</a></li>
			@elseif ( Auth::user()->role[0]->name == "AdOps" )
				<li class="{{ ($active == 'admin'?'active':'') }}"><a href="/qa-tool">Admin</a></li>
            @elseif ( Auth::user()->role[0]->name == "Gallery" )
                <li class="{{ ($active == 'admin'?'active':'') }}"><a href="/gallery">Admin</a></li>
            @endif

            @if ($type == 'transparent')
                <li id="auth-logout-btn"><a></a></li>
            @elseif ($type == 'white-hide')
                <li id="auth-logout-btn2"><a></a></li>
            @elseif ($type == 'white-show')
                <li id="auth-logout-btn3"><a></a></li>
            @endif
            <!-- <li><a id="auth-logout-btn"></a><a href="#" onclick="logOut();" class='adc-green-btn'>Log Out</a> -->
        @else
            <li><a @if ($home) onclick='toggleModal()' @else href='/auth/login' @endif class='adc-green-btn'>Log In</a></li>
        @endif
    </ul>
</header>
