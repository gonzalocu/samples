<div class='omw-product-modal-wrapper'>
	<div class='omw-product-modal-content'>
		<div class='omw-product-modal-title'></div>

		<div class='omw-product-modal-description'></div>

		@if ($modal !== true)
			<div class='omw-product-modal-description'>Speak to your friendly Opera Mediaworks contact about what we can do for your brand.</div>
		@endif

		<!-- @if ($modal)
			<a class='omw-product-modal-share-button' target="_blank">SHARE</a>
		@endif -->



		<div class="marvel-device iphone6 silver portrait">
		    <div class="top-bar"></div>
		    <div class="sleep"></div>
		    <div class="volume"></div>
		    <div class="camera"></div>
		    <div class="sensor"></div>
		    <div class="speaker"></div>
		    <div class="screen">
				<iframe class='omw-device-iframe' width='375' height='667' marginheight='0' marginwidth='0' scrolling='no' frameborder="0"></iframe>
		    </div>
		    <div class="home"></div>
		    <div class="bottom-bar"></div>
		</div>
	</div>
</div>

<div class='omw-product-modal-menu omw-product-modal-menu-hide'>
	<div class='omw-product-modal-logo'></div>

	<div class='omw-device-btns'>
		<input id='omw-device-btn-iphone-4' class='omw-device-btn' type='radio' value='iphone4s' name='device' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-iphone-4' message='iPhone 4'></label>

		<input id='omw-device-btn-iphone-5' class='omw-device-btn' type='radio' value='iphone5s' name='device' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-iphone-5' message='iPhone 5'></label>

		<input id='omw-device-btn-iphone-6' class='omw-device-btn' type='radio' value='iphone6' name='device' checked='true' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-iphone-6' message='iPhone 6'></label>

		<input id='omw-device-btn-iphone-6-plus' class='omw-device-btn' type='radio' value='iphone6plus' name='device' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-iphone-6-plus' message='iPhone 6 Plus'></label>

		<input id='omw-device-btn-galaxy-s5' class='omw-device-btn' type='radio' value='s5' name='device' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-galaxy-s5' message='Galaxy S5'></label>

		<input id='omw-device-btn-nexus-5' class='omw-device-btn' type='radio' value='nexus5' name='device' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-nexus-5' message='Nexus 5'></label>
	</div>

	<div class='omw-device-btns'>
		<input id='omw-device-btn-portrait' class='omw-device-btn' type='radio' value='portrait' name='orientation' checked='true' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-portrait' message='Portrait'></label>

		<input id='omw-device-btn-landscape' class='omw-device-btn' type='radio' value='landscape' name='orientation' onclick='toggleAdvertDevice()'>
		<label for='omw-device-btn-landscape' message='Landscape'></label>
	</div>

    @include("layouts/preview_qrcode")

    @if ($modal)
    <div class='omw-device-btns'>
        <a for='omw-device-btn-share' class='omw-device-btn-share omw-product-modal-share-button' message='Share'>Share</a>
    </div>
    @endif
</div>

@if ($modal)
	<div class='omw-product-modal-close' onclick='toggleAdvertModal()'></div>
@else
    <div class="omw-overlay">
        <ul class='omw-preload'>
            <li class='omw-preload-bar'></li>
            <li class='omw-preload-bar'></li>
            <li class='omw-preload-bar'></li>
        </ul>
    </div>

@endif
