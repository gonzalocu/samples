<!DOCTYPE html>
<html>
<head>
    <title>AdColony :: Tools</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="316502315250-qvql7cbbh7mrgaun8q2ctd17uprn1aig.apps.googleusercontent.com">
    <meta name="csrf_token" content="{{ csrf_token() }}" />

	<!-- Icons -->
    <link rel="shortcut icon" type="image/x-icon" href="/assets/images/favicons/favicon.ico" />
    <link rel="apple-touch-icon" href="/assets/images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicons/apple-touch-icon-180x180.png" />

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/bower_components/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    @yield('style')

	<!-- Script -->
    <script src="/js/main/all.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/js/main/ga.js"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @yield('script')
</head>

<body>
    @yield('header')
    <div class='omw-login omw-modal-hidden @if($errors->any()) omw-login-error @endif'>
        <div id="auth-login-btn"></div>
        <a href='/auth/login' class='omw-login-password'>I have been sent a password</a>
    </div>
    <div class='omw-content'>@yield('content')</div>
    @include("layouts/footer")
    <div class="omw-overlay">
        <ul class='omw-preload'>
            <li class='omw-preload-bar'></li>
            <li class='omw-preload-bar'></li>
            <li class='omw-preload-bar'></li>
        </ul>
    </div>
	@yield('script_bottom')
</body>
</html>
