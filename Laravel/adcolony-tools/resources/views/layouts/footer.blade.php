@if (Request::path() == "/" || Request::path() == 'home')
	<?php $home = 1 ?>
@else
	<?php $home = 0 ?>
@endif

<div class="full-footer hidden-md-down">
	<footer class="content-info">
		<div class="container-fluid">
			<div class="row">
    			<div class="col-sm-9">
        			<div class="row top-line">
        				<div class="col-sm-4 flex-xs-bottom">
        					<div class="footer-left-section">
        						<div class="social-title">
        							<div class="footer-title">Connect with us</div>
                                    <div class="footer-message">hello@adcolony.com</div>
        						</div>

        					</div>
        				</div>
        				<div class="col-sm-8">
        					<div class="footer-center-section">

        			      	</div>
        				</div>
        			</div>
        			<div class="row middle-line">
        				<div class="col-sm-4">
        					<div class="footer-left-section">

        	      				<div class="social-icons">
        			                <a href="https://www.facebook.com/AdColony" title="AdColony Facebook" target="_blank">
        			                    <div class="icons fb-icon"></div>
        			                </a>

        			                <a href="https://twitter.com/adcolony" title="Adcolony Twitter" target="_blank">
        			                    <div class="icons tw-icon"></div>
        			                </a>

        			                <a href="https://www.linkedin.com/company/935402" title="AdColony LinkedIn" target="_blank">
        			                    <div class="icons in-icon"></div>
        			                </a>

        			                <a href="https://www.instagram.com/adcolony/" title="AdColony Instagram" target="_blank">
        			                    <div class="icons intg-icon"></div>
        			                </a>

        			            </div>
        					</div>
        				</div>
        				<div class="col-sm-8">
        					<div class="footer-center-section">

        			      	</div>
        				</div>
        			</div>
    			</div>
    			<div class="col-sm-3">
                    @if (App::environment() !== 'production')
                    <div class="footer-lang">
                        <a href="/lang/en">English</a> / <a href="/lang/es">Español</a>
                    </div>
                    @endif
    			</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="footer-left-section">
	      				<a class="footer-logo" href="http://www.adcolony.com/">
	        				<!-- <img src="http://www.adcolony.com/wp-content/themes/adcolonyv3/dist/images/global/adcolony-logo-top@2x.png" alt="AdColony"> -->
	      				</a>
					</div>
				</div>
				<div class="col-md-5">
					<div class="footer-center-section">

			      	</div>
				</div>
				<div class="col-md-4">
					<div class="footer-right-section">
					        <div class="footer-copyright">&copy; <?php echo date("Y"); ?> AdColony. All Rights Reserved.</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>
