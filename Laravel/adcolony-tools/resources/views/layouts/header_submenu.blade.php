
<div class='omw-admin-header'>
    <ul class='omw-admin-navigation'>
        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 || Auth::user()->role[0]->id == 3)
            <li @if(Request::is("feature*") || Request::is("vertical*") || Request::is("category*") || Request::is("ad-sample*") || Request::is("admin/ad-locker*")) class='omw-admin-navigation-active' @endif >
            	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Website<i class="fa fa-caret-down"></i></a>
                <ul class="omw-admin-dropdown-menu">
                    <li><a href="/ad-sample">Ad Sample</a></li>
                    <li><a href="/admin/ad-locker">Ad Locker</a></li>
                </ul>
            </li>
        @endif
        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 || Auth::user()->role[0]->name == "Gallery" and App::environment() !== 'production')
            <li @if(Request::is("gallery*"))class='omw-admin-navigation-active'@endif><a href="/gallery">OM Gallery</a></li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 || Auth::user()->role[0]->id == 3 || Auth::user()->role[0]->id == 10 and Auth::user()->region_id == 2)
            <li @if(Request::is("admin/briefing*")) class='omw-admin-navigation-active' @endif ><a href="/admin/briefing">Briefing</a></li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 || Auth::user()->role[0]->id == 5)
            <li @if(Request::is("qa-tool*"))class='omw-admin-navigation-active'@endif><a href="/qa-tool/">QA Tool</a></li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 || Auth::user()->role[0]->id == 5 )
            <li @if(Request::is("tools*") || Request::is("admin/ad-tools*") || Request::is("tag-generator*") || Request::is("live-project*") || Request::is("preview*"))class='omw-admin-navigation-active'@endif><a href="/tools/">Tools UK</a></li>
            <li @if(Request::is("admin/useful-links*"))class='omw-admin-navigation-active'@endif><a href="/admin/useful-links/">Useful Links</a></li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2)
            <li @if(Request::is("admin/guideline*"))class='omw-admin-navigation-active'@endif><a href="/admin/guideline">Guideline</a></li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 || Auth::user()->role[0]->id == 2 )
            <li @if(Request::is("api/1.0/docs/*"))class='omw-admin-navigation-active' @endif >
            	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">API DOCS<i class="fa fa-caret-down"></i></a>
                <ul class="omw-admin-dropdown-menu">
                    <li><a href="/api/1.0/docs/ad-sample">Ad Sample</a></li>
                    <li><a href="/api/1.0/docs/ad-locker">Ad Locker</a></li>
                    <li><a href="/api/1.0/docs/campaign">Campaign</a></li>
                    <li><a href="/api/1.0/docs/feature">Feature</a></li>
                    <li><a href="/api/1.0/docs/vertical">Vertical</a></li>
                </ul>
            </li>
        @endif

        @if ( Auth::user()->role[0]->id == 1 )
        <li @if(Request::is("user*")) class='omw-admin-navigation-active' @endif >
            	<a href="/api/1.0/docs/campaign" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin<i class="fa fa-caret-down"></i></a>
                <ul class="omw-admin-dropdown-menu">
                    <li><a href="/user">User</a></li>
                </ul>
            </li>
        @endif
    </ul>
</div>
