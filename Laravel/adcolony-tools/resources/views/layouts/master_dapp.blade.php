<!DOCTYPE html>
<html>
<head>
    <title>AdColony Tools</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="316502315250-qvql7cbbh7mrgaun8q2ctd17uprn1aig.apps.googleusercontent.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Icons -->
    <link rel="shortcut icon" href="/assets/images/favicons/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/assets/images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicons/apple-touch-icon-180x180.png" />

    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/bower_components/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/omw/master.css" />
    <link rel="stylesheet" type="text/css" href="/css/omw/omw_dapp.css" />
    @yield('style')

    <!-- Script -->
    <script src="/js/jquery/jquery-2.1.4.min.js"></script>
    <script src="/js/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/js/react/react.js"></script>
    <script src="/js/react/react-dom.js"></script>
    <script src="/bower_components/babel-standalone/babel.min.js"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @yield('script')
</head>
<body>
    <div id='omw-dapp'>@yield('content')</div>
    @yield('script_bottom')
</body>
</html>
