<!-- resources/views/preview/index.blade.php -->

@extends('layouts.master_empty')

@section('style')
    <link rel="stylesheet" type="text/css" href="/css/home/styles.css">
@endsection

@section('script')
    <script src="/js/home/home.preload.js"></script>
@endsection

@section('script_bottom')
	<script src="/js/home/home.modal.js"></script>
	<script src="/js/home/preview.preload.js"></script>
	<script src="/js/main/ga.js"></script>
    <script>
        openAdvertModal('{{ $item->url }}', '', '', '');
    </script>
@endsection

@section('content')
	@include("layouts/preview", array('modal' => false))
@endsection
