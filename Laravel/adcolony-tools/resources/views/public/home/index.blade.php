<!-- resources/views/public/home/index.blade.php -->

@extends('layouts.master')

@section('header')
    @include("layouts/header_mainmenu", array('type' => 'transparent', 'active' => ''))
    @include("layouts/header_mainmenu", array('type' => 'white-hide', 'active' => ''))
@endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="/bower_components/owl/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/owl/owl-carousel/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="/css/home/styles.css">
@endsection

@section('script')
    <script src="/js/home/home.preload.js"></script>
	<script> var count_formats = {{ count($formats) }}; </script>
	<script src="/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
	<script src="/bower_components/owl/owl-carousel/owl.carousel.min.js"></script>
	<script src="/bower_components/jquery.typist/dist/jquery.typist.min.js"></script>
    <script>
        var herotext1 = "{{ trans('public.hero-text1') }}",
            herotext2 = "{{ trans('public.hero-text2') }}",
            herotext3 = "{{ trans('public.hero-text3') }}",
            herotext4 = "{{ trans('public.hero-text4') }}";
    </script>
@endsection

@section('script_bottom')
    <script src="/js/main/omw.js"></script>
	<script src="/js/home/home.overlay.js"></script>
	<script src="/js/home/home.modal.js"></script>
	<script src="/js/home/home.svg.js"></script>
	<script src="/js/home/home.carousel.js"></script>
	<script src="/js/home/home.isotope.js"></script>
	<script src="/js/home/home.typist.js"></script>
	<script src="/js/home/home.scroll.js"></script>
@endsection

@section('content')

	<div class='omw-hero-image noselect'>
        <div class='hero-logo'></div>

		<div class='omw-hero-text'>
			<svg class='omw-hero-text-underline' version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-51 154.8 152 25.9" style="enable-background:new -51 154.8 152 25.9;" xml:space="preserve">
				<path class='omw-hero-text-underline-path' d="M99,159.9c-11.8-1.9-126-3.7-147-0.3c24.3-1.4,112.1,2.1,139.8,7.8c3.3,0.8-97.8-6.9-114.2-1.2c24.4-3.1,86.2,3.3,92.1,4.3"/>
			</svg>
		</div>

		<div class='omw-hero-more adc-green-btn' onclick='scrollToElement(".omw-about")'>{{ trans('public.tell-me-more-btn') }}</div>

		<a name='omw-about'></a>
	</div>

	<div class='omw-about'>
		<div class='omw-about-title'>{{ trans('public.about-us-title') }}</div>
		<div class='omw-about-subtitle'>{{ trans('public.about-us-message') }}</div>

	    <div class='omw-flex-about-wrapper'>
			 <div class='omw-flex omw-flex-about'></div>
		</div>

        <div class='omw-about-arrow-down' onclick='scrollToElement(".omw-products")'>
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			viewBox="116.5 208.2 105 104.8" enable-background="new 116.5 208.2 105 104.8" xml:space="preserve">
				<g>
					<polygon points="169,277.7 140.1,248.7 138.2,250.6 169,281.4 199.8,250.6 197.9,248.7 "/>
					<circle fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="10" cx="169" cy="260.6" r="50"/>
				</g>
			</svg>
		</div>

		<a name='omw-products'></a>
	</div>

	<div class='omw-products'>
		<div class='omw-products-header'>
			<div class='omw-products-title'>{{ trans('public.sample-title') }}</div>
			<div class='omw-products-subtitle'>{{ trans('public.sample-message') }}</div>

			<form class='omw-products-filter'>
				<div class='omw-products-filter-title'></div>
					<input id='omw-products-filter-all' class='omw-products-filter-feature' type='radio' value='all' name='feature' checked='true' data-filter='*'>
					<label for='omw-products-filter-all' title='All'></label>

					@foreach ($categories as $category)
						<input id='omw-products-filter-{{ $category->slug }}' class='omw-products-filter-feature' type='radio' value='{{ $category->slug }}' name='feature' data-filter='.{{ $category->slug }}'>
						<label for='omw-products-filter-{{ $category->slug }}' title='{{ $category->name }}'></label>
					@endforeach
			</form>
            <div style="clear:both;"></div>
		</div>
        <div style="clear:both;"></div>
        <div class='omw-products-wrap'>
    		<div class='omw-flex omw-flex-products'>
    			@foreach ($formats as $format)
    				<div class='omw-product {{ $format->category->slug }}' data-category='{{ $format->category->slug }}'>
    					<div id="owl-carousel-{{ $loop->index }}" class="owl-carousel omw-product-images">
    						@foreach ($format->assets as $asset)
    							<img class='omw-product-image lazyOwl' data-src='{{ $asset->url }}' style='width: 320px;'></img>
    						@endforeach
    						@if (!empty($format->url()))
    						<div class='omw-product-image' style='background-image:url(data:image/png;base64,{{ base64_encode(QrCode::format('png')->merge('/public/assets/images/basic/qr-logo-overlay.png', .15)->size(230)->generate($format->url->url)) }}); width:320px;'><p class="omw-product-qr">Scan me with your QR reader</p></div>
    						@endif
    					</div>
    					<div class='omw-product-info'>
    						<div class='omw-product-name'>{{ $format->name }}</div>
    						<a class='omw-product-link adc-green-btn' onclick="toggleAdvertModal('{{ $format->url->url }}', '{{ $format->url->slug }}', '{{ $format->name}}', '{{ $format->description }}')">Demo &blacktriangleright;</a>
    					</div>
    				</div>
    			@endforeach
    		</div>
        <div class='omw-made-with'><div class='omw-about-subtitle'><div class='omw-heart-message'>{{ trans('public.made-with-message1') }}<i class='fa fa-heart'></i>{{ trans('public.made-with-message2') }}</div></div></div>
		</div>

	</div>
    @include("layouts/preview", array('modal' => true))
@endsection
