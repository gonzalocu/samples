<!-- resources/views/public/home/ad-locker.blade.php -->

@extends('layouts.master')

@section('header')
    @include("layouts/header_mainmenu", array('type' => 'white-show', 'active' => 'ad-locker'))
@endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="/js/autosuggest/css/autosuggest_clean.css" />
    <link rel="stylesheet" type="text/css" href="/css/home/styles.css">
@endsection

@section('script')
    <script src="/js/home/home.preload.js"></script>
@endsection

@section('script_bottom')
    <script src="/js/autosuggest/js/bsn.AutoSuggest_c_2.0.js"></script>
	<script src="/js/home/home.adlocker.js"></script>
	<script src="/js/home/home.modal.js"></script>
@endsection

@section('content')
	<div class='omw-about'>
		<div class='omw-flex-about-wrapper'>
			<div class='omw-flex omw-flex-adlocker' style="height: 75vh;">
				<div class="omw-adlocker-box">
					<div class='omw-adlocker-title'>Ad Locker</div>
					<div class='omw-adlocker-subtitle'>Show-and-tell for our mobile ads</div>
					<div class="omw-search-filter dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span id="omw-search-text">SEARCH CAMPAIGN NAMES</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li id='elem1'><a href="#">SEARCH CAMPAIGN NAMES</a></li>
                            <li id='elem2'><a href="#">SEARCH AD FEATURES</a></li>
                            <li id='elem3'><a href="#">SEARCH INDUSTRIAL VERTICALS</a></li>
                        </ul>
                    </div>
					<input type='text' id='omw-adlocker-search-input' class='omw-adlocker-search-input'  />
				</div>
			</div>
		</div>
	</div>
	@include("layouts/preview", array('modal' => true))
@endsection
