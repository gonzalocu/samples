<!-- resources/views/admin/vertical/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
    <div class='omw-table-wrapper'>

        </br>

	    {!! Form::open(array('url'=>'/vertical', 'method'=>'POST')) !!}
            {!! Form::text('fname', '', array('id'=>'fname','class'=>'form-control ', 'placeholder'=>'Name', 'require' => '')) !!}
            {!! Form::submit('Create Vertical', array('class'=>'')) !!}
	    {!! Form::close() !!}

        @include("layouts/notification")

	    <table>
		    <thead>
			    <tr>
			        <th>Name</th>
			        <th></th>
			    </tr>
		    </thead>
		    <tbody>
		    @foreach ($collection as $element)
			    <tr>
			        <td>{{ $element->name }}</td>
			        <td>
			            {!! Form::delete('/vertical/'.$element->id, 'Delete', array(),  array('class'=>'')) !!}
			        </td>
			    </tr>
		    @endforeach
		    </tbody>
	    </table>

	</div>

@endsection
