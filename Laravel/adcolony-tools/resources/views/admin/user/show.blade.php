<!-- resources/views/admin/user/show.blade.php -->

@extends('layouts.master_admin')

@section('content')

    <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="/{{ Request::segment(1) }}">Back</a>

		@include("layouts/notification")

	    <h3>Users</h3><br/>
	    <table class="table">
	    <thead>
	    <tr>
	        <th>Name</th>
	        <th>Description</th>
	        <th>Create at</th>
	        <th>Update at</th>
	        <th></th>
	    </tr>
	    </thead>
	    <tbody>
	    <tr>
	        <td>{{ $element->name }}</td>
	        <td>{{ $element->email }}</td>
	        <td>{{ $element->created_at->format('d/m/Y') }}</td>
	        <td>{{ $element->updated_at->format('d/m/Y') }}</td>
	        <td>
	            <a href="/user/{{ $element->id }}/edit" >Edit</a>
	        </td>
	        </tr>
	    </tbody>
	    </table>

	    <table class="table">
	    <thead>
	        <tr>
	            <th>Roles</th>
                <th>Region</th>
            </tr>
	    </thead>
	    <tbody>
	        <tr>
	            <td>{{ $element->role[0]['name'] }}</td>
	            <td> @if ($element->region)  {{ $element->region->name }} @endif </td>
	        </tr>
	    </tbody>
	    </table>
	</div>

@endsection
