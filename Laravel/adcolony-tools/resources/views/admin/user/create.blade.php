<!-- resources/views/admin/user/create.blade.php -->

@extends('layouts.master_admin')

@section('content')

    <div class='omw-table-wrapper'>
	    <a class='omw-admin-btn' href="/{{ Request::segment(1) }}">Back</a>

	    @include("layouts/notification")

	    {!! Form::open(array('url'=>'/'.Request::segment(1), 'method'=>'POST')) !!}
        {!! Form::label('name','Name') !!}
	    {!! Form::text('name', '', array('class'=>'form-control', 'placeholder'=>'')) !!}

	    {!! Form::label('email','Email') !!}
	    {!! Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'')) !!}

	    {!! Form::label('password','Password &nbsp; &nbsp; (Minimum 8 characters)') !!}
	    {!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'')) !!}

	    {!! Form::label('password_confirmation','Confirm Pasword') !!}
	    {!! Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'')) !!}

	    {!! Form::label('region','Region') !!}
	    {!! Form::select('region', Region::pluck('name', 'id'), NULL) !!}

        {!! Form::label('role','Roles') !!} <br/>
        <table>
            <thead>
            <tr>
                @foreach (Role::all() as $role)
        	        <th>{{ $role->name }}</th>
                @endforeach
	        </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach (Role::all() as $role)
                        <td>{!! Form::radio('role', $role->id) !!}</td>
                    @endforeach
                </tr>
            </tbody>
        </table>
        </br>

	    {!! Form::submit('Save') !!}
	    {!! Form::close() !!}

	</div>

@endsection
