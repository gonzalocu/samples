<!-- resources/views/admin/user/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
    <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="{{ Request::url() }}/create">Create User</a>

		@include("layouts/notification")

	    <table>
	    <thead>
	    <tr>
	        <th>Name</th>
	        <th>Email</th>
	        <th>Create at</th>
	        <th></th>
	    </tr>
	    </thead>
	    <tbody>

	    @foreach ($collection as $element)
	    <tr>
	        <td>{{ $element->name }}</td>
	        <td>{{ $element->email }}</td>
	        <td>{{ $element->created_at->format('d/m/Y') }}</td>
	        <td>
	            <a href="{{ Request::url() }}/{{ $element->id }}">View</a>
	        </td>
	        </tr>
	    @endforeach
	    </tbody>
	    </table>

    </div>
@endsection
