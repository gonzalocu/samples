<!-- resources/views/admin/user/edit.blade.php -->

@extends('layouts.master_admin')

@section('content')

    <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

		@include("layouts/notification")

	    {!! Form::open(array('url'=>'/user/'.$element->id, 'method'=>'PUT')) !!}
	    {!! Form::hidden('id', $element->id) !!}

	    {!! Form::label('name','Name') !!}
	    {!! Form::text('name', $element->name, array('class'=>'form-control', 'placeholder'=>'Name')) !!}

	    {!! Form::label('email','Email') !!}
	    {!! Form::text('email', $element->email, array('class'=>'form-control', 'placeholder'=>'Email')) !!}

	    {!! Form::label('password','Password &nbsp; &nbsp; (Minimum 8 characters)') !!}
	    {!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Same Password')) !!}

	    {!! Form::label('region','Region') !!}
	    {!! Form::select('region', Region::pluck('name', 'id'), $element->region->id) !!}

        {!! Form::label('role','Roles') !!}
        <table>
        <thead>
        <tr>
            @foreach ($roles as $role)
    	        <th>{{ $role->name }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($roles as $role)
                    <td>{!! Form::radio('role', $role->id, ($role->name == $element->role[0]['name']?true:false)) !!}</td>
                @endforeach
            </tr>
        </tbody>
        </table>
        </br>

        <div class="pull-left">
            {!! Form::submit('Save', array('class'=>'btn btn-primary ')) !!}
        </div>
	    {!! Form::close() !!}

	    <div class="pull-right">
            {!! Form::delete('/user/'.$element->id, 'Delete', array(),  array('class'=>'btn btn-danger ')) !!}
	    </div>

	</div>

@endsection
