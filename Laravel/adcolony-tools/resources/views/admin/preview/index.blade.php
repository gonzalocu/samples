<!-- resources/views/admin/preview/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
	<div class="omw-table-wrapper">
		@include("admin/tools/side")
		<div id='omw-tools-content'>
            <div class="panel panel-default">
				<div class="panel-heading">
					<h2 class="panel-title">Preview Page</h2>
				</div>
				<div class="panel-body">
		        {!! Form::open(array('url'=>'/preview', 'method'=>'POST')) !!}
					<div style="width:77%;float:left;">
		            	{!! Form::text('url', '', array('id'=>'url', 'class'=>'form-control', 'placeholder'=>'Url', 'require' => '')) !!}
					</div>
					<div id="btn-create-url" style="width:190px;float:right;">
						{!! Form::submit('Create Url') !!}
					</div>
                    <style>
                        #btn-create-url input {
                            margin:10px;
                        }
                    </style>
		        {!! Form::close() !!}

		    	@include("layouts/notification")

		        <table>
			    <thead>
			    <tr>
			        <th>Slug</th>
			        <th>Url</th>
			        <th><center>Admin</center></th>
			        <th></th>
			    </tr>
			    </thead>
			    <tbody>
			    @foreach (Url::all() as $item)
			    <tr>
			        <td><a href="http://preview.operamediaworks.tools/{{ $item->slug }}" class="nohover" target="_blank">{{ $item['slug'] }}</a></td>
			        <td><a href="{{ $item->url }}" class="nohover" target="_blank">{{ $item['url'] }}</a></td>
			        <td>
			            @if ( $l = AdLocker::where('url_id', $item['id'])->first() )
			              <a href="/admin/ad-locker/{{ $l->id }}" target="_blank" style="min-width:100px;display:block;text-align:center;">Ad Locker</a>
			            @endif
			            @if ( $s = Format::where('url_id', $item['id'])->first() )
			              <a href="/ad-format/{{ $s->id }}" target="_blank" style="min-width:100px;display:block;text-align:center;">Ad Sample</a>
			            @endif
			        </td>
			        <td>
			            {!! Form::delete('/preview/'.$item->id, 'Delete', array()) !!}
			        </td>
			    </tr>
			    @endforeach
			    </tbody>
			    </table>
			</div>
            </div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
