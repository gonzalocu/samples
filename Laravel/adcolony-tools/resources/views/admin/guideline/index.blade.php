<!-- resources/views/admin/guideline/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
    <script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
    <script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
    <script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/js/admin/admin.guideline.js"></script>
@endsection

@section('content')
<style>
    .grid_style {
        text-align: center;
    }

    .grid_style .row {
        background-color: #040548;
        margin:10px 0;
    }

    .grid_style .col {
    }

    .grid_style .col span {
        background-color: #123e7d;
        display: block;
        color: white;

        text-align: center;
        text-transform: uppercase;
        vertical-align: middle;
        min-height: 120px;
        line-height: 120px;
    }
</style>

<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Guideline</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right"></div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <h6>Grid System</h6>
        </div>
    </div>

    <div class="grid_style">
        <div class='row'>
            <div class="col col-12"><span>col1</span></div>
        </div>

        <div class='row'>
            <div class="col col-sm-6"><span>col1</span></div>
            <div class="col col-sm-6"><span>col2</span></div>
        </div>

        <div class='row'>
            <div class="col col-sm-8"><span>col1</span></div>
            <div class="col col-sm-4"><span>col2</span></div>
        </div>

        <div class='row'>
            <div class="col col-sm-4"><span>col1</span></div>
            <div class="col col-sm-8"><span>col2</span></div>
        </div>

        <div class='row'>
            <div class="col col-sm-4"><span>col1</span></div>
            <div class="col col-sm-4"><span>col2</span></div>
            <div class="col col-sm-4"><span>col3</span></div>
        </div>
    </div>
    <br/>
    <div class='row'>
        <div class="col-md-3">
            <h6>Paragraph</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet nibh eu augue mollis, at fringilla massa cursus. Phasellus semper nisi at neque vestibulum facilisis. Mauris eget arcu fringilla, pharetra elit vel, ultrices justo. Praesent laoreet ultricies odio, id pretium enim dictum ut.</p>
            <p>Etiam imperdiet est et ipsum convallis, ac rutrum velit iaculis. Fusce at est ipsum. Donec erat ligula, maximus a metus ac, gravida porta sem. Aliquam nunc augue, pretium sit amet risus ut, aliquam commodo dolor. Aenean consectetur at leo a dapibus. Nullam maximus nibh arcu, at ultricies augue at.</p>
        </div>
        <div class="col-md-3">
            <h6>Headers</h6>
            <h1>Header 1</h1>
            <h2>Header 2</h2>
            <h3>Header 3</h3>
            <h4>Header 4</h4>
            <h5>Header 5</h5>
            <h6>Header 6</h6>
        </div>
        <div class="col-md-3">
            <h6>Custom classes</h6>
        </div>
        <div class="col-md-3">
            <h6>Links</h6>
            <a href="#">Sample Link</a>
        </div>
    </div>
    <h6>Buttons</h6>
    <div class='row'>
        <div class="col-md-12">
            <a href="#" class="adc-green-btn">Button</a>
            <a class="adc-green-btn" href="#"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; New</a>
            <a class="adc-green-btn" href="#"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</a>
            <a class="adc-green-btn" href="#"><i class="fa fa-pencil"></i>&nbsp;&nbsp; Edit</a>
            <a class="adc-green-btn" href="#"><i class="fa fa-rocket"></i> Submit</a>
            <a class='adc-green-btn' href="#"><i class="fa fa-binoculars" aria-hidden="true"></i>&nbsp;&nbsp; Compare</a>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4">
            <div id="toggle1"></div>
        </div>
        <div class="col-md-4">
            <div id="toggle2"></div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4">
            <div id="textfield1"></div>
        </div>
        <div class="col-md-4">
            <div id="textarea1"></div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4">
            <h6>Select (Old Style)</h6>
            <div class="styled-select slate">
                <select name="type">
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="2">Option 3</option>
                    <option value="2">Option 4</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div id="select1"></div>
        </div>
        <div class="col-md-4">
            <div id="select2"></div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4">
            <div id="date1"></div>
        </div>
    </div>

    <div class='row'>
        <div class="col-md-12">
            <h6>Table</h6>
            <table class='table-admin'>
            <thead>
                <tr>
                    <th class="col-md-6">Name</th>
                    <th class="col-md-2">Date</th>
                    <th class="col-md-2">Visible</th>
                    <th class="col-md-2">Details</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="4"></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td>Name 1</td>
                    <td>dd/mm/yy</td>
                    <td><div class="fa fa-check"></div></td>
                    <td><a class="adc-green-btn" href="#">View</a></td>
                </tr>
                <tr>
                    <td>Name 2</td>
                    <td>dd/mm/yy</td>
                    <td><div class='fa fa-times'></div></td>
                    <td><a class="adc-green-btn" href="#">View</a></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
