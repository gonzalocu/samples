<!-- resources/views/admin/campaignt/show.blade.php -->

@extends('layouts.master_admin')

@section('content')
    <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

		@include("layouts/notification")

        <h3>Campaign</h3><br/>
        <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Platform Tested</th>
            <th>Publisher Tested</th>
            <th>Can Go Live</th>
    		<th>Impression Tracking</th>
    		<th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $collection->name }}</td>
            <td>{{ $collection->platform_tested }}</td>
            <td>{{ $collection->publishers_tested }}</td>
            <td>{{ $collection->can_go_live }}</td>
            <td>{{ $collection->impression_tracking }}</td>
            <td>
                <a href="/tag-generator/campaign/{{ $collection->id }}/edit"> Edit</a>
            </td>
            </tr>
        </tbody>
        </table>

        <table class="table">
        <thead>
        <tr>
            <th>Create at</th>
            <th>Update at</th>
            <th>Live Date</th>
            <th>End Date</th>
    		<th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $collection->created_at->format('d/m/Y') }}</td>
            <td>{{ $collection->updated_at->format('d/m/Y') }}</td>
            <td>{{ $collection->live_date }}</td>
            <td>{{ $collection->end_date }}</td>
            <td></td>
        </tr>
        </tbody>
        </table>

        <table class="table">
        <thead>
        <tr>
            <th>Banners</th>
    		<th></th>
        </tr>
        </thead>
        <tbody>
    	@foreach ($collection->banners as $banner)
        <tr>
            <td>{{ $banner->name }}</td>
            <td><a href="/tag-generator/campaign/{{ $collection->id }}/banner/{{ $banner->id }}"> View</a></td>
        </tr>
    	@endforeach
        </tbody>
        </table>
    </div>
@endsection
