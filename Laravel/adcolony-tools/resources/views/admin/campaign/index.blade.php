<!-- resources/views/admin/campaign/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
    <link rel="stylesheet" type="text/css" href="/js/autosuggest/css/autosuggest_inquisitor.css" />
@endsection

@section('script')
    <script src="/js/autosuggest/js/bsn.AutoSuggest_c_2.0.js"></script>
    <script src="/js/admin/admin.campaign.search.js"></script>
@endsection

@section('content')
    <style>
        .omw-view-search {
            position: absolute;
            width:0px;
            background-color:#4d4d4d;
            height:100%;
            float:left;
            left:-5%;

        }

        .omw-table-wrapper {

              -webkit-transition-duration: 0.3s; /* Safari */
    transition-duration: 0.3s;

        }

        .button {
            position: absolute;
            top:0px;
            left:0px;
            background-image:url('');
        }


        @-webkit-keyframes bounceInLeft {
  from, 60%, 75%, 90%, to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
    animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(-3000px, 0, 0);
    transform: translate3d(-3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(25px, 0, 0);
    transform: translate3d(25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(5px, 0, 0);
    transform: translate3d(5px, 0, 0);
  }

  to {
    -webkit-transform: none;
    transform: none;
  }
}

@keyframes bounceInLeft {
  from, 60%, 75%, 90%, to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
    animation-timing-function: cubic-bezier(0.215, 0.610, 0.355, 1.000);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(-3000px, 0, 0);
    transform: translate3d(-3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(25px, 0, 0);
    transform: translate3d(25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(5px, 0, 0);
    transform: translate3d(5px, 0, 0);
  }

  to {
    -webkit-transform: none;
    transform: none;
  }
}

.bounceInLeft {
  -webkit-animation-name: bounceInLeft;
  animation-name: bounceInLeft;
}


@-webkit-keyframes bounceOutLeft {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

@keyframes bounceOutLeft {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

.bounceOutLeft {
  -webkit-animation-name: bounceOutLeft;
  animation-name: bounceOutLeft;
}
    </style>
    <script>
        $( document ).ready(function() {


            $( ".button" ).click(function() {
                if ($('.omw-view-search').css('width') != '0px') {
                    $('.omw-view-search').css('-webkit-animation', "bounceOutLeft 1s 1");
                    $('.omw-view-search').css('animation', "bounceOutLeft 1s 1");

                    setTimeout(function(){

                        $('.omw-table-wrapper').css('width','100%');
                        $('.omw-table-wrapper').css('left','0%');
                    }, 300)

                    setTimeout(function(){
                        $('.omw-view-search').css('width','0%');
                    }, 800)

                } else {
                    $('.omw-view-search').css('-webkit-animation', "bounceInLeft 1s 1");
                    $('.omw-view-search').css('animation', "bounceInLeft 1s 1");
                    $('.omw-view-search').css('width','17%');
                    $('.omw-table-wrapper').css('width','90%');
                    $('.omw-table-wrapper').css('left','10%');
                    //$('.omw-view-search').style.WebkitAnimation = "bounceInLeft 4s 2";

                    //$('.omw-view-search').style.animation = "bounceInLeft 4s 2";
                }

            });
/*
        $( ".button" ).hover(function() {

                    $('.omw-view-search').css('-webkit-animation', "bounceInLeft 1s 1");
            $('.omw-view-search').css('animation', "bounceInLeft 1s 1");
            $('.omw-view-search').css('width','17%');
            $('.omw-table-wrapper').css('width','90%');
            $('.omw-table-wrapper').css('left','5%');

        }, function(){

             $('.omw-view-search').css('-webkit-animation', "bounceOutLeft 1s 1");
            $('.omw-view-search').css('animation', "bounceOutLeft 1s 1");

            setTimeout(function(){

                $('.omw-table-wrapper').css('width','100%');
                $('.omw-table-wrapper').css('left','0%');
            }, 300);

            setTimeout(function(){
                $('.omw-view-search').css('width','0%');
            }, 800);




        });
*/

        });
    </script>

    <div class="omw-view-search"></div>
    <div class='omw-table-wrapper'>

        {{-- <a class="button">Search</a> --}}
        <a class='omw-admin-btn' href="{{ Request::url() }}/create">Create A Campaign</a>

        {{-- <div class="form-group">
            {!! Form::text('text', '', array('id'=>'search-input','class'=>'form-control', 'placeholder' => 'Search')) !!}
        </div> --}}


        <div class="omw-tab-content tab-content">
            <ul class='omw-tabs'>
                <li role="presentation" class="@if ($section == 'watching') active @endif"><a data-toggle="tab" href="#omw-tab-watching">Watching</a></li>
                <li role="presentation" class="@if ($section == 'all') active @endif"><a data-toggle="tab" href="#omw-tab-all">All</a></li>
            </ul>

            <div id="omw-tab-watching" class="tab-pane @if ($section == 'watching') in active @endif">
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Banners</th>
                        <th style="width:170px;"></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach (Auth::user()->watching as $campaign)
                    <tr>
                        <td>{{ $campaign->name }}</td>
                        <td>
                            @foreach ($banners as $banner)
                                @if ( $banner->campaign_id == $campaign->id )
                                    <a href="/tag-generator/campaign/{{ $campaign->id }}/banner/{{ $banner->id }}">{{ $banner->name }}</a>
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <a href="/tag-generator/campaign/{{ $campaign->id }}">View</a>
                            @if (count($campaign->watching) == 0)
                                <a href="/tag-generator/campaign/{{ $campaign->name }}/watching">Watch</a>
                            @else
                                <a href="/tag-generator/campaign/{{ $campaign->name }}/unwatching">Unwatch</a>

                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div id="omw-tab-all" class="tab-pane @if ($section == 'all') in active @endif">
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Banners</th>
                        <th style="width:170px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($campaigns as $campaign)
                    <tr>
                        <td>{{ $campaign->name }}</td>
                        <td>
                        @foreach ($banners as $banner)
                            @if ( $banner->campaign_id == $campaign->id )
                                <a href="/tag-generator/campaign/{{ $campaign->id }}/banner/{{ $banner->id }}">{{ $banner->name }}</a>
                            @endif

                        @endforeach
                        </td>
                        <td>
                            <a href="/tag-generator/campaign/{{ $campaign->id }}">View</a>
                            @if (count($campaign->watching) == 0)
                                <a href="/tag-generator/campaign/{{ $campaign->name }}/watching">Watch</a>
                            @else
                                <a href="/tag-generator/campaign/{{ $campaign->name }}/unwatching">Unwatch</a>
                            @endif
                            <br/>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
