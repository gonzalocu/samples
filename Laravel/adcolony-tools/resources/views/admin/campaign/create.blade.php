<!-- resources/views/admin/campaign/create.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')
    <script type="text/javascript" charset="utf-8" src="/js/admin/campaign.create.js"></script>
@endsection

@section('content')
    <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

		@include("layouts/notification")

        {!! Form::open(array('url'=> "/" . Request::segment(1), 'method'=>'POST', 'files'=>true)) !!}

        {{-- PROJECT ID --}}
        {!! Form::label('project_id','Project Id') !!}
        {!! Form::text('project_id', NULL, array('class'=>'form-control-half')) !!}

        {{-- NAME --}}
        {!! Form::label('name','Name') !!}
        {!! Form::text('name', NULL, array('class'=>'form-control')) !!}

        {{-- TITLE --}}
        {!! Form::label('title','Title') !!}
        {!! Form::text('title', NULL, array('class'=>'form-control')) !!}

        {{-- PLATFORM TESTED --}}
        {!! Form::label('platform_tested','Platform Tested') !!}
        {!! Form::text('platform_tested', NULL, array('class'=>'form-control')) !!}

        {{-- PUBLISHER TESTED --}}
        {!! Form::label('publisher_tested','Publisher Tested') !!}
        {!! Form::text('publisher_tested', NULL, array('class'=>'form-control')) !!}

        {{-- CAN GO LIVE --}}
        {!! Form::label('can_go_live','Can Go Live') !!}
        {!! Form::text('can_go_live', NULL, array('class'=>'form-control')) !!}

        {{-- IMPRESSION TRACKING --}}
        {!! Form::label('impression_tracking','Impression Tracking') !!}
        {!! Form::text('impression_tracking', NULL, array('class'=>'form-control')) !!}

        {{-- NOTES --}}
        {!! Form::label('notes','Notes') !!}
        {!! Form::textarea('notes', '',array('class'=>'form-control')) !!}

        {!! Form::submit('Create', array('class'=>'omw-admin-btn')) !!}
        {!! Form::close() !!}
    </div>

@endsection
