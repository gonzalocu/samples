<!-- resources/views/admin/campaign/edit.blade.php -->

@extends('layouts.master_admin')

@section('script')
    <script type="text/javascript" charset="utf-8" src="/js/admin/admin.campaign.edit.js"></script>
    <script>BannerApp.constant("CSRF_TOKEN", '{{ csrf_token() }}');</script>
@endsection

@section('content')
  <div class='omw-table-wrapper'>
    <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

    @include("layouts/notification")

    {!! Form::open(array('url'=> "/" . Request::segment(1), 'method'=>'POST', 'files'=>true)) !!}

      {{-- PROJECT ID --}}
      {!! Form::label('project_id','Project Id') !!}
      {!! Form::text('project_id', $collection->project_id, array('class'=>'form-control-half')) !!}

      {{-- NAME --}}
      {!! Form::label('name','Name') !!}
      {!! Form::text('name', $collection->name, array('class'=>'form-control')) !!}

      {{-- TITLE --}}
      {!! Form::label('title','Title') !!}
      {!! Form::text('title', $collection->title, array('class'=>'form-control')) !!}

      {{-- PLATFORM TESTED --}}
      {!! Form::label('platform_tested','Platform Tested') !!}
      {!! Form::text('platform_tested', $collection->platform_tested, array('class'=>'form-control')) !!}

      {{-- PUBLISHER TESTED --}}
      {!! Form::label('publisher_tested','Publisher Tested') !!}
      {!! Form::text('publisher_tested', $collection->publishers_tested, array('class'=>'form-control')) !!}

      {{-- CAN GO LIVE --}}
      {!! Form::label('can_go_live','Can Go Live') !!}
      {!! Form::text('can_go_live', $collection->can_go_live , array('class'=>'form-control')) !!}

      {{-- IMPRESSION TRACKING --}}
      {!! Form::label('impression_tracking','Impression Tracking') !!}
      {!! Form::text('impression_tracking', $collection->impression_tracking, array('class'=>'form-control')) !!}

      {{-- NOTES --}}
      {!! Form::label('notes','Notes') !!}
      {!! Form::textarea('notes', '',array('class'=>'form-control')) !!}

      {!! Form::submit('Save', array('class'=>'omw-admin-btn')) !!}
      {!! Form::close() !!}

      <br><br>
      <div ng-app="bannerApp" ng-controller="bannerController">
        <h1>Banners</h1>

        <br>

        <p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

        <div class="comment" ng-hide="loading" ng-repeat="comment in comments">
          <h3>Comment #<% comment.name %> <small>by <% comment.slug %></h3>
          <p><% comment.text %></p>

          <p><a href="#" ng-click="deleteComment(comment.id)" class="text-muted">Delete</a></p>
        </div>

        <form ng-submit="submitComment()">

          {{-- BANNER TYPE --}}
          {!! Form::label('banner_name','Banner Name') !!}
          <input type="text" class="form-control input-sm" name="author" ng-model="commentData.name" placeholder="Name">

          {{-- BANNER TYPE --}}
          {!! Form::label('banner_type','Banner Type') !!}
          {!! Form::select('banner_type', DB::table('om_banner_type')->pluck('name', 'id')) !!}

          <label for="publisher_tested">Tag</label>
          <input type="file" class="form-control input-lg" name="comment" ng-model="commentData.text" >

          <button type="submit" class="omw-admin-btn">Save</button>
        </form>
      </div>
   </div>
@endsection
