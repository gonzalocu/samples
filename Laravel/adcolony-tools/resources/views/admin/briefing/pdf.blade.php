<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Project Briefing Sheet</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                font-size: 12px;
            }

            #header {
                font-size: 20px;
                font-weight: bold;
                height: 50px;
                line-height: 70px;
                margin-bottom: 30px;
                position: relative;
                text-align: center;
                text-transform: uppercase;
                width: 100%;
            }

            #logo {
                background: transparent url('/assets/images/basic-adc/adcolony-logo-black-pdf.png') center left no-repeat;
                background-size: 100px auto;
                height: 21px;
                position: relative;
                width: 100px;
            }

            .line {
                border-top: 1px solid #f0f0f0;
                clear: both;
                display: block;
                min-height: 40px;
                padding: 10px 0;
                position: relative;
            }

            .line label {
                display: inline-block;
                font-weight: bold;
                position: relative;
                text-transform: uppercase;
                width: 30%;
            }

            .line .line-item {
                display: inline-block;
                float: right;
                position: relative;
                right: 0;
                text-align: justify;
                width: 69%;
            }
        </style>
    </head>
    <body>
        <div id='logo'></div>
        <div id='header'>Project Briefing Sheet</div>
        @if ($item->name)
            <div class='line'>
                <label>Project Name</label>
                <div class='line-item'>{{ $item->name }}</div>
            </div>
        @endif
        @if ($item->salesforce_id)
            <div class='line'>
                <label>Salesforce ID</label>
                <div class='line-item'>{{ $item->salesforce_id }}</div>
            </div>
        @endif
        @if ($item->type)
            <div class='line'>
                <label>Type of Work</label>
                <div class='line-item'>{{ $item->type->name }}</div>
            </div>
        @endif
        @if ($item->campaign_budget)
            <div class='line'>
                <label>Campaign Budget</label>
                <div class='line-item'>{{ $item->campaign_budget }}</div>
            </div>
        @endif
        @if ($item->vertical)
            <div class='line'>
                <label>Vertical</label>
                <div class='line-item'>{{ $item->vertical->name }}</div>
            </div>
        @endif
        @if ($item->agency)
            <div class='line'>
                <label>Media Agency</label>
                <div class='line-item'>{{ $item->agency->name }}</div>
            </div>
        @endif
        @if ($item->due_date)
            <div class='line'>
                <label>Creative Due Date</label>
                <div class='line-item'>{{ $item->due_date->format('d/m/Y') }}</div>
            </div>
        @endif
        @if ($item->live_date)
            <div class='line'>
                <label>IO Live Date</label>
                <div class='line-item'>{{ $item->live_date->format('d/m/Y') }}</div>
            </div>
        @endif
        @if ($item->full_creative_briefing)
            <div class='line'>
                <label>Full Creative Briefing</label>
                <div class='line-item'>{!! nl2br($item->full_creative_briefing) !!}</div>
            </div>
        @endif
        @if ($item->campaign_website)
            <div class='line'>
                <label>Campaign Website</label>
                <div class='line-item'>{{ $item->campaign_website }}</div>
            </div>
        @endif

        @if ($item->kpi)
            <div class='line'>
                <label>KPI</label>
                <div class='line-item'>{{ $item->kpi }}</div>
            </div>
        @endif

        @if ($item->language)
            <div class='line'>
                <label>Multiple Languages</label>
                <div class='line-item'>{{ $item->language }}</div>
            </div>
        @endif

        @if ($item->celtra_build)
            <div class='line'>
                <label>Celtra Build</label>
                <div class='line-item'>{{ $item->celtra_build }}</div>
            </div>
        @endif

        @if (count($item->format))
            <div class='line'>
                <label>Format Requested</label>
                <div class='line-item'>
                    @foreach ($item->format as $i)
                        @if ($i->slug == 'other')
                            {{ $i->pivot->briefing_other }}<br/>
                        @else
                            {{ $i->name }}<br/>
                        @endif
                    @endforeach
                </div>
            </div>
        @endif
        @if (count($item->platform))
            <div class='line'>
                <label>Platform</label>
                <div class='line-item'>
                    @foreach ($item->platform as $i)
                        @if ($i->slug == 'other')
                            {{ $i->pivot->briefing_other }}<br/>
                        @else
                            {{ $i->name }}<br/>
                        @endif
                    @endforeach
                </div>
            </div>
        @endif
        @if (count($item->device))
            <div class='line'>
                <label>Device</label>
                <div class='line-item'>
                    @foreach ($item->device as $i)
                        {{ $i->name }}<br/>
                    @endforeach
                </div>
            </div>
        @endif
        @if (count($item->os))
            <div class='line'>
                <label>Operating System</label>
                <div class='line-item'>
                    @foreach ($item->os as $i)
                        {{ $i->name }}<br/>
                    @endforeach
                </div>
            </div>
        @endif
    </body>
</html>
