<!-- resources/views/admin/briefing/create.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.css" />
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.date.css" />
@endsection

@section('script')
	<script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/js/admin/admin.briefing.js"></script>
@endsection

@section('content')
	<div id="admin-content-wrap-inner">
	{!! Form::open(array('url'=> "/" . Request::segment(1) . "/" . Request::segment(2), 'method'=>'POST', 'files'=>true)) !!}
            <div class='row'>
                <div class="col-md-12">
                    @include("layouts/notification")
                </div>
            </div>
            <div class='row'>
                <div class="col-md-9">
                    <h2>Briefing</h2>
                    <p>Your email ({{ Auth::User()->email }}) will be recorded when you submit this form.</p>
                </div>
                <div class="col-md-3">
                    <div class="admin-utils-top-right">
                        <button type="submit" class="adc-green-btn"><i class="fa fa-rocket"></i> Submit</button>
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col col-md-8">
					<h5 class="briefing-label-title">Project Details <span>( <span class='pink'><span class='required-star'>*</span> Required</span> )</span></h5>
					<div class="row">
						<div class="col-sm-12 col-md-5">
                            <h6 class="briefing-label-subtitle">Project Name <span><span class='required-star'>*</span> ( Example project-name-2016-07-uk )</span></h6>
							{!! Form::text('name', NULL, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}
						</div>
						<div class="col-sm-6 col-md-3">
                            <h6 class="briefing-label-subtitle">Type of Work <span class='required-star'>*</span></h6>
							<div class="styled-select slate">
								{!! Form::select('type', DB::table('om_briefing_type')->pluck('name', 'id'), Input::old('type')) !!}
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
                            <h6 class="briefing-label-subtitle">Budget <span class='required-star'></span><span>( Please input currency )</span></h6>
				            {!! Form::text('campaign_budget', NULL, array('id'=>'campaign_budget','class'=>'form-control', 'placeholder'=>'Value')) !!}
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">Creative Due Date <span class='required-star'>*</span></h6>
							{!! Form::text('creative_due_date', NULL, array('id'=>'creative_due_date','class'=>'form-control', 'placeholder'=>'Date')) !!}
						</div>
						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">IO Live Date <span class='required-star'></span></h6>
							{!! Form::text('live_date', NULL, array('id'=>'live_date','class'=>'form-control', 'placeholder'=>'Date')) !!}
						</div>
						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">Language(s) <span class='required-star'>*</span></h6>
							{!! Form::text('multiples_languages', NULL, array('id'=>'multiples_languages','class'=>'form-control', 'placeholder'=>'Values')) !!}
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">Vertical <span class='required-star'></span></h6>
							<div class="styled-select slate">
								{!! Form::select('vertical', DB::table('om_briefing_vertical')->pluck('name', 'id'), Input::old('vertical')) !!}
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">Media Agency <span class='required-star'>*</span></h6>
							<div class="styled-select slate">
								{!! Form::select('agency', DB::table('om_briefing_agency')->pluck('name', 'id'), Input::old('agency')) !!}
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
                            <h6 class="briefing-label-subtitle">Salesforce ID</h6>
							{!! Form::text('salesforce_id', NULL, array('id'=>'salesforce_id','class'=>'form-control', 'placeholder'=>'ID')) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-8">
                            <h6 class="briefing-label-subtitle">Campaign Website <span class='required-star'>*</span></h6>
				            {!! Form::text('campaign_website', NULL, array('id'=>'campaign_website','class'=>'form-control', 'placeholder'=>'Url')) !!}
				        </div>

                        <div class="col-sm-6 col-md-4">
                            <h6 class="briefing-label-subtitle">KPI <span class='required-star'></span></h6>
				            {!! Form::text('kpi', NULL, array('id'=>'kpi','class'=>'form-control', 'placeholder'=>'Value')) !!}
				        </div>
					</div>

                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="briefing-label-title">Technical Details</h5>
                        </div>
                        <div class="col-md-8">
                            <h6 class="briefing-label-subtitle">Formats Requested <span class='required-star'>*</span></h6>
                            @foreach (DB::table('om_briefing_format')->orderBy('name', 'asc')->get()->all() as $item)
                                @if ($item->slug !== "other")
                                    <div class='admin-checkbox-item'>
                                        {!! Form::checkbox('format_'.$item->slug, true, false, array('id'=>'format_'.$item->slug)); !!}
                                        <label for="{{'format_'.$item->slug}}"><span>{{ $item->name }}</span></label>
                                    </div>
                                @endif
                            @endforeach
                            <div class='admin-checkbox-item admin-checkbox-item-other'>
                                {!! Form::checkbox('format_other', true, false, array('id'=>'format_other')); !!}
                                <label for="{{'format_other'}}"><span>Other</span></label>
                                {!! Form::text('format_other_text', NULL, array('id'=>'format_other_text','class'=>'form-control input-other', 'placeholder'=>'')) !!}
                            </div>
                            <div style='clear:both;'></div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-xs-6 col-md-12">
                                    <h6 class="briefing-label-subtitle">Celtra Build <span class='required-star'></span></h6>
                                    <div class='admin-checkbox-item'>
                                        {!! Form::checkbox('celtra_build', true, false, array('id'=>'celtra_build')); !!}
                                        <label for="{{'celtra_build'}}"><span>Yes</span></label>

                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-12">
                                    <h6 class="briefing-label-subtitle">Operating System <span class='required-star'></span></h6>
                                    @foreach (DB::table('om_briefing_os')->orderBy('name', 'asc')->get()->all() as $item)
                                        <div class='admin-checkbox-item'>
                                            {!! Form::checkbox('os_'.$item->slug, true, false, array('id'=>'os_'.$item->slug)); !!}
                                            <label for="{{'os_'.$item->slug}}"><span>{{ $item->name }}</span></label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
    					<div class="col-md-8">
                            <h6 class="briefing-label-subtitle">Platform <span class='required-star'></span></h6>
                            @foreach (DB::table('om_briefing_platform')->orderBy('name', 'asc')->get()->all() as $item)
                                @if ($item->slug !== "other")
                                    <div class='admin-checkbox-item'>
                                        {!! Form::checkbox('platform_'.$item->slug, true, false, array('id'=>'platform_'.$item->slug)); !!}
                                        <label for="{{'platform_'.$item->slug}}"><span>{{ $item->name }}</span></label>
                                    </div>
                                @endif
                            @endforeach
                            <div class='admin-checkbox-item admin-checkbox-item-other'>
                                {!! Form::checkbox('platform_other', true, false, array('id'=>'platform_other')); !!}
                                <label for="{{'platform_other'}}"><span>Other</span></label>
                                {!! Form::text('platform_other_text', NULL, array('id'=>'platform_other_text','class'=>'form-control input-other', 'placeholder'=>'')) !!}
                            </div>
                            <div style='clear:both;'></div>
                        </div>
                        <div class="col-md-4">
                            <h6 class="briefing-label-subtitle">Devices <span class='required-star'></span></h6>
                            @foreach (DB::table('om_briefing_device')->orderBy('name', 'asc')->get() as $item)
                                <div class='admin-checkbox-item'>
                                    {!! Form::checkbox('device_'.$item->slug, true, false, array('id'=>'device_'.$item->slug)); !!}
                                    <label for="{{'device_'.$item->slug}}"><span>{{ $item->name }}</span></label>
                                </div>
                            @endforeach
                        </div>
                    </div>





				</div>
				<div class="col col-right col-md-4">
                    <h6 class="briefing-label-subtitle">Full Creative Briefing <span class='required-star'>*</span></h6>
			        {!! Form::textarea('full_creative_briefing', NULL, array('id'=>'full_creative_briefing','class'=>'vertical-text-area', 'placeholder'=>'Description')) !!}
				</div>
			</div>


	{!! Form::close() !!}
</div>

	<script>
		$('#creative_due_date').pickadate({
		    format: 'dd/mm/yyyy',
			formatSubmit: 'yyyy/mm/dd',
		});
        $('#live_date').pickadate({
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy/mm/dd',
        });
	</script>
@endsection
