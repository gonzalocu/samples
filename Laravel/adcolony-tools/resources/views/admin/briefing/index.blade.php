<!-- resources/views/briefing/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Briefing</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                <a class='adc-green-btn' href="{{ "/" . Request::segment(1) . "/" . Request::segment(2) }}/create"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; New</a>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <h6>Completed</h6>
            @if(count($collection) == 0)
                <p>You have not submitted a briefing yet. </p>
            @else
                <table class='table-admin'>
                <thead>
                    <tr>
                        <th class="col-md-3">Name</th>
                        <th class="col-md-3">Created by</th>
                        <th class="col-md-2">Create at</th>
                        <th class="col-md-2">Due Date</th>
                        <th class="col-md-1">Details</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($collection as $item)
                        @if(Auth::user()->id === $item->user_id || Auth::user()->email == 'cemal.gunusen@adcolony.com' || Auth::user()->email == 'gonzalo.cuadrado@adcolony.com' || Auth::user()->email == 'victoria.winter@adcolony.com')
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->user->name }} ({{ $item->user->email }})</td>
                            <td>{{ $item->created_at->format('d/m/Y') }}</td>
                            <td>{{ $item->due_date->format('d/m/Y') }}</td>
                            <td><a class="adc-green-btn" href="{{ "/" . Request::segment(1) . "/" . Request::segment(2) . "/pdf/". $item->id }}" target="_blank">View</a></td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection
