<html>
<body>
    @if ($email_type == 'self')
        <p>Hi, {{ $email_to->name }}</p>
        <p>Thank you for completing the briefing for {{ ($item->type->slug == 'pitch-proactive' ? 'a pitch/pro-active' : 'an IO Signed') }}  campaign.  It is now with the design team, who will be in touch shortly.</p>
        <p>You can find your completed briefing in Basecamp <a href="{{ $basecamp_url }}">here</a></p>
        <br/>
    @else
        <p>Hi, {{ $email_to->name }}</p>
        <p>A new To Do List has just been created for {{ ($item->type->slug == 'pitch-proactive' ? 'a pitch/pro-active' : 'an IO Signed') }} and requires resourcing to a designer.</p>
        <p>You can find the completed briefing in Basecamp <a href="{{ $basecamp_url }}">here</a></p>
        <br/>
    @endif

    <p>Your answers are below for reference:</p>
    <p><b>Form submitter:</b> {{ $user->name }} ({{ $user->email }})</p>
    @if ($item->name)
        <p><b>Project Name:</b> {{ $item->name }}</p>
    @endif

    @if ($item->campaign_website)
        <p><b>Campaign Website:</b> {{ $item->campaign_website }}</p>
    @endif

    @if ($item->salesforce_id)
        <p><b>Saleforce ID:</b> {{ $item->salesforce_id }}</p>
    @endif

    @if ($item->campaign_budget)
        <p><b>Campaign Budget:</b> {{ $item->campaign_budget }}</p>
    @endif

    @if ($item->campaign_budget)
        <p><b>Campaign Budget:</b> {{ $item->campaign_budget }}</p>
    @endif

    @if ($item->kpi)
        <p><b>KPI:</b> {{ $item->kpi }}</p>
    @endif

    @if ($item->language)
        <p><b>Multiple Language:</b> {{ $item->language }}</p>
    @endif

    @if ($item->type)
        <p><b>Type of Work:</b> {{ $item->type->name }}</p>
    @endif

    @if ($item->vertical)
        <p><b>Vertical:</b> {{ $item->vertical->name }}</p>
    @endif

    @if ($item->agency)
        <p><b>Media Agency:</b> {{ $item->agency->name }}</p>
    @endif

    @if ($item->due_date)
        <p><b>Creative Due Date:</b> {{ $item->due_date->format('d/m/Y') }}</p>
    @endif

    @if ($item->live_date)
        <p><b>IO Live Date:</b> {{ $item->live_date->format('d/m/Y') }}</p>
    @endif

    @if ($item->full_creative_briefing)
        <p><b>Full Creative Briefing:</b> {!! nl2br($item->full_creative_briefing) !!}</p>
    @endif

    @if ($item->celtra_build)
        <p><b>Celtra Build:</b> {{ $item->celtra_build }}</p>
    @endif

    @if (count($item->format))
        <p><b>Format Requested:</b>
        @foreach ($item->format as $i)
            @if ($i->slug == 'other')
                {{ $i->pivot->briefing_other}}
            @else
                {{ $i->name }}
            @endif
            @if (!$loop->last){{", "}}@endif
        @endforeach
        </p>
    @endif

    @if (count($item->platform))
        <p><b>Platform:</b>
        @foreach ($item->platform as $i)
            @if ($i->slug == 'other')
                {{ $i->pivot->briefing_other}}
            @else
                {{ $i->name }}
            @endif
            @if (!$loop->last){{", "}}@endif
        @endforeach
        </p>
    @endif

    @if (count($item->device))
        <p><b>Device:</b>
        @foreach ($item->device as $i)
            {{ $i->name }}
            @if (!$loop->last){{", "}}@endif
        @endforeach
        </p>
    @endif

    @if (count($item->os))
        <p><b>Operating System:</b>
        @foreach ($item->os as $i)
            {{ $i->name }}
            @if (!$loop->last){{", "}}@endif
        @endforeach
        </p>
    @endif
</body>
</html>
