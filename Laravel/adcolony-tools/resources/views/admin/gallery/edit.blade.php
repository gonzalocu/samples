<!-- resources/views/admin/gallery/edit.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" type="text/css" href="/bower_components/pickadate/lib/compressed/themes/classic.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/pickadate/lib/compressed/themes/classic.date.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/basic.min.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/dropzone.min.css" />
@endsection

@section('script')
	<script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/bower_components/dropzone/dist/min/dropzone.min.js"></script>
	<script src="/js/admin/admin.gallery.edit.js"></script>
@endsection

@section('script_bottom')
<script>
    var baseUrl = "{{ url('/') }}";
    var token = "{{ Session::getToken() }}";
</script>
@endsection

@section('content')
<div id='admin-content-wrap-inner'>
    {!! Form::open(array('url'=> "/" . Request::segment(1) . "/" . Request::segment(2) , 'method'=>'PUT', 'files'=>true)) !!}
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>OM Gallery</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            {{-- SHOWCASE IMAGE --}}
            {!! Form::label('showcase_image_url','Showcase image URL (200 x 200)') !!}
            <div id='showcase-container'>
    	        @if (Session::has('tmp_show_files'))
    	            {!! Form::hidden('showcase_image_url', 'true', array('id'=>'showcase_image_url')) !!}
                    @foreach (Session::get('tmp_show_files') as $asset)
                        <input type="hidden" name="_show_file" value="true">
                        <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");' ></div>
    				@endforeach
                @elseif ($item->showcase_image_url )
    	            {!! Form::hidden('showcase_image_url', 'true', array('id'=>'showcase_image_url')) !!}
                    <input type="hidden" name="_show_file" value="true">
                    <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->showcase_image_url }}");' ></div>
                @else
                    {!! Form::hidden('showcase_image_url', '', array('id'=>'showcase_image_url')) !!}
    			@endif
    	    </div>
            <div id="showcase-dropzone" class="dropzone"></div>

            {{-- NAME --}}
            {!! Form::label('name','Name') !!}
            {!! Form::text('name', $item->name, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {{-- DESCRIPTION --}}
            {!! Form::label('description','Description') !!}
            {!! Form::text('description', $item->description, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}

            {{-- FEATURE IMAGE --}}
            {!! Form::label('feature_image_url','Feature image URL (640 x 535)') !!}
            <div id='feature-container'>
    	        @if (Session::has('tmp_fea_files'))
                    {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
                    @foreach (Session::get('tmp_fea_files') as $asset)
                    <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
    				@endforeach
                @elseif ($item->feature_image_url )
                    {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
                    <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->feature_image_url }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
                @else
                    {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
    			@endif
    	    </div>
            <div id="feature-dropzone" class="dropzone"></div>

            {{-- BRAND --}}
            {!! Form::label('brand','Brand') !!}
            {!! Form::text('brand', $item->brand, array('id'=>'brand','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {{-- AGENCY --}}
            {!! Form::label('agency','Agency') !!}
            {!! Form::text('agency', $item->agency, array('id'=>'agency','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {{-- ORIENTATION --}}
            {!! Form::label('orientation','Orientation') !!}
            {!! Form::select('orientation', DB::table('om_gallery_orientation')->pluck('name', 'id'), $item->orientation_id) !!}

            {{-- LABEL --}}
            {!! Form::label('position','Position') !!}
            {!! Form::select('position', DB::table('om_gallery_position')->pluck('name', 'id'), $item->position_id) !!}

            {{-- FORMAT --}}
            {!! Form::label('format','Format') !!}
            {!! Form::select('format', DB::table('om_gallery_format')->pluck('name', 'id'), $item->format_id) !!}

            {{-- REGION --}}
            @if ( Auth::user()->role[0]->name == "Administrator")
                {!! Form::label('region','Region') !!}
                {!! Form::select('region', DB::table('om_region')->pluck('name', 'id'), $item->region_id) !!}
            @endif

            {{-- VERTICAL --}}
            {!! Form::label('vertical','Vertical') !!}
            {!! Form::select('vertical', Vertical::orderBy('name', 'asc')->pluck('name', 'id'), ($item->vertical?$item->vertical->id:'') ) !!}

            {{-- FEATURE --}}
            {!! Form::label('feature','Features') !!}
            @foreach (Feature::orderBy('name', 'asc')->get()->all() as $feature)
                <?php $selected = false; ?>
                @foreach ($item->features as $current_features)
                    @if ($current_features->slug == $feature->slug)
                        <?php $selected = true; ?>
                    @endif
                @endforeach
                <div class='omw-admin-format-feature'>
                    {!! Form::checkbox('feature_'.$feature->slug, true, $selected, array('id'=>'feature_'.$feature->slug)); !!}
                    <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label>
                </div>
            @endforeach

            {{-- LIVE DATE --}}
            {!! Form::label('live_date','Live Date') !!}
            {!! Form::text('live_date',  \Carbon\Carbon::createFromTimeStamp(strtotime($item->live_date))->format('Y/m/d') , array('id'=>'live_date','class'=>'form-control form-control-date', 'placeholder'=>'Date')) !!}

            {{-- TAG --}}
            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', $item->tag, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

            {{-- APPROVAL --}}
            {{--
                {!! Form::checkbox('approval', true, $item->is_approval_request, array('id' => 'approval', 'class' => 'ios7checkbox')); !!}
                <label for='approval' tabindex="-1"><span class="check"></span>Approval Request</label>
            --}}

            {{-- FEATURED --}}
            {!! Form::checkbox('featured', true, $item->is_featured_creative, array('id' => 'featured', 'class' => 'ios7checkbox')); !!}
    		<label for='featured' tabindex="-1"><span class="check"></span>Featured Creative</label>

            {{-- PRIVATE --}}
            {!! Form::checkbox('private', true, $item->is_private, array('id' => 'private', 'class' => 'ios7checkbox')); !!}
    		<label for='private' tabindex="-1"><span class="check"></span>Private</label>

            {{-- VISIBLE --}}
            {!! Form::checkbox('visible', true, $item->visible, array('id' => 'visible', 'class' => 'ios7checkbox')); !!}
    		<label for='visible' tabindex="-1"><span class="check"></span>Visible</label>

        </div>
    </div>
    {!! Form::close() !!}
    <div class='row'>
        <div class="col-md-12">
            {!! Form::delete('/' . Request::segment(1) . '/' . Request::segment(2) , 'Delete', array('class'=>'omw-admin-delete-btn')) !!}
        </div>
    </div>
</div>


  <div class='omw-table-wrapper'>
        <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

		@include("layouts/notification")

        {!! Form::open(array('url'=> "/" . Request::segment(1) . "/" . Request::segment(2) , 'method'=>'PUT', 'files'=>true)) !!}

        {{-- SHOWCASE IMAGE --}}
        {!! Form::label('showcase_image_url','Showcase image URL (200 x 200)') !!}
        <div id='showcase-container'>
	        @if (Session::has('tmp_show_files'))
	            {!! Form::hidden('showcase_image_url', 'true', array('id'=>'showcase_image_url')) !!}
                @foreach (Session::get('tmp_show_files') as $asset)
                    <input type="hidden" name="_show_file" value="true">
                    <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");' ></div>
				@endforeach
            @elseif ($item->showcase_image_url )
	            {!! Form::hidden('showcase_image_url', 'true', array('id'=>'showcase_image_url')) !!}
                <input type="hidden" name="_show_file" value="true">
                <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->showcase_image_url }}");' ></div>
            @else
                {!! Form::hidden('showcase_image_url', '', array('id'=>'showcase_image_url')) !!}
			@endif
	    </div>
        <div id="showcase-dropzone" class="dropzone"></div>

        {{-- NAME --}}
        {!! Form::label('name','Name') !!}
        {!! Form::text('name', $item->name, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}

        {{-- DESCRIPTION --}}
        {!! Form::label('description','Description') !!}
        {!! Form::text('description', $item->description, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}

        {{-- FEATURE IMAGE --}}
        {!! Form::label('feature_image_url','Feature image URL (640 x 535)') !!}
        <div id='feature-container'>
	        @if (Session::has('tmp_fea_files'))
                {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
                @foreach (Session::get('tmp_fea_files') as $asset)
                <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
				@endforeach
            @elseif ($item->feature_image_url )
                {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
                <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->feature_image_url }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
            @else
                {!! Form::hidden('feature_image_url', 'true', array('id'=>'feature_image_url')) !!}
			@endif
	    </div>
        <div id="feature-dropzone" class="dropzone"></div>

        {{-- BRAND --}}
        {!! Form::label('brand','Brand') !!}
        {!! Form::text('brand', $item->brand, array('id'=>'brand','class'=>'form-control', 'placeholder'=>'Name')) !!}

        {{-- AGENCY --}}
        {!! Form::label('agency','Agency') !!}
        {!! Form::text('agency', $item->agency, array('id'=>'agency','class'=>'form-control', 'placeholder'=>'Name')) !!}

        {{-- ORIENTATION --}}
        {!! Form::label('orientation','Orientation') !!}
        {!! Form::select('orientation', DB::table('om_gallery_orientation')->pluck('name', 'id'), $item->orientation_id) !!}

        {{-- LABEL --}}
        {!! Form::label('position','Position') !!}
        {!! Form::select('position', DB::table('om_gallery_position')->pluck('name', 'id'), $item->position_id) !!}

        {{-- FORMAT --}}
        {!! Form::label('format','Format') !!}
        {!! Form::select('format', DB::table('om_gallery_format')->pluck('name', 'id'), $item->format_id) !!}

        {{-- REGION --}}
        @if ( Auth::user()->role[0]->name == "Administrator")
            {!! Form::label('region','Region') !!}
            {!! Form::select('region', DB::table('om_region')->pluck('name', 'id'), $item->region_id) !!}
        @endif

        {{-- VERTICAL --}}
        {!! Form::label('vertical','Vertical') !!}
        {!! Form::select('vertical', Vertical::orderBy('name', 'asc')->pluck('name', 'id'), ($item->vertical?$item->vertical->id:'') ) !!}

        {{-- FEATURE --}}
        {!! Form::label('feature','Features') !!}
        @foreach (Feature::orderBy('name', 'asc')->get()->all() as $feature)
            <?php $selected = false; ?>
            @foreach ($item->features as $current_features)
                @if ($current_features->slug == $feature->slug)
                    <?php $selected = true; ?>
                @endif
            @endforeach
            <div class='omw-admin-format-feature'>
                {!! Form::checkbox('feature_'.$feature->slug, true, $selected, array('id'=>'feature_'.$feature->slug)); !!}
                <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label>
            </div>
        @endforeach

        {{-- LIVE DATE --}}
        {!! Form::label('live_date','Live Date') !!}
        {!! Form::text('live_date',  \Carbon\Carbon::createFromTimeStamp(strtotime($item->live_date))->format('Y/m/d') , array('id'=>'live_date','class'=>'form-control form-control-date', 'placeholder'=>'Date')) !!}

        {{-- TAG --}}
        {!! Form::label('tag','Ad Tag') !!}
        {!! Form::textarea('tag', $item->tag, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

        {{-- APPROVAL --}}
        {{--
            {!! Form::checkbox('approval', true, $item->is_approval_request, array('id' => 'approval', 'class' => 'ios7checkbox')); !!}
            <label for='approval' tabindex="-1"><span class="check"></span>Approval Request</label>
        --}}

        {{-- FEATURED --}}
        {!! Form::checkbox('featured', true, $item->is_featured_creative, array('id' => 'featured', 'class' => 'ios7checkbox')); !!}
		<label for='featured' tabindex="-1"><span class="check"></span>Featured Creative</label>

        {{-- PRIVATE --}}
        {!! Form::checkbox('private', true, $item->is_private, array('id' => 'private', 'class' => 'ios7checkbox')); !!}
		<label for='private' tabindex="-1"><span class="check"></span>Private</label>

        {{-- VISIBLE --}}
        {!! Form::checkbox('visible', true, $item->visible, array('id' => 'visible', 'class' => 'ios7checkbox')); !!}
		<label for='visible' tabindex="-1"><span class="check"></span>Visible</label>

        {!! Form::submit('Save', array('class'=>'omw-admin-btn')) !!}
        {!! Form::close() !!}

        {!! Form::delete('/' . Request::segment(1) . '/' . Request::segment(2) , 'Delete', array('class'=>'omw-admin-delete-btn')) !!}
	</div>

@endsection
