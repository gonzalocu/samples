<!-- resources/views/admin/gallery/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>OM Gallery</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                <a class='adc-green-btn' href="{{ Request::url() }}/create"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; New</a>

                <!-- @if ( Auth::user()->role[0]->name == "Administrator" || Auth::user()->role[0]->name == "Developer" || Auth::user()->role[0]->name == "Designer"  )
                    <a class='omw-admin-btn-small' href="/feature" target="_blank">Manage Features &nbsp;<span class="fa fa-cog fa-lg"></span></a>
                    <a class='omw-admin-btn-small' href="/vertical" target="_blank">Manage Verticals &nbsp;<span class="fa fa-cog fa-lg"></span></a>
                @endif -->
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Region</th>
                    <th>Featured</th>
                    <th>Private</th>
                    <th>Visible</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="6"></td>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($collection as $element)
                <tr>
                    <td style="height:50px;line-height:50px;">
                        @if ($element->showcase_image_url )
                        <img src="{{ $element->showcase_image_url }}" style="width:50px;height:50px;border-radius:50px;margin:5px 0px;vertical-align:middle;margin-right:20px;" />
                        @else
                        <img src="/assets/images/basic/default-showcase.jpg" style="width:50px;height:50px;border-radius:50px;margin:5px 0px;vertical-align:middle;margin-right:20px;" />
                        @endif
                        {{ $element->name }}</td>
                    <td>{{ $element->region->name }}</td>
                    <td>
                        @if ($element->is_featured_creative == 1)
                           <div class="fa fa-check"></div>
                        @else
                            <div class='fa fa-times'></div>
                        @endif
                    </td>
                    <td>
                        @if ($element->is_private == 1)
                           <div class="fa fa-check"></div>
                        @else
                            <div class='fa fa-times'></div>
                        @endif
                    </td>
                    <td>
                        @if ($element->visible == 1)
                           <div class="fa fa-check"></div>
                        @else
                            <div class='fa fa-times'></div>
                        @endif
                    </td>
                    <td><a class="adc-green-btn" href="{{ Request::url() }}/{{ $element->id }}">View</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
