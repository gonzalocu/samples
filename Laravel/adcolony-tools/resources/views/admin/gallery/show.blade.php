<!-- resources/views/admin/gallery/show.blade.php -->

@extends('layouts.master_admin')

@section('script')
	<script type="text/javascript" src="/js/admin/admin.gallery.show.js"></script>
@endsection

@section('content')

<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>OM Gallery</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                @if (Auth::user()->role[0]->id == 1 || Auth::user()->region->id == $item->region->id )
                    <a class="adc-green-btn" href="{{ Request::url() }}/edit"><i class="fa fa-pencil"></i>&nbsp;&nbsp; Edit</a>
                @endif
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Region</th>
                        <th>Featured</th>
                        <th>Private</th>
                        <th>Visible</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @if ($item->showcase_image_url )
                                <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->showcase_image_url }}");vertical-align:middle;margin-right:20px;' ></div>
                            @else
                                 <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/assets/images/basic/default-showcase.jpg");vertical-align:middle;margin-right:20px;' ></div>
                            @endif
                            {{ $item->name }}</td>
                        <td>{{ $item->region->name }}</td>
                        <td>
                            @if ($item->is_featured_creative == 1)
                               <div class="fa fa-check"></div>
                            @else
                                <div class='fa fa-times'></div>
                            @endif
                        </td>
                        <td>
                            @if ($item->is_private == 1)
                               <div class="fa fa-check"></div>
                            @else
                                <div class='fa fa-times'></div>
                            @endif
                        </td>
                        <td>
                            @if ($item->visible == 1)
                               <div class="fa fa-check"></div>

                            @else
                                <div class='fa fa-times'></div>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class='table-admin'>
                <thead>
                    <tr>
                        <th>Orientation</th>
                        <th>Position</th>
                        <th>Brand</th>
                        <th>Agency</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td>{{ $item->orientation()->name }}</td>
                        <td>{{ $item->position()->name }}</td>
                        <td>{{ $item->brand }}</td>
                        <td>{{ $item->agency }}</td>
                    </tr>
                </tbody>
            </table>

            <table class='table-admin'>
                <thead>
                    <tr>
                        <th>Format</th>
                        <th>Features</th>
                        <th>Vertical</th>
                        <th>Live Date</th>
                        @if ($item->password)
                            <th>Password</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td>{{ $item->format()->name }}</td>
                        <td>
                            @foreach ($item->features as $feature)
                                {{ $feature->name }}<br/>
                            @endforeach
                        </td>
                        <td> @if($item->vertical) {{ $item->vertical->name }} @endif</td>
                        <td>{{ $item->live_date }}</td>
                        @if ($item->password)
                            <td>{{ $item->password }}</td>
                        @endif
                    </tr>
                </tbody>
            </table>

            @if ($item->feature_image_url )
                {!! Form::label('images','Feature Creative Image') !!}
                <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->feature_image_url }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
            @endif


            {!! Form::label('description','Description') !!}
            {!! Form::text(false, $item->description, array('class'=>'form-control', 'readonly')) !!}

            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', $item->tag, array('class'=>'form-control', 'readonly')) !!}

        </div>
    </div>
</div>

<div class='omw-table-wrapper'>
    <a class='omw-admin-btn' href="/{{ Request::segment(1) }}">Back</a>

	@include("layouts/notification")

    <table>
        <thead>
            <tr>
                <th></th>
	            <th>Name</th>
	            <th>Region</th>
	            <th>Featured</th>
	            <th>Private</th>
	            <th>Visible</th>
	            <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    @if ($item->showcase_image_url )
                        <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->showcase_image_url }}");' ></div>
                    @else
                         <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/assets/images/basic/default-showcase.jpg");' ></div>
                    @endif
                </td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->region->name }}</td>
	            <td>
		            @if ($item->is_featured_creative == 1)
		               <div class="fa fa-check"></div>
		            @else
		                <div class='fa fa-times'></div>
		            @endif
	            </td>
	            <td>
		            @if ($item->is_private == 1)
		               <div class="fa fa-check"></div>
		            @else
		                <div class='fa fa-times'></div>
		            @endif
	            </td>
                <td>
	                @if ($item->visible == 1)
	                   <div class="fa fa-check"></div>

                    @else
                        <div class='fa fa-times'></div>
	                @endif
	            </td>
                <td>
                    @if (Auth::user()->role[0]->id == 1 || Auth::user()->region->id == $item->region->id )
                        <a href="{{ Request::url() }}/edit">Edit</a>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
	            <th>Orientation</th>
	            <th>Position</th>
	            <th>Brand</th>
	            <th>Agency</th>
            </tr>
        </thead>
        <tbody>
            <tr>

                <td>{{ $item->orientation()->name }}</td>
                <td>{{ $item->position()->name }}</td>
	            <td>{{ $item->brand }}</td>
	            <td>{{ $item->agency }}</td>
            </tr>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
	            <th>Format</th>
	            <th>Features</th>
                <th>Vertical</th>
                <th>Live Date</th>
                @if ($item->password)
                    <th>Password</th>
                @endif
            </tr>
        </thead>
        <tbody>
            <tr>

                <td>{{ $item->format()->name }}</td>
	            <td>
	                @foreach ($item->features as $feature)
                        {{ $feature->name }}<br/>
	                @endforeach
	            </td>
                <td> @if($item->vertical) {{ $item->vertical->name }} @endif</td>
                <td>{{ $item->live_date }}</td>
                @if ($item->password)
                    <td>{{ $item->password }}</td>
                @endif
            </tr>
        </tbody>
    </table>

    @if ($item->feature_image_url )
        {!! Form::label('images','Feature Creative Image') !!}
        <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("{{ $item->feature_image_url }}");' ><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
    @endif


    {!! Form::label('description','Description') !!}
    {!! Form::text(false, $item->description, array('class'=>'form-control', 'readonly')) !!}

    {!! Form::label('tag','Ad Tag') !!}
    {!! Form::textarea('tag', $item->tag, array('class'=>'form-control', 'readonly')) !!}

</div>
@endsection
