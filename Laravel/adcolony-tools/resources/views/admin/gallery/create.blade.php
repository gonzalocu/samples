<!-- resources/views/admin/gallery/create.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" type="text/css" href="/bower_components/pickadate/lib/compressed/themes/classic.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/pickadate/lib/compressed/themes/classic.date.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/basic.min.css" />
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/dropzone.min.css" />
@endsection

@section('script')
	<script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/bower_components/dropzone/dist/min/dropzone.min.js"></script>
	<script src="/js/admin/admin.gallery.edit.js"></script>
@endsection

@section('script_bottom')
<script>
    var baseUrl = "{{ url('/') }}";
    var token = "{{ Session::getToken() }}";
</script>
@endsection

@section('content')
<div id='admin-content-wrap-inner'>
    {!! Form::open(array('url'=> "/" . Request::segment(1) , 'method'=>'POST', 'files'=>true)) !!}
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>OM Gallery</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            {{-- SHOWCASE IMAGE --}}
            {!! Form::label('showcase_image_url_label','Showcase image URL (200 x 200)') !!}
            {!! Form::hidden('showcase_image_url', (Session::has('tmp_show_files')?'true':''), array('id'=>'showcase_image_url')) !!}
            <div id='showcase-container'>
    	        @if (Session::has('tmp_show_files'))
                    @foreach (Session::get('tmp_show_files') as $asset)
                    <div id='showcase-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");' ></div>
    				@endforeach
    			@endif
    	    </div>
            <div id="showcase-dropzone" class="dropzone"></div>

            {{-- NAME --}}
            {!! Form::label('name','Name') !!}
            {!! Form::text('name', NULL, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}

             {{-- DESCRIPTION --}}
            {!! Form::label('description','Description') !!}
            {!! Form::text('description', NULL, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}

            {{-- FEATURE IMAGE --}}
            {!! Form::label('feature_image_url_label','Feature image URL (640 x 535)') !!}
            {!! Form::hidden('feature_image_url', (Session::has('tmp_fea_files')?'true':''), array('id'=>'feature_image_url')) !!}
            <div id='feature-container'>
    	        @if (Session::has('tmp_fea_files'))
                    @foreach (Session::get('tmp_fea_files') as $asset)
                    <div id='feature-image' class='omw-admin-format-image' style='opacity:0;background-image:url("/{{ $asset }}");'><div id="feature-header">Featured Creative</div><div id="feature-mask"></div></div>
    				@endforeach
    			@endif
    	    </div>
            <div id="feature-dropzone" class="dropzone"></div>

            {{-- BRAND --}}
            {!! Form::label('brand','Brand') !!}
            {!! Form::text('brand', NULL, array('id'=>'brand','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {{-- AGENCY --}}
            {!! Form::label('agency','Agency') !!}
            {!! Form::text('agency', NULL, array('id'=>'agency','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {{-- ORIENTATION --}}
            {!! Form::label('orientation','Orientation') !!}
            {!! Form::select('orientation', DB::table('om_gallery_orientation')->pluck('name', 'id'), Input::old('orientation')) !!}

            {{-- LABEL --}}
            {!! Form::label('position','Position') !!}
            {!! Form::select('position', DB::table('om_gallery_position')->pluck('name', 'id'), Input::old('position')) !!}

            {{-- FORMAT --}}
            {!! Form::label('format','Format') !!}
            {!! Form::select('format', DB::table('om_gallery_format')->pluck('name', 'id'), Input::old('format')) !!}

            {{-- REGION --}}
            @if ( Auth::user()->role[0]->name == "Administrator")
                {!! Form::label('region','Region') !!}
                {!! Form::select('region', DB::table('om_region')->pluck('name', 'id'), Input::old('region')) !!}
            @endif

            {{-- VERTICAL --}}
            {!! Form::label('vertical','Vertical') !!}
            {!! Form::select('vertical', Vertical::orderBy('name', 'asc')->pluck('name','id'), Input::old('vertical')) !!}

            {{-- FEATURE --}}
            {!! Form::label('feature','Features') !!}
            @foreach (Feature::orderBy('name', 'asc')->get()->all() as $feature)
                <div class='omw-admin-format-feature'>
                    {!! Form::checkbox('feature_'.$feature->slug, true, false, array('id'=>'feature_'.$feature->slug)); !!}
                    <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label>
                </div>
            @endforeach

            {{-- LIVE DATE --}}
            {!! Form::label('live_date','Live Date') !!}
            {!! Form::text('live_date', NULL, array('id'=>'live_date','class'=>'form-control form-control-date', 'placeholder'=>'Date')) !!}

            {{-- TAG --}}
            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', NULL, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

            {{-- APPROVAL --}}
            {{--
                {!! Form::checkbox('approval', NULl, false, array('id' => 'approval', 'class' => 'ios7checkbox')); !!}
                <label for='approval' tabindex="-1"><span class="check"></span>Approval Request</label>
            --}}

            {{-- FEATURED --}}
            {!! Form::checkbox('featured', true, false, array('id' => 'featured', 'class' => 'ios7checkbox')); !!}
    		<label for='featured' tabindex="-1"><span class="check"></span>Featured Creative</label>

            {{-- PRIVATE --}}
            {!! Form::checkbox('private', true, false, array('id' => 'private', 'class' => 'ios7checkbox')); !!}
    		<label for='private' tabindex="-1"><span class="check"></span>Private</label>

            {{-- VISIBLE --}}
            {!! Form::checkbox('visible', true, false, array('id' => 'visible', 'class' => 'ios7checkbox')); !!}
    		<label for='visible' tabindex="-1"><span class="check"></span>Visible</label>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection
