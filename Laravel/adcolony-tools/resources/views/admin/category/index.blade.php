<!-- resources/views/admin/category/index.blade.php -->

@extends('layouts.master_admin')

@section('content')

    <div class='omw-table-wrapper'>
        @include("layouts/notification")

        <br/>

        {!! Form::open(array('url'=>'/category', 'method'=>'POST')) !!}
            {!! Form::text('fname', '', array('id'=>'fname','class'=>'form-control ', 'placeholder'=>'Name', 'require' => '')) !!}
            {!! Form::submit('Create Category', array('class'=>'')) !!}
        {!! Form::close() !!}

        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ($collection as $element)
            <tr>
                <td>{{ $element->name }}</td>
                <td>
                    {!! Form::delete('/category/'.$element->id, 'Delete', array(),  array('class'=>'')) !!}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
