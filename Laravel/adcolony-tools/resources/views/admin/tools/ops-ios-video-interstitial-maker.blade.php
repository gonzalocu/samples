<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
    <script src="/js/omw/ad-tools/jQuery.AjaxFileUpload.js" type="text/javascript"></script>
@endsection

@section('script_bottom')
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js" type="text/javascript"></script>
	<script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script>

	<script type="text/javascript">

		(function() {

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');

		$('form').ajaxForm({
		beforeSend: function() {
		status.empty();
		var percentVal = '0%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
		var percentVal = percentComplete + '%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		success: function() {
		var percentVal = '100%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		complete: function(xhr) {

		window.imgResponse = JSON.parse(xhr.responseText);
		status.html(window.imgResponse.path);
		}
		});

		})();

		makeTag = function(){
		var urlTag= document.getElementById("tag1").value;

		var width = document.getElementById("upload").width;
		var height = document.getElementById("upload").height;

		document.getElementById("tag2").value =  stBit + window.imgResponse.path + ndBit + urlTag + rdBit;

		addIframe();
		}


		var stBit = "<script src='http://admarvel.s3.amazonaws.com/js/admarvel_compete_v1.1.js'> </scr"+"ipt><video id='my-video' style='position:absolute;top:0px;left:0px;height:100%;width:320px;' webkit-playsinline  autoplay src='";
		var ndBit = "'></video><div id='clickToUrl' style='position:absolute;left:0px;top:0px;height:100%;width:100%;z-index:10;' onclick=\"ormma.open('{clickurl}";
		var rdBit = "')\"></div><script src='http://4sa.s3.amazonaws.com/common-assets/scripts/ios-interstitial-vid-ad-unit-rotate.js' type='text/javascript'></scr"+"ipt><scr"+"ipt> var adEventTracker = new AdMarvelAdEventTracker({partnerId:'{partnerid}', siteId: '{zoneid}', bannerId:'{bannerid}', targetParams: '{targetparams}', cacheBuster:'{timestamp}'});</scr"+"ipt>";
		var addIframe = function(){
		var ifrm = document.getElementById('my-frame');
		ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
		ifrm.document.open();
		ifrm.document.write(document.getElementById("tag2").value);
		ifrm.document.close();
		}

	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">IOS Video Interstitial Maker</h3>
				</div>
  				<div class="panel-body">
					<form action="http://54.229.56.139:2001/image-upload" method="post" enctype="multipart/form-data">
		                <input type="file" name="video">
		                <input type="submit" value="Upload Video to Server" class="btn-tools" id="upload">
		            </form>

		            <div class="progress">
		                <div class="bar progress-bar"></div>

		                <div class="percent"></div>
		            </div>

		            <div id="status"></div>

		            <h2>Click URL</h2>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag1" placeholder="Paste your URL here"></textarea>
		            <button onclick="makeTag();" class="btn-tools">Make Tag</button>

		            <h2>Output (tag)</h2>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag2"></textarea>

		            <h2>Sample</h2>
		            <iframe height=" 480px" width="336px" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0"></iframe>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
