<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<style>
		.iic-title {
			font-size:30px;
		}
		.iic-subtitle {
			font-size:20px;
		}
	</style>
@endsection

@section('script')
    <script src="/js/omw/ad-tools/ZeroClipboard.min.js"></script>
@endsection

@section('script_bottom')
	<script>

		var client = new ZeroClipboard(document.getElementById("button"));

		client.on("copy", function (event) {
			makeTag();

			if (!window.duplicateTag) {
				var clipboard = event.clipboardData;
				clipboard.setData("text/plain", document.getElementById('tag1').value);
			}

			else {
				alert('Duplicate Tag in Field');
			}
		});
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Tag Sanitizer</h3>
				</div>
  				<div class="panel-body">
					<div class='iic-subtitle'>Network</div>
					<select id='network' onchange='makeTag();'>
						<option value="AdMarvel" selected>AdMarvel</option>
						<option value="AdMarvel Off Network">AdMarvel Off Network</option>
						<option value="Opera Response">Opera Response</option>
					</select>

					<div class='iic-subtitle'>Environment</div>
					<select id='environment' onchange='makeTag();' style='margin-bottom: 10px'>
						<option value="Staging">Staging</option>
						<option value="Live" selected>Live</option>
					</select>
					<script>
						makeTag = function() {
							var doc = document.getElementById('advert').contentWindow.document;
							doc.open();
							doc.write();
							doc.close();

							var initialTag = document.getElementById("tag1").value;
							var convertedTag;

							initialTag = initialTag.replace(/""/g, '"')
												   .replace(/\[timestamp\]/g, "{timestamp}")
												   .replace(/%%REALRAND%%/ig, "{timestamp}")
												   .replace(/width=device-width,/g, "");

							//Removes Google Double Quotes
							(initialTag[0] === '"' ? initialTag = initialTag.slice(1, -1) : null);

							switch(document.getElementById('environment').value) {
								case 'Staging':
									initialTag = initialTag
										.replace(/http:\/\/d28z9ox7dl322g.cloudfront.net\/live/g, "http://4sa.s3.amazonaws.com/staging")
										.replace(/live/g, "staging");
									break;

								case 'Live':
									initialTag = initialTag
										.replace(/http:\/\/4sa.s3.amazonaws.com\/staging/g, "http://d28z9ox7dl322g.cloudfront.net/live")
										.replace(/staging/g, "live");
									break;
							}

							switch(document.getElementById('network').value) {
								case 'AdMarvel':
									initialTag = initialTag
										.replace(/'{clickurl}{TARGET_clickurl}'/g, "'{clickurl}'")
										.replace(/'{click_url}'"/g, "'{clickurl}'");
									break;

								case 'AdMarvel Off Network':
									initialTag = initialTag
										.replace(/'{clickurl}'/g, "'{clickurl}{TARGET_clickurl}'")
										.replace(/'{click_url}'/g, "'{clickurl}{TARGET_clickurl}'");
									break;

								case 'Opera Response':
									initialTag = initialTag
										.replace(/'{clickurl}'/g, "'{click_url}'")
										.replace(/'{clickurl}{TARGET_clickurl}'/g, "'{click_url}'");
									break;
							}

							if (initialTag.length > 0) {
								(initialTag.match(/ad-container-id/g).length > 1 ? window.duplicateTag = true : window.duplicateTag = false);
							}

							convertedTag = initialTag;
							console.log(initialTag)
							console.log(convertedTag.replace('adUnit.initialiseTemplate();', 'AdCore.Config.mobileWeb = true; adUnit.initialiseTemplate();'));

							document.getElementById("tag1").value = convertedTag;

							if (!window.duplicateTag) {
								doc.open();
								doc.write(convertedTag.replace('adUnit.initialiseTemplate();', 'AdCore.Config.mobileWeb = true; adUnit.initialiseTemplate();'));
								doc.close();
							}

							else {
								doc.open();
								doc.write();
								doc.close();
							}
						}
					</script>
					<textarea style="width:100%;outline:none;min-height:200px;" id="tag1" placeholder="Paste your tag here"></textarea>
					<button id="button" class='btn-tools' data-clipboard-target="tag1">Sanitize Tag</button>
					<button id="button-2" class='btn-tools' onclick='makeTag()'>Preview Without Sanitizing Tag</button>
					<br/><br/>
					<div class='iic-subtitle'>Advert</div>
					<iframe style='border: 1px solid #ddd' scrolling='no' id="advert" src="" width='320' height='568' marginheight='0' marginwidth='0' frameborder="0"></iframe>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
