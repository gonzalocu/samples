<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script type="text/javascript" src="/js/omw/ad-tools/dragresize.js"></script>
	<script type="text/javascript" src="/js/omw/ad-tools/underscore.js"></script>
	<script>
		function replaceText(){
			document.getElementById('tag2').value = document.getElementById('tag1').value
				.replace(/\[timestamp\]/g, '{timestamp}');

			if (document.getElementById('tag1').value.match(/SCRIPT/i)){
				if (!document.getElementById('tag1').value.match('{clickurl}')){
					document.getElementById('tag2').value = document.getElementById('tag2').value
						.replace('320x50', '320x50;click={clickurl}')
						.replace('320x480', '320x480;click={clickurl}')
						.replace('300x50', '300x50;click={clickurl}')
						.replace('300x250', '300x250;click={clickurl}')
						.replace('728x90', '728x90;click={clickurl}')
				}
			} else {
				if (!document.getElementById('tag1').value.match('{clickurl}')) {
					document.getElementById('tag2').value = document.getElementById('tag2').value
						.replace(/\<A HREF=\"/g, '<A HREF="{clickurl}');
				}
			}
		}
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Doubleclick Tag Converter</h3>
				</div>
  				<div class="panel-body">
					<textarea id="tag1" style="width:100%;outline:none;min-height:200px;" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your text here"></textarea>

					<button onclick="replaceText();" class='btn-tools'>Convert Tag</button>

					<textarea id="tag2" style="width:100%;outline:none;min-height:200px;"></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
