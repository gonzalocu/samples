<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script src="/js/omw/ad-tools/ZeroClipboard.min.js"></script>
    {{-- <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script> --}}
    <script src="/js/omw/ad-tools/jQuery.AjaxFileUpload.js" type="text/javascript"></script>
    <script src="/js/omw/ad-tools/jscolor.min.js" type="text/javascript"></script>
    <script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script>
@endsection

@section('script_bottom')
    <script src="/js/omw/ad-tools/zen-wrapper.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">ZenWrapper Ad Tool</h3>
				</div>
  				<div class="panel-body">
					<div onclick="ZenWrapper.showHideSample(this);" class="active" id="show-hide-sample" style="display:none;">Hide Sample</div>
		            <div id='iframe-container' style="margin-top:20px;position:absolute;z-index:0;top:-100px;right:-90px;">
		                <iframe class="sample-iframe" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0"></iframe>
		                <div id="celtra-not-available">Sample preview for Celtra will not work unfortunately</div>
		                <div id="offnetwork-not-available">Sample preview for Off-Network Tag will not work unfortunately</div>
		            </div>
		            <select id='menu-dropdown'>
		                <option value="zenMenuButton">Change Zen Factor</option>
		    <!--         	<option value="imageInterstitialMenuButton">Interstitial ZenWrapper Creator</option>
		     -->        	<option value="dfaMenuButton">DFA ZenWrapper</option>
		                <option value="celtraMenuButton">Celtra ZenWrapper</option>
		            </select>
		            <style>
		                .iic-title {
		                    font-size:30px;
		                }
		                .iic-subtitle {
		                    font-size:20px;
		                }
		                #menu-dropdown {
		                    margin:25px 110px;
		                }

						#omw-tools-content .container {
							display:none;
						}

						#omw-tools-content .container.active {
							display:block;
						}
		            </style>
		            <div class="container active" id="zenContainer">
		                <div style='max-width:70%'>
		                    <div class='iic-title'>ZenWrapper</div>
		                    <span><b>Please Note: Use any browser apart from Firefox.</b></span><br/>
		                    <span>This tool will allow conversion of ordinary DFA and Celtra tags into ZenWrapped DFA/Celtra tags.<br/>  Will also provide means of changing a Zen Factor.</h2>
		                    <br/><br/>
		                    <div class='iic-subtitle'>Current tag</div>
		                    <textarea style="width:80%;outline:none;min-height:200px;" id="current-tag-zen" placeholder="Paste your tag here"></textarea>

		                    <div class='iic-subtitle'>New Zen Factor</div>
		                    <input id="new-zen-factor" type="number" value="0"/>


						</br><button class="btn-tools" id="button-change-zen-factor">Make Tag</button>
						</br></br>
		                    <div class='iic-subtitle'>New Tag</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="output-tag-zen"></textarea>
		                </div>
		                </div>

		                <div class="container" id="imageInterstitialContainer">
		                <div style='max-width:70%'>
		                    <div class='iic-title'>ZenWrapper</div>
		                    <span><b>Please Note: Use any browser apart from Firefox.</b></span><br/>
		                    <span>This tool will allow conversion of ordinary DFA and Celtra tags into ZenWrapped DFA/Celtra tags.  Will also provide means of changing a Zen Factor.</h2>
		                    <br/><br/>
		                    <div class='iic-subtitle'>Image Interstitial Creator</div>

		                     <div class='iic-subtitle'>Upload Image URL</div>

		                    <!-- <form action="http:\/10.100.11.100/interstitials/image-upload" method="post" enctype="multipart/form-data"> -->
		                    <form action="http://54.229.56.139:2001/image-upload" method="post" enctype="multipart/form-data">
		                        <input type="file" name="image">
		                        <input type="submit" value="Upload File to Server" class="btn-tools" id="upload">
		                    </form>

		                    <div class="progress">
		                        <div class="bar progress-bar"></div>

		                        <div class="percent"></div>
		                    </div>

		                    <div id="status"></div>
		                    <h3>or enter Image URL here</h3>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-image-url" placeholder="Paste your URL here"></textarea>
		                    <div class='iic-subtitle'>Impression URL</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-impression-url" placeholder="Paste your URL here"></textarea>
		                    <div class='iic-subtitle'>Click URL</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-click-url" placeholder="Paste your URL here"></textarea>
		                    <div class='iic-subtitle'>Integral(extra) Tracking</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-integral-url" placeholder="Paste your URL here"></textarea>
		                    <div class='iic-subtitle'>Network</div>
		                    <select id='image-interstitial-network'>
		                        <option value="or">Opera Response</option>
		                        <option value="omax">OMAX (AdMarvel)</option>
		                    </select>
		                    <div class='iic-subtitle'>Zen Factor (0 default)</div>
		                    <input type='number' id="image-interstitial-zen-factor" value='0'/>
		                    <div class='iic-subtitle'>Background colour</div>
		                    <!-- <input type='color' background-name='colour'  id="image-interstitial-background-colour" value='#ffffff'/> -->
		                    <input class="jscolor" value="ffffff" id="image-interstitial-background-colour" />
		                    <div class='iic-subtitle'>Scale Image</div>
		                    <input type='checkbox' id="image-interstitial-scale-image" value="unchecked"/>      </br>
		                    <h2>Remove MRAID Close Button (NOTE: This close doesn't display in sample)</h2>
		                    <input type='checkbox' id="image-interstitial-remove-close" value="unchecked"/>
		                    </br>

		                    <button id="image-interstitial-button" class="btn-tools">Make Tag</button>
		                     <!-- onclick="makeZenWrapperTag();"  -->
		                    <h2>Output tag</h2>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-output-tag"></textarea>
		                </div>
		                </div>

		                <div class="container" id="dfaContainer">
		                <div style='max-width:70%'>
		                    <div class='iic-title'>ZenWrapper</div>
		                    <span><b>Please Note: Use any browser apart from Firefox.</b></span><br/>
		                    <span>This tool will allow conversion of ordinary DFA and Celtra tags into ZenWrapped DFA/Celtra tags. <br/>Will also provide means of changing a Zen Factor.</h2>
		                    <br/><br/>
		                    <div class='iic-subtitle'>DFA Tag Converter</div>
		                    <span>If Integral pixel needs to be added, please add in original DFA tag before converting.</span>
		                    <br/><br/>

		                    <div class='iic-subtitle'>DFA Tag</div>
		                    <textarea style="width:80%;outline:none;min-height:200px;" id="dfa-tag" placeholder="Input DFA tag here"></textarea>
		                    <div class='iic-subtitle'>Network</div>
		                    <select id='dfa-network'>
		                        <option value="or">Opera Response</option>
		                        <option value="omax">OMAX (AdMarvel)</option>
		                    </select>
		                    <div class='iic-subtitle'>Zen Factor (0 default)</div>
		                    <input type='number' id="dfa-zen-factor" value='0'/>
						</br><button id="dfa-wrap-button" class="btn-tools">Make Tag</button>

		                    <div class='iic-subtitle'>Output tag</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="dfa-output-tag"></textarea>
		                </div>
		                </div>

		                <div class="container" id="celtraContainer">
		                <div style='max-width:70%'>
		                    <div class='iic-title'>ZenWrapper</div>
		                    <span><b>Please Note: Use any browser apart from Firefox.</b></span><br/>
		                    <span>This tool will allow conversion of ordinary DFA and Celtra tags into ZenWrapped DFA/Celtra tags. <br/>Will also provide means of changing a Zen Factor.</h2>
		                    <br/><br/>
		                    <div class='iic-subtitle'>Celtra Tag</div>
		                    <textarea style="width:80%;outline:none;min-height:200px;" id="celtra-tag" placeholder="Input Celtra tag here"></textarea>
		                    <div class='iic-subtitle'>Integral Pixel</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="celtra-integral-pixel" placeholder="Input Integral pixel here" value=''></textarea>
		                    <div class='iic-subtitle'>Extra Impression Pixel</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="celtra-impression-pixel" placeholder="Input Impression pixel here"></textarea>
		                    <div class='iic-subtitle'>Extra Click Tracker</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="celtra-click-url" placeholder="Input Click tracker here"></textarea>
		                    <div class='iic-subtitle'>Network</div>
		                    <select id='celtra-network'>
		                        <option value="or">Opera Response</option>
		                        <option value="omax">OMAX (AdMarvel)</option>
		                    </select>
		                    <div class='iic-subtitle'>Zen Factor (0 default)</div>
		                    <input type='number' id="celtra-zen-factor" value='0'/>


						</br><button id="celtra-wrap-button" class="btn-tools">Make Tag</button>

		                    <div class='iic-subtitle'>Output tag</div>
		                    <textarea style="width:100%;outline:none;min-height:200px;" id="celtra-output-tag"></textarea>
		                </div>
		                </div>
				</div>
			</div>
			<img id="uploaded-image" />
		</div>
		<div class="clear"></div>
	</div>
@endsection
