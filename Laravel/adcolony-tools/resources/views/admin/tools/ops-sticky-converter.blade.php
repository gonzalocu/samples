<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script_bottom')
	<script>
		makeTag = function() {
			var closeTag="";
			var pretag="<div id='stickyAdContainer' style='position:fixed;bottom:0;width:320px;height:50px;left:0px;textAlign=center;lineHeight=initial;z-index:9999999999999;'>";
			var posttag="</div><scr"+"ipt>if(document.getElementsByClassName('advert--banner-wrap')){document.getElementsByClassName('advert--banner-wrap')[0].style.display = 'none';}; if(window.document.getElementById('adv1')){window.document.getElementById('adv1').childNodes[1].classList.remove('resp-banner');}; if(window.frameElement){try{window.frameElement.parentNode.style.margin='0';window.frameElement.parentNode.parentNode.style.position='fixed';window.frameElement.parentNode.parentNode.style.bottom='0px';window.frameElement.parentNode.parentNode.style.height='50px';window.frameElement.parentNode.parentNode.style.zIndex=999999999999;}catch(e){}}else{try{document.body.appendChild(document.getElementById('stickyAdContainer'));}catch(e){}}document.getElementById('stickyAdContainer').childNodes[0].style.margin = '0px auto';document.getElementById('stickyAdContainer').style.left = (parseInt(window.innerWidth) - parseInt(document.getElementById('stickyAdContainer').style.width))/2 + 'px';</scr"+"ipt>";

			if(document.getElementById('addClose').checked == true) {
				var closeTag="<img style=\"margin: 0px auto;position: absolute;z-index: 999999999999999;width: 30px;height: 30px;top: -15px;right: 3px;\"onload=\"if(window.frameElement){this.style.top=0;};\" src=\"http:\/\/4sa.s3.amazonaws.com\/common-assets\/images\/close_x2.png\" onclick=\"if(window.frameElement){try{window.frameElement.parentNode.parentNode.style.display='none';}catch(e){}}else{try{this.parentNode.style.display='none'}catch(e){}}\">";
			} else {
				var closeTag="";
			}

			var initialTag= document.getElementById("tag1").value;
			if(initialTag == 'paste your tag here') {
			} else {
				document.getElementById("tag2").value =  pretag + closeTag + initialTag + posttag;
			}
		}
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Sticky Converter</h3>
				</div>
  				<div class="panel-body">
					<input type="checkbox" id="addClose">Add Close Button<br>
						<textarea style="width:100%;outline:none;min-height:200px;" id="tag1" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your tag here"></textarea>
		                <button onclick="makeTag();" class='btn-tools'> Convert to Sticky tag</button>
		                <textarea style="width:100%;outline:none;min-height:200px;" id="tag2"></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
