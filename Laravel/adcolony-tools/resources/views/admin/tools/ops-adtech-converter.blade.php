<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			function escapeHtml(unsafe) {
				return unsafe
						.replace(/\//g, "\\\\/")
						.replace(/"/g, '\\\\\\"')
						.replace(/'/g, "\\\\'")
						.replace(/(\r\n|\n|\r)/gm,"")
			}

			makeTag = function ()
			{
				var initialTag = document.getElementById("tag1").value;
				document.getElementById("tag2").value = pretag + escapeHtml("<scr"+"ipt src='mraid.js'></scr"+"ipt>" + initialTag.replace('<scr'+'ipt src="http://admarvel.s3.amazonaws.com/js/admarvel_compete_v1.1.js"></scr'+'ipt>','')) + posttag;
			}
		}, false);
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">AdTech Converter</h3>
				</div>
				<div class="panel-body">
		            <textarea id="tag1" style="width:100%;outline:none;min-height:200px;" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your tag here."></textarea>

		            <button onclick="makeTag();" class='btn-tools'> Convert to AdTech tag</button>
		            <textarea id="tag2" style="width:100%;outline:none;min-height:200px;" rows="10" cols="144"></textarea>
		            <script>
		                var pretag = '{\r\n"offlineEnabled": "1",\r\n"refreshInterval": "20",\r\n"adSnippet": "document.write(\'';
		                var posttag = '\');",\r\n"animationType": "ANIMATION_NONE",\r\n"type": "mraid",\r\n"deliveryStatus": "success",\r\n"cacheable": "0",\r\n"cacheSize": "0"\r\n}';
		            </script>
		        </div>
			</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
