<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script type="text/javascript" src="/js/omw/ad-tools/dragresize.js"></script>
	<script type="text/javascript" src="/js/omw/ad-tools/underscore.js"></script>
	<script>
		var pretag= "<div id='forceCloseContainer' style='width:100%;height:100%;position:absolute;z-index:2;'><img id='forceCloseButton' src='http://4sa.s3.amazonaws.com/common-assets/images/close.png' style='top:2%;left:5%;width:6%;z-index:2147483638;position:absolute;' onclick='document.getElementById(\"forceCloseContainer\").style.display=\"none\";'><div style='position:relative;z-index:1'>";
		var posttag = "</div></div>";

		function escapeHtml(unsafe) {
			return unsafe
					.replace(/href=\"/gi, 'href=\"{clickurl}')
		}

		makeTag = function(){
			var initialTag= document.getElementById("tag1").value;
			document.getElementById("tag2").value = pretag + initialTag + posttag;
		}
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Interstitial Close Button Adder</h3>
				</div>
  				<div class="panel-body">
					<textarea style="width:100%;outline:none;min-height:200px;" id="tag1" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your tag here"></textarea>
		            <button onclick="makeTag();" class="btn-tools">Add close Button</button>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag2"></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
