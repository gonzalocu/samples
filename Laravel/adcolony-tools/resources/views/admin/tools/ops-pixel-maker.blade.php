<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script>
		var pretag = "<img src='",
			postTag = "'style='height:1px;width:1px;display:none'/>";

		function escapeHtml(unsafe) {
			return unsafe
					.replace(/href=\"/gi, 'href=\"{clickurl}')
		}

		makeTag = function(){
			var initialTag= document.getElementById("tag1").value;
			document.getElementById("tag2").value =  pretag + initialTag + postTag;
		}
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Pixel Maker</h3>
				</div>
  				<div class="panel-body">
					<textarea id="tag1" style="width:100%;outline:none;min-height:200px;" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your url here"></textarea>
		            <button onclick="makeTag();" class="btn-tools">Make Pixel</button>
		            <textarea id="tag2" style="width:100%;outline:none;min-height:200px;"></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
