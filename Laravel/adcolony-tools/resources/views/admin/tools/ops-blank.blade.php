<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link href="/css/omw/ad-tools/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/css/omw/ad-tools/bootstrap.min.css" rel="stylesheet">
    <link href="/css/omw/ad-tools/style.css" rel="stylesheet">
@endsection

@section('script')

@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title"></h3>
				</div>
  				<div class="panel-body">
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
