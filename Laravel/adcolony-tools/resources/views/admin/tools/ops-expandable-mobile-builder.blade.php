<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<style type="text/css">
	    /* Required CSS classes: must be included in all pages using this script */

	    /* Apply the element you want to drag/resize */
	    .drsElement {
	     position: absolute;
	     border: 1px solid #333;
	    }

	    /*
	     The main mouse handle that moves the whole element.
	     You can apply to the same tag as drsElement if you want.
	    */
	    .drsMoveHandle {
	     height: 20px;
	     background-color: #CCC;
	     border-bottom: 1px solid #666;
	     cursor: move;
	    }

	    /*
	     The DragResize object name is automatically applied to all generated
	     corner resize handles, as well as one of the individual classes below.
	    */
	    .dragresize {
	     position: absolute;
	     width: 5px;
	     height: 5px;
	     font-size: 1px;
	     background: #EEE;
	     border: 1px solid #333;
	    }

	    /*
	     Individual corner classes - required for resize support.
	     These are based on the object name plus the handle ID.
	    */
	    .dragresize-tl {
	     top: -8px;
	     left: -8px;
	     cursor: nw-resize;
	    }
	    .dragresize-tm {
	     top: -8px;
	     left: 50%;
	     margin-left: -4px;
	     cursor: n-resize;
	    }
	    .dragresize-tr {
	     top: -8px;
	     right: -8px;
	     cursor: ne-resize;
	    }

	    .dragresize-ml {
	     top: 50%;
	     margin-top: -4px;
	     left: -8px;
	     cursor: w-resize;
	    }
	    .dragresize-mr {
	     top: 50%;
	     margin-top: -4px;
	     right: -8px;
	     cursor: e-resize;
	    }

	    .dragresize-bl {
	     bottom: -8px;
	     left: -8px;
	     cursor: sw-resize;
	    }
	    .dragresize-bm {
	     bottom: -8px;
	     left: 50%;
	     margin-left: -4px;
	     cursor: s-resize;
	    }
	    .dragresize-br {
	     bottom: -8px;
	     right: -8px;
	     cursor: se-resize;
	    }
    </style>
	<style>
		#omw-tools-content input[type='text'] {
			border:1px solid;
			width:100%;
		}
	</style>
@endsection

@section('script_bottom')
	<script type="text/javascript" src="/js/omw/ad-tools/dragresize.js"></script>
	<script type="text/javascript" src="/js/omw/ad-tools/underscore.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script>

		 <script type="text/javascript">

		 window.isCollapsed = true;
		 window.collapsedImgUrl = '';
		 window.expandedImgUrl = '';


			 (function() {

			 var bar = $('.bar');
			 var percent = $('.percent');
			 var status = $('#status');

			 $('form').ajaxForm({
			 beforeSend: function() {
			 status.empty();
			 var percentVal = '0%';
			 bar.width(percentVal)
			 percent.html(percentVal);
			 },
			 uploadProgress: function(event, position, total, percentComplete) {
			 var percentVal = percentComplete + '%';
			 bar.width(percentVal)
			 percent.html(percentVal);
			 },
			 success: function() {
			 var percentVal = '100%';
			 bar.width(percentVal)
			 percent.html(percentVal);
			 },
			 complete: function(xhr) {

			 window.imgResponse = JSON.parse(xhr.responseText);
			/*  status.html(window.imgResponse.path); */

			 if(window.isCollapsed ==true){
			 document.getElementById('collapsed-container').style.backgroundImage = "url('"+window.imgResponse.path+"')";
			 window.collapsedImgUrl = window.imgResponse.path;
			 }else{
			 document.getElementById('drag-container').style.backgroundImage = "url('"+window.imgResponse.path+"')";
			 window.expandedImgUrl = window.imgResponse.path;
			 }

			 }
			 });

			 })();

			 makeTag = function(){

			 var urlTag= document.getElementById("tag1").value;

			 var width = document.getElementById("upload").width;
			 var height = document.getElementById("upload").height;

		   if(window.i){}

			 document.getElementById("tag2").value =  stBit + urlTag + ndBit + window.imgResponse.path + rdBit;

			 addIframe();
			 }

			 var stBit="<a href=\"";
			 var ndBit="\"target=\"_blank\"><img src=\"";
			 var rdBit="\"><\/a>";
			 var addIframe = function(){
			 var ifrm = document.getElementById('my-frame');
			 ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
			 ifrm.document.open();

			 ifrm.document.write(document.getElementById("tag2").value);

			 ifrm.document.close();
			 }

		 </script>
		 <script type="text/javascript">

		 window.adName = encodeURI(document.getElementById('adName').value);
		 window.touchPointCount = 0;
		 window.touchPoints = [];
		 window.bannerTracking = '';
		 window.closeButton = '';
		 window.mraidInc = '';
		 window.competeInc = "<scr" + "ipt src='http://admarvel.s3.amazonaws.com/js/admarvel_compete_v1.1.js'></scr" + "ipt>";

		 window.loadTag = function(){

			document.getElementById('tag2').value = window.mraidInc + "<div ad-container-id='"+window.adName+"'></div>" + window.competeInc + "<scr" + "ipt src='http://4sa.s3.amazonaws.com/ad-templates/expandable/generated/{partner_id}/{device_os}/expandable-ad-template.js' type='text/javascript'></scr" + "ipt><scr" + "ipt src='http://4sa.s3.amazonaws.com/expandables/generated-ads/"+window.adName+"/ad-unit.js' type='text/javascript'></scr"+"ipt><scr"+ "ipt type='text/javascr" + "ipt'>expandableAdUnit = new ExpandableAdUnit();expandableAdUnit.adId = '" + window.adName + "';expandableAdUnit.adEnvironment = 'production';expandableAdUnit.cacheBusterToken = '{timestamp}';expandableAdUnit.clickUrl = '{clickurl}"+window.bannerTracking+"';expandableAdUnit.adEventTracker = new AdMarvelAdEventTracker({partnerId:'{partnerid}', siteId: '{zoneid}', bannerId:'{bannerid}', targetParams: '{targetparams}', cacheBuster:'{timestamp}'});expandableAdUnit.collapsedAdWidth = 320;expandableAdUnit.collapsedAdHeight = 50;expandableAdUnit.expandedAdWidth = 300;expandableAdUnit.expandedAdHeight = 300;" + window.closeButton + "expandableAdUnit.initialiseTemplate();</scr"+"ipt>"
		 }

		 window.includeMraid = function(isChecked){

			if(isChecked != true){
				window.mraidInc = '';
				window.competeInc = "<scr" + "ipt src='http://admarvel.s3.amazonaws.com/js/admarvel_compete_v1.1.js'></scr" + "ipt>"
			}else{
				window.mraidInc = "<scr"+"ipt src='mraid.js'></scr"+"ipt>";
				window.competeInc = '';
			}

				if(document.getElementById('tag2').value != ''){
			window.loadTag();
			}

		 }


		 window.updateBannerTracking = function(tracking){

			window.bannerTracking = tracking.replace(/(\r\n|\n|\r)/gm,"");

			if(document.getElementById('tag2').value != ''){
			window.loadTag();
			}

		 }

		 window.addCloseButton = function(isChecked){

			if(isChecked != true){
				window.closeButton = '';
			}else{
				window.closeButton = "expandableAdUnit.addCloseButton = 'right';";
			}

			if(document.getElementById('tag2').value != ''){
			window.loadTag();
			}

		 }

		 var loadSample = function(){

				content = document.getElementById("tag2").value;
				content = content.replace("{partner_id}", "0d4dd9769e164713");
				content = content.replace("{device_os}", "iPhone OS");

				ifrm = document.getElementById('sampleFrame');
				ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
				ifrm.document.open();
				ifrm.document.write(content);
				ifrm.document.close();

		 }


		 var createTouchPoint = function(){


		 //Unpopulated Fields Handler

		 if(document.getElementById('adName').value == ''){

		 alert('Please name your ad');

			return;

		 }else if(document.getElementById('touchPointName').value == ''){

		 alert('Please add a Touch Point Name - this is required for tracking');

			return;

		 }else if(document.getElementById('touchPointURL').value.indexOf('http://') == -1){

			if(document.getElementById('touchPointURL').value.indexOf('webcal://') == -1){

				alert('Please include a "http://" or "webcal:" (if linking to iCals).');

				return;

			}

		 }else if(document.getElementById('touchPointURL').value == ''){

		 alert('Please add a URL - this is where the touch point will direct to.');

			return;

		 }else if(window.collapsedImgUrl == ''){

		 alert('Please Upload a Collapsed Image.');

			return;

		 }else if(window.expandedImgUrl == ''){

		 alert('Please Upload an Expanded Image.');

			return;
		 }


		 window.adName = encodeURI(document.getElementById('adName').value.replace(/(\r\n|\n|\r)/gm,""));

		 window.touchPointCount ++;

		 var div1 = document.createElement("div");
		 $(div1).attr( 'class', 'drsElement' );
		 $(div1).attr( 'id', 'touchPoint' + window.touchPointCount);
		 div1.style.left = "50px";
		 div1.style.top = "0px";
		 div1.style.width = "250px";
		 div1.style.height = "120px";
		 div1.style.textAlign = "center"


		 var div2 = document.createElement("div");
		 $(div2).attr( 'class', 'drsMoveHandle' );
		 div2.innerHTML = document.getElementById('touchPointName').value;

		 div1.appendChild(div2);
		 document.getElementById('drag-container').appendChild(div1);

		 window.touchPoints.push( { "id" : "touchPoint" + window.touchPointCount, "x":0,"y":0,"width":0,"height":0, "name": encodeURIComponent(document.getElementById('touchPointName').value.replace(/(\r\n|\n|\r)/gm,"")), "url": encodeURIComponent(document.getElementById('touchPointURL').value.replace(/(\r\n|\n|\r)/gm,"")) } )

		 //clear Text areas

		 document.getElementById('touchPointName').value = '';

		 document.getElementById('touchPointURL').value = '';


		 /* alert(window.touchPoints[].id) */

		 }

		 var dragresize = new DragResize('dragresize',
		  { minWidth: 50, minHeight: 50, minLeft: 0, minTop: 0, maxLeft: 300, maxTop: 300 });

		 // Optional settings/properties of the DragResize object are:
		 //  enabled: Toggle whether the object is active.
		 //  handles[]: An array of drag handles to use (see the .JS file).
		 //  minWidth, minHeight: Minimum size to which elements are resized (in pixels).
		 //  minLeft, maxLeft, minTop, maxTop: Bounding box (in pixels).

		 // Next, you must define two functions, isElement and isHandle. These are passed
		 // a given DOM element, and must "return true" if the element in question is a
		 // draggable element or draggable handle. Here, I'm checking for the CSS classname
		 // of the elements, but you have have any combination of conditions you like:

		 dragresize.isElement = function(elm)
		 {
		  if (elm.className && elm.className.indexOf('drsElement') > -1) return true;
		 };
		 dragresize.isHandle = function(elm)
		 {
		  if (elm.className && elm.className.indexOf('drsMoveHandle') > -1) return true;
		 };


		 dragresize.ondragfocus = function() { };
		 dragresize.ondragstart = function(isResize) { };
		 dragresize.ondragmove = function(isResize) { };
		 dragresize.ondragend = function(isResize) { };
		 dragresize.ondragblur = function() { };

		 dragresize.apply(document);

		 var sendJSON = function(){

		 if (window.touchPoints.length == 0){

		 alert('please add a touch point!');

		 return;

		 }

		 var ifrm = document.getElementById('sampleFrame');
							ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
							ifrm.document.open();
							ifrm.document.write("<img src='http://4sa.s3.amazonaws.com/common-assets/images/loading.gif' width='100%'>");
							ifrm.document.close();



		 var finalParams = '';
		 var touchPoints = [];

		 finalParams += encodeURIComponent(window.collapsedImgUrl) + '/';
		 finalParams += encodeURIComponent(window.expandedImgUrl) + '/';
		 finalParams += encodeURIComponent(window.adName) + '/';


			_.each(window.touchPoints, function(obj){

			obj.x = parseInt(document.getElementById(obj.id).style.left);
			obj.y = parseInt(document.getElementById(obj.id).style.top);
			obj.width = parseInt(document.getElementById(obj.id).style.width);
			obj.height = parseInt(document.getElementById(obj.id).style.height);

			 touchPoints.push(obj);

				})

			finalParams += JSON.stringify(touchPoints);

			console.log(finalParams);

		 //Node error handler

		 $.ajaxSetup({
		   error: function(xhr, status, error) {

			var img = new Image(1,1);
			img.src = 'https://logs-01.loggly.com/inputs/2b59a37d-55fa-4553-826f-f6b251bfbc7c.gif?expandableBuilderNodeerror=true';
			 alert("An error occured with the server: " + status + "\nError: " + error +'your administrator has been alerted.');
		   }
		 });


		 $.get("http://10.100.11.100:2500/make-expandable/" + finalParams ,function(data,status){


		 if(status == 'success'){

		 window.loadTag();
		 loadSample();

			return
		 }else{
			var img = new Image(1,1);
			img.src = 'https://logs-01.loggly.com/inputs/2b59a37d-55fa-4553-826f-f6b251bfbc7c.gif?expandableBuilders3error=true';
			alert('There was an error uploading the ad to the s3, your administrator has been alerted.')
		 }

		   });


		   //window.loadTag();
			//sendImg.src='localhost:3500/make-expandable/' + finalParams;

		 }


		 //<![CDATA[

		 // Using DragResize is simple!
		 // You first declare a new DragResize() object, passing its own name and an object
		 // whose keys constitute optional parameters/settings:

		 // You can define optional functions that are called as elements are dragged/resized.
		 // Some are passed true if the source event was a resize, or false if it's a drag.
		 // The focus/blur events are called as handles are added/removed from an object,
		 // and the others are called as users drag, move and release the object's handles.
		 // You might use these to examine the properties of the DragResize object to sync
		 // other page elements, etc.


		 // Finally, you must apply() your DragResize object to a DOM node; all children of this
		 // node will then be made draggable. Here, I'm applying to the entire document.


		 //]]>

		 </script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Expandable Mobile Builder</h3>
				</div>
  				<div class="panel-body">
					<form action="http://54.229.56.139:2001/image-upload" method="post" enctype="multipart/form-data">
		                <input type="file" name="image">
		                <input type="submit" value="Upload Collapsed Img" class="btn-tools" id="upload" onclick="window.isCollapsed = true;">
		                <input type="submit" value="Upload Expanded Img" class="btn-tools" id="upload" onclick="window.isCollapsed = false;">
		            </form>

		            <div class="progress">
		                <div class="bar progress-bar"></div>
		                <div class="percent"></div>
		            </div>

		           <!--  <div id="status"></div> -->

		            <!-- <button onclick="makeTag();" class="btn btn-default make-tag">Make Tag</button>  -->
		            Ad Name
					<br/>
		            <input type="text" id="adName" placeholder="Please enter the: Ad Name">
					<br/>
		            Touch Point Name (this will be the name used in AdMarvel's Event Tracking):
					<br/>
		            <input  type="text" id="touchPointName" placeholder="Please enter the : Touch Point Name">
					<br/>
					Touch Point URL:
					<br/>
		            <input  type="text" id="touchPointURL" placeholder="Please enter the : Touch Point URL">
		            </br>
		            <button type="button" onclick="createTouchPoint()" class="btn-tools">Create New Touch Point</button>
		            </br></br></br>
		            Collapsed
		            <div id="collapsed-container" style="left: 35%; top: 150px; width: 300px; height: 50px; border:1px solid black; background-repeat: no-repeat; background-size: 100% 100%;"></div>

		            <input type="checkbox" onclick="window.addCloseButton(this.checked)">Add Close Button<br>

		            <input type="checkbox" onclick="window.includeMraid(this.checked)">Check if this tag is for Sky<br>

		            Expanded
		            <div id="drag-container" style="position: relative; left:0;top:0; width: 300px; height: 300px; border:1px solid black; background-repeat: no-repeat; background-size: 100% 100%;">
						<!--
							<div class="drsElement" position="relative" style=" position: relative; left: 50px; top: 150px; width: 250px; height: 120px; background: #CDF; text-align: center;">
								<div class="drsMoveHandle">Div 0</div>
		             			Content
		            		</div>
						-->
		            </div>
					<br/>
		            <button type="button" onclick="sendJSON()" class="btn-tools">GO!</button>

		            <h2>Banner Tap Tracking</h2>
		            <form action="#" id="form_field">
		            <input type="text" id="textfield1" value="" onKeyUp="window.updateBannerTracking(this.value);">
		            </form>


		            <h2>Output (tag)</h2>
		            <textarea id="tag2" style="width:100%;outline:none;min-height:200px;"></textarea>



		            <h2>Sample</h2>
		            <iframe id="sampleFrame" height=" 480px" width="300px" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0" style="border:1px solid black;"></iframe>
					<br/>
					<button type="button" onclick="loadSample();" class="btn-tools">Reload</button>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
