<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script>
		function escapeHtml(unsafe) {
			return unsafe.replace(/href=\"/gi, 'href=\"{clickurl}')
		}

		function makeTag() {
			var initialTag= document.getElementById("tag1").value;
			document.getElementById("tag2").value =  pretag + initialTag;
		}
		var pretag="<scr" + "ipt src='http://admarvel.s3.amazonaws.com/js/admarvel_v1.1.js'></scr" + "ipt><scr" + "ipt>function initCallback(){}admarvelInit('initCallback');try{document.body.style.margin = 0;}catch(e){};</scr"+"ipt>";
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Admarvel Initialisation</h3>
				</div>
  				<div class="panel-body">
					<textarea id="tag1" style="width:100%;outline:none;min-height:200px;"  onfocus="document.getElementById('tag1').value='';" placeholder="Paste your tag here"></textarea>
					<button onclick="makeTag();" class="btn-tools">Convert to DART tag</button>
					<textarea id="tag2" style="width:100%;outline:none;min-height:200px;" ></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
