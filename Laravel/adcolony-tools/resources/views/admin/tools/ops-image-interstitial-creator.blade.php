<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
    <script src="/js/omw/ad-tools/ZeroClipboard.min.js"></script>
	<script src="/js/omw/ad-tools/jQuery.AjaxFileUpload.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
@endsection

@section('script_bottom')
    <script src="/js/omw/ad-tools/zen-wrapper.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Image Interstitial Creator</h3>
				</div>
  				<div class="panel-body">

					<div onclick="ZenWrapper.showHideSample(this);" class="active" id="show-hide-sample" style="display:none;">Hide Sample</div>
		            <div id='iframe-container' style="margin-top:20px;position:absolute;z-index:0;top:-100px;right:-90px;">
		                <iframe class="sample-iframe" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0"></iframe>
		                <div id="celtra-not-available">Sample preview for Celtra will not work unfortunately</div>
		                <div id="offnetwork-not-available">Sample preview for Off-Network Tag will not work unfortunately</div>
		            </div>
		            <style>
		                .iic-title {
		                    font-size:30px;
		                }
		                .iic-subtitle {
		                    font-size:20px;
		                }
		            </style>
		    	    <div class="" id="imageInterstitialContainer">

		                <div style='max-width:"70%"'>
							<br/>
		    	    	        <span><b>Please Note: Use any browser apart from Firefox.</b></span><br/>
		                        <span>This tool will build an image interstitial with impression, click and integral tracking if needed. <br/> Will also scale image if required or center image allowing choice of background colour.</span>
		                        <br/><br/>
		                </div>

						<br/>
		    	        <div class='iic-subtitle'>Interstitial Type (ZenWrapped?)</div>
		    			    <select id='interstitial-menu-dropdown'>
		    		        	<option value="nonZenWrapped" selected>Build Normal Interstitial</option>
		    		        	<option value="zenWrapped">Build ZenWrapped Interstitial</option>
		    		    	</select>
		                    <br/><br/>
		    	        	 <div class='iic-subtitle'>Upload Image URL</div>

		    	            <!-- <form action="http:\/10.100.11.100/interstitials/image-upload" method="post" enctype="multipart/form-data"> -->
		    	            <form action="http://54.229.56.139:2001/image-upload" method="post" enctype="multipart/form-data">
		    	                <input type="file" name="image">
		    	                <input type="submit" value="Upload File to Server" class="btn-tools" id="upload">
		    	            </form>
		                    <br/>
		    	            <div class="progress" style="width:40%;">
		    	                <div class="bar progress-bar"></div>

		    	                <div class="percent"></div>
		    	            </div>

		    	            <div id="status"></div>
		    	            <div class='iic-subtitle'>or enter Image URL here</div>
		    	            <textarea style="width:100%;outline:none;" id="image-interstitial-image-url" placeholder="Paste your URL here"></textarea>
		    	            <div class='iic-subtitle'>Impression URL</div>
		    	            <textarea style="width:100%;outline:none;" id="image-interstitial-impression-url" placeholder="Paste your URL here"></textarea>
		    	            <div class='iic-subtitle'>Click URL</div>
		    	            <textarea style="width:100%;outline:none;" id="image-interstitial-click-url" placeholder="Paste your URL here"></textarea>
		    	            <div class='iic-subtitle'>Integral(extra) Tracking</div>
		    	            <textarea id="image-interstitial-integral-url" style="width:100%;outline:none;" placeholder="Paste your URL here"></textarea>
		    	            <div class='iic-subtitle'>Network</div>
		    	            <select id='image-interstitial-network'>
		                    	<option value="or">Opera Response</option>
		                    	<option value="omax">AdMarvel (OMAX)</option>
		                	</select>
		    	            <div id="zen-h2" class='iic-subtitle'>Zen Factor (0 default)</div>
		    	            <input type='number' id="image-interstitial-zen-factor" value='0'/>
		    	            <div class='iic-subtitle'>Background colour (NOTE: This feature does not work in Safari/IE)</div>
		    	            <input type='color' background-name='colour'  id="image-interstitial-background-colour" value='#ffffff'/>
		    	            <!-- <input class="jscolor" value="ffffff" id="image-interstitial-background-colour" /> -->
		                    <div style='clear:both;'></div>
		    	            <div class='iic-subtitle' style='float:left;'>Scale Image&nbsp;</div>
		    	            <input type='checkbox' style='float:left;' id="image-interstitial-scale-image" value="unchecked"/>      </br>
		                    <div style='clear:both;'></div>
		    	            <div class='iic-subtitle' style='float:left;'>Add Close Button (NOTE: This button doesn't display in sample)&nbsp;</div>
		    	            <input type='checkbox' style='float:left;' id="image-interstitial-remove-close" value="unchecked"/>
		    				</br>
		                    <div style='clear:both;'></div>

		    	             <!-- onclick="makeZenWrapperTag();"  -->
							 <br/>
		    	            <div class='iic-subtitle'>Output tag</div>
		    	            <textarea style="width:100%;outline:none;min-height:200px;" id="image-interstitial-output-tag"></textarea>
		    				<button id="image-interstitial-button" class="btn-tools">Make Tag</button>

		    	    </div>
		    	    <img id="uploaded-image" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
