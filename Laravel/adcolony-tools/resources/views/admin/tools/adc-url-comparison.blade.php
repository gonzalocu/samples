<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')

	<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
	<link href="/js/omw/tools/styles.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/projects/stimenu.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/css/style.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/cloud-zoom/cloud-zoom.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/fancybox/jquery.fancybox-1.3.4.css" />
	<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/aboutme/css/default.css" />
	<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://v-fab.com/peter/css/admincss.css' type='text/css' media='all' />
	<link rel='stylesheet' id='lightboxStyle-css'  href='http://v-fab.com/peter/css/colorbox.css' type='text/css' media='screen' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

	<script src='http://v-fab.com/peter/js/jquery.tools.min.js?ver=3.0.4'></script>
	<script src="http://v-fab.com/peter/js/include.js"></script>
	<script src="http://v-fab.com/peter/js/jquery.cycle.all.min.js"></script>

	<script src='/js/omw/tools/tracking.js'></script>
	<script src='/js/omw/tools/compare_urls.js'></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">URL comparison Tool</h3>
				</div>
  				<div class="panel-body">
				{{-- <div class='iic-title'>URL comparison Tool</div><br/>
			<h3 class="black" id="geturl_title">URL comparison Tool &nbsp;&nbsp;</h3> --}}
			<input type="button" class="btn-tools" value="Compare URLs" id="comp_url_toggle">
			<div style="clear:both;"></div>

			<div id="geturl1">
				Paste the URL below:
				<br />
				<form id="valform">
					<textarea name="input_url" id="input_url"></textarea>
					<input type="button" class="btn-tools" value="Check URL" onclick="checkURLgen('input_url')">

					<input type="button" class="btn-tools" value="Get parameters" onclick="getURLparams('input_url',urlsplitdiv)">
					<input type="button" class="btn-tools" value="Sort parameters" onclick="getURLparamsSort('input_url',urlsplitdiv)">
					<br />
					<br />
				</form>
				<div id="urlsplitdiv"></div>
			</div>

			<div id="geturl2">
				Paste the URL below:
				<br />
				<form id="valform2">
					<textarea name="input_url2" id="input_url2"></textarea>
					<input type="button" class="btn-tools" value="Compare!" id="compare_button" onclick="compareURLs1()">
					<input type="button" class="btn-tools" value="Compare and sort A-Z!" id="compare_button" onclick="compareURLs2()">
					<br />
					<br />
				</form>
				<div id="urlsplitdiv2"></div>
			</div>
			<div style="clear: both;"></div>

			<div id="samediv">
				<span id="samealert"></span>
			</div>

			<div id="keydiv" style="display: none;">
				<span style="font-weight: bold; color: black; font-size: 16px;">Key:</span>
				<br />
				<table id="keytable" style="margin-top: 5px;">
					<tr>
						<td style='color: green; background-color: #DFD;'>Green:</td>
						<td>All good!</td>
					</tr>
					<tr>
						<td style='color: #C90; background-color: #FFE9AD;'>Orange:</td>
						<td>Same parameter, different value</td>
					</tr>
					<tr>
						<td style='color: #D00; background-color: #FFADB1;'>Red:</td>
						<td>Different parameter / base URL</td>
					</tr>
				</table>
				<br />
			</div>

			<div id="redurldiv"></div>
			<div id="vnotes_comment"></div>
			<div id="vnotes_alert"></div>

			<h3 class="black" id="uninh3">Uninstall process on Android</h3>
			<ol style="list-style: inherit;">
				<li>Uninstall the app</li>
				<li>Accounts -> remove the google account</li>
				<li>reset the google ad id (GAID)</li>
				<li>kill google play store (clear data)</li>
				<li>kill the test app (clear data)</li>
			</ol>
			<br />

			{{-- <h3 class="black">Useful links</h3>

			<a class="blue" href="https://docs.google.com/document/d/1x3ABDVaR-Xy5lnCxdr9ej6DHT_ol-Il62pnXEd_6dak/edit" target="_blank">Jessi's - Partner notes</a>
			<br />
			<a class="blue" href="https://docs.google.com/spreadsheets/d/1iD3ezrKDVpiITO7VGhLsymI2y1NxdE54eAJJ-smPFVQ/edit#gid=0" target="_blank">Jessi's - List of tracking parameters</a>
			<br />
			<a class="blue" href="https://docs.google.com/spreadsheets/d/1T5uFxml-w-74V8Yro6C8RKEbaVkk9VfLJspKt6uny2w/edit#gid=0" target="_blank">Jessi's - Tracking Partners + View Through</a>
			<br />
			<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ua/macros-external-document" target="_blank">MACROs</a>
			<br />
			<a class="blue" href="https://docs.google.com/spreadsheets/d/1uExJLD_Ql7EjZz2IIJnv02-Y3nMoKZWJVnKAqy7yqd4/edit?ts=569446e4#gid=0" target="_blank">API keys</a>
			<br />
			<a class="blue" href="http://cpa.adtilt.com/most_recent_actions?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=850404112" target="_blank">CPA tool (postback example)</a>
			<br />
			<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">3rd party dashboards</a>
			<br />
			<a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/vpn-to-another-country" target="_blank">VPN to Another Country</a>
			<br />
			<a class="blue" href="https://docs.google.com/spreadsheets/d/1ZUL3K6kqQYzqwDglnRirUPHJer1fxraDWSkgdha-dyg/edit?ts=569e97f2#gid=0" target="_blank">Partner Speadsheet</a>
			<br />
			<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners" target="_blank">Post Install Events for Top Tracking Partners</a>
			<br />
			<a class="blue" href="http://cpa.adcolony.com/is_blacklisted?api_key=bb2cf0647ba654d7228dd3f9405bbc6a&product_id=834393815&device_id=" target="_blank">Blacklist URL</a>
			<br />
			<a class="blue" href="https://docs.google.com/spreadsheets/d/1TDx5lXO57IbAigODRN0miueYbI3-cswGBKDesQ14blY/edit?ts=568cf7a4#gid=0" target="_blank">Login details of different accounts</a>
			<br />
			<br />
			<div class="clear"></div> --}}
			</div>
		</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
