<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')

	<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
	<link href="/js/omw/tools/styles.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/projects/stimenu.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/css/style.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/cloud-zoom/cloud-zoom.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/fancybox/jquery.fancybox-1.3.4.css" />
	<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/aboutme/css/default.css" />
	<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://v-fab.com/peter/css/admincss.css' type='text/css' media='all' />
	<link rel='stylesheet' id='lightboxStyle-css'  href='http://v-fab.com/peter/css/colorbox.css' type='text/css' media='screen' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

	<script src='http://v-fab.com/peter/js/jquery.tools.min.js?ver=3.0.4'></script>
	<script src="http://v-fab.com/peter/js/include.js"></script>
	<script src="http://v-fab.com/peter/js/jquery.cycle.all.min.js"></script>

	<script src='/js/omw/tools/tracking.js'></script>
	<script src='/js/omw/tools/compare_urls.js'></script>

	<script type="text/javascript" src="http://v-fab.com/peter/adcolony/tracking/king/king.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">King.com</h3>
				</div>
				<div class="panel-body">
			<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
			<div id="notesdiv" class="ndiv">
				We take the "targetAppId" from the URL and use the same URLs we have for the existing campaigns in the dash.
				<br />
				Check the redirect and make sure the link will open a legit page (No server errors etc...).
				<br /><br />
				<h3 class="black">Example URLs:</h3>
				Click:<br />
				http://play.king.com/click?type=ad&idfa_raw=[IDFA]&network=adcolony&os=[PLATFORM]&product_id=[PRODUCT_ID]&raw_advertising_id=[IDFA]&appId=[STORE_ID]&targetAppId=76&countryCode=[COUNTRY_CODE]&publisher=[PUBLISHER_ID]&site=[APP_ID]&campaignName=[RAW_AD_CAMPAIGN_ID]&creativeName=[AD_CREATIVE_NAME]&creativeSize=&noRedirect=TRUE&bypassFingerprint=true
				<br /><br />
				Impression:<br />
				http://play.king.com/impression?type=video&idfa_raw=[IDFA]&network=adcolony&os=[PLATFORM]&product_id=[PRODUCT_ID]&raw_advertising_id=[IDFA]&appId=[STORE_ID]&targetAppId=76&countryCode=[COUNTRY_CODE]&publisher=[PUBLISHER_ID]&site=[APP_ID]&campaignName=[RAW_AD_CAMPAIGN_ID]&creativeName=[AD_CREATIVE_NAME]&creativeSize=&noRedirect=TRUE&bypassFingerprint=true
				<br /><br />

			</div>

			<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
			<div id="valdiv">
				Paste the click URL below:
				<br />
				<form id="valform">
					<textarea name="king_url" id="king_url"></textarea>
					<br />
					<input type="button" class="btn-tools" value="Validate" onclick="Validateking()">
					<br /><br />
				</form>
				<h3 class="black plink" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
				<div id="vnotes">
					<div id="redurldiv"></div>
					<div id="vnotes_comment"></div>
					<div id="vnotes_alert"></div>
				</div>
				<div class="clear"></div>
			</div>

			<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
			<div id="pbdiv" class="ndiv">
				They don't have a dashboard - we don't have to check the postback. (King has set up a global postback.)
				<br /><br />
			</div>

			<h3 class="black plink" id="imptitle">4. Click / Impression URLs <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
			<div id="impdiv">
				<form>
					<fieldset id="king_output">
						<textarea name="king_imp" id="king_imp"></textarea>
						<br />
					</fieldset>
				</form>
				<br />
			</div>

			<h3 class="black plink" id="vttitle">5. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
			<div id="vtdiv">
				<table id="vt_table">
					<tr>
						<th colspan="2">VIEW ATTRIBUTION</th>
						<th colspan="3">LOOKBACK WINDOWS</th>
						<th>CLIENT-SIDE ACTION</th>
					</tr>
					<tr class="tep" id="tpid">
						<th>Dedicated View Tags</th>
						<th>Recommended Implementation</th>
						<th style="min-width: 120px;">Flexible Lookback Window</th>
						<th>Default Click Lookback</th>
						<th>Default View Lookback</th>
						<th></th>
					</tr>
					<tr>
						<td>Yes</td>
						<td class="lefta">View tag goes on complete, click tag goes on HTML5.</td>
						<td>No</td>
						<td>7 days</td>
						<td>12 hours</td>
						<td class="lefta">No action necessary.<br />Can be enabled at app level.</td>
					</tr>
				</table>
				<br />
			</div>
			<h3 class="black plink" id="pietitle">6. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
			<div id="piediv">
				King will set up the PIE events on their side.<br/><br/>
			</div>
			</div>
		</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
