<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
<style>
	.column-left {
			position:relative;
			float:left;
			width: 49%;
	}

	.column-right {
			position:relative;
			float:right;
			width: 49%;
	}

	.column-right {

	}

	input[type='text'] {
		width:100%;
		outline:none;
		border: 1px solid;
	}

	input[type='text'].red {
		background-color: #ffe6e6;
	}

	input[type='text'].green {
		background-color: #ebfaeb;
	}

</style>
@endsection

@section('script')
<script>

	$( document ).ready(function() {
		var ids = ['campaign_id', 'campaign_name', 'impressions', 'cvvs', 'total_clicks', 'ctr', 'spend', 'total_campaign_spend_limit', 'ecpi', 'installs'];

		function fillForm () {
			for (var x in data) {
				if (data[x]['campaign_id'] == $('#select_ac_campaign').val()){
					var selectedItem = data[x];
					for(var i in ids) {
						$('.column-left #' + ids[i]).val(selectedItem[ids[i]]);
					}
				}
			}
			var selectedGroups = [];
			for (var x in data_group) {
				if (data_group[x]['campaign_id'] == $('#select_ac_campaign').val()){
					selectedGroups.push(data_group[x]);
				}
			}

			console.log(selectedGroups);
		}

		function cleanForm () {
			for(var i in ids) {
				$('.column-right #' + ids[i]).val('');
				$('.column-right #' + ids[i])[0].className = '';
				$('.column-left #' + ids[i])[0].className = '';
			}
		}

		$('#select_ac_campaign').on('change', function() {
			cleanForm();
			fillForm();
		});

		$('.column-right input[type="text"]').on('change', function() {
				if (this.value == '') {
					$('.column-left #' + this.id)[0].className = '';
					$('.column-right #' + this.id)[0].className = '';
				} else {
					if ($('.column-left #' + this.id).val() == $('.column-right #' + this.id).val()) {
						$('.column-left #' + this.id)[0].className = 'green';
						$('.column-right #' + this.id)[0].className = 'green';
					} else {
						$('.column-left #' + this.id)[0].className = 'red';
						$('.column-right #' + this.id)[0].className = 'red';
					}
				}
		});

		fillForm();
	});
</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Ad Colony Validation</h3>
				</div>
				<div class="panel-body">
					Campaigns
					{!! Form::select('select_ac_campaign',  DB::table('om_ac_campaign')->orderBy('campaign_name')->pluck('campaign_name', 'campaign_id'), null, array('id' => 'select_ac_campaign')) !!}
					<br/>
					<div class="column-left">
						{!! Form::label('campaign_id','Campaign ID') !!}
						{!! Form::text('campaign_id', '', array('id'=>'campaign_id', 'readonly'=>'true')) !!}

						{!! Form::label('campaign_name','Campaign Name') !!}
						{!! Form::text('campaign_name', '', array('id'=>'campaign_name', 'readonly'=>'true')) !!}

						{!! Form::label('impressions','Impressions') !!}
						{!! Form::text('impressions', '', array('id'=>'impressions', 'readonly'=>'true')) !!}

						{!! Form::label('cvvs','CVVS') !!}
						{!! Form::text('cvvs', '', array('id'=>'cvvs', 'readonly'=>'true')) !!}

						{!! Form::label('total_clicks','Total Clicks') !!}
						{!! Form::text('total_clicks', '', array('id'=>'total_clicks', 'readonly'=>'true')) !!}

						{!! Form::label('ctr','CTR') !!}
						{!! Form::text('ctr', '', array('id'=>'ctr', 'readonly'=>'true')) !!}

						{!! Form::label('spend','Spend') !!}
						{!! Form::text('spend', '', array('id'=>'spend', 'readonly'=>'true')) !!}

						{!! Form::label('total_campaign_spend_limit','Total Campaign Spend Limit') !!}
						{!! Form::text('total_campaign_spend_limit', '', array('id'=>'total_campaign_spend_limit', 'readonly'=>'true')) !!}

						{!! Form::label('ecpi','ECPI') !!}
						{!! Form::text('ecpi', '', array('id'=>'ecpi', 'readonly'=>'true')) !!}

						{!! Form::label('installs','Installs') !!}
						{!! Form::text('installs', '', array('id'=>'installs', 'readonly'=>'true')) !!}
					</div>

					<div class="column-right">
						{!! Form::label('campaign_id','Campaign ID') !!}
						{!! Form::text('campaign_id', '', array('id'=>'campaign_id', 'placeholder' => 'Campaign ID')) !!}

						{!! Form::label('campaign_name','Campaign Name') !!}
						{!! Form::text('campaign_name', '', array('id'=>'campaign_name', 'placeholder' => 'Campaign Name')) !!}

						{!! Form::label('impressions','Impressions') !!}
						{!! Form::text('impressions', '', array('id'=>'impressions', 'placeholder' => 'Impressions')) !!}

						{!! Form::label('cvvs','CVVS') !!}
						{!! Form::text('cvvs', '', array('id'=>'cvvs', 'placeholder' => 'CVVS')) !!}

						{!! Form::label('total_clicks','Total Clicks') !!}
						{!! Form::text('total_clicks', '', array('id'=>'total_clicks', 'placeholder' => 'Total Clicks')) !!}

						{!! Form::label('ctr','CTR') !!}
						{!! Form::text('ctr', '', array('id'=>'ctr', 'placeholder' => 'CTR')) !!}

						{!! Form::label('spend','Spend') !!}
						{!! Form::text('spend', '', array('id'=>'spend', 'placeholder' => 'Spend')) !!}

						{!! Form::label('total_campaign_spend_limit','Total Campaign Spend Limit') !!}
						{!! Form::text('total_campaign_spend_limit', '', array('id'=>'total_campaign_spend_limit', 'placeholder' => 'Total Campaign Limit')) !!}

						{!! Form::label('ecpi','ECPI') !!}
						{!! Form::text('ecpi', '', array('id'=>'ecpi', 'placeholder' => 'ECPI')) !!}

						{!! Form::label('installs','Installs') !!}
						{!! Form::text('installs', '', array('id'=>'installs', 'placeholder' => 'Install')) !!}
					</div>

					Group
					{!! Form::select('group_type', array() , 'group_id') !!}

					{{-- Group
					{!! Form::select('group_type', DB::table('om_ac_group')->orderBy('group_name')->pluck('group_name', 'group_id')) !!} --}}
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	@include ('footer')
@endsection
