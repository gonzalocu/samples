<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<style>
		.iic-title {
			font-size:30px;
		}
		.iic-subtitle {
			font-size:20px;
		}
	</style>
@endsection

@section('script')

@endsection

@section('script_bottom')
	<script src="/js/omw/ad-tools/script.js" ></script>
	<script>
		window.loadPubDet = function(){
			window.createNetworkSelect('networkList');
		}

		window.errorPubDet = function(){
			alert('Pub file doesn\'t exist or has incorrect ACL settings!!!');
		}

		var fileref=document.createElement('script');
		fileref.setAttribute("type","text/javascript");
		fileref.setAttribute("src", "/js/omw/ad-tools/pubDet.js?" + (new Date())/7);
		document.body.appendChild(fileref)
		fileref.onload = window.loadPubDet;
		fileref.onerror = window.errorPubDet;
	</script>
	<script src="/js/omw/ad-tools/Demo.js" ></script>
	<script src="/js/omw/ad-tools/FileSaver.js" ></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Off Network Tag Creator</h3>
				</div>
  				<div class="panel-body">

					            <div class='iic-subtitle'>Ad-Network</div>
					            <div id="networkListHolder"></div>
					            <br>

					            <div class='iic-subtitle'>Publisher</div>
					            <div  id="pubListHolder"></div>
					            <br>

					            <div class='iic-subtitle'>Wrapped?</div>

					            <br>
					            <select name="interstitial" class="form-control" id="interstitial" onchange="window.interstitial=this.value;window.setParamValues();">
					              <option value="no">no</option>
					              <option value="yes">yes</option>
					            </select>

					            <div class='iic-subtitle'>Campaign</div>
					            <input type="text" id="campaign" style="border: 1px solid;width: 100%;" placeholder="Insert Campaign Name Here" onchange="window.campaign=this.value;window.setParamValues();">
					            <div class='iic-subtitle'>Environment/OS</div>


					            <br>
					            <select name="environment" class="form-control" id="environment" onchange="window.environment=this.value;window.setParamValues();">
					              <option value="app">app</option>
					              <option value="msite">msite</option>
					              <option value="general">general</option>
					            </select>

					            <div class='iic-subtitle'>Device</div>
					            <br>
					            <select class="form-control" id="deviceSelect" onchange="window.device=this.value;window.setParamValues();">
					              <option value="smartphone">smartphone</option>
					              <option value="feature">feature</option>
					              <option value="tablet">tablet</option>
					              <option value="mobile">mobile</option>
					            </select>

					            <div class='iic-subtitle'>Size</div>
					            <br>
					            <select class="form-control" id="size" onchange="window.size=this.value;window.setParamValues();">
					              <option value="300x50">300x50</option>
					              <option value="320x50">320x50</option>
					              <option value="320x480">320x480</option>
					              <option value="320x568">320x568</option>
					              <option value="15secs">15secs</option>
					              <option value="30secs">30secs</option>
					              <option value="responsive">responsive</option>
					            </select>

					            <div class='iic-subtitle'>Format</div>
					            <br>
					            <select class="form-control" id="format" onchange="window.format=this.value;window.setParamValues();">
					              <option value="standard">standard</option>
					              <option value="autoexpand">autoexpand</option>
					              <option value="video">video</option>
					            </select>

					            <div class='iic-subtitle'>SDK</div>

					            <div id="sdkListHolder"></div>
					            <br>

					            <div class='iic-subtitle'>Device Id</div>
					            <input type="text" style="border: 1px solid;width: 100%;" id="devId" onchange="window.updateTextArea();">


					            <!-- warning div for custom click macro -->
					            <div id="clickURLWarn" style="color:red;"></div>


					            <div class='iic-subtitle'>Click URL</div>
					            <input type="text" id="clickUrl" style="border: 1px solid;width: 100%;" onchange="if(this.value != window.clickURL){document.getElementById('clickURLWarn').innerHTML = '!!!WARNING!!! YOU ARE USING A CUSTOM CLICKURL!!!';}else{document.getElementById('clickURLWarn').innerHTML =''};window.updateTextArea();">

					            <div class='iic-subtitle'>Extra</div>
					            <input type="text" id="extra" style="border: 1px solid;width: 100%;" onchange="window.updateTextArea();">
					            <br>

					            <div class='iic-subtitle'>Number of Encodes:</div>
					            <input type="text" id="encode" style="border: 1px solid;width: 100px;" onchange="window.numberOfEncodes = this.value;window.updateTextArea();">
					            <br>
					            <div class='iic-subtitle'>Mraid Script Tag:</div>
					            <p><input type="checkbox" id="mraidTag"  onchange="if(window.mraidTagValue == ''){window.mraidTagValue = window.mraidTagGeneric;}else{window.mraidTagValue = '';} ;window.updateTextArea();"> <br></p>
					            <p>
					            TAG:

					            	    <textarea style="width:100%;outline:none;min-height:200px;"  readonly id="textArea" >
					            	    </textarea>
					            <br>
					            </p>

					            <button class="btn-tools" id="tagGen">Click For tag</button>

					            <br/>
								<br/>
					            <h3 align="left"><p>Targeting Values (Name Value Pairs)</p></h3>
								<br/>
					            <div class="bs-example">
					                <table class="table" >
					                  <thead>
					                    <tr>
					                      <th>Name</th>
					                      <th>Value</th>
					                    </tr>
					                  </thead>
					                  <tbody>
					                    <tr>
					                      <td>campaign</td>
					                      <td id="campaignTable"></td>
					                    </tr>
					                    <tr>
					                      <td>device</td>
					                      <td id="deviceTable"></td>
					                    </tr>
					                    <tr>
					                      <td>format</td>
					                      <td id="formatTable"></td>
					                    </tr>
					                    <tr>
					                      <td>size</td>
					                      <td id="sizeTable"></td>
					                    </tr>
					                    <tr>
					                      <td>environment</td>
					                      <td id="environmentTable"></td>
					                    </tr>
					                  </tbody>
					                </table>
					              </div>
					            </table>

					            <div id="paramsList"></div>
					            <br>
					            <button class="btn-tools" onclick="var ifrm = document.getElementById('testFrame');ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;ifrm.document.open();ifrm.document.write('<scr'+'ipt>setTimeout(function(){document.getElementsByTagName(\'script\')[0].parentNode.removeChild(document.getElementsByTagName(\'script\')[0]);document.getElementsByTagName(\'script\')[0].parentNode.removeChild(document.getElementsByTagName(\'script\')[0]);document.getElementsByTagName(\'head\')[0].parentNode.removeChild(document.getElementsByTagName(\'head\')[0]);if(document.all[0].innerHTML.replace(\'</body>\',\'\').replace(\'<body>\',\'\')==\'\'){window.frameElement.parentNode.parentNode.parentNode.getElementsByTagName(\'textarea\')[1].value=\'RESPONSE IS EMPTY\';}else{window.frameElement.parentNode.parentNode.parentNode.getElementsByTagName(\'textarea\')[1].value = document.all[0].innerHTML.replace(\'</body>\',\'\').replace(\'<body>\',\'\');}}, 4000)</scr' + 'ipt>' + document.getElementById('textArea').value);ifrm.document.close();">TEST</button>
					            <br>
					            <iframe id="testFrame" style="width:320px;height:416px;"></iframe>
					            <textarea style="width:100%;outline:none;min-height:200px;"  id="resPonseFrame" style="width:320px;height:416px;" placeholder="RESPONSE" disabled></textarea>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
