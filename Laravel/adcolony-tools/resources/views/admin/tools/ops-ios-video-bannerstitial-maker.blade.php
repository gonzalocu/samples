<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
    <script src="/js/omw/ad-tools/jQuery.AjaxFileUpload.js" type="text/javascript"></script>
@endsection

@section('script_bottom')
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js" type="text/javascript"></script>
	<script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script>

	<script type="text/javascript">

		(function() {

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');

		$('form').ajaxForm({
		beforeSend: function() {
		status.empty();
		var percentVal = '0%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
		var percentVal = percentComplete + '%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		success: function() {
		var percentVal = '100%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		complete: function(xhr) {

		window.imgResponse = JSON.parse(xhr.responseText);
		status.html(window.imgResponse.path);
		}
		});

		})();

		makeTag = function(){
		var urlTag= document.getElementById("tag1").value;

		var width = document.getElementById("upload").width;
		var height = document.getElementById("upload").height;

		document.getElementById("tag2").value =  stBit + urlTag + ndBit + window.imgResponse.path + rdBit;

		addIframe();
		}


		var stBit = "<scr"+"ipt src='mraid.js'></scr"+"ipt><meta name=\"viewport\" content=\"width=device-width, user-scalable=no\"><div id='clickToUrl' style='position:absolute;left:0px;top:0px;height:100%;width:100%;z-index:10;' onclick=\"mraid.open('{clickurl}";
		var ndBit = "')\"></div><div id=\"container\"><div id=\"button-holder\"><div id='_4sa-skip-button' style=\"opacity:0;display:none;\"></div></div><video id='my-video' style='position:absolute;top:0px;left:0px;height:100%;width:100%;' webkit-playsinline  autoplay src='";
		var rdBit = "'></video></div><scr"+"ipt> _4sa = window._4sa || {}; _4sa.macros = {a:'{timestamp}',b:'{partner_id}',c:'{site_id}',d:'{target_params}', e:'{format}', f:'{language}', g:'{model_name}',h:'{device_model}', i:'{device_os}', j:'{excluded_banners}', k:'{sdk_version}', l:'{device_details}', m:'{device_systemversion}', n:'{device_orientation}', o:'{resolution_width}', p:'{max_image_width}', q:'{resolution_height}', r:'{max_image_height}', s:'{device_density}', t:'{admarvel_audio_level}',u:'{hardware_accelerated}', v:'{device_connection}', w:'{realua}', x:'{adtype}', y:'{retrynum}', z:'{app_identifier}', aa:'{app_supportedOrientations}', ab:'{device_hardware}', ac:'{bannerid}'};</scr"+"ipt><scr"+"ipt src=\"http://4sa.s3.amazonaws.com/common-assets/scripts/ios-bannerstitial-video.js\"></scr"+"ipt>";
		var addIframe = function(){
		var ifrm = document.getElementById('my-frame');
		ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
		ifrm.document.open();
		ifrm.document.write(document.getElementById("tag2").value.replace("<script src = 'mraid.js'>", '').replace("mraid.open('{clickurl}" + window.imgResponse.path + "')" ,"document.location = " + window.imgResponse.path ));
		ifrm.document.close();
		}

	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">IOS Video Bannerstitial Maker</h3>
				</div>
  				<div class="panel-body">
					<form action="http://54.229.56.139:2001/video-upload" method="post" enctype="multipart/form-data">
		                <input type="file" name="video">
		                <input type="submit" value="Upload Video to Server" class="btn-tools" id="upload">
		            </form>

		            <div class="progress">
		                <div class="bar progress-bar"></div>

		                <div class="percent"></div>
		            </div>

		            <div id="status"></div>

		            <h2>Click URL</h2>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag1" placeholder="Paste your URL here"></textarea>
		            <button onclick="makeTag();" class="btn-tools">Make Tag</button>

		            <h2>Output (tag)</h2>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag2"></textarea>

		            <h2>Sample</h2>
		            <iframe height=" 480px" width="336px" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0"></iframe>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
