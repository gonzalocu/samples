<!-- resources/views/admin/tools/side.blade.php -->
<aside id="admin-content-aside">

    <h4>Developer Tools</h4>
    <nav class="menu-shylock" >
    <ul class="menu-list">
        <li class="menu__item @if(Request::is("tools/adtech-converter*")) menu__item--current @endif"><a href="/tools/adtech-converter" alt="Converts tag to Adtech tag (Sky)">Adtech Converter</a></li>
        <li class="menu__item @if(Request::is("tools/csv-to-json-converter*")) menu__item--current @endif"><a href="/tools/csv-to-json-converter" alt="Converts CSV data format to json">CSV to JSON Converter</a></li>
        <li class="menu__item @if(Request::is("preview*")) menu__item--current @endif"><a href="/preview" alt="Generate preview pages">Preview Page</a></li>
        <li class="menu__item @if(Request::is("tools/tag-converter*")) menu__item--current @endif"><a href="/tools/tag-converter" alt="Converts tag to Javascript tag">Tag Converter</a></li>
        {{-- <li class="menu__item @if(Request::is("tag-generator/campaign*")) menu__item--current @endif"><a href="/tag-generator/campaign" alt="Create and view UK Tags">Tag Generator</a></li> --}}
    </ul>
    </nav>

    <h4>Operation Tools</h4>
    <nav class="menu-shylock" >
    <ul class="menu-list">
        <li class="menu__item"><a href="/tools/admarvel-initialisation" alt="Converts tag to Javascript tag">Admarvel Initialisation</a></li>
        <li class="menu__item"><a href="/tools/app-detect-tag-creator" alt="Converts tag to Javascript tag">App Detect Tag Creator</a></li>
        <li class="menu__item"><a href="/tools/doubleclick-tag-converter" alt="Converts tag to Javascript tag">Doubleclick Tag Converter</a></li>
        <li class="menu__item"><a href="/tools/expandable-mobile-builder" alt="Converts tag to Javascript tag">Expandable Mobile Builder</a></li>
        <li class="menu__item"><a href="/tools/expandable-tablet-builder" alt="Converts tag to Javascript tag">Expandable Tablet Builder</a></li>
        <li class="menu__item"><a href="/tools/image-interstitial-creator" alt="Converts tag to Javascript tag">Image Interstitial Creator</a></li>
        <li class="menu__item"><a href="/tools/interstitial-close-button-adder" alt="Converts tag to Javascript tag">Interstitial close button adder</a></li>
        <li class="menu__item"><a href="/tools/ios-video-bannerstitial-maker" alt="Converts tag to Javascript tag">iOS Video Bannerstitial Maker</a></li>
        <li class="menu__item"><a href="/tools/ios-video-interstitial-maker" alt="Converts tag to Javascript tag">iOS Video Interstitial Maker</a></li>
        <li class="menu__item"><a href="/tools/off-network-tag-creator" alt="Converts tag to Javascript tag">Off network tag creator</a></li>
        <li class="menu__item"><a href="/tools/pixel-maker" alt="Converts tag to Javascript tag">Pixel Maker</a></li>
        <li class="menu__item"><a href="/tools/pixel-management" alt="Create Pixel">Pixel Management</a></li>
        <li class="menu__item"><a href="/tools/research-tag-wrapper" alt="Converts tag to Javascript tag">Research Tag Wrapper</a></li>
        <li class="menu__item"><a href="/tools/sticky-converter" alt="Converts tag to Javascript tag">Sticky Converter</a></li>
        <li class="menu__item"><a href="/tools/tag-creator" alt="Converts tag to Javascript tag">Tag Creator</a></li>
        <li class="menu__item"><a href="/tools/tag-sanitizer" alt="Converts tag to Javascript tag">Tag Sanitizer</a></li>
        <li class="menu__item @if(Request::is("tools/zenwrapper-ad-tool*")) menu__item--current @endif"><a href="/tools/zenwrapper-ad-tool" alt="Converts tag to Javascript tag">ZenWrapper Ad Tool</a></li>
    </ul>
    </nav>

    <h4>Ad Colony Tools</h4>
    <nav class="menu-shylock" >
    <ul class="menu-list">
        <li class="menu__item @if(Request::is("tools/url-comparison*")) menu__item--current @endif"><a href="/tools/url-comparison" alt="">Url Comparison</a></li>
        <li class="menu__item @if(Request::is("tools/adjust*")) menu__item--current @endif"><a href="/tools/adjust" alt="">Adjust</a></li>
        <li class="menu__item @if(Request::is("tools/appsflyer*")) menu__item--current @endif"><a href="/tools/appsflyer" alt="">AppsFlyer</a></li>
        {{-- <li class="menu__item @if(Request::is("tools/apsalar*")) menu__item--current @endif"><a href="/tools/apsalar" alt="">ApSalar</a></li> --}}
        <li class="menu__item @if(Request::is("tools/king*")) menu__item--current @endif"><a href="/tools/king" alt="">King.com</a></li>
        <li class="menu__item @if(Request::is("tools/kochava*")) menu__item--current @endif"><a href="/tools/kochava" alt="">Kochava</a></li>
        <li class="menu__item @if(Request::is("tools/tune*")) menu__item--current @endif"><a href="/tools/tune" alt="">Tune</a></li>
        <li class="menu__item @if(Request::is("tools/adcolony-validation*")) menu__item--current @endif"><a href="/tools/adcolony-validation" alt="Converts tag to Javascript tag">AdColony Validation</a></li>
    </ul>
    </nav>

    <div class="clear"></div>

</aside>
