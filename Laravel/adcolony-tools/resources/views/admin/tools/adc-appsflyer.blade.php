<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')

	<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
	<link href="/js/omw/tools/styles.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/projects/stimenu.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/css/style.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/cloud-zoom/cloud-zoom.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/fancybox/jquery.fancybox-1.3.4.css" />
	<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/aboutme/css/default.css" />
	<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://v-fab.com/peter/css/admincss.css' type='text/css' media='all' />
	<link rel='stylesheet' id='lightboxStyle-css'  href='http://v-fab.com/peter/css/colorbox.css' type='text/css' media='screen' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

	<script src='http://v-fab.com/peter/js/jquery.tools.min.js?ver=3.0.4'></script>
	<script src="http://v-fab.com/peter/js/include.js"></script>
	<script src="http://v-fab.com/peter/js/jquery.cycle.all.min.js"></script>

	<script src='/js/omw/tools/tracking.js'></script>
	<script src='/js/omw/tools/compare_urls.js'></script>

	<script type="text/javascript" src="http://v-fab.com/peter/adcolony/tracking/appsflyer/appsflyer.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">AppsFlyer</h3>
				</div>
				<div class="panel-body">

			<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
			<div id="notesdiv" class="ndiv">
				<ul>
					<li>
						Click on the tag and make sure it redirects to the correct app
					</li>
					<li>
						FYI I updated our integration with Appsflyer so that we don't have to hardcode the product_id in the URL anymore.
					</li>
					<li>
						And our PRODUCT_ID macro will insert the goal ID that we have set for the ad group. Makes things a little easier, eh? :)
					</li>
					<li>
						We can use links that look like:
						<br />
						http://app.appsflyer.com/id583008410?pid=adcolony_int&c=AdcolonyOPM_iOS_CPI_Non-Incent_iPad&clickid=[MAC_SHA1]&idfa=[IDFA]&app_id=[PRODUCT_ID]&af_siteid=[APP_ID]
					</li>
				</ul>
				<br />
				<h3>Example URL (Amazon):</h3>
				http://app.appsflyer.com/com.ximad.catsandcards-amazon?pid=adcolony_int&clickid=[MAC_SHA1]&sha1_android_id=[SHA1_ANDROID_ID]&advertising_id=[GOOGLE_AD_ID]&app_id=[PRODUCT_ID]&redirect=false&af_siteid=[APP_ID]&af_sub5=[STORE_ID]&af_r=http://www.amazon.com/dp/B00XCDBMU4/
				<br />
				<br />
				<h3>Example URLs (custom CT and VT):</h3>
				Click:<br />
				https://app.appsflyer.com/id949785353?pid=adcolony_int&af_click_lookback=14d&clickid=[TRANS_ID]&idfa=[IDFA]&app_id=[PRODUCT_ID]&af_siteid=[APP_ID]&af_sub5=[STORE_ID]&af_ip=[IP_ADDRESS]&af_ua=[USER_AGENT]&af_lang=[LANGUAGE]
				<br />
				<br />
				Impression:<br />
				https://impression.appsflyer.com/id949785353?pid=adcolony_int&af_viewthrough_lookback​=2d&clickid=[TRANS_ID]&idfa=[IDFA]&app_id=[PRODUCT_ID]&af_siteid=[APP_ID]&af_sub5=[STORE_ID]&af_ip=[IP_ADDRESS]&af_ua=[USER_AGENT]&af_lang=[LANGUAGE]
				<br />
				<br />
			</div>

			<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
			<div id="valdiv">

				Paste the click URL below:
				<br />
				<form id="valform">
					<textarea name="appsflyer_url" id="appsflyer_url" style="margin-bottom: 5px;"></textarea>
					<br />

					<div id="cvtdiv" style="display: none;">
						Paste the impression URL below and press: <input type="button" class="btn-tools" value="Get the VT value from the URL" onclick="ValidateAppsflyer()">
						<textarea name="appsflyer_url2" id="appsflyer_url2" style="margin-top: 10px; min-height: 60px;"></textarea>
						<br />
						<br />
					</div>
					<input type="radio" name="iosvt" id="iosvt0" value="No view-through">
					No view-through
					<br>
					<input type="radio" name="iosvt" id="iosvt1" value="1 day">
					1 day view-through
					<br />
					<input type="radio" name="iosvt" id="iosvt7" value="7 days">
					7 days view-through
					<br />
					<input type="radio" name="iosvt" id="cust1" value="cust1">
					Custom view-through, the VT value is:
					<input type="text" id="cvt" style="width: 24px; padding:1px;">
					("2d", "12h" etc...)
					<br />
					<input type="radio" name="iosvt" id="cust2" value="cust2">
					Custom view-through, I have the impression URL
					<br />
					<textarea name="apps_imp_url" id="apps_imp_url" style="width: 440px; margin-top: 5px; display: none;"></textarea>
					<br />
					<input type="button" class="btn-tools" value="Validate" onclick="ValidateAppsflyer()">
				</form>
				<h3 class="black plink appsfl" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
				<div id="urlsplit_div"></div>
				<div id="appsflyerparams">
					<table id="appsflyer_paramtable">
						<tr>
							<th class="firstcol">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="pid">
							<td>pid</td>
							<td>adcolony_int</td>
							<td></td>
						</tr>
						<tr>
							<td>c</td>
							<td>specified by CLIENT</td>
							<td>campaign that will appear in Appsflyer</td>
						</tr>
						<tr class="tep" id="clickid">
							<td>clickid</td>
							<td>[MAC_SHA1] / [TRANS_ID] / [CLICK_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="idfa">
							<td>idfa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr id="sha1">
							<td>sha1_android_id</td>
							<td>[SHA1_ANDROID_ID]</td>
							<td></td>
						</tr>
						<tr class="tep" id="advid">
							<td>advertising_id</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="red">
							<td>redirect</td>
							<td>false</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="appid">
							<td>app_id</td>
							<td>[PRODUCT_ID]</td>
							<td>**value can also be the mobile app id or bundle id</td>
						</tr>
						<tr id="af_siteid">
							<td>af_siteid</td>
							<td>[APP_ID]</td>
							<td></td>
						</tr>
						<tr id="af_sub1">
							<td>af_sub1</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub2">
							<td>af_sub2</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub3">
							<td>af_sub3</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub4">
							<td>af_sub4</td>
							<td></td>
							<td></td>
						</tr>
						<tr id="af_sub5">
							<td>af_sub5</td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
				<br />
				<div id="vnotes">
					<div id="redurldiv"></div>
					<div id="vnotes_comment"></div>
					<div id="vnotes_alert"></div>
				</div>
				<div class="clear"></div>
			</div>

			<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
			<div id="pbdiv" class="ndiv">
				The postback should be automatically set up for every app in the AppsFlyer dash.
				<br />
				<br />
				<a class="blue" id="appsflyer-dashboard-url" href="https://partners.appsflyer.com" target="_blank">AppsFlyer Dashboard</a>
				<br />
				Login details can be found <a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">here</a>. (AdColony internal document.)
				<br />
				<br />
			</div>

			<h3 class="black plink" id="imptitle">4. Impression tags <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
			<div id="impdiv">
				<form>
					<fieldset id="appsflyer_output">
						<textarea name="appsflyer_imp" id="appsflyer_imp"></textarea>
					</fieldset>
				</form>
				<br />
			</div>

			<h3 class="black plink" id="vttitle">5. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
			<div id="vtdiv">
				<table id="vt_table_appsfl">
					<tr>
						<th colspan="2">VIEW ATTRIBUTION</th>
						<th colspan="3">LOOKBACK WINDOWS</th>
						<th>CLIENT-SIDE ACTION</th>
					</tr>
					<tr class="tep" id="tpid">
						<th>Dedicated View Tags</th>
						<th>Recommended Implementation</th>
						<th style="min-width: 120px;">Flexible Lookback Window</th>
						<th>Default Click Lookback</th>
						<th>Default View Lookback</th>
						<th></th>
					</tr>
					<tr>
						<td>No</td>
						<td class="lefta">Click tag (with &af_sub1=impression) goes on complete, regular click tag goes on HTML5.</td>
						<td>UNCHANGEABLE</td>
						<td>7 days</td>
						<td>7 days
						<br />
						(matched to click window)</td>
						<td class="lefta">No action necessary - we implement on our end.</td>
					</tr>
					<tr>
						<td>Yes</td>
						<td class="lefta">Implement impression tag on complete, copy click tag and replace app.appsflyer with impression.appsflyer</td>
						<td>UNCHANGEABLE</td>
						<td>7 days</td>
						<td>1 day</td>
						<td class="lefta">Enable view through tracking in their integration with Appsflyer.
						<br />
						Can be enabled at App level.</td>
					</tr>
				</table>
				<br />
			</div>
			<h3 class="black plink" id="pietitle">6. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
			<div id="piediv">
				PIE events should be implemented by the client in AppsFlyer dashboard.
				<br />
				More info about the client setup can be found here:
				<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Appsflyer" target="_blank">Appsflyer PIE setup</a>
			</div>
			</div>
		</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
