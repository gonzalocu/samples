<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react-dom.js"></script>
    <script src="/js/tools/pixel_management/all.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Pixel Management</h3>
				</div>
				<div class="panel-body">
					<div id="pixel-management-react-div"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
