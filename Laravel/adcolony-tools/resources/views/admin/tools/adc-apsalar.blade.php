<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')

@endsection

@section('script')

	<link rel="stylesheet" type="text/css" media="all" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
	<link href="/js/omw/tools/styles.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/projects/stimenu.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Wire+One&v1' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/css/style.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/cloud-zoom/cloud-zoom.css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/publications/fancybox/jquery.fancybox-1.3.4.css" />
	<link href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="http://v-fab.com/peter/pages/aboutme/css/default.css" />
	<link rel='stylesheet' id='taylorjames_custom_style-css'  href='http://v-fab.com/peter/css/admincss.css' type='text/css' media='all' />
	<link rel='stylesheet' id='lightboxStyle-css'  href='http://v-fab.com/peter/css/colorbox.css' type='text/css' media='screen' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

	<script src='http://v-fab.com/peter/js/jquery.tools.min.js?ver=3.0.4'></script>
	<script src="http://v-fab.com/peter/js/include.js"></script>
	<script src="http://v-fab.com/peter/js/jquery.cycle.all.min.js"></script>

	<script src='/js/omw/tools/tracking.js'></script>
	<script src='/js/omw/tools/compare_urls.js'></script>

	<script type="text/javascript" src="http://v-fab.com/peter/adcolony/tracking/apsalar/apsalar.js"></script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Apsalar</h3>
				</div>
				<div class="panel-body">

			<h3 class="black plink" id="notestitle">1. Notes <i class="fa fa-arrow-down" id="notesdown"></i><i class="fa fa-arrow-up" id="notesup"></i></h3>
			<div id="notesdiv" class="ndiv">
				Ideal setup: the postback is set up correctly and it’s set up for all downloads. If this is the case, then it’s good to test and can be handed over to adops.
				<br />
				<br />
				If the postback logs indicate that the postbacks have not been set up for all installs, then alert the account manager that it is set up only for “Attribute Installs,” and recommend that the client set it up for “All”. Also let adops know that the campaign is still good to test.
				<br />
				<br />
				<h3 class="black">Example URL:</h3>
				https://168298.measurementapi.com/serve?action=click&publisher_id=168298&site_id=116661&mac_address_sha1=[MAC_SHA1]&odin=[ODIN1]&open_udid=[OPEN_UDID]&ios_ifa=[IDFA]&sub_publisher=[PUBLISHER_ID]&sub_site=[APP_ID]&device_ip=[IP_ADDRESS]&sub_placement=[STORE_ID]&sub_campaign=[AD_CAMPAIGN_NAME]&response_format=json&my_campaign=vine_ios_UK&my_ad=[AD_CREATIVE_NAME]
				<br />
				<br />
			</div>

			<h3 id="valtitle" class="black plink">2. Tracking URL validation <i class="fa fa-arrow-down" id="valdown"></i><i class="fa fa-arrow-up" id="valup"></i></h3>
			<div id="valdiv">

				Paste the click URL below:
				<br />
				<form id="valform">
					<textarea name="tune_url" id="tune_url"></textarea>
					<br />
					<br />
					<input type="radio" name="vt" id="vt0" value="not enabled">
					No view-through
					<br>
					<input type="radio" name="vt" id="vt1" value="enabled">
					View-through is enabled
					<br />
					<br />
					<input type="button" class="btn-class" value="Validate" onclick="ValidateTune()">
					<br />
					<br />
				</form>
				<h3 class="black plink" id="urlparamstitle">Check URL parameters <i class="fa fa-arrow-down" id="urlpdown"></i><i class="fa fa-arrow-up" id="urlpup"></i></h3>
				<div id="urlsplit_div"></div>
				<div id="tuneparams">
					<table id="tune_paramtable">
						<tr>
							<th class="firstcol">Parameter</th>
							<th class="secondcol">Macro/Value</th>
							<th class="thirdcol">Notes</th>
						</tr>
						<tr class="tep" id="tpid">
							<td>publisher_id</td>
							<td>specified by MAT</td>
							<td>numerical</td>
						</tr>
						<tr class="tep" id="tsid">
							<td>site_id</td>
							<td>specified by MAT</td>
							<td>numerical</td>
						</tr>
						<tr class="tep" id="tidfa">
							<td>ios_ifa</td>
							<td>[IDFA]</td>
							<td>for iOS</td>
						</tr>
						<tr class="tep" id="tgaid">
							<td>google_aid</td>
							<td>[GOOGLE_AD_ID]</td>
							<td>for Android</td>
						</tr>
						<tr class="tep" id="tjson">
							<td>response_format</td>
							<td>json</td>
							<td></td>
						</tr>
					</table>
				</div>
				<div id="vnotes">
					<div id="redurldiv"></div>
					<div id="vnotes_comment"></div>
					<div id="vnotes_alert"></div>
				</div>
				<div class="clear"></div>
			</div>

			<h3 class="black plink" id="pbtitle">3. Postback - check the 3rd party Dashboard <i class="fa fa-arrow-down" id="pbdown"></i><i class="fa fa-arrow-up" id="pbup"></i></h3>
			<div id="pbdiv" class="ndiv">

				<a class="blue" href="https://login.mobileapptracking.com" target="_blank">Tune Dashboard</a>
				<br />
				Login details can be found <a class="blue" href="https://sites.google.com/a/adcolony.com/adcolony-internal/install-tracking-knowledge-base/ad-ops/3rd-party-dashboards" target="_blank">here</a>. (AdColony internal document.)
				<br />
				<br />
				<h3 class="black plink" id="pbmtitle">More <i class="fa fa-arrow-down" id="pbmdown"></i><i class="fa fa-arrow-up" id="pbmup"></i></h3>
				<div id="pbmdiv">
					Validate the postback URL:
					<br />
					<br />
					<ul>
						<li>
							Is it set up? Filter for the publisher ID in red: https://<span style="color: #D00;">6036</span>.api-03.com
							<ul>
								<li>
									If you don’t see the publisher then filter for the advertiser name
								</li>
								<li>
									If the postback is not set up, there may be a delay. You should click on the tracking URL several times and check back every hour.
								</li>
							</ul>
						</li>
						<li>
							Is the stem http://cpa.adtilt.com/on_user_action?
							<ul>
								<li>
									If it reads http://pie.adcolony.com/ you will need to edit the postback URL:
									<ol>
										<li>
											Click “edit”
										</li>
										<li>
											Navigate to Template Preferences
										</li>
										<li>
											Select the Install Template
										</li>
									</ol>
								</li>
							</ul>
						</li>
						<li>
							Is the product id (for ios) correct?
							<ul>
								<li>
									If product_id={store_app_id} is inserted that is fine
								</li>
								<li>
									For android, product_id={package_name}
								</li>
							</ul>
						</li>
						<br />
						<li>
							Check the Attribution Window of the advertiser
							<ul>
								<li>
									If they want to enable view through, you will need to generate start and complete tags.
								</li>
							</ul>
						</li>
						<li>
							Check the postback logs and confirm if they are sending postbacks for all installs. If so, you should see postback logs with “adc_conversion=0”
						</li>
					</ul>
				</div>
				<br />
			</div>

			<h3 class="black plink" id="imptitle">4. Click / Impression URLs <i class="fa fa-arrow-down" id="impdown"></i><i class="fa fa-arrow-up" id="impup"></i></h3>
			<div id="impdiv">
				<form>
					<fieldset id="tune_output">
						<textarea name="tune_imp" id="tune_imp"></textarea>
						<br />
						<h3 class="black plink" id="quartiletitle">Quartiles <i class="fa fa-arrow-down" id="quardown"></i><i class="fa fa-arrow-up" id="quarup"></i></h3>
						<div id="quartilediv">
							<textarea name="tune_imp25" id="tune_imp25" rows="8" cols="107"></textarea>
						</div>
					</fieldset>
				</form>
				<br />
			</div>


			<h3 class="black plink" id="vttitle">5. View Through Attribution Window <i class="fa fa-arrow-down" id="vtdown"></i><i class="fa fa-arrow-up" id="vtup"></i></h3>
			<div id="vtdiv">
				<table id="vt_table">
					<tr>
						<th colspan="2">VIEW ATTRIBUTION</th>
						<th colspan="3">LOOKBACK WINDOWS</th>
						<th>CLIENT-SIDE ACTION</th>
					</tr>
					<tr class="tep" id="tpid">
						<th>Dedicated View Tags</th>
						<th>Recommended Implementation</th>
						<th style="min-width: 120px;">Flexible Lookback Window</th>
						<th>Default Click Lookback</th>
						<th>Default View Lookback</th>
						<th></th>
					</tr>
					<tr>
						<td>Yes</td>
						<td class="lefta">Implement impression tags in the start and complete fields. See Wiki.</td>
						<td>Yes</td>
						<td>7 days</td>
						<td>7 days</td>
						<td class="lefta">Enable and/or adjust the impression window in Tune.<br />Can be enabled at Advertiser level.</td>
					</tr>
				</table>
				<br />
			</div>
			<h3 class="black plink" id="pietitle">6. PIE <i class="fa fa-arrow-down" id="piedown"></i><i class="fa fa-arrow-up" id="pieup"></i></h3>
			<div id="piediv">
				PIE events should be implemented by the client in TUNE dashboard.
				<br />
				More info about the client setup can be found here:
				<a class="blue" href="http://support.adcolony.com/customer/portal/articles/1914521-postbacks-for-top-tracking-partners#Tune" target="_blank">Tune PIE setup</a>
			</div>
		</div>
	</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
