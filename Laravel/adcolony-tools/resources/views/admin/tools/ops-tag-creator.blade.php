<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
<style>
	.iic-title {
		font-size:30px;
	}
	.iic-subtitle {
		font-size:20px;
	}
</style>
@endsection

@section('script')
	<script src="/js/omw/ad-tools/jQuery.AjaxFileUpload.js" type="text/javascript"></script>
    <script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script>
	<script type="text/javascript">

		(function() {

		var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');

		$('form').ajaxForm({
		beforeSend: function() {
		status.empty();
		var percentVal = '0%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
		var percentVal = percentComplete + '%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		success: function() {
		var percentVal = '100%';
		bar.width(percentVal)
		percent.html(percentVal);
		},
		complete: function(xhr) {

		window.imgResponse = JSON.parse(xhr.responseText);
		status.html(window.imgResponse.path);
		}
		});

		})();

		makeTag = function(){

		if(document.getElementById('admarvel').checked == true)
			var stBit="<a href=\"{clickurl}";
		else
			var stBit="<a href=\"";

		var urlTag= document.getElementById("tag1").value;

		var width = document.getElementById("upload").width;
		var height = document.getElementById("upload").height;

		if(window.i){}

		document.getElementById("tag2").value =  stBit + urlTag + ndBit+ "style=\"width:" + document.getElementById("width").value + ";height:" + document.getElementById("height").value + ";\"" + ndBitTwo + window.imgResponse.path.replace('4sa.s3.amazonaws.com','d28z9ox7dl322g.cloudfront.net') + rdBit;

		addIframe();
		}


		var ndBit="\"target=\"_blank\"><img ";
		var ndBitTwo = "src=\"";
		var rdBit="\"/><\/a>";
		var addIframe = function(){
		var ifrm = document.getElementById('my-frame');
		ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
		ifrm.document.open();
		ifrm.document.write(document.getElementById("tag2").value);
		ifrm.document.close();
		}

	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Tag Creator</h3>
				</div>
  				<div class="panel-body">
					<form action="http://54.229.56.139:2001/image-upload" method="post" enctype="multipart/form-data">
		                <input type="file" name="image">
		                <input type="submit" value="Upload File to Server" class="btn-tools" id="upload">
		                </form>

		            <div class="progress">
		                <div class="bar progress-bar"></div>

		                <div class="percent"></div>
		            </div>

		            <div id="status"></div>

		            <input type="checkbox" name="admarvel" id="admarvel" checked>  Admarvel macro? <br><br>
		            <div class='iic-subtitle'>URL</div>


		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag1" placeholder="Paste your URL here"></textarea>
		            Hardcode Dimensions:</br></br>
		            Width:
					<br/>
					<input type="number" value="320" id="width" style="padding:5px;">
					<br/>
		            Height:
					<br/>
					<input type="number" value="50" id="height"style="padding:5px;">
					</br><button onclick="makeTag();" class="btn-tools">Make Tag</button>

					<br/><br/>
		            <div class='iic-subtitle'>Output (tag)</div>
		            <textarea style="width:100%;outline:none;min-height:200px;" id="tag2"></textarea>

		            <div class='iic-subtitle'>Sample</div>
		            <iframe height=" 480px" width="336px" id="my-frame" scrolling="no" frameBorder="0" marginheight="0" marginwidth="0"></iframe>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
@endsection
