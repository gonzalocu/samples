<!-- resources/views/admin/tools/index.blade.php -->

@extends('layouts.master_admin')

@section('style')
@endsection

@section('script')
	<script>
		function escapeHtml(unsafe) {
			return unsafe
					.replace(/&/g, "&amp;")
					.replace(/</g, "&lt;")
					.replace(/>/g, "&gt;")
					.replace(/"/g, "&quot;")
		}

		makeTag = function() {
			if ($('#auto').is(":checked")) {
				var initialTag= document.getElementById("tag1").value;
				pretag = "<ad type='javascript' source='campaign'  ave='1'  aae='YES' iha='1' aie='1'  ><xhtml>";
				document.getElementById("tag2").value = pretag + escapeHtml(initialTag) + posttag;
			} else {
				var initialTag= document.getElementById("tag1").value;
				pretag="<ad type='javascript' source='campaign'><xhtml>";
				document.getElementById("tag2").value = pretag + escapeHtml(initialTag) + posttag;
			}
		}
	</script>
@endsection

@section('content')
	<div class='omw-table-wrapper'>
		@include("admin/tools/side")
		<div id='omw-tools-content'>
			<div class="panel panel-default">
				<div class="panel-heading">
  					<h3 class="panel-title">Tag Converter</h3>
				</div>
  				<div class="panel-body">
					<textarea id="tag1" style="width:100%;outline:none;min-height:200px;" onfocus="document.getElementById('tag1').value='';" placeholder="Paste your tag here"></textarea>
					<input type="checkbox" id="auto" style="margin:4px;">Autoexpand<br/>
					<button onclick="makeTag();" class='btn-tools'>Convert to Javascript tag</button>
					<textarea id="tag2" style="width:100%;outline:none;min-height:200px;"></textarea>
					<script>
						var pretag="<ad type='javascript' source='campaign'><xhtml>";
						var posttag="</xhtml></ad>";
					</script>
			</div>
			</div>
		</div>
		<div class="clear"></div>
    </div>
@endsection
