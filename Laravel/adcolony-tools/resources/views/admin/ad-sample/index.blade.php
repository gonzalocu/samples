<!-- resources/views/ad-sample/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Sample</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                <a class='adc-green-btn' href="{{ Request::url() }}/create"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; New</a>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
            <thead>
    	        <tr>
    	            <th>Name</th>
    	            <th>Description</th>
    	            <th>Create at</th>
    	            <th>Visible</th>
    	            <th>Details</th>
    	        </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5"></td>
                </tr>
            </tfoot>
            <tbody>
    	        @foreach ($formats as $format)
    	        <tr>
    	            <td>{{ $format->name }}</td>
    	            <td>{{ str_limit($format->description, 80) }}</td>
    	            <td>{{ $format->created_at->format('d/m/Y') }}</td>
    	            <td>
    		            @if ($format->visible == 1)
    		               <div class="fa fa-check"></div>
    		            @else
    		                <div class='fa fa-times'></div>
    		            @endif
    	            </td>
    	            <td><a class="adc-green-btn" href="/ad-sample/{{ $format->id }}">View</a></td>
    	        </tr>
    	        @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
