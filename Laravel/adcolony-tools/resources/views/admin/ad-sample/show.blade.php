<!-- resources/views/ad-sample/show.blade.php -->

@extends('layouts.master_admin')

@section('content')

<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Sample</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <a class="adc-green-btn" href="/ad-sample/{{ $item->id }}/edit"><i class="fa fa-pencil"></i>&nbsp;&nbsp; Edit</a>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Category</th>
                        <th>Features</th>
                        <th>Create at</th>
                        <th>Visible</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $item->category->name }}</td>
                        <td> @foreach ($item->features as $feature) {{ $feature->name }} </br> @endforeach</td>
                        <td>{{ $item->created_at->format('d/m/Y') }}</td>
                        <td>
                            @if ($item->visible == 1)
                               <div class="fa fa-check"></div>

                            @else
                                <div class='fa fa-times'></div>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            <div>
                @foreach ($item->assets as $asset)
                    <div class='omw-admin-format-image' id="asset_{{ $asset->id }}">
                        <img src="{{ $asset->url }}" />
                    </div>
                @endforeach
            </div>

            @if ($item->tag)
                {!! Form::label('tag','Ad Tag') !!}
                {!! Form::textarea(false, $item->tag, array('class'=>'form-control', 'readonly')) !!}
            @endif

            @if ($item->url)
                {!! Form::label('url','Ad Sample Url') !!}
                {!! Form::text(false, $item->url->url, array('class'=>'form-control', 'readonly')) !!}

                {!! Form::label('url','Ad Sample Share Url') !!}
                {!! Form::text(false, "http://preview.operamediaworks.tools/".$item->url->slug, array('class'=>'form-control', 'readonly')) !!}
            @endif
        </div>
    </div>


</div>
@endsection
