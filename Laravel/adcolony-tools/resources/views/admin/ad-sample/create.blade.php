<!-- resources/views/ad-sample/create.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/dropzone.min.css" />

@endsection

@section('script')
	<script src="/bower_components/dropzone/dist/min/dropzone.min.js"></script>
    <script src="/js/admin/admin.ad-sample.js"></script>
@endsection

@section('content')

{!! Form::open(array('url'=>'/ad-sample', 'method'=>'POST', 'files'=>true)) !!}
<div id='admin-content-wrap-inner'>

    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Sample</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4">
            <div class='row'>
                <div class="col-md-6">
                    <h6>Name</h6>
                    {!! Form::text('name', NULL, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}
                </div>
                <div class="col-md-6">
                    <h6>Category</h6>
                    <div class="styled-select slate">
                        {!! Form::select('category', $categories) !!}
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <div id="category1"></div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <div id="category2"></div>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <h6>Description</h6>
                    {!! Form::text('description', NULL, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}
                </div>
            </div>

            {!! Form::label('image', 'Ad Images') !!}
            <div>
                @if (Session::has('tmp_files'))
                    @foreach (Session::get('tmp_files') as $asset)
                        <div class='omw-admin-format-image' >
                            <img src="/{{ $asset }}" />
                        </div>
                    @endforeach
                @endif
            </div>
            <div id="dropzone-div" class="dropzone"></div>

        </div>
        <div class="col-md-4">
            {!! Form::checkbox('feature_visible', true, false, array('id' => 'feature_visible', 'class' => 'ios7checkbox')); !!}
            <label for='feature_visible' tabindex="-1"><span class="check"></span>Visible</label>

            <h6>Features</h6>
            @foreach ($features as $feature)
                <div class='admin-checkbox-item'>
                    {!! Form::checkbox('feature_'.$feature->slug, true, false, array('id'=>'feature_'.$feature->slug)); !!}
                    <label for="{{'feature_'.$feature->slug}}"><span>{{ $feature->name }}</span></label>
                </div>
            @endforeach
        </div>
        <div class="col-md-4">
            <h6>Ad</h6>
            {!! Form::checkbox('urlortag', true, false, array('id' => 'urlortag', 'class' => 'ios7largecheckbox')); !!}
            <label for='urlortag' tabindex="-1"><span class="check"></span><span class="text"></span></label>


            {!! Form::label('url','') !!}
            {!! Form::text('url', NULL, array('id'=>'url','class'=>'form-control', 'placeholder'=>'Link')) !!}


            {!! Form::label('tag','') !!}
            {!! Form::textarea('tag', NULL, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
