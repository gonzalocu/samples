<!-- resources/views/ad-sample/edit.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" type="text/css" href="/bower_components/dropzone/dist/min/dropzone.min.css" />
@endsection

@section('script')
	<script src="/bower_components/dropzone/dist/min/dropzone.min.js"></script>
	<script src="/bower_components/jquery-form/jquery.form.js"></script>
	<script src="/js/admin/admin.ad-sample.js"></script>
@endsection

@section('content')
<div id='admin-content-wrap-inner'>
    {!! Form::open(array('url'=>'/ad-sample/'.$item->id, 'method'=>'PUT', 'files'=>true)) !!}
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Sample</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            {!! Form::text('name', $item->name, array('id'=>'fname','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {!! Form::label('description','Ad Description') !!}
            {!! Form::text('description', $item->description, array('id'=>'fdescription','class'=>'form-control', 'placeholder'=>'Description')) !!}

            {!! Form::label('category','Category') !!}
            <select id="category" name="category">
                @foreach ($categories as $category)
                <option value="{{$loop->index}}" @if ( $category->id == $item->category_id  ) selected @endif >{{ $category->name }}</option>
                @endforeach
            </select>

            {!! Form::label('feature','Features') !!}
            @foreach ($features as $feature)
                <?php $selected = false; ?>
                @foreach ($item->features as $current_features)
                    @if ($current_features->slug == $feature->slug)
                        <?php $selected = true; ?>
                    @endif
                @endforeach

               <div class='omw-admin-format-feature'>{!! Form::checkbox('feature_'.$feature->slug, true, $selected, array('id'=>'feature_'.$feature->slug)); !!} <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label></div>
            @endforeach

    		{!! Form::label('fimages','Ad Images') !!}
    		<div>
    		   	 @foreach ($item->assets as $asset)
    		   	 	<div class='omw-admin-format-image' id="asset_{{ $asset->id }}">
    				    <img src="{{ $asset->url }}" />
                        <input id="input_asset_{{ $asset->id }}" name="input_asset_{{ $asset->id }}" type="hidden" value="true" />
                        <div><a href="/ad-sample/remove-file/{{ $asset->id }}">Delete</a></div>

                    </div>
    			@endforeach
    	    </div>
            <div id="dropzone-div" class="dropzone"></div>

            {!! Form::checkbox('urlortag', true, false, array('id' => 'urlortag', 'class' => 'ios7largecheckbox')); !!}
    		<label for='urlortag' tabindex="-1"><span class="check"></span><span class="text">Url</span></label>

            {!! Form::label('url','Ad Sample URL') !!}
            {!! Form::text('url', ($item->url?$item->url->url:''), array('id'=>'url','class'=>'form-control', 'placeholder'=>'URL')) !!}

            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', $item->tag, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

            {!! Form::checkbox('feature_visible', true, $item->visible, array('id' => 'feature_visible', 'class' => 'ios7checkbox')); !!}
    		<label for='feature_visible' tabindex="-1"><span class="check"></span>Visible</label>

        </div>
    </div>
    {!! Form::close() !!}
    <div class='row'>
        <div class="col-md-12">
            {!! Form::delete('/ad-sample/'.$item->id, 'Delete', array('class'=>'omw-admin-delete-btn')) !!}
        </div>
    </div>
</div>



@endsection
