<!-- resources/views/banner/show.blade.php -->

@extends('layouts.master_admin')

@section('content')
  <div class='omw-table-wrapper'>
      <a class='omw-admin-btn' href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}">Back</a>

  @include("layouts/notification")

    <h2>Tag Generator</h2>
    <br/>
    <table class="table">
    <thead>
    <tr>
        <th>Campaign</th>
        <th>Banner</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $collection->name }}</td>
        <td>{{ $collection->title }}</td>
    </tr>
    </tbody>
    </table>

    {{-- IMPRESSION TRACKING --}}
    {!! Form::label('impression_tracking','Impression Tracking') !!}
    {!! Form::text('impression_tracking', $collection->project_id, array('class'=>'form-control')) !!}

    {{-- BANNER TAP TRACKING --}}
    {!! Form::label('bannertap_tracking','Banner Tap Tracking') !!}
    {!! Form::text('bannertap_tracking', $collection->project_id, array('class'=>'form-control')) !!}

    {{-- CTA TRACKING --}}
    {!! Form::label('cta_tracking','CTA Tracking') !!}
    {!! Form::text('cta_tracking', $collection->project_id, array('class'=>'form-control')) !!}

    {{-- CTA TRACKING --}}
    {!! Form::label('cta_tracking','CTA Tracking') !!}
    {!! Form::text('cta_tracking', $collection->project_id, array('class'=>'form-control')) !!}

</div>
@endsection
