<!-- resources/views/admin/ad-locker/index.blade.php -->

@extends('layouts.master_admin')

@section('content')
<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Locker</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                <a class='adc-green-btn' href="{{ Request::url() }}/create"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; New</a>

            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
            <thead>
                <tr>
                    <th class="col-md-9">Name</th>
                    <th class="col-md-1">Visible</th>
                    <th class="col-md-1">Details</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($collection as $element)
                <tr>
                    <td>{{ $element->name }}</td>
                    <td>
                        @if ($element->visible == 1)
                           <div class="fa fa-check"></div>
                        @else
                            <div class='fa fa-times'></div>
                        @endif
                    </td>
                    <td><a class="adc-green-btn"  href="{{ Request::url() }}/{{ $element->id }}">View</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
