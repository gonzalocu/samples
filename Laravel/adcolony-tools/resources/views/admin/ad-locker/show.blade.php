<!-- resources/views/admin/ad-locker/show.blade.php -->

@extends('layouts.master_admin')

@section('content')

<div id='admin-content-wrap-inner'>
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Locker</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <a class="adc-green-btn" href="{{ Request::url() }}/edit"><i class="fa fa-pencil"></i>&nbsp;&nbsp; Edit</a>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            <table class='table-admin'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Vertical</th>
                        <th>Features</th>
                        <th>Create at</th>
                        <th>Visible</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->vertical->name }}</td>
                        <td> @foreach ($item->features as $feature) {{ $feature->name }} </br> @endforeach</td>
                        <td>{{ $item->created_at->format('d/m/Y') }}</td>
                        <td>
                            @if ($item->visible == 1)
                               <div class="fa fa-check"></div>

                            @else
                                <div class='fa fa-times'></div>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>

            {!! Form::label('fdescription','Ad Locker Description') !!}
            {!! Form::text(false, $item->description, array('class'=>'form-control', 'readonly')) !!}

            @if ($item->url)
                {!! Form::label('furl','Ad Locker URL') !!}
                {!! Form::text(false, $item->url->url, array('class'=>'form-control', 'readonly')) !!}

                {!! Form::label('furl','Ad Locker Share URL') !!}
                {!! Form::text(false, "http://preview.operamediaworks.tools/".$item->url->slug, array('class'=>'form-control', 'readonly')) !!}
            @endif

            @if (!empty($item->tag))
                {!! Form::label('tag','Ad Tag') !!}
                {!! Form::textarea(false, $item->tag, array('class'=>'form-control', 'readonly')) !!}
            @endif
        </div>
    </div>

</div>
@endsection
