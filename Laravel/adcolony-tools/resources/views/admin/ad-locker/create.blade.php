<!-- resources/views/admin/ad-locker/create.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.css" />
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.date.css" />
@endsection

@section('script')
	<script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/js/admin/admin.ad-locker.js"></script>
@endsection

@section('content')
<div id='admin-content-wrap-inner'>
    {!! Form::open(array('url'=> "/" . Request::segment(1) . "/" . Request::segment(2), 'method'=>'POST', 'files'=>true)) !!}
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Locker</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            {!! Form::label('name','Ad Locker Name') !!}
            {!! Form::text('name', NULL, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {!! Form::label('description','Ad Locker Description') !!}
            {!! Form::text('description', NULL, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}

            {{-- VERTICAL --}}
            {!! Form::label('vertical','Vertical') !!}
            {!! Form::select('vertical', Vertical::orderBy('name', 'asc')->pluck('name','id'), Input::old('vertical')) !!}

            {{-- FEATURE --}}
            {!! Form::label('feature','Features') !!}
            @foreach (Feature::orderBy('name', 'asc')->get()->all() as $feature)
                <div class='omw-admin-format-feature'>
                    {!! Form::checkbox('feature_'.$feature->slug, true, false, array('id'=>'feature_'.$feature->slug)); !!}
                    <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label>
                </div>
            @endforeach

            {!! Form::checkbox('urlortag', true, false, array('id' => 'urlortag', 'class' => 'ios7largecheckbox')); !!}
            <label for='urlortag' tabindex="-1"><span class="check"></span><span class="text">Url</span></label>

            {!! Form::label('url','Ad Locker URL') !!}
            {!! Form::text('url', NULL, array('id'=>'url','class'=>'form-control', 'placeholder'=>'URL')) !!}

            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', NULL, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

            {!! Form::label('date','Ad Locker Date') !!}
            {!! Form::text('date', NULL, array('id'=>'date','class'=>'form-control form-control-date', 'placeholder'=>'Date')) !!}

            {!! Form::checkbox('visible', true, false, array('id' => 'visible', 'class' => 'ios7checkbox')); !!}
            <label for='visible' tabindex="-1"><span class="check"></span>Visible</label>

        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
