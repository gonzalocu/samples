<!-- resources/views/admin/ad-locker/edit.blade.php -->

@extends('layouts.master_admin')

@section('style')
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.css" />
	<link rel="stylesheet" href="/bower_components/pickadate/lib/compressed/themes/classic.date.css" />
@endsection

@section('script')
	<script src="/bower_components/pickadate/lib/compressed/picker.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/picker.date.js"></script>
	<script src="/bower_components/pickadate/lib/compressed/legacy.js"></script>
	<script src="/js/admin/admin.ad-locker.js"></script>
@endsection


@section('content')
<div id='admin-content-wrap-inner'>
    {!! Form::open(array('url'=> "/" . Request::segment(1) . "/" . Request::segment(2) . "/" . Request::segment(3), 'method'=>'PUT', 'files'=>true)) !!}
    <div class='row'>
        <div class="col-md-12">
            @include("layouts/notification")
        </div>
    </div>
    <div class='row'>
        <div class="col-md-9">
            <h2>Ad Locker</h2>
        </div>
        <div class="col-md-3">
            <div class="admin-utils-top-right">
                    <button type="submit" class="adc-green-btn"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp; Save</button>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-12">
            {!! Form::label('name','Ad Locker Name') !!}
            {!! Form::text('name', $item->name, array('id'=>'name','class'=>'form-control', 'placeholder'=>'Name')) !!}

            {!! Form::label('description','Ad Locker Description') !!}
            {!! Form::text('description', $item->description, array('id'=>'description','class'=>'form-control', 'placeholder'=>'Description')) !!}

            {!! Form::checkbox('urlortag', true, false, array('id' => 'urlortag', 'class' => 'ios7largecheckbox')); !!}
            <label for='urlortag' tabindex="-1"><span class="check"></span><span class="text">Url</span></label>

            {{-- VERTICAL --}}
            {!! Form::label('vertical','Vertical') !!}
            {!! Form::select('vertical', Vertical::orderBy('name', 'asc')->pluck('name', 'id'), ($item->vertical?$item->vertical->id:'') ) !!}

            {{-- FEATURE --}}
            {!! Form::label('feature','Features') !!}
            @foreach (Feature::orderBy('name', 'asc')->get()->all() as $feature)
                <?php $selected = false; ?>
                @foreach ($item->features as $current_features)
                    @if ($current_features->slug == $feature->slug)
                        <?php $selected = true; ?>
                    @endif
                @endforeach
                <div class='omw-admin-format-feature'>
                    {!! Form::checkbox('feature_'.$feature->slug, true, $selected, array('id'=>'feature_'.$feature->slug)); !!}
                    <label for="{{'feature_'.$feature->slug}}">{{ $feature->name }}</label>
                </div>
            @endforeach

            {!! Form::label('url','Ad Locker URL') !!}
            {!! Form::text('url', ($item->url?$item->url->url:''), array('id'=>'url','class'=>'form-control', 'placeholder'=>'URL')) !!}

            {!! Form::label('tag','Ad Tag') !!}
            {!! Form::textarea('tag', $item->tag, array('id'=>'tag','class'=>'form-control', 'placeholder'=>'Tag')) !!}

            {!! Form::label('date','Ad Locker Date') !!}
            {!! Form::text('date', $item->created_at->format('Y/m/d'), array('id'=>'date','class'=>'form-control form-control-date', 'placeholder'=>'Date')) !!}

            {!! Form::checkbox('visible', true, $item->visible, array('id' => 'visible', 'class' => 'ios7checkbox')); !!}
            <label for='visible' tabindex="-1"><span class="check"></span>Visible</label>
        </div>
    </div>
    {!! Form::close() !!}
    <div class='row'>
        <div class="col-md-12">
            {!! Form::delete('/' . Request::segment(1) . '/' . Request::segment(2) .'/'. Request::segment(3), 'Delete', array('class'=>'omw-admin-delete-btn')) !!}
        </div>
    </div>
</div>
@endsection
