<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="/images/favicons/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon-180x180.png" />

	<title>Page not found</title>

    <style>
        html, body {
            height: 100%;
        }

        body {
            font-size: 1em;
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Arial';
            color: #fff;
        }

        @media (max-width: 768px) {
            body {
                font-size: 0.8em;
            }
        }

        @media (max-width: 480px) {
            body {
                font-size: 0.6em;
            }
        }

        .omw-error-gif {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;

            background-image: url('/assets/images/basic/error404.gif');
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
        }

        .omw-error-logo {
            position: absolute;
            top: 0;
            background-image: url('/assets/images/basic-adc/adcolony-logo-white.png');
            background-size: auto 40px;
            background-position: center center;
            background-repeat: no-repeat;
            width: 100%;
            height: 68px;
            padding-top: 50px;

        }

        .omw-error-content {
            position: absolute;
            top: 50%;
            width: 100%;

            margin-bottom: 2em;

            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            -ms-transform: translateY-50%);

            text-align: center;
        }

        .omw-error-title {
            font-size: 4em;
            margin-bottom: 0.25em;
        }

        .omw-error-subtitle {
            font-size: 2em;
            margin-bottom: 2em;
        }

        .omw-error-btn {
            font-size: 1.2em;
            padding: 10px 25px;

            color: #040548;
            background-color: #36fbd0;
            border-radius: 3px;

            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class='omw-error-gif'></div>
    <a href="/"><div class='omw-error-logo'></div></a>
	<div class="omw-error-content">
        <div class="omw-error-title">That awkward moment</div>
        <div class='omw-error-subtitle'>when you find a 404.</div>
        <a class='omw-error-btn' href="/">Let's try again</a>
    </div>
</body>
</html>
