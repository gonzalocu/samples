var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    mix.sass('public/styles.scss', 'public/css/home/styles.css')
       .sass('admin/styles.scss',  'public/css/admin/styles.css');

    mix.copy('resources/assets/js/autosuggest', 'public/js/autosuggest');
    mix.copy('resources/assets/js/omw', 'public/js/omw');

    mix.copy('resources/assets/css/omw', 'public/css/omw');
    mix.copy('resources/assets/css/owl', 'public/css/owl');

    mix.rollup('main/app.js', 'public/js/main/all.js')
       .rollup('main/omw.js', 'public/js/main/omw.js')
       .rollup('admin/admin.ad-sample.js', 'public/js/admin/admin.ad-sample.js')
       .rollup('admin/admin.guideline.js', 'public/js/admin/admin.guideline.js');
});
