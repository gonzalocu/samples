var uglify = require('rollup-plugin-uglify'),
    babel = require('rollup-plugin-babel'),
    resolve = require('rollup-plugin-node-resolve'),
    cjs = require('rollup-plugin-commonjs'),
    replace = require('rollup-plugin-replace');

module.exports = {
    //format: 'cjs',
    format: 'iife',
    globals: {
        react: 'React'
    },
    plugins: [
        babel({
            babelrc: false,
            exclude: 'node_modules/**',
            presets: [['es2015', { modules: false }], 'react'],
            plugins: ['external-helpers']
        }),
        cjs({
            include: 'node_modules/**'
        }),
        //replace({ 'process.env.NODE_ENV': JSON.stringify('development') }),
        replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
        resolve({
            main: true,
            browser: true
        }),
        uglify()
    ],
    onwarn: function (warning) { if (warning.code === 'THIS_IS_UNDEFINED') { return; } },
    sourceMap: true
};
