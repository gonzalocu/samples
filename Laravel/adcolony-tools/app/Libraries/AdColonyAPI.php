<?php

namespace App\Libraries;

use DB;

class AdColonyAPI {


    protected $_interval = 'today';
    protected $_baseUrl = 'http://clients-api.adcolony.com/api/v2/advertiser_summary?';

    protected $_error;
    protected $_response;

    protected $_url;

    public function getAllCampaign () {

        $_credential = getenv('ADCOLONY_API_KEY');
        $url = $this->_baseUrl . 'user_credentials=' . $_credential . '&format=json&group_by=campaign&interval=' . $this->_interval;

        return $this->call($url);

    }

    public function getAllGroup () {

        $_credential = getenv('ADCOLONY_API_KEY');
        $url = $this->_baseUrl . 'user_credentials=' . $_credential . '&format=json&group_by=ad_group&interval=' . $this->_interval;

        return $this->call($url);

    }

    public function getAllCreative () {

        $_credential = getenv('ADCOLONY_API_KEY');
        $url = $this->_baseUrl . 'user_credentials=' . $_credential . '&format=json&group_by=creative&interval=' . $this->_interval;

        return $this->call($url);

    }

    protected function call ($url) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $resp = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        $json_return = json_decode($resp);

        return $json_return->results;

    }

    public function getError() {
        return $this->_error;
    }

    public function getResponse() {
        return $this->_response;
    }
}

?>
