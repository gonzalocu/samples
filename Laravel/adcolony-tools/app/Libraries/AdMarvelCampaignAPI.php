<?php

namespace App\Libraries;

class AdMarvelCampaignAPI {

    protected $_url     = 'https://map.admarvel.com/admarvelapi';
    protected $_baseXML = '<?xml version="1.0" encoding="UTF-8"?><admarvel><admarvelAction></admarvelAction></admarvel>';

    protected $_username;
    protected $_password;

    protected $_advertiserid;

    protected $_error;
    protected $_errorNum;
    protected $_response;

    // status
    const STATUS_RESTART    = 'RESTART';
    const STATUS_PAUSE      = 'PAUSE';

    protected function _createRequestXML($action, $params) {

        $user = array(
            'user' => array(
                'username' => getenv('AM_USER'),
                'password' => getenv('AM_PASSWORD')
            )
        );
        $params = $user + $params;

        $xml = new \SimpleXMLElement($this->_baseXML);
        $xmlBody = $xml->admarvelAction;

        $xmlBody->addAttribute('actionid', $action);

        self::array2xml($xmlBody, $params);

        return $xml->asXML();
    }

    protected static function array2xml($xml, $params) {
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                if (!self::is_associative($value)) {
                    foreach ($value as $subkey => $subvalue) {
                        $node = $xml->addChild($key);
                        self::array2xml($node, $subvalue);
                    }
                } else {
                    $node = $xml->addChild($key);
                    self::array2xml($node, $value);
                }
            } else {
                //$xml->addChild($key, $value);
                $xml->$key = $value;
            }
        }
    }

    protected static function xml2array($xml) {
        $arr = array();

        foreach ($xml->children() as $children) {
            if (count($children->children()) == 0) {
                $arr[$children->getName()] = strval($children);
            } else {
                $arr[$children->getName()][] = self::xml2array($children);
            }
        }

        return $arr;
    }

    protected static function is_associative ($arr) {
        return in_array(false, array_map('is_numeric', array_keys($arr)));
    }

    protected function _call($xml) {
        $postdata = 'api_xml='. urlencode($xml);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_URL, $this->_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);

        $responseXML = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if (!$responseXML) {
            $this->_error = $error;
            return false;
        } else {
            try {
                $responseXML = trim($responseXML);
                $response = new \SimpleXMLElement($responseXML);
                $response = $response->admarvelAction;
            } catch (Exception $e) {
                $this->_error = 'Error parsing response';
                return false;
            }

            if (strval($response['status']) == 'SUCCESS') {
                $this->_response = self::xml2array($response);
                return true;
            } else {
                $this->_errorNum = $response->error->code;
                $this->_error = 'Error '. $response->error->code . ': ' . $response->error->message;
                return false;
            }
        }
    }

    public function getError() {
        return $this->_error;
    }

    public function getErrorNum() {
        return $this->_errorNum;
    }

    public function getResponse() {
        return $this->_response;
    }

    public function getActiveCampaignsBanners($advertiserId, $campaignId = null) {
        $params = array(
            'advertiser' => array('id' => $advertiserId),
            'campaign' => array('campaignid' => ($campaignId == null)?-1:$campaignId)
        );

        $requestXML = $this->_createRequestXML('get_active_campaigns_banners', $params);

        return $this->_call($requestXML);
    }

    public function addCampaign($params) {
        $params['campaign']['advertiserid'] = getenv('AM_ADVERTISERID');

        $requestXML = $this->_createRequestXML('add_campaign', $params);

        return $this->_call($requestXML);
    }

    public function getCampaignStatus($campaignId) {
        $params['campaign']['campaignid'] = $campaignId;

        $requestXML = $this->_createRequestXML('get_campaign_status', $params);

        return $this->_call($requestXML);
    }

    public function setCampaignStatus($campaignId, $status) {
        $params = array(
            'campaign' => array(
                'campaignid' => $campaignId,
                'status' => $status
            )
        );

        $requestXML = $this->_createRequestXML('set_campaign_status', $params);

        return $this->_call($requestXML);
    }

    public function getBannerStatus($bannerId) {
        $params['banner']['bannerid'] = $bannerId;

        $requestXML = $this->_createRequestXML('get_banner_status', $params);

        return $this->_call($requestXML);
    }

    public function setBannerStatus($bannerId, $status) {
        $params = array(
            'banner' => array(
                'bannerid' => $bannerId,
                'status' => $status
            )
        );

        $requestXML = $this->_createRequestXML('set_banner_status', $params);

        return $this->_call($requestXML);
    }

    public function getActiveCampaignAndBanner () {
        $params = array(
            'advertiser' => array(
                'id' => getenv('AM_ADVERTISERID')
            ),
            'campaign' => array(
                'campaignid' => '-1'
            )
        );

        $requestXML = $this->_createRequestXML('get_active_campaigns_banners', $params);

        return $this->_call($requestXML);
    }

    public function addBanner($bannername, $bannercontent) {
        $params = array(
            'campaign' => array(
                'campaignid' => getenv('AM_CAMPAIGNID')
            ),
            'banner' => array(
                'bannername' => $bannername,
                'bannercontent' => $bannercontent
            )
        );

        $requestXML = $this->_createRequestXML('add_banner', $params);

        return $this->_call($requestXML);
    }

    public function editBanner($bannerid, $bannername, $bannercontent, $targetvalue) {

        $params = array(
            'banner' => array(
                'bannerid' => $bannerid,
                'bannername' => $bannername,
                'bannercontent' => $bannercontent,
                'targeting' => array(
                    'custom_targeting' => array (
                        'target_key' => 'name',
                        'target_value' => $targetvalue
                    )
                )
            )
        );

        $requestXML = $this->_createRequestXML('edit_banner', $params);

        return $this->_call($requestXML);
    }


    public function addAudiencePixel($name, $description, $adserverType = 'admarvel') {
        $params = array(
            'audiencePixel' => array(
                'audiencePixelName' => $name,
                'description' => $description,
                'adserverType' => $adserverType
            )
        );

        $requestXML = $this->_createRequestXML('add_audience_pixel', $params);

        return $this->_call($requestXML);
    }
}

?>
