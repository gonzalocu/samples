<?php

namespace App\Http\Controllers\OMTools;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Format;
use Category;

class HomeController2 extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('public.home.index', [ 'formats' => Format::orderBy('created_at', 'DESC')->where('visible', '=', true)->get()->all(),
                                           'categories' => Category::orderBy('name', 'ASC')->get()->all()]);
    }

    public function adlocker () {
	    return view('public.home.ad-locker');
    }

}
