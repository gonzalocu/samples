<?php

namespace App\Http\Controllers\OMTools;

use Auth;
use DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\Campaign;
use App\Models\Banner;

class CampaignController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        return view('admin.campaign.index', [ 'campaigns' => Campaign::all(),
                                        'banners' => Banner::all(),
                                        'section' => 'all' ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('admin.campaign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {


        $file = Request::file('file');
        //$file = $request::file('file');
        $cname = Input::get('cname');
        $ctitle = Input::get('ctitle');
        $vtitle = Input::get('vtitle');

        if (empty($file)) {
            return view('admin.campaign.add', ['error' => 'Tag is empty', 'cname' => $cname, 'ctitle' => $ctitle, 'vtitle' => $vtitle ]);
        }

        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));

        $campaign = new Campaign();
        $campaign->name  = $cname;
        $campaign->title = $ctitle;
        $campaign->save();

        $diff = new Banner();
        $diff->name = $vtitle;
        $diff->title = $vtitle;
        $diff->campaign_id =  $campaign->id;
        $diff->tag_text = File::get($file);
        $diff->save();

        return view('admin.campaign.add', ['success' => 'The campaign has been created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return view('admin.campaign.show', [ 'collection' => Campaign::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        return view('admin.campaign.edit', [ 'collection' => Campaign::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function addWatching ($campaign) {

        $user = new User;
        $currentUser = $user->currentUser();
        $campaign  = DB::table('om_campaign')->where('name', $campaign)->first();

        DB::table('om_watching')->insert( ['user_id' => Auth::user()->id, 'campaign_id' => $campaign->id] );


        return redirect()->action('OMTools\CampaignController@index');

    }

    public function removeWatching ($campaign) {

        $user = new User;
        $currentUser = $user->currentUser();
        $campaign  = DB::table('om_campaign')->where('name', $campaign)->first();


        DB::table('om_watching')->where('user_id', '=', Auth::user()->id)->where('campaign_id', '=', $campaign->id)->delete();

        return redirect()->action('OMTools\CampaignController@index');
    }

	public function searchCampaignJSON ($input = ''){

        $campaigns = DB::table ('om_campaign')->where('name', 'like', "%{$input}%")->orWhere('id', 'like', "%{$input}%")->get()->all();

        $arr = [];

        if ($input != '') {
            foreach ($campaigns as $key => $value) {
                $arr1 = ["id" => "$value->id", "value" => "$value->name&nbsp;&nbsp;[$value->id]", "info" => "$value->name"];
                array_push($arr, $arr1);
            }
        }

        return response()->json([ 'results' => $arr]);

    }

}
