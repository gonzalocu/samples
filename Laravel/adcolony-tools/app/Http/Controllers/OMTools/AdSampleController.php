<?php

namespace App\Http\Controllers\OMTools;

// Basic
use Controller;
use Input;
use Request;
use Session;
use Image;
use Form;
use File;
use Redirect;
use Lang;
use Storage;

use Carbon\Carbon;

// Models
use Format;
use Category;
use Feature;
use Asset;
use Url;

// Requests
use FormatFormRequest;

class AdSampleController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
	    Session::forget('tmp_files');
	    Session::forget('tmp_filename');
        return view('admin.ad-sample.index', [  'formats' => Format::orderBy('created_at', 'DESC')->get()->all(),
                                                'success' => Session::get('success')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

		return view('admin.ad-sample.create', [ 'format' => new Format(),
                                                'categories' => array_pluck(Category::orderBy('name', 'ASC')->get()->toArray(), 'name'),
                                                'features' => Feature::orderBy('name', 'ASC')->get()->all(),
                                                'error' => session('error'),
                                                'success' => session('success')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(FormatFormRequest $request) {

        $temp = new Format();
        $temp->name = Input::get('name');
        $temp->description = Input::get('description');
        $temp->visible = (Input::get('feature_visible')?true:false);

        $path = Input::get('url');
        $filename = 'tags/s-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }
        if (Input::get('urlortag')) {
    		// Creating the tag file in the storage
            $temp->tag = Input::get('tag');
            if (!empty($temp->tag)) {
                $res= Storage::disk('local')->put($filename, $temp->tag);
                $path = '/tag/s-'.$temp->slug;
            }
		} else {
    		$temp->tag = "";
		}

        $url_object = Url::where('url', $path)->first();
        if(!$url_object) {
            $url_object = new Url();
            $url_object->url = $path;
            $url_object->type = "adlocker";
            $url_object->save();
        }

        $temp->url_id = $url_object->id;

        foreach (Category::orderBy('name', 'ASC')->get()->all() as $key=>$c) {
            if ($key == Input::get('category')) {
                $temp->category_id = $c->id;
            }
        }

        $temp->save();

        foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
            if (Input::get('feature_'.$f->slug)) {
                $temp->features()->attach($f);
            }
        }

        if (Session::has('tmp_files')) {
				$tmp = Session::pull('tmp_files');
				$tmp_filename = Session::pull('tmp_filename');

				Session::forget('tmp_files');
				Session::forget('tmp_filename');

				foreach ($tmp as $key => $path) {

					$newdest = 'assets/images/uploads';
					File::move(public_path($path), public_path($newdest.$tmp_filename[$key]));

					$asset = new Asset;
	                $asset->caption = "Image for ".$temp->name;
	                $asset->type = "image";
	                $asset->url = "/".$newdest.$tmp_filename[$key];
	                $asset->created_at = Carbon::now();
	                $asset->updated_at = Carbon::now();
	                $asset->save();

					$temp->assets()->attach($asset);

				}
		}

        return Redirect::to('ad-sample')->with('success', Lang::get('validation.success_created', array('attribute' => 'format')));
    }

	public function removeFile($id) {

			$temp = Asset::find($id);
			File::delete(public_path( substr($temp->url, 1)));
			$temp->delete();

			return redirect()->back();
	}

	public function uploadFile() {

		if (Input::hasfile('file')) {
				$file = Input::file('file');
				$filename = time() . strtolower (str_random(2)) . '.' . $file->getClientOriginalExtension();

				$path = 'assets/images/temp/' . $filename;

				$upload_success = Image::make($file->getRealPath())->resize(375, 490)->save(public_path($path));

			if( $upload_success ) {

				Session::push('tmp_files', $path);
				Session::push('tmp_filename', $filename);

				return response()->json(array(	'success' => true,
												'message' => 'Successfully uploaded file.',
												'path' => $path), 200);
			} else {
				return response()->json('error', 400);
			}
		} else {
			return response()->json('error', 400);
		}

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return view('admin.ad-sample.show', [ 'item' => Format::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        return view('admin.ad-sample.edit', [ 'item' => Format::find($id),
        									  'categories' => Category::orderBy('name', 'ASC')->get()->all(),
        									  'features' => Feature::orderBy('name', 'ASC')->get()->all(),]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $temp = Format::find($id);
        $temp->name = Input::get('name');
        $temp->description = Input::get('description');
        $temp->visible = (Input::get('feature_visible')?true:false);

        $path = Input::get('url');
        $filename = 'tags/s-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }
        if (Input::get('urlortag')) {
            $temp->tag = Input::get('tag');
            if (!empty($temp->tag)) {
                $res= Storage::disk('local')->put($filename, $temp->tag);
                $path = '/tag/s-'.$temp->slug;
            }
		} else {
    		$temp->tag = "";
		}

        $url_object = Url::where('url', $path)->first();
        if(!$url_object) {
            $url_object = new Url();
            $url_object->url = $path;
            $url_object->type = "adlocker";
            $url_object->save();
        }

        $temp->url_id = $url_object->id;

        foreach($temp->assets as $asset) {
            if (Input::get('input_asset_'.$asset->id) == false ) {
                $temp->assets()->detach($asset);
            }
        }

		if (Session::has('tmp_files')) {
				$tmp = Session::pull('tmp_files');
				$tmp_filename = Session::pull('tmp_filename');

				Session::forget('tmp_files');
				Session::forget('tmp_filename');

				foreach ($tmp as $key => $path) {

					$newdest = 'assets/images/';
					File::move(public_path($path), public_path($newdest.$tmp_filename[$key]));

					$asset = new Asset;
	                $asset->caption = "Image for ".$temp->name;
	                $asset->type = "image";
	                $asset->url = "/".$newdest.$tmp_filename[$key];
	                $asset->created_at = Carbon::now();
	                $asset->updated_at = Carbon::now();
	                $asset->save();

					$temp->assets()->attach($asset);

				}
		}

        foreach (Category::orderBy('name', 'ASC')->get()->all() as $key=>$c) {
            if ($key == Input::get('category')) {
                $temp->category_id = $c->id;
            }
        }

        $temp->save();

        foreach ($temp->features as $f) {
			$temp->features()->detach($f);
		}

        foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
            if (Input::get('feature_'.$f->slug)) {
                $temp->features()->attach($f);
            }
        }

        return Redirect::to('ad-sample/'.$temp->id)->with('success', Lang::get('validation.success_updated', array('attribute' => 'format')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $temp = Format::find($id);

        // Deleting the tag (if exists)
        $filename = 'tags/s-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }

        $temp->delete();

        return Redirect::to('ad-sample')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'format')));

    }
}
