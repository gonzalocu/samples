<?php

namespace App\Http\Controllers\OMTools;

// Basic
use Controller;
use Input;
use Image;
use File;
use Request;
use Redirect;
use Auth;
use Lang;
use Session;
use Storage;
use Carbon\Carbon;

use Gallery;
use Feature;
use Vertical;
use GalleryFormRequest;
use Asset;

use AdMarvelCampaignAPI;

use Slot;

use App;

class GalleryController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
	    Session::forget('tmp_show_files');
	    Session::forget('tmp_show_filename');
	    Session::forget('tmp_fea_files');
	    Session::forget('tmp_fea_filename');

        return view('admin.gallery.index', [ 'collection'   => Gallery::orderBy('name', 'ASC')->get()->all(),
                                             'success'      => Session::get('success')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $slots = new Slot();
        if (count($slots->freeSlots()) == 0) {
            return Redirect::to('gallery')->withErrors('Not enough free slots. Please contact with the administrator.');
        }

	    return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryFormRequest $request) {




        $slot = new Slot();
        if (count($slot->freeSlots()) == 0) {
            return Redirect::to('gallery')->withErrors('Not enough free slots. Please contact with the administrator.');
        } else {

            $slot = $slot->freeSlots()->first();

            $temp = new Gallery();
            $temp->slug = Auth::User()->region->slug . "-" . str_slug(Input::get('name'));
            $temp->name = Input::get('name');
            $temp->description = Input::get('description');
            $temp->region_id = Input::get('region')?Input::get('region'):Auth::User()->region->id;
            $temp->orientation_id = Input::get('orientation');
            $temp->position_id = Input::get('position');
            $temp->format_id = Input::get('format');
            $temp->brand = Input::get('brand');
            $temp->agency = Input::get('agency');
            $temp->tag = Input::get('tag');
            $temp->vertical_id = Input::get('vertical');
            $temp->is_featured_creative = (Input::get('featured')?true:false);
            $temp->is_approval_request = (Input::get('approval')?true:false);
            $temp->is_private = (Input::get('private')?true:false);

            if ($temp->is_private) {
                $faker = \Faker\Factory::create();
                $temp->password = $faker->regexify('[0-9]{6}');
            }

            if (Session::has('tmp_show_files')) {
				$tmp = Session::pull('tmp_show_files');
				$tmp_filename = Session::pull('tmp_show_filename');

				foreach ($tmp as $key => $path) {

    				$img = Image::make($path);

				    Storage::put('/app-resources/omgallery/images/' . $temp->slug . '/showcase_' . $tmp_filename[$key], (string)$img->encode(), true);
				    $temp->showcase_image_url = 'http://4sa.s3.amazonaws.com/app-resources/omgallery/images/'. $temp->slug . '/showcase_' . $tmp_filename[$key];

				}
    		}

            if (Session::has('tmp_fea_files')) {
				$tmp = Session::pull('tmp_fea_files');
				$tmp_filename = Session::pull('tmp_fea_filename');

				foreach ($tmp as $key => $path) {

    				$img = Image::make($path);

				    Storage::put('/app-resources/omgallery/images/' . $temp->slug . '/profile_' . $tmp_filename[$key], (string)$img->encode(), true);
				    $temp->feature_image_url = 'http://4sa.s3.amazonaws.com/app-resources/omgallery/images/'. $temp->slug . '/profile_' . $tmp_filename[$key];

				}
    		}

            $temp->visible = (Input::get('visible')?true:false);
            $temp->live_date = Input::get('live_date');

            $temp->save();

            foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
                if (Input::get('feature_'.$f->slug)) {
                    $temp->features()->attach($f);
                }
            }

            if (App::environment() === 'production') {
                $api = new AdMarvelCampaignAPI();

                if ($api->editBanner($slot->admarvel_id, "Slot ".str_pad($slot->id, 3, 0, STR_PAD_LEFT)." - ".$temp->slug, $temp->tag, $temp->slug)) {
                    $response = $api->getResponse();

                    $slot->gallery_id = $temp->id;
                    $slot->save();

                    $api->setBannerStatus($slot->admarvel_id, 'RESTART');

                } else {
                    $response = $api->getError();
                }
            }

            if (App::environment() === 'production') {
                $this->xml();
            }

            return Redirect::to('gallery')->with('success', Lang::get('validation.success_created', array('attribute' => 'ad gallery')));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
	    Session::forget('tmp_fea_files');
	    Session::forget('tmp_fea_filename');
	    Session::forget('tmp_show_files');
	    Session::forget('tmp_show_filename');
        return view('admin.gallery.show', [ 'item' => Gallery::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view('admin.gallery.edit', [ 'item' => Gallery::find($id) ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryFormRequest $request, $id) {



        $orientation = array('both','landscape', 'portrait');

        $temp = Gallery::find($id);
        $temp->slug = $temp->region->slug . "-" . str_slug(Input::get('name'));
        $temp->name = Input::get('name');
        $temp->description = Input::get('description');
        $temp->region_id = Input::get('region')?Input::get('region'):Auth::User()->region->id;
        //$temp->feature_image_url = Input::get('feature_image_url');
        //$temp->showcase_image_url = Input::get('showcase_image_url');
        $temp->orientation_id = Input::get('orientation');
        $temp->position_id = Input::get('position');
        $temp->format_id = Input::get('format');
        $temp->brand = Input::get('brand');
        $temp->agency = Input::get('agency');
        $temp->tag = Input::get('tag');
        $temp->vertical_id = Input::get('vertical');
        $temp->is_featured_creative = (Input::get('featured')?true:false);
        $temp->is_approval_request = (Input::get('approval')?true:false);
        $temp->is_private = (Input::get('private')?true:false);

        if ($temp->is_private) {
                if (!$temp->password) {
                    do {
                        $faker = \Faker\Factory::create();
                        $temp->password = $faker->regexify('[0-9]{6}');
                    } while(Gallery::where('password', '=', $temp->password)->first());
                }
        } else {
             $temp->password = "";
        }

        if (Session::has('tmp_show_files')) {
			$tmp = Session::pull('tmp_show_files');
			$tmp_filename = Session::pull('tmp_show_filename');

			foreach ($tmp as $key => $path) {

				$img = Image::make($path);

			    Storage::put('/app-resources/omgallery/images/' . $temp->slug . '/showcase_' . $tmp_filename[$key], (string)$img->encode(), true);
			    $temp->showcase_image_url = 'http://4sa.s3.amazonaws.com/app-resources/omgallery/images/'. $temp->slug . '/showcase_' . $tmp_filename[$key];

			}
		}

        if (Session::has('tmp_fea_files')) {
			$tmp = Session::pull('tmp_fea_files');
			$tmp_filename = Session::pull('tmp_fea_filename');

			foreach ($tmp as $key => $path) {

				$img = Image::make($path);

			    Storage::put('/app-resources/omgallery/images/' . $temp->slug . '/profile_' . $tmp_filename[$key], (string)$img->encode(), true);
			    $temp->feature_image_url = 'http://4sa.s3.amazonaws.com/app-resources/omgallery/images/'. $temp->slug . '/profile_' . $tmp_filename[$key];

			}
		}

        $temp->visible = (Input::get('visible')?true:false);
        $temp->live_date = Input::get('live_date');

        $temp->save();


        // FEATURES
        foreach ($temp->features as $f) {
			$temp->features()->detach($f);
		}

        foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
            if (Input::get('feature_'.$f->slug)) {
                $temp->features()->attach($f);
            }
        }


        // SLOT
        if (App::environment() === 'production') {

            $api = new AdMarvelCampaignAPI();

            if ($api->editBanner($temp->slot->admarvel_id, "Slot ".str_pad($temp->slot->id, 3, 0, STR_PAD_LEFT)." - ".$temp->slug, $temp->tag, $temp->slug)) {
                $response = $api->getResponse();

                $api->setBannerStatus($temp->slot->admarvel_id, 'RESTART');

            } else {
                $response = $api->getError();
            }

            $this->xml();
        }

        return Redirect::to('gallery/'.$temp->id)->with('success', Lang::get('validation.success_updated', array('attribute' => 'ad gallery')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $temp = Gallery::find($id);

        if ($temp->slot) {
            $slot = Slot::find($temp->slot->id);

            if (App::environment() === 'production') {
                $api = new AdMarvelCampaignAPI();

                if ($api->editBanner($slot->admarvel_id, "Slot ".str_pad($slot->id, 3, 0, STR_PAD_LEFT), "none", "none")) {
                    $response = $api->getResponse();

                    $api->setBannerStatus($slot->admarvel_id, 'PAUSE');

                    $slot->gallery_id = "";
                    $slot->save();

                } else {
                    $response = $api->getError();
                }
            }
        }
        $temp->delete();

        if (App::environment() === 'production') {
            $this->xml();
        }

        return Redirect::to('gallery/')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'ad gallery')));
    }

	public function uploadShowcaseFile() {

		if (Input::hasfile('file')) {
				$file = Input::file('file');
				$filename = time() . strtolower (str_random(2)) . '.' . $file->getClientOriginalExtension();

				$path = 'assets/images/temp/' . $filename;

				$upload_success = Image::make($file->getRealPath())->resize(200, 200)->save(public_path($path));

			if( $upload_success ) {

				Session::forget('tmp_show_files');
				Session::forget('tmp_show_filename');

				Session::push('tmp_show_files', $path);
				Session::push('tmp_show_filename', $filename);

				return response()->json(array(	'success' => true,
												'message' => 'Successfully uploaded file.',
												'path' => $path), 200);
			} else {
				return response()->json('error', 400);
			}
		} else {
			return response()->json('error', 400);
		}

	}

	public function uploadFeatureFile() {

		if (Input::hasfile('file')) {
				$file = Input::file('file');
				$filename = time() . strtolower (str_random(2)) . '.' . $file->getClientOriginalExtension();

				$path = 'assets/images/temp/' . $filename;

				$upload_success = Image::make($file->getRealPath())->resize(640, 490)->save(public_path($path));

			if( $upload_success ) {

				Session::forget('tmp_fea_files');
				Session::forget('tmp_fea_filename');

				Session::push('tmp_fea_files', $path);
				Session::push('tmp_fea_filename', $filename);

				return response()->json(array(	'success' => true,
												'message' => 'Successfully uploaded file.',
												'path' => $path), 200);
			} else {
				return response()->json('error', 400);
			}
		} else {
			return response()->json('error', 400);
		}

	}

    public function xml () {

        $all = Gallery::orderBy('name', 'asc')->where('visible','=','1')->get()->all();

        $xml =[];

        foreach ($all as $item) {

            $tag = [];
            $tag['vertical'] = $item->vertical->name;

            foreach($item->features as $f) {
                $tag['feature'.$f->id] = $f->name;
            }

            array_push ($xml,
                array (
                    'name' => $item->slug,
                    'title' => $item->name,
                    'description' => $item->description,
                    'adtype' => $item->format()->slug,
                    'adformat' => $item->format()->slug,
                    'agency' => $item->agency,
                    'brand' => $item->brand,
                    'date' => $item->live_date,
                    'timestamp' => $item->updated_at->format('Y/m/d h:m:s') ,
                    'large_image' => $item->feature_image_url,
                    'small_image' => $item->showcase_image_url,
                    'site_id' => ($item->slot?$item->slot->site_id:'68612'),
                    'partner_id' => ($item->slot?$item->slot->partner_id:'3939de5680fdd652'),
                    'featured_creative' => $item->is_featured_creative,
                    'approval_request' => $item->is_approval_request,
                    'is_private' => $item->is_private,
                    'password' => md5($item->password),
                    'region' => $item->region->slug,
                    'orientation' => $item->orientation()->slug,
                    'tags' => $tag,
                )
            );

        };


        $obj = \SoapBox\Formatter\Formatter::make($xml, \SoapBox\Formatter\Formatter::ARR);

        $ret = preg_replace('/<\/feature([0-9]*)>/', '</feature>', preg_replace('/<feature([0-9]*)>/', '<feature>', $obj->toXml()));

        $ret = preg_replace('/<\/xml>/', '</adunits>', preg_replace('/<xml>/', '<adunits>', $ret));
        $ret = preg_replace('/<\/item>/', '</adunit>', preg_replace('/<item>/', '<adunit>', $ret));

        Storage::copy('/app-resources/omgallery/ads.xml', '/app-resources/omgallery/backup/ads-'.date('Ymdhms').'.xml');
        Storage::put('/app-resources/omgallery/ads.xml', $ret, true);


    }
}
