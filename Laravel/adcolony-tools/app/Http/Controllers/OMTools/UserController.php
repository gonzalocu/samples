<?php

namespace App\Http\Controllers\OMTools;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Role;

use Hash;
use Redirect;
use Session;
use Lang;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('admin.user.index', [   'collection' => User::orderBy('name', 'ASC')->get()->all(),
                                            'success' => Session::get('success')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
         return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(UserFormRequest $request) {
        $temp = new User();
        $temp->name = Input::get('name');
        $temp->email = Input::get('email');
        $temp->password = Hash::make(Input::get('password'));
        $temp->region_id = Input::get('region');
        $temp->save();

        $temp->role()->attach(Input::get('role'));
        return Redirect::to('user')->with('success', Lang::get('validation.success_created', array('attribute' => 'user')));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return view('admin.user.show', [ 'element' => User::find($id),
                                         'success' => Session::get('success')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        return view('admin.user.edit', [  'element' => User::find($id),
                                          'roles' => Role::all() ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UserFormRequest $request, $id) {

        $temp = User::find($id);
        $temp->name = Input::get('name');
        $temp->email = Input::get('email');
        $temp->region_id = Input::get('region');

        if (!empty(Input::get('password'))) {
            $temp->password = Hash::make(Input::get('password'));
        }


        foreach (Role::all() as $r) {
			$temp->role()->detach($r);
		}

		$role = Role::find(Input::get('role'));
		$temp->role()->attach(Input::get('role'));


        $temp->save();

         return Redirect::to('user/'.$temp->id)->with('success', Lang::get('validation.success_updated', array('attribute' => 'user')));
                                     'success' => 'The user has been updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $temp = User::find($id);
        $temp->delete();
        return Redirect::to('user')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'user')));

    }
}
