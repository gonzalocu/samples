<?php

namespace App\Http\Controllers\OMTools;

// Basic
use Controller;
use Input;
use Request;
use Session;
use Image;
use Form;
use File;
use Redirect;
use Lang;
use Storage;

use Carbon\Carbon;

// Models
use AdLocker;
use Category;
use Feature;
use Asset;
use Url;

// Requests
use AdLockerFormRequest;

class AdlockerController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        return view('admin.ad-locker.index', [  'collection' => AdLocker::orderBy('name', 'ASC')->get()->all(),
                                                'success' => Session::get('success')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
	    return view('admin.ad-locker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(AdLockerFormRequest $request) {

        $temp = new AdLocker();
        $temp->name = Input::get('name');
        $temp->description = Input::get('description');
        $temp->vertical_id = Input::get('vertical');
        $temp->created_at = Input::get('date');
        $temp->visible = (Input::get('visible')?true:false);

        $path = Input::get('url');
        $filename = 'tags/l-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }
        if (Input::get('urlortag')) {
    		// Creating the tag file in the storage
            $temp->tag = Input::get('tag');
            if (!empty($temp->tag)) {
                $res= Storage::disk('local')->put($filename, $temp->tag);
                $path = '/tag/l-'.$temp->slug;
            }
		} else {
    		$temp->tag = "";
		}

        $url_object = Url::where('url', $path)->first();
        if(!$url_object) {
            $url_object = new Url();
            $url_object->url = $path;
            $url_object->type = "adlocker";
            $url_object->save();
        }

        $temp->url_id = $url_object->id;

        $temp->save();

        foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
            if (Input::get('feature_'.$f->slug)) {
                $temp->features()->attach($f);
            }
        }

        return Redirect::to('admin/ad-locker')->with('success', Lang::get('validation.success_created', array('attribute' => 'Ad Locker')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return view('admin.ad-locker.show', [ 'item' => AdLocker::find($id),
                                              'success' => Session::get('success')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        return view('admin.ad-locker.edit', [ 'item' => AdLocker::find($id) ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {

	    $temp = AdLocker::find($id);
        $temp->name = Input::get('name');
        $temp->description = Input::get('description');
        $temp->vertical_id = Input::get('vertical');
        $temp->created_at = Input::get('date');
        $temp->visible = (Input::get('visible')?true:false);

        $path = Input::get('url');
        $filename = 'tags/l-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }
        if (Input::get('urlortag')) {
    		// Creating the tag file in the storage
            $temp->tag = Input::get('tag');
            if (!empty($temp->tag)) {
                $res= Storage::disk('local')->put($filename, $temp->tag);
                $path = '/tag/l-'.$temp->slug;
            }
		} else {
    		$temp->tag = "";
		}

        $url_object = Url::where('url', $path)->first();
        if(!$url_object) {
            $url_object = new Url();
            $url_object->url = $path;
            $url_object->type = "adlocker";
            $url_object->save();
        }

        $temp->url_id = $url_object->id;

        $temp->save();

        // FEATURES
        foreach ($temp->features as $f) {
			$temp->features()->detach($f);
		}

        foreach (Feature::orderBy('name', 'ASC')->get()->all() as $f) {
            if (Input::get('feature_'.$f->slug)) {
                $temp->features()->attach($f);
            }
        }

        return Redirect::to('admin/ad-locker/'.$temp->id)->with('success', Lang::get('validation.success_updated', array('attribute' => 'Ad Locker')));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $temp = AdLocker::find($id);

        // Deleting the tag (if exists)
        $filename = 'tags/l-' . $temp->slug . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            Storage::disk('local')->delete($filename);
        }

        $temp->delete();

        return Redirect::to('admin/ad-locker/')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'Ad Locker')));

    }
}
