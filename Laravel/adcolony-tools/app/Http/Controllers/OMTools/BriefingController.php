<?php

namespace App\Http\Controllers\OMTools;

use App\Http\Requests;

use Auth, BasecampUtils, Briefing, BriefingFormRequest, Controller, DB, Lang, Input, Redirect, Request, Session, PDF, Mail;

class BriefingController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.briefing.index', [ 'collection' => Briefing::orderBy('created_at', 'DESC')->get()->all(),
                                              'success' => Session::get('success') ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.briefing.create');
    }

    private function chck_env() {
        list($realHost,)=explode(':',Request::server('HTTP_HOST'));
        if ($realHost == 'dapp.local' || $realHost == 'dapp.adcolony.tools') {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BriefingFormRequest $request) {

        $temp = new Briefing();
        $temp->user_id = Auth::User()->id;
        $temp->name = Input::get('name');
        $temp->campaign_website = Input::get('campaign_website');
        $temp->salesforce_id = Input::get('salesforce_id');
        $temp->campaign_budget = Input::get('campaign_budget');
        $temp->kpi = Input::get('kpi');
        $temp->language = Input::get('multiples_languages');
        $temp->type_work_id = Input::get('type');
        $temp->vertical_id = Input::get('vertical');
        $temp->media_agency_id = Input::get('agency');
        $temp->due_date = \DateTime::createFromFormat('d/m/Y', Input::get('creative_due_date'));
        $temp->live_date = \DateTime::createFromFormat('d/m/Y', Input::get('live_date'));
        $temp->full_creative_briefing = Input::get('full_creative_briefing');
        $temp->celtra_build = (Input::get('celtra_build')? 'yes' : 'no');

        $temp->save();

        foreach (['format', 'platform', 'device', 'os'] as $name) {
            foreach (DB::table('om_briefing_'.$name)->get()->all() as $f) {
                if (Input::get($name.'_'.$f->slug)) {
                    $other = ($f->slug == 'other' ? ['briefing_other' => Input::get($name.'_other_text')] : [] );
                    $temp->$name()->attach($f->id, $other);
                }
            }
        }


        $pdf_path = storage_path('app/briefing.pdf');
        self::creatingPdfFile ($temp, $pdf_path );
        self::sendingNotification($temp, $pdf_path );
        BasecampUtils::createBasecampItemsByBriefing($temp, $pdf_path);

        return Redirect::to('admin/briefing')->withSuccess(Lang::get('validation.success_created', array('attribute' => 'briefing')));
    }

    private function creatingPdfFile ($item, $path) {

        $view =  \View::make('admin.briefing.pdf')->with(compact('item'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait')->save($path);
    }

    public function streamPdfFile ($id) {

        $item = Briefing::find($id);
        $view =  \View::make('admin.briefing.pdf')->with(compact('item'))->render();
        return \App::make('dompdf.wrapper')->loadHTML($view)->setPaper('a4', 'portrait')->stream();
    }

    private function sendingNotification ($item, $path) {
        $user = Auth::User();
        if ($item->type_work_id == 2) {
            $basecamp_url = BasecampUtils::getPreSaleProjectUrl();
        } else {
            $basecamp_url = BasecampUtils::getPostSaleProjectUrl();
        }

        $email_type = "self";
        Mail::queue('admin.briefing.email_notification', ['email_type' => $email_type, 'email_to' => $user, 'user' => $user, 'item' => $item, 'basecamp_url' => $basecamp_url], function ($m) use ($user, $item, $path) {
            $m->from(env('MAIL_FROM'), 'AdColony Tools');
            $m->to($user->email, $user->name)->subject('New project briefing created :: ' . $item->name );
            $m->attach($path);
        });

        foreach (DB::table('om_briefing_email')->get()->all() as $f) {
            $email_type = "admin";
            Mail::queue('admin.briefing.email_notification', ['email_type' => $email_type, 'email_to' => $f, 'user' => $user, 'item' => $item, 'basecamp_url' => $basecamp_url], function ($m) use ($f, $user, $item, $path) {
                $m->from(env('MAIL_FROM'), 'AdColony Tools');
                $m->to($f->email, $f->name)->subject('New project briefing created :: ' . $item->name );
                $m->attach($path);
            });
        }
    }
}
