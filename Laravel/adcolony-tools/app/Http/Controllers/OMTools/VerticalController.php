<?php

namespace App\Http\Controllers\OMTools;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vertical;

class VerticalController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('admin.vertical.index', [ 'collection' => Vertical::orderBy('name', 'ASC')->get()->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $temp = new Vertical();
        $temp->name = Input::get('fname');
        if (empty($temp->name)) {
            return view('admin.vertical.index', [  'collection' => Vertical::orderBy('name', 'ASC')->get()->all(),
                                             'error' => 'Vertical name is empty']);
        }

        $temp->save();
        return redirect()->action('OMTools\VerticalController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return redirect()->action('OMTools\VerticalController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $temp = Vertical::find($id);

        if (!empty($temp)){
            $temp->delete();
            return view('admin.vertical.index', [ 'collection' => Vertical::orderBy('name', 'ASC')->get()->all(),
                                            'success' => 'The vertical has been deleted successfully.']);
        }
    }
}
