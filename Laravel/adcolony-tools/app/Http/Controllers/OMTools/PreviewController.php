<?php

namespace App\Http\Controllers\OMTools;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;

use Redirect;

use Url;
use AdLocker;
use Format;
use Lang;
use Session;
use PreviewFormRequest;



class PreviewController extends Controller {

    public function index ($hashedSlug) {

        $url = Url::where('slug', $hashedSlug)->first();

        if ($url) {
            $url->hit = $url->hit + 1;
            return view('public.preview.index', [ 'item' => $url ]);
        } else {
            return Redirect::to('http://operamediaworks.tools/');
        }

    }

    public function tag (Request $request, $name) {
        $filename = 'tags/' . $name . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            echo Storage::disk('local')->get('tags/' . $name . '.txt');
        } else {
            return Redirect::to('http://operamediaworks.tools/');
        }
    }

    public function listing () {
        return view('admin.preview.index', ['success' => Session::get('success')]);
    }

    public function store(PreviewFormRequest $request) {
        $temp = new Url();
        $temp->url = Input::get('url');
        $temp->type = "preview-admin";
        $temp->save();

        return Redirect::to('preview')->with('success', Lang::get('validation.success_created', array('attribute' => 'Preview Page')));
    }

    public function destroy($id) {
        $temp = Url::find($id);

        if (!empty($temp)){
            $temp->delete();

            if (AdLocker::where('url_id', $item['id'])->first()) {
                return Redirect::to('preview')->with('error', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
            }

            if (AdLocker::where('url_id', $item['id'])->first()) {
                return Redirect::to('preview')->with('error', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
            }

            return Redirect::to('preview')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
        }

    }

}
