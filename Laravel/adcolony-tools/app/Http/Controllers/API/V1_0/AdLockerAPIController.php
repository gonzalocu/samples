<?php

namespace App\Http\Controllers\API\V1_0;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AdLocker;
use App\Models\Feature;
use App\Models\Vertical;


class AdLockerAPIController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return response()->json(AdLocker::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return response()->json(AdLocker::find($id));
    }

	public function search ($input = '') {

        $arr = [];
        $input = str_replace ('&i=', '', $input);

        $collection = AdLocker::where('name', 'like', "%{$input}%")->where('slug', 'like', "%{$input}%")->orderBy('name', 'asc')->get();

        if ($input != '') {
            foreach ($collection as $key => $value) {

                $arr1 = ["id" => "$value->id", "value" => "$value->name", "info" => "$value->description", "url" => $value->url];
                array_push($arr, $arr1);
            }
        }

        return response()->json([ 'results' => $arr]);
    }


    public function searchAdsByFeature ($input = '') {

        $arr = [];
        $coincidences = [];

        $input = str_replace ('&i=', '', $input);

        if ($input != '') {

            $coincidences = Feature::where('name', 'like', "{$input}%")->get()->pluck('name');

            $collection = AdLocker::whereHas('features', function ($query) use ($input) {
                $query->where('name', 'like', "{$input}%");
            })->get();

            foreach ($collection as $key => $value) {
                $arr1 = ["id" => "$value->id", "value" => "$value->name", "info" => "$value->description", "url" => $value->url];
                array_push($arr, $arr1);
            }
        }

        return response()->json(['all' => Feature::pluck('name'),'coincidences' => ($coincidences?$coincidences->toArray():''), 'results' => $arr]);

    }

    public function searchAdsByVertical ($input = '') {

        $arr = [];
        $coincidences = [];

        $input = str_replace ('&i=', '', $input);

        if ($input != '') {

            $coincidences = Vertical::where('name', 'like', "{$input}%")->get()->pluck('name');

            $collection = AdLocker::whereHas('vertical', function ($query) use ($input) {
                $query->where('name', 'like', "{$input}%");
            })->get();

            foreach ($collection as $key => $value) {
                $arr1 = ["id" => "$value->id", "value" => "$value->name", "info" => "$value->description", "url" => $value->url];
                array_push($arr, $arr1);
            }
        }

        return response()->json(['all' => Vertical::pluck('name'), 'coincidences' => ($coincidences?$coincidences->toArray():''), 'results' => $arr]);
    }

}
