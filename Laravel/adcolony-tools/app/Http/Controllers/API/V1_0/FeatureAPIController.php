<?php

namespace App\Http\Controllers\API\V1_0;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Feature;

class FeatureAPIController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return response()->json(Feature::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) { 
        return response()->json(Feature::find($id));
    }
}
