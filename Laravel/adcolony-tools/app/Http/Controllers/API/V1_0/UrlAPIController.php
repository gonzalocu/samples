<?php

namespace App\Http\Controllers\API\V1_0;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use App\Models\Url;


class UrlAPIController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function createLiveUrl($project = '') {
        
        $validator = Validator::make(array('project' => $project), ['project' => 'required']);
         
        if ($validator->fails()) {
            return response()->json('fails');
        } else {
            $path = "http://4sa.s3.amazonaws.com/live/".$project."/overlay/standard/sample.html";
            $url_object = Url::where('url', $path)->first();
            if(!$url_object) {
                $url_object = new Url();
                $url_object->url = $path;
                $url_object->type = "grunt";
                $url_object->save();
            }
    
            return response()->json($url_object);
        }
    }
    
    public function createStagingUrl($project = '') {
        
        $validator = Validator::make(array('project' => $project), ['project' => 'required']);
         
        if ($validator->fails()) {
            return response()->json('fails');
        } else {
            $path = "http://4sa.s3.amazonaws.com/staging/".$project."/overlay/standard/sample.html";
            $url_object = Url::where('url', $path)->first();
            if(!$url_object) {
                $url_object = new Url();
                $url_object->url = $path;
                $url_object->type = "grunt";
                $url_object->save();
            }
    
            return response()->json($url_object);
        }
    }

}
