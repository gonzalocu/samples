<?php

namespace App\Http\Controllers\API\V1_0;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;

class BannerAPIController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($cid) {
        return response()->json(Banner::where('campaign_id', $cid)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($cid, $bid) {
        return response()->json(Banner::find($bid));
    }

}
