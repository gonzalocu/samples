<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BasecampUtilsController extends Controller {

    private static function getClient() {
        return \Basecamp\BasecampClient::factory(array(
            'auth' => 'http',
            'username' => env('BASECAMP_USERNAME'),
            'password' => env('BASECAMP_PASSWORD'),
            'user_id' => env('BASECAMP_USERID'),
            'app_name' => env('BASECAMP_APPNAME'),
            'app_contact' => env('BASECAMP_APPCONTACT')
        ));
    }

    public static function getPreSaleProjectUrl () {
        return "http://basecamp.com/" . env('BASECAMP_USERID') . "/projects/" . env('BASECAMP_PROJECTPRESALEID');
    }

    public static function getPostSaleProjectUrl () {
        return "http://basecamp.com/" . env('BASECAMP_USERID') . "/projects/" . env('BASECAMP_PROJECTPOSTSALEID');
    }

    private static function getProjectId ($type) {
        $projectId = false;
        switch ($type) {
            case 'PRE-SALE':
                $projectId = intval(env('BASECAMP_PROJECTPRESALEID'));
                break;
            case 'POST-SALE':
                $projectId = intval(env('BASECAMP_PROJECTPOSTSALEID'));
                break;
            case 'AD-OPS':
                $projectId = intval(env('BASECAMP_PROJECTADOPSID'));
                break;
        }

        return $projectId;
    }

    private static function createPDFAttachment ($path) {
        $client = self::getClient();
        return $client->createAttachment( array(
            'mimeType' => 'application/pdf',
            'data' => file_get_contents($path),
        ));
    }

    private static function createTodolistByProject ($type, $name, $description) {
        $client = self::getClient();
        $projectId = self::getProjectId($type);

        if ($projectId) {
            return $client->createTodolistByProject( array(
                'projectId' => $projectId,
                'name' => $name,
                'description' => $description
            ));
        }

        return false;
    }

    private static function createCommentByTodolist ($type, $todolistid, $content, $attachment) {
        $client = self::getClient();
        $projectId = self::getProjectId($type);

        return $client->createCommentByTodolist( array(
            'projectId' => $projectId,
            'todolistId' => $todolistid,
            'content' => $content,
            'attachments' => array( array( 'token' => $attachment['token'], 'name' => $attachment['name'] ) ),
        ));
    }

    private static function createTodoByTodolist ($type, $todolistid, $content) {
        $client = self::getClient();
        $projectId = self::getProjectId($type);

        return $client->createTodoByTodolist( array(
            'projectId' => $projectId,
            'todolistId' => $todolistid,
            'content' => $content,
        ));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function createBasecampItemsByBriefing($item, $path) {
        $client = self::getClient();

        if ($item->type_work_id == 2){
            $attachment = self::createPDFAttachment ($path);
            $todolist = self::createTodolistByProject('PRE-SALE', $item->name, '');
            self::createCommentByTodolist ('PRE-SALE', $todolist['id'], 'Project Briefing', array( 'token' => $attachment['token'], 'name' => $path ));
            self::createTodoByTodolist ('PRE-SALE', $todolist['id'], 'Design');
        } else if ($item->type_work_id == 1) {
            // POST-SALE TODOLIST
            $attachment2 = self::createPDFAttachment ($path);
            $todolist2 = self::createTodolistByProject('POST-SALE', $item->name, '');
            self::createCommentByTodolist ('POST-SALE', $todolist2['id'], 'Project Briefing', array( 'token' => $attachment2['token'], 'name' => $path ));
            self::createTodoByTodolist ('POST-SALE', $todolist2['id'], 'Design');
            self::createTodoByTodolist ('POST-SALE', $todolist2['id'], 'Development');
            self::createTodoByTodolist ('POST-SALE', $todolist2['id'], 'Tracking Implementation');
            self::createTodoByTodolist ('POST-SALE', $todolist2['id'], 'Testing');
        }

    }
}
