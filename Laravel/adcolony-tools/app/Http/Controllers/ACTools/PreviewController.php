<?php

namespace App\Http\Controllers\ACTools;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use AdLocker, Format, Lang, PreviewFormRequest, Redirect, Session, Storage, Url;

class PreviewController extends Controller {

    /**
     * Display a preview page or redirect if it doesn't exist
     *
     * @param string $hashedSlug
     * @return Response
     */
    public function index ($hashedSlug) {
        $url = Url::where('slug', $hashedSlug)->first();

        if ($url) {
            $url->hit = $url->hit + 1;
            return view('public.preview.index', [ 'item' => $url ]);
        } else {
            return Redirect::to('http://adcolony.tools/');
        }
    }

    /**
     * Display a tag page if it exists in /storage/app/tag
     *
     * @param Request $request
     * @param string $name
     * @return Response
     */
    public function tag (Request $request, $name) {
        $filename = 'tags/' . $name . '.txt';
        if (Storage::disk('local')->exists($filename)) {
            echo Storage::disk('local')->get('tags/' . $name . '.txt');
        } else {
            return Redirect::to('http://adcolony.tools/');
        }
    }

    /**
     * Display preview page list
     *
     * @return Response
     */
    public function listing () {
        return view('admin.preview.index', ['success' => Session::get('success')]);
    }

    /**
     * Save a preview page in the db
     *
     * @param PreviewFormRequest $request
     * @return Response
     */
    public function store(PreviewFormRequest $request) {
        $temp = new Url();
        $temp->url = Input::get('url');
        $temp->type = "preview-admin";
        $temp->save();

        return Redirect::to('preview')->with('success', Lang::get('validation.success_created', array('attribute' => 'Preview Page')));
    }

    /**
     * Delete a preview page
     *
     * @param PreviewFormRequest $request
     * @return Response
     */
    public function destroy($id) {
        $temp = Url::find($id);

        if (!empty($temp)){
            $temp->delete();

            if (AdLocker::where('url_id', $item['id'])->first()) {
                return Redirect::to('preview')->with('error', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
            }

            if (AdLocker::where('url_id', $item['id'])->first()) {
                return Redirect::to('preview')->with('error', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
            }

            return Redirect::to('preview')->with('success', Lang::get('validation.success_deleted', array('attribute' => 'Preview Page')));
        }
    }
}
