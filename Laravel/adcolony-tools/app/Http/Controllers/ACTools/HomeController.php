<?php

namespace App\Http\Controllers\ACTools;

use App\Http\Controllers\Controller;

use Category, Format;

class HomeController extends Controller {

    /**
     * Display a home page
     *
     * @return Response
     */
    public function index() {
        return view('public.home.index', [ 'formats'    => Format::orderBy('created_at', 'DESC')->where('visible', '=', true)->get()->all(),
                                           'categories' => Category::orderBy('name', 'ASC')->get()->all()]);
    }

    /**
     * Display a adlocker page
     *
     * @return Response
     */
    public function adlocker () {
	    return view('public.home.ad-locker');
    }
}
