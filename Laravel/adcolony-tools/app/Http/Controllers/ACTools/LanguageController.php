<?php

namespace App\Http\Controllers\ACTools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Config, Redirect, Session;

class LanguageController extends Controller {

    public function switchLang($lang) {


        if (array_key_exists($lang, Config::get('languages'))) {
            Session::set('applocale', $lang);
        }

        return Redirect::back();
    }
}
