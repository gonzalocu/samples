<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use Auth, Input, Redirect, Role, User, Validator;

class AuthController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers;

    protected $redirectAfterLogout = "/";
    protected $redirectTo = '/';
    protected $redirectPath = '/';
    protected $loginPath = '/';

    public function getLogin()
       {
           return view('auth.login');
       }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLoginDapp() {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login_dapp');
    }

    public function googleSignIn(Request $request) {
        $user = User::where('email', 'like', Input::get('email'))->first();

        if (!$user) {
            $user = new User();
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->region_id = 2;
            $user->save();

            if(preg_match("/@opera\.com$/", $user->email)) {
                $role = Role::where('name', '=', 'Staff')->first();
                $user->role()->attach($role->id);
            } else if(preg_match("/@adcolony\.com$/", $user->email)) {
                $role = Role::where('name', '=', 'Staff')->first();
                $user->role()->attach($role->id);
            } else {
                $role = Role::where('name', '=', 'Guest')->first();
                $user->role()->attach($role->id);
            }
        }

        Auth::loginUsingId($user->id, true);

        return Redirect::to('/');
    }
}
