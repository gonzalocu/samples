<?php

namespace App\Http\Requests;

use FormRequest;
use Response;
use Auth;

class PreviewFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'url'		   => 'required|unique:om_url,url|url'
        ];
    }

    public function forbiddenResponse() {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else
        return redirect('/');
    }


}
