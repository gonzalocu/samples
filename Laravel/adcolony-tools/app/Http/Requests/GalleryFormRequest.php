<?php

namespace App\Http\Requests;

use FormRequest;
use Response;
use Auth;
use Request;
use Input;

use Gallery;

class GalleryFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
       return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
       	
        switch($this->method()) {
	        case 'GET':
	        case 'DELETE': {
	            return [];
	        }
	        case 'POST': {
	            return [
		            'name' 			     => 'required|unique:om_gallery,name',
		            'description' 	     => 'required',
					'vertical'           => 'required',
		            'showcase_image_url' => 'required_without:private',
					'feature_image_url'  => 'required_with:featured',	
		            'live_date'			 => 'required|date',
		            'tag'                => 'required'
		        ];
	        }
	        case 'PUT':
	        case 'PATCH': {

		        $item = Gallery::find(Request::segment(2));

	            return [
					'name'               => 'required|unique:om_gallery,name,'.$item->id,
					'description'        => 'required',
					'vertical'           => 'required',
		            'showcase_image_url' => 'required_without:private',
					'feature_image_url'  => 'required_with:featured',
		            'live_date'			 => 'required|date',
		            'tag'                => 'required'
	            ];
	        }
	        default:break;
	    }
    }
}