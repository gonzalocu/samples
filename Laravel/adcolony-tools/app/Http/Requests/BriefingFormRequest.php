<?php

namespace App\Http\Requests;

use FormRequest;
use Response;
use Auth;

class BriefingFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        switch($this->method()) {
            case 'POST': {
	            return [
		            'name'                   => 'required|unique:om_briefing,name',
                    'campaign_website'       => 'required|url',
                    'multiples_languages'    => 'required',
                    'type'                   => 'required',
                    'agency'                 => 'required',
                    'creative_due_date'      => 'required|date_format:d/m/Y',
                    'live_date'              => 'date_format:d/m/Y',
                    'full_creative_briefing' => 'required',
                    'destination_url'        => 'url'
		        ];
	        }
	        default:break;
	    }
    }
}
