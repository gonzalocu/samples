<?php

namespace App\Http\Requests;

use FormRequest;
use Response;
use Auth;

class FormatFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name'         => 'required|unique:om_format,name',
            'description'  => 'required',
            'url'		   => 'required_without:urlortag|url',
            'tag'          => 'required_with:urlortag'
        ];
    }

    public function forbiddenResponse() {
        // Optionally, send a custom response on authorize failure
        // (default is to just redirect to initial page with errors)
        //
        // Can return a response, a view, a redirect, or whatever else
        return redirect('/');
    }


}
