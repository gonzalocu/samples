<?php

namespace App\Http\Requests;

use FormRequest;
use User;
use Auth;
use Input;


class UserFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        $user = User::find(Auth::user()->id);  
        return $user->hasRole('Administrator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        switch($this->method()) {
	        case 'GET':
	        case 'DELETE': {
	            return [];
	        }
	        case 'POST': {
	            return [
		            'name' 			          => 'required',
		            'email' 	              => 'required|email|unique:om_user,email',
		            'password'		          => 'required|min:8',	
		            'password_confirmation'	  => 'required|same:password',
		            'role'                    => 'required'
		        ];
	        }
	        case 'PUT':
	        case 'PATCH':{

	            return [
    	            'name' 			          => 'required',
		            'email' 	              => 'required|email|unique:om_user,email,'.$this->get('id'),
		            'role'                    => 'required'
	            ];
	        }
	        default:break;
	    }
    }
}
