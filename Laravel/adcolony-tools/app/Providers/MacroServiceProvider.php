<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        require app_path().'/Macros/macros.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {

    }
}
