<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
	/**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'om_banner';
    
    public function setNameAttribute ($value) {
		$this->attributes['name'] = ucfirst($value);

		if (! $this->exists) {
			
			if (Banner::where('name', '=', $this->attributes['name'])->count() == 0) {
				$this->attributes['slug'] = str_slug($value);
			} else {
				$id = 0;
				do  {
					$id++;
					$count = Banner::where('slug', '=', str_slug($this->attributes['name'])."-".$id)->count();
				} while ($count != 0);
				$this->attributes['slug'] = str_slug($value)."-".$id;
			}
       }
    }
}
