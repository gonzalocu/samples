<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {
    protected $table = 'om_region';
    
    public $timestamps = false;
}
