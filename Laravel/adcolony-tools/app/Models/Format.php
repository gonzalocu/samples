<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Format extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'om_format';
    
    public function setNameAttribute ($value) {
       $this->attributes['name'] = ucfirst($value);

       if (! $this->exists) {
           $this->attributes['slug'] = str_slug($value);
       }
    }
    
    public function assets() {
    	return $this->belongsToMany('App\Models\Asset', 'om_format_asset');
	}
	
    public function features() {
    	return $this->belongsToMany('App\Models\Feature', 'om_format_feature');
	}
	
    public function category() {
    	return $this->hasOne('App\Models\Category', 'id', 'category_id');
	}
	
    public function url() {
    	return $this->hasOne('App\Models\Url', 'id', 'url_id');
	}
}
