<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Gallery extends Model {
    
    protected $table = 'om_gallery';
    
    public function region() {
    	return $this->hasOne('App\Models\Region', 'id', 'region_id');
	}
	
    public function features() {
    	return $this->belongsToMany('App\Models\Feature', 'om_gallery_feature');
	}
	
    public function vertical() {
    	return $this->hasOne('App\Models\Vertical', 'id', 'vertical_id');
	}
	
    public function slot() {
    	return $this->hasOne('App\Models\Slot', 'gallery_id', 'id');
	}
	
	public function format() {
    	return DB::table('om_gallery_format')->find($this->format_id);
	}
	
	public function orientation() {
    	return DB::table('om_gallery_orientation')->find($this->orientation_id);
	}
	
	public function position() {
    	return DB::table('om_gallery_position')->find($this->position_id);
	}

}
