<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model {
    protected $table = 'om_slot';
    
    public $timestamps = false;
    
    public function gallery() {
    	return $this->hasOne('App\Models\Gallery', 'id', 'gallery_id');
	}
	
	public function freeSlots() {
    	return Slot::where('gallery_id', '=', '')->get();
	}
}
