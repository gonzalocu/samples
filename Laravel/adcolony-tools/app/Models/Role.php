<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'om_role';
    
    public function users() {
        return $this->belongsToMany('App\Models\User');
    }
}
