<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'om_category';
    
    public $timestamps = false;
    
    public function setNameAttribute ($value) {
       $this->attributes['name'] = ucfirst($value);

       if (! $this->exists) {
           $this->attributes['slug'] = str_slug($value);
       }
    }
}
