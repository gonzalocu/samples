<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Briefing extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'om_briefing';

    protected $dates = [
        'created_at',
        'updated_at',
        'due_date',
        'live_date'
    ];

    public function setDueDateAttribute ($value) {
        // if(strtotime($value) == 0) {
        //     dd('true');
        // }
            //dd($value->format('d/m/Y'));
          if ($value) {
              $this->attributes['due_date'] = $value;
          } else {
              $this->attributes['due_date'] = null;
          }
    }

    public function setLiveDateAttribute ($value) {
        // if(strtotime($value) == 0) {
        //     dd('true');
        // }
        //dd($value);
          if ($value) {
              $this->attributes['live_date'] = $value;
          } else {
              $this->attributes['live_date'] = null;
          }
    }

    public function setNameAttribute ($value) {
       $this->attributes['name'] = ucfirst($value);

       if (! $this->exists) {
           $this->attributes['slug'] = str_slug($value);
       }
    }

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function type() {
        return $this->hasOne('App\Models\BriefingType', 'id', 'type_work_id');
    }

    public function vertical() {
        return $this->hasOne('App\Models\BriefingVertical', 'id', 'vertical_id');
    }

    public function agency() {
        return $this->hasOne('App\Models\BriefingAgency', 'id', 'media_agency_id');
    }

    public function format() {
        return $this->belongsToMany('App\Models\BriefingFormat', 'om_briefing_format_xref')->withPivot('briefing_other');
    }

    public function platform() {
        return $this->belongsToMany('App\Models\BriefingPlatform', 'om_briefing_platform_xref')->withPivot('briefing_other');
    }

    public function os() {
        return $this->belongsToMany('App\Models\BriefingOs', 'om_briefing_os_xref');
    }

    public function device() {
        return $this->belongsToMany('App\Models\BriefingDevice', 'om_briefing_device_xref');
    }
}
