<?php

namespace App\Models;

use Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'om_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function region() {
    	return $this->hasOne('App\Models\Region', 'id', 'region_id');
	}

    public function role() {
    	return $this->belongsToMany('App\Models\Role', 'om_user_role');
	}


    public function hasRole($check) {
        if (is_array ($check) ) {
            $ret = false;
            foreach ($check as $val) {
                if (in_array($val,  array_pluck($this->role->toArray(), 'name'))){
                    $ret = true;
                }
            }
            return $ret;
        } else {
            return in_array($check,  array_pluck($this->role->toArray(), 'name'));
        }
    }

    public function watching() {
        return $this->belongsToMany('App\Models\Campaign', 'om_watching');
    }

    public function currentUser() {
        return User::find(Auth::user()->id);
    }
}
