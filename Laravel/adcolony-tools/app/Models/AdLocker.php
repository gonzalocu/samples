<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdLocker extends Model {

    protected $table = 'om_adlocker';

    public function setNameAttribute ($value) {
	   
        $this->attributes['name'] = ucfirst($value);
        if (! $this->exists) {
		    $this->attributes['slug'] = str_slug($value);           
        }
    }
    
    public function setCreatedAtAttribute ($value) {
	    
	    $this->attributes['created_at'] = $value;
	    
        if (! $this->exists) {
	        $this->attributes['slug'] = $this->attributes['slug'] . '-' . date('y', strtotime($value)) . '-' . date('m', strtotime($value));
        }
	}
    
	public function getUpdatedAtColumn() {
		return null;
	}
	   
    public function url() {
    	return $this->hasOne('App\Models\Url', 'id', 'url_id');
	}
	
    public function features() {
    	return $this->belongsToMany('App\Models\Feature', 'om_adlocker_feature');
	}
	
    public function vertical() {
    	return $this->hasOne('App\Models\Vertical', 'id', 'vertical_id');
	}

}
