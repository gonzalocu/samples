<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'om_campaign';
    
    public function setNameAttribute ($value) {
		$this->attributes['name'] = ucfirst($value);

		if (! $this->exists) {
	       
			if (Campaign::where('name', '=', $this->attributes['name'])->count() == 0) {
				$this->attributes['slug'] = str_slug($value)."-".date('m')."-".date('y');
			} else {
				$id = 0;
				do  {
					$id++;
					$count = Campaign::where('slug', '=', str_slug($this->attributes['name'])."-".$id."-".date('m')."-".date('y'))->count();
				} while ($count != 0);
				$this->attributes['slug'] = str_slug($value)."-".$id."-".date('m')."-".date('y');
			}
           
       }
    }
    
    public function watching() {
        return $this->belongsToMany('App\Models\User', 'om_watching');
    }
    
    public function features() {
    	return $this->belongsToMany('App\Models\Feature', 'om_campaign_feature');
	}
	
    public function banners() {
    	return $this->hasMany('App\Models\Banner');
	}
}
