<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BriefingAgency extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'om_briefing_agency';
}
