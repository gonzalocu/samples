<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Url extends Model {

    protected $table = 'om_url';
    
    public $timestamps = false;
    
    public function setUrlAttribute ($value) {
        
        $this->attributes['url'] = $value;
       
        if (! $this->exists) {
            
            $faker = \Faker\Factory::create();
            
            do {
                $slug = $faker->regexify('[A-Za-z0-9.-]{8}');
            } while (Url::where('slug', $slug)->first());
           
            $this->attributes['slug'] = $slug;
        }
    }
}
