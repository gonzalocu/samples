<?php


class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return Response::json([
                'error' => false,
                'data' => $users->toArray()
                ], 200, [], JSON_PRETTY_PRINT
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $response = $this->performValidation();

        if (!$response['error']) {
            $user = new User;
            $user->firstname = Request::get('firstname');
            $user->lastname = Request::get('lastname');
            $user->email = Request::get('email');

            $user->save();

            $response['data'] = $user->toArray();

            $email = new EmailController();
            $email->sendWelcomeEmail(Request::get('firstname'), Request::get('email'));
        }

        return Response::json($response, 200 , [], JSON_PRETTY_PRINT);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return Response::json([
                'error' => false,
                'data' => $user->toArray()
                ], 200, [], JSON_PRETTY_PRINT
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $response = $this->performValidation();

        if (!$response['error']) {
            $user = User::find($id);
            $user->firstname = Request::get('firstname');
            $user->lastname = Request::get('lastname');
            $user->email = Request::get('email');
            $user->save();

            $response['data'] = $user->toArray();
        }

        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return Response::json([], 204);
    }

    /**
     * Perform validation.
     *
     * @return Response
     */
    protected function performValidation(){
        $validator = Validator::make(
            array(
                'firstname' => Request::get('firstname'),
                'lastname' => Request::get('lastname'),
                'email' => Request::get('email')
            ),
            array(
                'firstname' => array('required'),
                'lastname' => array('required'),
                'email' => array('required','email', 'unique:users,email')
            )
        );


        if ($validator->fails())
        {
            $messages = $validator->messages();

            return ['error' => true, 'data' => $messages->all()];

        } else {

            return ['error' => false];
        }
    }
}
