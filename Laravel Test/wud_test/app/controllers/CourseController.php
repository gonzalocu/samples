<?php

class CourseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $courses = new Course();

        return Response::json([
                'error' => false,
                'data' => (Request::get('with') == 'userScores'?$courses->array_get_all('2016'):$courses->array_get_all())
                ], 200, [], JSON_PRETTY_PRINT
        );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $response = $this->performValidation();

        if (!$response['error']) {
            $course = new Course;
            $course->name = Request::get('name');
            $course->save();

            $response['data'] = $course->toArray();
        }

        return Response::json($response, 200 , [], JSON_PRETTY_PRINT);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $course = Course::findOrFail($id);

        return Response::json([
                'error' => false,
                'data' => (Request::get('with') == 'userScores'?$course->array_get('2016'):$course->array_get())
                ], 200, [], JSON_PRETTY_PRINT
        );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $response = $this->performValidation();

        if (!$response['error']) {
            $course = Course::find($id);
            $course->name = Request::get('name');
            $course->save();

            $response['data'] = $course->toArray();
        }

        return Response::json($response, 200, [], JSON_PRETTY_PRINT);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $course = Course::find($id);
        $course->delete();

        return Response::json([], 204);
	}

    /**
	 * Perform validation.
	 *
	 * @return Response
	 */
    protected function performValidation(){
        $validator = Validator::make(
            array('name' => Request::get('name')),
            array('name' => array('required', 'unique:courses,name'))
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();

            return ['error' => true, 'data' => $messages->all()];

        } else {

            return ['error' => false];
        }
    }
}
