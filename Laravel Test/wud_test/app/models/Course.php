<?php

class Course extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';

    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany('User','user_scores','course_id','user_id')->withPivot('score', 'year');
    }

    public function array_get_all($year = false)
    {
        $response = [];
        foreach ($this->get() as $course) {
            array_push($response, $course->array_get($year));
        }

        return $response;
    }

    public function array_get($year = false)
    {
        $course = $this->toArray();
        $pivots = array_fetch($this->users->toArray(), 'pivot');
        if ($year)
        {
            $pivots_filtering = array_values(array_where($pivots, function ($key, $value) use ($year) {
                return ((string)$value['year'] == (string)$year);
            }));

            return array_add($course, 'user_scores', $pivots_filtering);
        } else {
            return array_add($course, 'user_scores', $pivots);
        }
    }
}
